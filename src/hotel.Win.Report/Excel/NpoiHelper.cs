﻿using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using System.IO;
using System.Windows.Forms;
using System;

namespace hotel.Win.Report.Excel
{
    public class NpoiHelper
    {
        public static void WriteToFile(HSSFWorkbook hssfworkbook, string path = null)
        {
            string fileName="";
            if (string.IsNullOrEmpty(path))
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                saveFileDialog.Filter = "文本文件(*.xls)|*.xls|所有文件(*.*)|*.*";
                if (saveFileDialog.ShowDialog() != DialogResult.OK)
                    return;               
                fileName = saveFileDialog.FileName;
            }
            else
                fileName = path;            
            FileStream file = new FileStream(fileName, FileMode.Create);
            hssfworkbook.Write(file);
            file.Close();
        }
        
        public static HSSFWorkbook CreateWorkbook()
        {
            var hssfworkbook = new HSSFWorkbook();
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "成都芯源科技";
            hssfworkbook.DocumentSummaryInformation = dsi;
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "核燃料元件零部件信息管理系统";
            hssfworkbook.SummaryInformation = si;
            return hssfworkbook;
        }
    }
}
