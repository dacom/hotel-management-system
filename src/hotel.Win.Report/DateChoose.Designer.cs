﻿namespace hotel.Win.Report
{
    partial class DateChoose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnok = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtpkssj = new System.Windows.Forms.DateTimePicker();
            this.dtpjssj = new System.Windows.Forms.DateTimePicker();
            this.dtpjsrq = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpksrq = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtprq = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(100, 7);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(82, 32);
            this.btnok.TabIndex = 3;
            this.btnok.Text = "确定";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(212, 6);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(82, 32);
            this.btncancel.TabIndex = 3;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtpkssj);
            this.panel1.Controls.Add(this.dtpjssj);
            this.panel1.Controls.Add(this.dtpjsrq);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dtpksrq);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(337, 75);
            this.panel1.TabIndex = 4;
            // 
            // dtpkssj
            // 
            this.dtpkssj.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpkssj.Location = new System.Drawing.Point(225, 9);
            this.dtpkssj.Name = "dtpkssj";
            this.dtpkssj.Size = new System.Drawing.Size(97, 23);
            this.dtpkssj.TabIndex = 6;
            // 
            // dtpjssj
            // 
            this.dtpjssj.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpjssj.Location = new System.Drawing.Point(225, 42);
            this.dtpjssj.Name = "dtpjssj";
            this.dtpjssj.Size = new System.Drawing.Size(97, 23);
            this.dtpjssj.TabIndex = 8;
            // 
            // dtpjsrq
            // 
            this.dtpjsrq.Location = new System.Drawing.Point(91, 42);
            this.dtpjsrq.Name = "dtpjsrq";
            this.dtpjsrq.Size = new System.Drawing.Size(128, 23);
            this.dtpjsrq.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "结束日期:";
            // 
            // dtpksrq
            // 
            this.dtpksrq.Location = new System.Drawing.Point(91, 9);
            this.dtpksrq.Name = "dtpksrq";
            this.dtpksrq.Size = new System.Drawing.Size(128, 23);
            this.dtpksrq.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "开始日期:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dtprq);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 75);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(337, 75);
            this.panel2.TabIndex = 5;
            // 
            // dtprq
            // 
            this.dtprq.Location = new System.Drawing.Point(124, 22);
            this.dtprq.Name = "dtprq";
            this.dtprq.Size = new System.Drawing.Size(128, 23);
            this.dtprq.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "日期选择:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnok);
            this.panel3.Controls.Add(this.btncancel);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 114);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(337, 44);
            this.panel3.TabIndex = 6;
            // 
            // DateChoose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 158);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DateChoose";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "日期选择";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpkssj;
        private System.Windows.Forms.DateTimePicker dtpjssj;
        private System.Windows.Forms.DateTimePicker dtpjsrq;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpksrq;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker dtprq;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
    }
}