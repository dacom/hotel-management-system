﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.Win.Report.ReportModel
{
    public class ZhangHuYueReport
    {
        public string RZJL_ID{get;set;}
        public string ROOM_ID{get;set;}
        public string KRXM{get;set;}
        public string KRXB{get;set;}
        public decimal SUM_JE{get;set;}
        public decimal SUM_YJ{get;set;}
        public decimal SUM_YE{get;set;}        
    }
}
