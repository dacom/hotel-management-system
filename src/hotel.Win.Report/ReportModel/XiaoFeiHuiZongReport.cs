﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.Win.Report.ReportModel
{
    public class XiaoFeiHuiZongReport
    {
        public string FLMC { get; set; }
        public string XFXM_MC { get; set; }
        public int SL { get; set; }
        public decimal JE { get; set; }
    }
}
