﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;
namespace hotel.Win.Report
{
    public partial class ReportViewFrom : Form
    {
        private MyReportStruct _reportStruct;
        public ReportViewFrom(MyReportStruct reportStruct)
        {
            InitializeComponent();
            LoadReport(reportStruct);
           _reportStruct= reportStruct;
        }

        private void LoadReport(MyReportStruct reportStruct)
        {
            this.reportViewer1.ProcessingMode = ProcessingMode.Local;

            this.reportViewer1.LocalReport.EnableHyperlinks = true; // if there is URL links in your RDLC, this is need
            this.reportViewer1.LocalReport.DataSources.Clear();
             
           
            // Generate data automatically
            if (reportStruct.DataSources != null && reportStruct.DataSources.Count > 0)
            {
                Array.ForEach(reportStruct.DataSources.ToArray(),p=>{
                    this.reportViewer1.LocalReport.DataSources.Add(p);
                });                
            }
            else if (reportStruct.DataSource != null)
            {
                this.reportViewer1.LocalReport.DataSources.Add(reportStruct.DataSource);
            }                
            this.reportViewer1.LocalReport.ReportPath = reportStruct.ReportPath;
            // SetParameters
            if (reportStruct.ReportParameters != null && reportStruct.ReportParameters.Count > 0)
                this.reportViewer1.LocalReport.SetParameters(reportStruct.ReportParameters);            
            //this.reportViewer1.LocalReport.Refresh();
            //this.reportViewer1.RefreshReport();
        }

        private void ReportViewFrom_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void ReportViewFrom_FormClosed(object sender, FormClosedEventArgs e)
        {
            reportViewer1.LocalReport.ReleaseSandboxAppDomain();
        }

        private void reportViewer1_PrintingBegin(object sender, ReportPrintEventArgs e)
        {
            CCRZManager.Add(_reportStruct.LogObject);
        }
    }

    /// <summary>
    /// 我的报表结构参数
    /// </summary>
    public class MyReportStruct
    {
       public  ReportDataSource DataSource { get; set; }
       public  List<ReportDataSource> DataSources { get; set; }
       public  List<ReportParameter> ReportParameters { get; set; }
       public string ReportPath { get; set; }
        /// <summary>
        /// 报表类型
        /// </summary>
       public string ReportType { get; set; }
        /// <summary>
        /// 打印日志对象
        /// </summary>
       public CcrzInfo LogObject { get; set; }
    }
}
