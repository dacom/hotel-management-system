﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace hotel.Win.Report
{
    public partial class DateChoose : Form
    {
        private int panelIndex;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index">可见的panel,1 or 2 </param>
        public DateChoose(int index)
        {
            InitializeComponent();
            panelIndex = index;
            panel1.Visible = index == 1;
            panel2.Visible = index == 2;
            dtprq.Value = DateTime.Now.Date;
            dtpksrq.Value = DateTime.Now.AddDays(-30).Date;
            dtpjsrq.Value = DateTime.Now.Date;
            dtpkssj.Value = DateTime.Now.Date;
            dtpjssj.Value = DateTime.Now.Date.AddDays(0.99999);
        }
        protected DateChoose():this(1)
        {
        }
        /// <summary>
        /// 开始日期，1=包括时间,2=不包括
        /// </summary>
        public DateTime StartDate
        {
            get
            {
                if (panelIndex == 1)
                {
                    return Convert.ToDateTime(dtpksrq.Value.Date.ToString("yyyy-MM-dd") + " " + dtpkssj.Value.ToShortTimeString());
                }
                else if (panelIndex == 2)
                {
                    return dtprq.Value.Date;
                }
                return DateTime.MinValue;
            }
            set
            {
                if (panelIndex == 1)
                {
                    dtpksrq.Value = value;
                }
                else if (panelIndex == 2)
                {
                    dtprq.Value = value;
                }
            }
        }

        public DateTime EndDate
        {
            get
            {
                if (panelIndex == 1)
                {
                    return Convert.ToDateTime(dtpjsrq.Value.Date.ToString("yyyy-MM-dd") + " " + dtpjssj.Value.ToShortTimeString());
                }
                else
                {
                    return DateTime.MinValue;
                }
            }

            set
            {
                dtpjsrq.Value = value;
            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }        
    }
}
