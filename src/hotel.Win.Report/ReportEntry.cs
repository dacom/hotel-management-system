﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;
using System.Data.Common;
using System.Data.Entity;
using hotel.Win.Common;
using hotel.Common;
using Microsoft.Reporting.WinForms;
using hotel.DAL;
using System.Data.SqlClient;
using System.Data.EntityClient;
using hotel.Win.Report.ReportModel;
namespace hotel.Win.Report
{
    /// <summary>
    /// 所有报表调用入口
    /// </summary>
    public partial class ReportEntry
    {
        private static string path = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\\")) + "\\RDLC\\";

        /// <summary>
        /// 显示报表窗体
        /// </summary>
        /// <param name="reportStruct"></param>
        private static void ShowReportViewFrom(MyReportStruct reportStruct)
        {
            var frm = new ReportViewFrom(reportStruct);
            frm.ShowDialog();
        }

        /// <summary>
        /// 散客预订报表
        /// </summary>
        /// <param name="dyry"></param>
        public static void ShowSanKeYuDingReport()
        {
            string dyry = UserLoginInfo.UserName;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = from rz in _context.RZJL
                      join ry in _context.RZRY on rz.ID equals ry.RZJL.ID into rzrys
                      where rz.YDBZ == "是" && rz.KRLB == RoomStstusCnName.SanKe
                      orderby rz.CJRQ descending
                      select new
                      {
                          rz.ID,
                          ROOM_ID = rz.ROOM.ID,
                          ROOM_TYPE = rz.ROOM.ROOM_TYPE.MC,
                          DFRY = rzrys.FirstOrDefault().KRXM,
                          GS = rzrys.FirstOrDefault().DW,
                          SKFJ = rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.BZSM
                      };

            repStruct.DataSource = new ReportDataSource("hotelDataSet_RZJL", qry.ToList());
            repStruct.ReportPath = path + "SanKeYuDingReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            ShowReportViewFrom(repStruct);
        }
        /// <summary>
        /// 团队预订报表
        /// </summary>
        /// <param name="dyry"></param>
        public static void ShowTuanDuiYuDingReport()
        {
            string dyry = UserLoginInfo.UserName;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = from rz in _context.RZJL
                      where rz.YDBZ == "是" && rz.TDMC != null
                      orderby rz.CJRQ descending
                      select new
                      {

                          rz.ID,
                          ROOM_ID = rz.ROOM.ID,
                          ROOM_TYPE = rz.ROOM.ROOM_TYPE.MC,
                          rz.TDDM,
                          rz.TDMC,
                          SKFJ=rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.BZSM
                      };
            repStruct.DataSource = new ReportDataSource("hotelDataSet_RZJL", qry);
            repStruct.ReportPath = path + "TuanDuiYuDingReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 维修房报表
        /// </summary>
        /// <param name="dyry"></param>
        public static void ShowWeiXiuFangReport()
        {
            string dyry = UserLoginInfo.UserName;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = from p in _context.ROOM_MAINTAIN
                      where p.WCBZ == "否"
                      orderby p.SQRQ
                      select p;
            repStruct.DataSource = new ReportDataSource("hotelDataSet_ROOM_MAINTAIN", qry);
            repStruct.ReportPath = path + "WeiXiuFangReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 日入住客人报表
        /// </summary>
        /// <param name="dyry">打印人员</param>
        /// <param name="rzrq">入住日期</param>
        public static void ShowRuZhuRenYuanReport()
        {

            var rqfrm = new DateChoose(2);
            if (rqfrm.ShowDialog() != DialogResult.OK)
                return;
            string dyry = UserLoginInfo.UserName;
            DateTime rzrq = rqfrm.StartDate;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = from rz in _context.RZJL
                      join ry in _context.RZRY on rz.ID equals ry.RZJL.ID into rzrys
                      join xf in _context.XFJL on rz.ID equals xf.RZJL.ID into xfs
                      where rz.RZBZ == "是" && rz.DDRQ.Value.Year == rzrq.Year && rz.DDRQ.Value.Month == rzrq.Month && rz.DDRQ.Value.Day == rzrq.Day
                      orderby rz.CJRQ
                      select new
                      {
                          rz.ID,
                          ROOM_ID = rz.ROOM.ID,
                          rz.KRLB,
                          KRXM = rzrys.FirstOrDefault().KRXM,
                          KRXB = rzrys.FirstOrDefault().KRXB,
                          YJ = xfs.Sum(p => p.YJ),
                          XFHJ = xfs.Sum(p => p.JE),
                          rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.BZSM
                      };
            repStruct.DataSource = new ReportDataSource("hotelDataSet_RZJL", qry);
            repStruct.ReportPath = path + "RuZhuRenYuanReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            repStruct.ReportParameters.Add(new ReportParameter("RZRQ", rzrq.ToString("yyyy-MM-dd")));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 日结帐客人报表
        /// </summary>
        /// <param name="dyry">打印人员</param>
        /// <param name="rzrq">入住日期</param>
        public static void ShowJieZhangRenYuanReport()
        {
            var rqfrm = new DateChoose(1);
            if (rqfrm.ShowDialog() != DialogResult.OK)
                return;
            string dyry = UserLoginInfo.UserName;
            DateTime jzrq1 = rqfrm.StartDate;
            DateTime jzrq2 = rqfrm.EndDate;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = from rz in _context.RZJL_ARC
                      join ry in _context.RZRY_ARC on rz.ID equals ry.RZJL_ARC.ID into rzrys
                      join xf in _context.XFJL_ARC on rz.ID equals xf.RZJL_ARC.ID into xfs
                      where rz.JZBZ == "是" && rz.JZRQ >= jzrq1 && rz.JZRQ <= jzrq2
                      orderby rz.JZRQ
                      select new
                      {
                          rz.ID,
                          rz.ROOM_ID,
                          rz.KRLB,
                          KRXM = rzrys.FirstOrDefault().KRXM,
                          KRXB = rzrys.FirstOrDefault().KRXB,
                          XFHJ = xfs.Where(p => p.XFBZ == "消").Sum(p => p.JE),
                          rz.SJFJ,
                          rz.DDRQ,
                          rz.JZRQ,
                          rz.BZSM
                      };
            repStruct.DataSource = new ReportDataSource("hotelDataSet_RZJL", qry);
            repStruct.ReportPath = path + "JieZhangRenYuanReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            repStruct.ReportParameters.Add(new ReportParameter("JZRQ1", jzrq1.ToString("yyyy-MM-dd")));
            repStruct.ReportParameters.Add(new ReportParameter("JZRQ2", jzrq2.ToString("yyyy-MM-dd")));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 在住客人房租日报表
        /// </summary>
        public static void ShowZaiZhuFangZuReport()
        {
            string dyry = UserLoginInfo.UserName;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = from rz in _context.RZJL
                      join ry in _context.RZRY on rz.ID equals ry.RZJL.ID into rzrys
                      where rz.RZBZ == "是"
                      orderby rz.ROOM.ID
                      select new
                      {
                          rz.ID,
                          ROOM_ID = rz.ROOM.ID,
                          ROOM_TYPE = rz.ROOM.ROOM_TYPE.MC,
                          rz.KRLB,
                          KRXM = rzrys.FirstOrDefault().KRXM,
                          KRXB = rzrys.FirstOrDefault().KRXB,
                          rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.BZSM
                      };
            repStruct.DataSource = new ReportDataSource("hotelDataSet_RZJL", qry.ToList());
            repStruct.ReportPath = path + "ZaiZhuFangZuReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 协议公司入住报表
        /// </summary>
        public static void ShowXieYiGongSiRZReport()
        {
            string dyry = UserLoginInfo.UserName;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = from rz in _context.RZJL
                      join ry in _context.RZRY on rz.ID equals ry.RZJL.ID into rzrys
                      where rz.RZBZ == "是" && rz.XYDW_MC != null
                      orderby rz.ROOM.ID
                      select new
                      {
                          rz.ID,
                          ROOM_ID = rz.ROOM.ID,
                          rz.XYDW_DM,
                          rz.XYDW_MC,
                          KRXM = rzrys.FirstOrDefault().KRXM,
                          KRXB = rzrys.FirstOrDefault().KRXB,
                          rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.BZSM
                      };
            repStruct.DataSource = new ReportDataSource("hotelDataSet_RZJL", qry);
            repStruct.ReportPath = path + "XieYiGongSiRZReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            ShowReportViewFrom(repStruct);
        }
        /// <summary>
        /// 房间状态报表
        /// </summary>
        public static void ShowFangJiZhuangTaiReport()
        {
            string dyry = UserLoginInfo.UserName;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = from rz in _context.UV_ROOM_RZJL_RZ
                      where rz.ID!= SysConsts.Room9999
                      orderby rz.ID
                      select new
                      {
                          rz.ID,
                          rz.ROOM_TYPE_MC,
                          rz.ZT,
                          rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.BZSM
                      };
            repStruct.DataSource = new ReportDataSource("hotelDataSet_UV_ROOM_RZJL_RZ", qry);
            repStruct.ReportPath = path + "FangJiZhuangTaiReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            ShowReportViewFrom(repStruct);
        }
        /// <summary>
        /// 消费分类汇总表
        /// </summary>
        public static void ShowXiaoFeiHuiZongReport()
        {
            string dyry = UserLoginInfo.UserName;
            var rqfrm = new XiaoFeiDateChoose();
            if (rqfrm.ShowDialog() != DialogResult.OK)
                return;

            DateTime rq1 = rqfrm.StartDate;
            DateTime rq2 = rqfrm.EndDate;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            string title = "消费分类汇总表";
            string sqlstr;
            if (rqfrm.Scope == 0)
            {
                sqlstr = "select a.FLMC ,a.XFXM_MC,SUM(a.SL) as SL,SUM(a.JE) as JE "
                + "from UV_XFJL_FL_ALL as a "
                + "where  a.cjrq between @rq1 and @rq2 "
                + "group by  a.FLMC,a.XFXM_MC order by a.FLMC,a.XFXM_MC ";
                title += "[全部]";
            }
            else if (rqfrm.Scope == 1)
            {
                sqlstr = "select a.FLMC ,a.XFXM_MC,SUM(a.SL) as SL,SUM(a.JE) as JE "
                + "from UV_XFJL_ARC_FL as a "
                + "where a.cjrq between @rq1 and @rq2 "
                + "group by  a.FLMC,a.XFXM_MC order by a.FLMC,a.XFXM_MC ";
                title += "[已结帐]";
            }
            else
            {
                sqlstr = "select a.FLMC ,a.XFXM_MC,SUM(a.SL) as SL,SUM(a.JE) as JE "
                + "from UV_XFJL_FL as a "
                + "where a.cjrq between @rq1 and @rq2 "
                + "group by  a.FLMC,a.XFXM_MC order by a.FLMC,a.XFXM_MC ";
                title += "[未结帐]";
            }
                        
            var args = new DbParameter[] { new SqlParameter { ParameterName = "rq1", Value = rq1 }, new SqlParameter { ParameterName = "rq2", Value = rq2 } };
            var query = _context.Database.SqlQuery<XiaoFeiHuiZongReport>(sqlstr, args);            
            repStruct.DataSource = new ReportDataSource("hotelDataSet_UV_RZJL_XFJL", query);
            repStruct.ReportPath = path + "XiaoFeiHuiZongReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            repStruct.ReportParameters.Add(new ReportParameter("JZRQ1", rq1.ToString("yyyy-MM-dd")));
            repStruct.ReportParameters.Add(new ReportParameter("JZRQ2", rq2.ToString("yyyy-MM-dd")));
            repStruct.ReportParameters.Add(new ReportParameter("Title", title));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 消费项目明细表
        /// </summary>
        public static void ShowXiaoFeiMingXiReport()
        {
            string dyry = UserLoginInfo.UserName;
            var rqfrm = new XiaoFeiDateChoose();
            if (rqfrm.ShowDialog() != DialogResult.OK)
                return;

            DateTime rq1 = rqfrm.StartDate;
            DateTime rq2 = rqfrm.EndDate;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            object dataSource;
            string title = "消费项目明细表";
            if (rqfrm.Scope == 0)
            {
                dataSource = _context.UV_XFJL_FL_ALL.Where(p => p.CJRQ >= rq1 && p.CJRQ <= rq2).OrderBy(p => p.CJRQ);
                title += "[全部]";
            }
            else if (rqfrm.Scope == 1)
            {
                dataSource = _context.UV_XFJL_ARC_FL.Where(p => p.CJRQ >= rq1 && p.CJRQ <= rq2).OrderBy(p => p.CJRQ);
                title += "[已结帐]";
            }
            else
            {
                dataSource = _context.UV_XFJL_FL.Where(p => p.CJRQ >= rq1 && p.CJRQ <= rq2).OrderBy(p => p.CJRQ);
                title += "[未结帐]";
            }
            repStruct.DataSource = new ReportDataSource("hotelDataSet_UV_XFJL_FL", dataSource);
            repStruct.ReportPath = path + "XiaoFeiMingXiReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            repStruct.ReportParameters.Add(new ReportParameter("JZRQ1", rq1.ToString("yyyy-MM-dd")));
            repStruct.ReportParameters.Add(new ReportParameter("JZRQ2", rq2.ToString("yyyy-MM-dd")));
            repStruct.ReportParameters.Add(new ReportParameter("Title", title));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 押金明细报表
        /// </summary>
        public static void ShowYaJinMingXiReport()
        {
            string dyry = UserLoginInfo.UserName;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = from yj in _context.UV_XFJL_RY                     
                      orderby yj.ROOM_ID,yj.CJRQ
                      select yj;
            repStruct.DataSource = new ReportDataSource("hotelDataSet_UV_XFJL_RY", qry);
            repStruct.ReportPath = path + "YaJinMingXiReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            ShowReportViewFrom(repStruct);
        }
        /// <summary>
        /// 客人帐户余额细报表
        /// </summary>
        public static void ShowZhangHuYueReport()
        {
            string dyry = UserLoginInfo.UserName;
            MyReportStruct repStruct = new MyReportStruct();
           
            string sqlstr = " select a.RZJL_ID,A.ROOM_ID,A.KRXM,A.KRXB,SUM(A.JE) AS SUM_JE,SUM(A.YJ) AS  SUM_YJ,SUM(A.YJ) - SUM(A.JE) AS SUM_YE"
                    + " FROM UV_XFJL_RY as A "
                    + " where A.ROOM_ID<>" + Utility.QuotedStr(SysConsts.Room9999)
                    + " GROUP BY a.RZJL_ID,A.ROOM_ID,A.KRXM,A.KRXB "
                    + " order by A.ROOM_ID ";
            using (var _context = MyDataContext.GetDataContext)
            {
                var qry = _context.Database.SqlQuery<ZhangHuYueReport>(sqlstr).ToList();
                repStruct.DataSource = new ReportDataSource("hotelDataSet_UV_XFJL_RY", qry);
            }
          
            repStruct.ReportPath = path + "ZhangHuYueReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            ShowReportViewFrom(repStruct);
        }
        /// <summary>
        /// 协议单位支付明细报表
        /// </summary>
        public static void ShowXieYiDanWeiZFReport()
        {
            var rqfrm = new DateChoose(1);
            if (rqfrm.ShowDialog() != DialogResult.OK)
                return;
            string dyry = UserLoginInfo.UserName;
            DateTime jzrq1 = rqfrm.StartDate;
            DateTime jzrq2 = rqfrm.EndDate;
            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            var qry = _context.UV_XYDW_ZF.Where(p => p.ZFRQ >= jzrq1 && p.ZFRQ <= jzrq2).OrderBy(p => p.XYDW_MC).OrderBy(p => p.ZFRQ);
            repStruct.DataSource = new ReportDataSource("hotelDataSet_UV_XYDW_ZF", qry.ToList());
            repStruct.ReportPath = path + "XieYiDanWZHFReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", dyry));
            repStruct.ReportParameters.Add(new ReportParameter("JZRQ1", jzrq1.ToString("yyyy-MM-dd")));
            repStruct.ReportParameters.Add(new ReportParameter("JZRQ2", jzrq2.ToString("yyyy-MM-dd")));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 营业日报表
        /// </summary>
        /// <param name="jbid">交班ID,0为上个交班</param>
        public static void ShowYingYeRiBaoReport(decimal jbid)
        {
            string dyry = UserLoginInfo.UserName;

            MyReportStruct repStruct = new MyReportStruct();
            var _context = MyDataContext.GetDataContext;
            JBJL dmModel = new JBJL();
            var rq = XTCSModel.GetXTCS.YYRQ;
            decimal? sbye =0;//上班余额
            decimal sjje = 0, xfje = 0;//上交，下发营业款
            if (_context.JBJL.Count() > 0)
            {
                if (jbid > 0)
                {
                    dmModel = _context.JBJL.FirstOrDefault(p => p.ID == jbid);
                    sjje = dmModel.SJ_YYK.Value;
                    xfje = dmModel.XF_YYK.Value;
                    var id0 = _context.JBJL.Where(p=>p.ID != jbid).Max(p => p.ID);
                    var tmp =  _context.JBJL.FirstOrDefault(p => p.ID == id0);
                    if (tmp == null)
                        sbye = 0;
                    else
                        sbye = tmp.JJJE - tmp.ZMYJ;
                   
                }
                else
                {
                    var id = _context.JBJL.Max(p => p.ID);
                    dmModel = _context.JBJL.FirstOrDefault(p => p.ID == id);
                    if (dmModel == null)
                        sbye = 0;
                    else
                    sbye = dmModel.JJJE - dmModel.ZMYJ;
                }                
            }
            else
            {
                dmModel.XB_JSSJ = Convert.ToDateTime(rq);
                dmModel.JJJE = 0;
                dmModel.ZMYJ = 0;
                sbye = 0;
            }
            DateTime sbsj;
            //交接完成的开始时间
            if (jbid > 0)
                sbsj=(DateTime)(dmModel.SB_KSSJ);
            else
                sbsj=(DateTime)(dmModel.XB_JSSJ);
            dmModel = null;
            //结算收入
            decimal? je = _context.XFJL_ARC.Where(p => p.RZJL_ARC.JZRQ >= sbsj && p.RZJL_ARC.JZBZ == "是").Sum(p => p.JE);
            decimal jssr = je == null ? 0 : (decimal)je;
            //结算收入-房租
            je = _context.XFJL_ARC.Where(p => p.RZJL_ARC.JZRQ >= sbsj && p.RZJL_ARC.JZBZ == "是" && p.XFXM_MC == "房租").Sum(p => p.JE);
            decimal jssr_fz = je == null ? 0 : (decimal)je;
            //结算收入-除房租的其他消费
            je = _context.XFJL_ARC.Where(p => p.RZJL_ARC.JZRQ >= sbsj && p.RZJL_ARC.JZBZ == "是" && p.XFXM_MC != "房租").Sum(p => p.JE);
            decimal jssr_xf = je == null ? 0 : (decimal)je;
            //非住房消费,包括茶楼
            je = _context.XFJL_ARC.Where(p => p.CJRQ >= sbsj && p.RZJL_ID == SysConsts.Room9999RZID).Sum(p => p.JE);
            decimal fzf_xf = je == null ? 0 : (decimal)je;
            jssr += fzf_xf;
            //茶楼消费
            je = _context.XFJL_ARC.Where(p => p.CJRQ >= sbsj && p.RZJL_ID == SysConsts.Room9999RZID && p.XFXM_MC == "茶楼费").Sum(p => p.JE);
            decimal clxf = je == null ? 0 : (decimal)je;
            //帐面押金
            je = _context.UV_RZJL_YJ.Sum(p => p.YJ);
            var zmyj = je == null ? 0 : je;
            //前台交款
            je = sbye + jssr + zmyj - sjje + xfje;
            var ztjk = je.ToString();
            repStruct.DataSource = null;
            repStruct.DataSources = new List<ReportDataSource>();
            var qry = _context.UV_RZJL_XFJL_ARC_SUMJE.Where(p => p.JZRQ >= sbsj && p.JZBZ == "是").OrderBy(p => p.ROOM_ID);
            repStruct.DataSources.Add(new ReportDataSource("DataSet", qry.ToList()));
            var qry2 = _context.UV_RZJL_YJ.OrderBy(p=>p.CJRQ);
            repStruct.DataSources.Add(new ReportDataSource("DataSetYJ", qry2.ToList().OrderBy(p => p.ROOM_ID)));
            repStruct.ReportPath = path + "YingYeRiBaoReport.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("CZY", dyry));
            repStruct.ReportParameters.Add(new ReportParameter("RQ", rq));
            repStruct.ReportParameters.Add(new ReportParameter("ZTJK", ztjk.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("JDTitle", XTCSModel.GetXTCS.JDMC));
            repStruct.ReportParameters.Add(new ReportParameter("JSSR", jssr.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("JSSR_XF", jssr_xf.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("JSSR_FZ", jssr_fz.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("ZYJ", zmyj.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("FZF_XF", fzf_xf.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("CLXF", clxf.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("SBYE", sbye.ToString()));
            ShowReportViewFrom(repStruct);
        }
    }
}
