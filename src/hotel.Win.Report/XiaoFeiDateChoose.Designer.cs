﻿namespace hotel.Win.Report
{
    partial class XiaoFeiDateChoose
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnwjz = new System.Windows.Forms.RadioButton();
            this.rbtnyjz = new System.Windows.Forms.RadioButton();
            this.rbtnall = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnwjz);
            this.groupBox1.Controls.Add(this.rbtnyjz);
            this.groupBox1.Controls.Add(this.rbtnall);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(337, 52);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "范围选择";
            // 
            // rbtnwjz
            // 
            this.rbtnwjz.AutoSize = true;
            this.rbtnwjz.Location = new System.Drawing.Point(150, 18);
            this.rbtnwjz.Name = "rbtnwjz";
            this.rbtnwjz.Size = new System.Drawing.Size(67, 18);
            this.rbtnwjz.TabIndex = 1;
            this.rbtnwjz.Text = "未结帐";
            this.rbtnwjz.UseVisualStyleBackColor = true;
            // 
            // rbtnyjz
            // 
            this.rbtnyjz.AutoSize = true;
            this.rbtnyjz.Checked = true;
            this.rbtnyjz.Location = new System.Drawing.Point(77, 18);
            this.rbtnyjz.Name = "rbtnyjz";
            this.rbtnyjz.Size = new System.Drawing.Size(67, 18);
            this.rbtnyjz.TabIndex = 1;
            this.rbtnyjz.TabStop = true;
            this.rbtnyjz.Text = "已结帐";
            this.rbtnyjz.UseVisualStyleBackColor = true;
            // 
            // rbtnall
            // 
            this.rbtnall.AutoSize = true;
            this.rbtnall.Location = new System.Drawing.Point(18, 18);
            this.rbtnall.Name = "rbtnall";
            this.rbtnall.Size = new System.Drawing.Size(53, 18);
            this.rbtnall.TabIndex = 0;
            this.rbtnall.Text = "全部";
            this.rbtnall.UseVisualStyleBackColor = true;
            // 
            // XiaoFeiDateChoose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(337, 177);
            this.Controls.Add(this.groupBox1);
            this.Name = "XiaoFeiDateChoose";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtnwjz;
        private System.Windows.Forms.RadioButton rbtnyjz;
        private System.Windows.Forms.RadioButton rbtnall;
    }
}
