﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace hotel.Win.Report
{
    public partial class XiaoFeiDateChoose : hotel.Win.Report.DateChoose
    {
        public XiaoFeiDateChoose():base(1)
        {
            InitializeComponent();
        }

        /// <summary>
        /// 范围0:全部，1:已结帐,2:未结帐
        /// </summary>
        public int Scope
        {
            get {
                if (rbtnall.Checked)
                    return 0;
                else if (rbtnyjz.Checked)
                    return 1;//已结帐
                else
                    return 2;
            }
        }
    }
}
