﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;
using System.Data.Common;
using hotel.Win.Common;
using hotel.DAL;
using Microsoft.Reporting.WinForms;
using hotel.Common;
namespace hotel.Win.Report
{
    /// <summary>
    /// 所有报表调用入口
    /// </summary>
    public partial class ReportEntry
    {
        private static string pathBill = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\\")) + "\\PrintBill\\";
        /// <summary>
        /// 客人入住登记单
        /// </summary>
        /// <param name="jdTitle">酒店名称</param>
        public static void ShowRuZhuDengJiBill(string jdTitle, object dataSource, CcrzInfo logObject = null)
        {
            MyReportStruct repStruct = new MyReportStruct();
            repStruct.DataSource = new ReportDataSource("hotelDataSet_UV_RZJL_RZRY", dataSource);
            repStruct.LogObject = logObject;
            repStruct.ReportPath = pathBill + SysConsts.ReportRuZhuDengJiBill; //"RuZhuDengJiBill.rdlc";
            repStruct.ReportType = "ReportRuZhuDengJiBill";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("Title", jdTitle));
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", UserLoginInfo.UserName));
            repStruct.ReportParameters.Add(new ReportParameter("BTFF_SJ", XTCSModel.GetXTCS.JBTFZ_SJ));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 客人结帐消费汇总
        /// </summary>
        /// <param name="jdTitle">酒店名称</param>
        public static void ShowXiaoFeiSumBill(string jdTitle, string rzid, CcrzInfo logInfo=null)
        {
            //金额或押金可能为空，特别是联房时
            var _context = MyDataContext.GetDataContext;
            decimal? je;
            je= _context.XFJL_ARC.Where(p => p.RZJL_ID == rzid).Sum(p => p.JE);
            decimal xiaoFeiHeJi = je != null ? je.Value : 0;
            je = _context.XFJL_ARC.Where(p => p.RZJL_ID == rzid).Sum(p => p.YJ);
            decimal yaJinHeJi = je != null ? je.Value : 0;         
            foreach ( var item in _context.RZJL_ARC.Where(p => p.F_ID == rzid))
            {
                je =  _context.XFJL_ARC.Where(p => p.RZJL_ID == item.ID).Sum(p => p.JE);
                xiaoFeiHeJi += je != null ? je.Value : 0;
                je = _context.XFJL_ARC.Where(p => p.RZJL_ID == item.ID).Sum(p => p.YJ);
                yaJinHeJi += je != null ? je.Value : 0;
            }
            MyReportStruct repStruct = new MyReportStruct();
            repStruct.DataSource = new ReportDataSource("hotelDataSet_UV_RZJL_RZRY");
            repStruct.ReportPath = pathBill + "XiaoFeiSumBill.rdlc";
            repStruct.LogObject = logInfo;
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("Title", jdTitle));
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", UserLoginInfo.UserName));
            repStruct.ReportParameters.Add(new ReportParameter("XiaoFeiHeJi", xiaoFeiHeJi.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("YaJinHeJi", yaJinHeJi.ToString()));                    

            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 客人结帐消费明细
        /// </summary>
        /// <param name="jdTitle">酒店名称</param>
        /// <param name="isHeDan">是否合单</param>
        public static void ShowXiaoFeiMingXiBill(string jdTitle, string rzjl_id,bool isHeDan, CcrzInfo logInfo=null)
        {
            var _context = MyDataContext.GetDataContext;
            MyReportStruct repStruct = new MyReportStruct();
            repStruct.DataSource = null;
            repStruct.DataSources = new List<ReportDataSource>();
            repStruct.LogObject = logInfo;
            // && p.JE > 0,包括押金和消费
            List<UV_RZJL_XFJL_ARC> qry;
            qry = _context.UV_RZJL_XFJL_ARC.Where(p => p.ID == rzjl_id).OrderBy(p => p.XF_CJRQ).ToList();
            if (isHeDan)
            {
                //子单
                qry.AddRange(_context.UV_RZJL_XFJL_ARC.Where(p => p.F_ID == rzjl_id).ToList());
                qry = qry.OrderBy(p => p.XF_CJRQ).ToList();
            }           
            
            repStruct.DataSources.Add(new ReportDataSource("hotelDataSet_UV_RZJL_XFJL", qry));

            var dataSource = _context.UV_RZJL_RZRY_ARC.Where(p => p.ID == rzjl_id);
            repStruct.DataSources.Add(new ReportDataSource("hotelDataSet_UV_RZJL_RZRY", dataSource.ToList()));
            repStruct.ReportType = "XiaoFeiMingXiBill";
            repStruct.ReportPath = pathBill + SysConsts.XiaoFeiMingXiBill;
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("Title", jdTitle));
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", UserLoginInfo.UserName));
            ShowReportViewFrom(repStruct);
        }


        /// <summary>
        /// 客人结帐消费明细-ARC(已结帐)
        /// </summary>
        /// <param name="jdTitle">酒店名称</param>
        public static void ShowXiaoFeiMingXi_ARCBill(string jdTitle, string rzjl_id)
        {
            //不支持同开房
            var _context = MyDataContext.GetDataContext;
            MyReportStruct repStruct = new MyReportStruct();
            repStruct.DataSource = null;            
            repStruct.DataSources = new List<ReportDataSource>();

            var qry = _context.UV_RZJL_XFJL_ARC.Where(p => p.ID == rzjl_id && p.JE > 0).OrderBy(p => p.CJRQ).ToList();

            repStruct.DataSources.Add(new ReportDataSource("hotelDataSet_UV_RZJL_XFJL", qry));

            var dataSource = _context.UV_RZJL_RZRY_ARC.Where(p => p.ID == rzjl_id);
            repStruct.DataSources.Add(new ReportDataSource("hotelDataSet_UV_RZJL_RZRY", dataSource.ToList()));

            repStruct.ReportPath = pathBill + "XiaoFeiMingXiBill.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("Title", jdTitle));
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", UserLoginInfo.UserName));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 押金单
        /// </summary>
        /// <param name="yjid">消费的押金ID</param>
        public static void ShowYaJinBill(int yjid, CcrzInfo logInfo=null)
        {           
            var _context = MyDataContext.GetDataContext;
            MyReportStruct repStruct = new MyReportStruct();
            repStruct.LogObject = logInfo;
            repStruct.ReportType = "ShowYaJinBill";
            repStruct.DataSource = null;
            repStruct.DataSources = new List<ReportDataSource>();
            var qry = _context.UV_RZJL_XFJL.Where(p=>p.XFJL_ID == yjid).ToList();
            if (qry.Count == 0)
                return;
            var rzid = qry.First().ID;
            repStruct.DataSources.Add(new ReportDataSource("DataSet", qry));
            repStruct.ReportPath = pathBill + "YaJinBill.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            string krxm ="", krxb="";
            var d = _context.RZRY.FirstOrDefault(p=>p.RZJL_ID==rzid);
            if (d != null)
            {
                krxm = d.KRXM;
                krxb = d.KRXB;
            }
            var jdInfo = XTCSModel.GetXTCS;
            repStruct.ReportParameters.Add(new ReportParameter("JDMC", jdInfo.JDMC));
            repStruct.ReportParameters.Add(new ReportParameter("XM", krxm));
            repStruct.ReportParameters.Add(new ReportParameter("KRXB", krxb));
            repStruct.ReportParameters.Add(new ReportParameter("btfsj", jdInfo.JBTFZ_SJ));
            repStruct.ReportParameters.Add(new ReportParameter("qtfsj", jdInfo.JYTFZ_SJ));
            ShowReportViewFrom(repStruct);
        }

        /// <summary>
        /// 客人换房打印表
        /// </summary>
        /// <param name="jdTitle">酒店名称</param>
        public static void ShowKeRenHuanFangBill(string krxm,DateTime ddrq,
            string yfjh,string xfjh,decimal yfj,decimal xfj,string hfyy,CcrzInfo logInfo = null)
        {             
            MyReportStruct repStruct = new MyReportStruct();
            repStruct.DataSource = null;
            repStruct.ReportPath = pathBill + "KeRenHuanFangBill.rdlc";
            repStruct.LogObject = logInfo;
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("JDMC", XTCSModel.GetXTCS.JDMC));
            repStruct.ReportParameters.Add(new ReportParameter("KRXM" ,krxm ));
            repStruct.ReportParameters.Add(new ReportParameter("DDRQ" ,ddrq.ToString() ));
            repStruct.ReportParameters.Add(new ReportParameter("YFJH", yfjh));
            repStruct.ReportParameters.Add(new ReportParameter("XFJH", xfjh));
            repStruct.ReportParameters.Add(new ReportParameter("YFJ", yfj.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("XFJ", xfj.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("HFYY", hfyy));
            repStruct.ReportParameters.Add(new ReportParameter("CZRY", UserLoginInfo.UserName));
            repStruct.ReportParameters.Add(new ReportParameter("CCRQ", DateTime.Now.ToString()));

            ShowReportViewFrom(repStruct);
        }
        /// <summary>
        /// 协议单位结算打印
        /// </summary>        
        public static void ShowXieYiDanWeiJSBill(string dwmc,string zffs, decimal zfje,string bzsm, CcrzInfo logInfo = null)
        {
            MyReportStruct repStruct = new MyReportStruct();
            repStruct.DataSource = null;
            repStruct.ReportPath = pathBill + "XieYiDanWeiJSBill.rdlc";
            repStruct.LogObject = logInfo;
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("JDMC", XTCSModel.GetXTCS.JDMC));
            repStruct.ReportParameters.Add(new ReportParameter("DWMC", dwmc));
            repStruct.ReportParameters.Add(new ReportParameter("ZFFS", zffs));
            repStruct.ReportParameters.Add(new ReportParameter("ZFJE", zfje.ToString()));
            repStruct.ReportParameters.Add(new ReportParameter("BZSM", bzsm));            
            repStruct.ReportParameters.Add(new ReportParameter("CZRY", UserLoginInfo.UserName));
            repStruct.ReportParameters.Add(new ReportParameter("CCRQ", DateTime.Now.ToString()));

            ShowReportViewFrom(repStruct);
        }
        /// <summary>
        /// 非消费明细单打印
        /// </summary>
        /// <param name="logInfo"></param>
        public static void ShowFeiXiaoFeiMingXiBill(CcrzInfo logInfo = null)
        {
            var _context = MyDataContext.GetDataContext;
            MyReportStruct repStruct = new MyReportStruct();
            repStruct.LogObject = logInfo;
            var qry = _context.XFJL.Where(p => p.RZJL_ID == SysConsts.Room9999RZID).OrderBy(p => p.CJRQ);
            repStruct.DataSource = new ReportDataSource("DataSetXFJL", qry.ToList());
            repStruct.ReportType = "FeiXiaoFeiMingXiBill";
            repStruct.ReportPath = pathBill + "FeiXiaoFeiMingXiBill.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("Title", XTCSModel.GetXTCS.JDMC));
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", UserLoginInfo.UserName));
            ShowReportViewFrom(repStruct);
        }
        /// <summary>
        /// 非消费明细单打印(补打),多个ID打印
        /// </summary>
        /// <param name="logInfo"></param>
        public static void ShowFeiXiaoFeiMingXiBill(List<int> ids,CcrzInfo logInfo = null)
        {
            // in 语句
            var _context = MyDataContext.GetDataContext;
            MyReportStruct repStruct = new MyReportStruct();
            repStruct.LogObject = logInfo;
            var qry = from q in _context.XFJL_ARC
                      where ids.Contains(q.ID)
                      orderby q.CJRQ
                      select q;    

            repStruct.DataSource = new ReportDataSource("DataSetXFJL", qry.ToList());
            repStruct.ReportType = "FeiXiaoFeiMingXiBill";
            repStruct.ReportPath = pathBill + "FeiXiaoFeiMingXiBill.rdlc";
            repStruct.ReportParameters = new List<ReportParameter>();
            repStruct.ReportParameters.Add(new ReportParameter("Title", XTCSModel.GetXTCS.JDMC));
            repStruct.ReportParameters.Add(new ReportParameter("DYRY", UserLoginInfo.UserName));
            ShowReportViewFrom(repStruct);
        }
    }
}
