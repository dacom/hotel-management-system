﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Common;

namespace hotel.Win.Common
{
    public partial class ReadKeycardFrm : Form
    {
       
        public ReadKeycardFrm()
        {
            InitializeComponent();
            OriginalInfo = "";
        }
        public string OriginalInfo { get; set; }

        public void ShowCardInfo()
        {
            var card = Utility.CreateKeycarder();
            var info = card.ReadGuestCardInfo();
           
            if (info == null)
                return;
            OriginalInfo = info.OriginalInfo;
           
            // 卡号,卡类型,楼栋号,楼层号,房间号,套间号,开始时间,结束时间,开特权门标记
            lbkid.Text = info.CardId;
            txtld.Text = info.FloorCode.ToString();
            txtlc.Text = info.FloorLayCode.ToString();
            txtfjh.Text = info.RoomCode.ToString();
            txttfh.Text = info.SubRoomCode.ToString();
            txtrq1.Text =  info.StartDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            txtrq2.Text = info.EndDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            txttq.Text = info.IsRight?"是":"否";
            this.ShowDialog();
        }
        private void lbkid_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show(OriginalInfo);
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
