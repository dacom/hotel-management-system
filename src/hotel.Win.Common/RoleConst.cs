﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.Win.Common
{
    /// <summary>
    /// 系统角色枚举
    /// </summary>
    public enum EnumRoole { Admin, YeShen, QianTai,QianTaiJingLi };

    /// <summary>
    /// 系统角色常量
    /// </summary>
    public class RoleConst
    {
        public static string Admin { get { return "管理员角色"; } }
        public static string YeShen { get { return "夜审角色"; } }
        public static string QianTai { get { return "前台角色"; } }
        public static string QianTaiJingLi { get { return "前台经理"; } }
        
    }
}
