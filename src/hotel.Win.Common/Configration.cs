﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace hotel.Win.Common
{
    public class Configration
    {
        private static bool? _AutoLoadRoomStatus;
        /// <summary>
        /// 是否自动加载实时房态,只用调试系统
        /// </summary>
        public static bool IsAutoLoadRoomStatus
        {
            get {
                if (_AutoLoadRoomStatus==null)                    
                {
                    _AutoLoadRoomStatus = ConfigurationManager.AppSettings["AutoLoadRoomStatus"] == "1";
                }
                return (bool)_AutoLoadRoomStatus;
            }
        }
    }
}
