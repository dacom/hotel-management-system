﻿namespace hotel.Win.Common
{
    partial class ReadKeycardFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbkid = new System.Windows.Forms.LinkLabel();
            this.txtld = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtlc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtfjh = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txttfh = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtrq1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtrq2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txttq = new System.Windows.Forms.TextBox();
            this.btnclose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "卡序号";
            // 
            // lbkid
            // 
            this.lbkid.AutoSize = true;
            this.lbkid.Location = new System.Drawing.Point(87, 13);
            this.lbkid.Name = "lbkid";
            this.lbkid.Size = new System.Drawing.Size(35, 14);
            this.lbkid.TabIndex = 1;
            this.lbkid.TabStop = true;
            this.lbkid.Text = "none";
            this.lbkid.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbkid_LinkClicked);
            // 
            // txtld
            // 
            this.txtld.Location = new System.Drawing.Point(90, 41);
            this.txtld.Name = "txtld";
            this.txtld.Size = new System.Drawing.Size(74, 23);
            this.txtld.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "楼栋号";
            // 
            // txtlc
            // 
            this.txtlc.Location = new System.Drawing.Point(90, 70);
            this.txtlc.Name = "txtlc";
            this.txtlc.Size = new System.Drawing.Size(74, 23);
            this.txtlc.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "楼层号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "房间号";
            // 
            // txtfjh
            // 
            this.txtfjh.Location = new System.Drawing.Point(90, 99);
            this.txtfjh.Name = "txtfjh";
            this.txtfjh.Size = new System.Drawing.Size(74, 23);
            this.txtfjh.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(172, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "套间号";
            // 
            // txttfh
            // 
            this.txttfh.Location = new System.Drawing.Point(236, 102);
            this.txttfh.Name = "txttfh";
            this.txttfh.Size = new System.Drawing.Size(52, 23);
            this.txttfh.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "开始时间";
            // 
            // txtrq1
            // 
            this.txtrq1.Location = new System.Drawing.Point(90, 128);
            this.txtrq1.Name = "txtrq1";
            this.txtrq1.Size = new System.Drawing.Size(198, 23);
            this.txtrq1.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "结束时间";
            // 
            // txtrq2
            // 
            this.txtrq2.Location = new System.Drawing.Point(90, 157);
            this.txtrq2.Name = "txtrq2";
            this.txtrq2.Size = new System.Drawing.Size(198, 23);
            this.txtrq2.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 189);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "开特权门";
            // 
            // txttq
            // 
            this.txttq.Location = new System.Drawing.Point(90, 186);
            this.txttq.Name = "txttq";
            this.txttq.Size = new System.Drawing.Size(74, 23);
            this.txttq.TabIndex = 2;
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(183, 224);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(105, 39);
            this.btnclose.TabIndex = 3;
            this.btnclose.Text = "关  闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // ReadKeycardFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 270);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.txttfh);
            this.Controls.Add(this.txtrq2);
            this.Controls.Add(this.txtrq1);
            this.Controls.Add(this.txttq);
            this.Controls.Add(this.txtfjh);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtlc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtld);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbkid);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("SimSun", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ReadKeycardFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "房卡信息";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel lbkid;
        private System.Windows.Forms.TextBox txtld;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtlc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtfjh;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txttfh;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtrq1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtrq2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txttq;
        private System.Windows.Forms.Button btnclose;
    }
}