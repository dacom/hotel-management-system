﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Common;
using System.IO;

namespace hotel.Win.Common
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (txtzcm.Text == "")
                return;
            Microsoft.Win32.RegistryKey retkey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", true).CreateSubKey("dacongHotel");
            retkey.SetValue("Key", txtzcm.Text);
            var log = Log.Instance;
            log.Abstract = LogAbstractEnum.增加数据;
            log.Message = "系统注册，注册码[" + txtzcm.Text + "]";
            log.Info();
            //写入文件
            var f = File.CreateText(".\\license.lic");
            f.WriteLine(txtzcm.Text);
            f.Flush();
            f.Close();
            
            MessageBox.Show("注册成功，下次启动生效！");
            Application.Exit();
        }

        private void RegisterForm_Load(object sender, EventArgs e)
        {
            txtyzm.Text = hotel.Common.Computer.Instance.GetZhuCeMa();
        }

        private void btncopy1_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(txtyzm.Text);
        }
    }
}
