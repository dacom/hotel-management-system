﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
namespace hotel.Win.Common
{
    public class Grant
    {
        static string pubkey = "<RSAKeyValue><Modulus>wAauLh78Eg/QpRuVfHGdmN8LbFqErwGUOKp0n6tHiluSq2oaG6WIKDcvVIodJKf2TS2zZNnaRLQvcTTwnhOhJ40ymuFJFm3LMHxxdJ4lvgEveoMXtLLHxRZp6/ZMP9RTjWDiQUwQJwND4yF0bk23vn0nU5KNNRidh+i3Au5/2lc=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        /// <summary>
        /// 是否被授权
        /// </summary>
        public static bool IsGrant
        {
            get
            {
                string keystr = null;
                Microsoft.Win32.RegistryKey retkey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", true).OpenSubKey("dacongHotel");
                if (retkey == null || retkey.GetValue("Key") == null)
                {
                    //从文件中读取注册码
                    if (!File.Exists(".\\license.lic"))
                        return false;
                    var f = File.OpenText(".\\license.lic");
                    keystr = f.ReadLine();
                }
                else
                {
                    keystr = retkey.GetValue("Key").ToString();
                }
              if (string.IsNullOrEmpty(keystr))
                  return false;
                try
                {
                    using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
                    {
                        rsa.FromXmlString(pubkey);
                        RSAPKCS1SignatureDeformatter f = new RSAPKCS1SignatureDeformatter(rsa);

                        f.SetHashAlgorithm("SHA1");

                        byte[] key = Convert.FromBase64String(keystr);
                        var cp = hotel.Common.Computer.Instance;
                        SHA1Managed sha = new SHA1Managed();
                        byte[] name = sha.ComputeHash(ASCIIEncoding.ASCII.GetBytes(cp.GetZhuCeMa()));
                        if (f.VerifySignature(name, key))
                            return true;
                        else
                            return false;
                    }
                }
                catch 
                {
                    System.Windows.Forms.MessageBox.Show("注册码验证失败,请重新注册");
                    return false;
                }
            }
        }

        /// <summary>
        /// 试用版
        /// </summary>
        public static bool isTrial
        {
            get
            {
                //正式版时删除System_Init_Date
                if (string.IsNullOrEmpty(XTCSSys.GetXTCS.SystemInitDate))
                    return false;
                try
                {                    
                    string prikey = "<RSAKeyValue><Modulus>wRn06rdK96H5CebdwNDlvy1P7B7n3ODBr+RpnVlYca/sIy+SDgfF3/fB0u/7QlW9vol7ov3rWeL5DNEcIiMISCg+3KjiOpxtJ4TBaqkgda8nspSpG8fkytAUfbObrif7/n0Z/fb+ClcV0DmhTQZgwJDhZqmMuHBmjsyktrDLcN0=</Modulus><Exponent>AQAB</Exponent><P>/ZxvB16oaNFxBnKkzzLhQwePvMof+6Uz9bfONkVvn1Ko15k37fspHiTJ6+qBRkdmBSWAxUWR6/AdkTZnx39zGw==</P><Q>wuuboP8uNHj4OqPpUO1H3MrYz76cfBYvZagIaxUDQL1gnltnU/v08lJmN9ddY3nv/uh+821mqEbgdLr5wV1zZw==</Q><DP>N+b5mlhFKwWvskonBf0jJlFOX8D2F9QshedELzZTCn1/9jYx2+VXBOv7DmOcp08pr8y6e8GADs/Y1J93dMQlLQ==</DP><DQ>YIfeIvU9JK9j/2Z9fZ6FIEs05G05ghpajrWQtbdKTR18Y/BQU79Lp1Y3WSBHF6SjvlHVzSg41s2DC0wttB1bsQ==</DQ><InverseQ>v9Bm1gvXCQ/Et4k+c7qkNuotahARjyBXPEEK5RVk2wE5Efu7TlsCQvOgO7FD0sbjG8Te0ltEAduJ3+Erdc0DVQ==</InverseQ><D>qnqDCpGFwvghyIck8ay7mLulWr2yUAOxm9sZDwypE72PyUSfOnnMTT4LxXGPLvJJkSUJnU11cab73z/iCZVKIfZApkhEmY/PGZDeENN3CyBj2PcxtVQtKaQUNYfdRTvq6ilD6cgCc6ipNIX6+xEUwNnRbYlnhIYdDEEQZDbb1xU=</D></RSAKeyValue>";
                    string r = hotel.Win.Key.Encryption.RSADecrypt(prikey,XTCSSys.GetXTCS.SystemInitDate);
                    if (DateTime.Now - Convert.ToDateTime(r) > new TimeSpan(30, 0, 0, 0, 0))
                    {
                        System.Windows.Forms.MessageBox.Show("系统试用版已经过期,超期30天不能使用，请注册正版!");
                        return false;
                    }
                    else
                        return true;
                }
                catch 
                {
                    System.Windows.Forms.MessageBox.Show("系统试用版已经过期");
                    return false;
                }
            }
        }
    }
}
