﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace hotel.Win.Common
{
   public partial class AboutBox : Form
    {
        public AboutBox()
        {
            InitializeComponent();
            this.Text = String.Format("关于 {0} {0}", AssemblyTitle);
            this.labelProductName.Text = AssemblyProduct;
            this.labelVersion.Text = String.Format("版本 {0}", AssemblyVersion);
            this.labelCopyright.Text = AssemblyCopyright;
            this.labelCompanyName.Text = AssemblyCompany;
            this.textBoxDescription.Text = AssemblyDescription;
            this.lbzcb.Text = GetGrant;
        }

        #region 程序集属性访问器

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }
        
        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion

        public string GetGrant
        {
            get
            {
                if (Grant.IsGrant)
                    return "注册版";
                else if (Grant.isTrial)
                {
                    try
                    {
                        string prikey = "<RSAKeyValue><Modulus>wRn06rdK96H5CebdwNDlvy1P7B7n3ODBr+RpnVlYca/sIy+SDgfF3/fB0u/7QlW9vol7ov3rWeL5DNEcIiMISCg+3KjiOpxtJ4TBaqkgda8nspSpG8fkytAUfbObrif7/n0Z/fb+ClcV0DmhTQZgwJDhZqmMuHBmjsyktrDLcN0=</Modulus><Exponent>AQAB</Exponent><P>/ZxvB16oaNFxBnKkzzLhQwePvMof+6Uz9bfONkVvn1Ko15k37fspHiTJ6+qBRkdmBSWAxUWR6/AdkTZnx39zGw==</P><Q>wuuboP8uNHj4OqPpUO1H3MrYz76cfBYvZagIaxUDQL1gnltnU/v08lJmN9ddY3nv/uh+821mqEbgdLr5wV1zZw==</Q><DP>N+b5mlhFKwWvskonBf0jJlFOX8D2F9QshedELzZTCn1/9jYx2+VXBOv7DmOcp08pr8y6e8GADs/Y1J93dMQlLQ==</DP><DQ>YIfeIvU9JK9j/2Z9fZ6FIEs05G05ghpajrWQtbdKTR18Y/BQU79Lp1Y3WSBHF6SjvlHVzSg41s2DC0wttB1bsQ==</DQ><InverseQ>v9Bm1gvXCQ/Et4k+c7qkNuotahARjyBXPEEK5RVk2wE5Efu7TlsCQvOgO7FD0sbjG8Te0ltEAduJ3+Erdc0DVQ==</InverseQ><D>qnqDCpGFwvghyIck8ay7mLulWr2yUAOxm9sZDwypE72PyUSfOnnMTT4LxXGPLvJJkSUJnU11cab73z/iCZVKIfZApkhEmY/PGZDeENN3CyBj2PcxtVQtKaQUNYfdRTvq6ilD6cgCc6ipNIX6+xEUwNnRbYlnhIYdDEEQZDbb1xU=</D></RSAKeyValue>";
                        string r = hotel.Win.Key.Encryption.RSADecrypt(prikey, XTCSSys.GetXTCS.SystemInitDate);
                        return "试用版 到期日期:" + r;
                    }
                    catch
                    {
                        return "试用版";
                    }
                }
                    
                else
                    return "未知版本";
            }
        }
        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
