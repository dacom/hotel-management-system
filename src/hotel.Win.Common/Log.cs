﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using hotel.Common;
namespace hotel.Win.Common
{
    /// <summary>
    /// 对log4net再次封装
    /// </summary>
    public class Log
    {
        static Log _log;
        log4net.ILog netlog;
        LogMessage logMessage;
        protected Log()
        {
            log4net.Config.XmlConfigurator.Configure();
            netlog = LogManager.Exists("MyLog");
            if (netlog == null)
                throw new Exception("无法从App.congfig中读取log4net的初始化配置");
            logMessage = new LogMessage();
            logMessage.UserName = UserLoginInfo.FullName;
        }
        public static Log Instance
        {
            get
            {
                if (_log == null)
                    _log = new Log();
                return _log;
            }

        }
        public string Message
        {
            get { return logMessage.Message; }
            set { logMessage.Message = value; }
        }

        /// <summary>
        /// 类型，默认值：用户
        /// </summary>
        public string Type
        {
            get { return logMessage.Type; }
            set { logMessage.Type = value; }
        }
        public LogAbstractEnum Abstract
        {
            get { return logMessage.Abstract; }
            set { logMessage.Abstract = value; }
        }

        public void Debug()
        {
            netlog.Debug(logMessage);
        }
        public void Error()
        {
            netlog.Error(logMessage);
        }
        public void Error(Exception exception)
        {
            netlog.Error(logMessage,exception);
        }
        public void Fatal()
        {
            netlog.Fatal(logMessage);
        }
        public void Info()
        {
            netlog.Info(logMessage);
        }
        public void Warn()
        {
            netlog.Warn(logMessage);
        }
    }
}
