﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.Win.Common
{
    /// <summary>
    /// 当前登录用户信息
    /// </summary>
    public static class UserLoginInfo
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public static string UserId { get; set; }
        /// <summary>
        /// 用户中文名称
        /// </summary>
        public static string UserName { get; set; }
        /// <summary>
        /// 用户全名
        /// </summary>
        public static string FullName { get { return UserName + "[" + UserId + "]"; } }
        /// <summary>
        /// 默认的角色名
        /// </summary>
        public static EnumRoole DefaultRoleName { get; set; }

        /// <summary>
        /// 当前用户所拥有角色
        /// </summary>
        public static List<string> Roles { get; set; }

        /// <summary>
        /// 是否有指定的权限代码
        /// </summary>
        /// <param name="PrivilegeId"></param>
        /// <returns></returns>
        public static bool HavePrivilege(string PrivilegeId)
        {
            return true;//没启用
        }

        /// <summary>
        /// 是否有指定的角色代码
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public static bool HaveRole(EnumRoole roleId)
        {
            if (Roles == null)
                return false;
            //管理员默认有权限
            if (Roles.Contains(EnumRoole.Admin.ToString()))
                return true;
            //注意大小写            
            return Roles.Contains(roleId.ToString());
        }
    }
}
