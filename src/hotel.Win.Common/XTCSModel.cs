﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hotel.DAL;
using hotel.Common;
namespace hotel.Win.Common
{
    /// <summary>
    /// 系统参数模型类--常规
    /// </summary>
    public partial class XTCSModel
    {
        static XTCSModel _xtcs;
        public static XTCSModel GetXTCS
        {
            get
            {
                if (_xtcs == null)
                {
                    _xtcs = new XTCSModel();
                }
                return _xtcs;
            }
            set { _xtcs = null; }
        }
        public XTCSModel()
        {
            hotelEntities _context = MyDataContext.GetDataContext;
            var qry = _context.XTCS.Where(p => p.FL == "常规");
            foreach (var item in qry)
            {
                #region 所有属性赋值
                switch (item.CSDM)
                {
                    case "YYRQ":
                        if (Utility.IsDate(item.CSZ))
                            YYRQ = Convert.ToDateTime(item.CSZ).ToString("yyyy-MM-dd");
                        else
                            YYRQ = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        break;
                    case "BFWJLJ":
                        BFWJLJ = item.CSZ;
                        break;
                    case "SF_ZDYS":
                        SF_ZDYS = item.CSZ == "1";
                        break;
                    case "SFDY_YYD":
                        SFDY_YYD = item.CSZ == "1";
                        break;
                    case "ZDYS_IP":
                        ZDYS_IP = item.CSZ;
                        break;
                    case "ZDYS_SJ":
                        ZDYS_SJ = item.CSZ;
                        break;
                    case "JDMC":
                        JDMC = item.CSZ;
                        break;
                    case "JDDH":
                        JDDH = item.CSZ;
                        break;
                    case "JBTFZ_SJ":
                        JBTFZ_SJ = item.CSZ;
                        break;
                    case "JYTFZ_SJ":
                        JYTFZ_SJ = item.CSZ;
                        break;
                    case "SF_ZDBF":
                        SF_ZDBF = item.CSZ == "1";
                        break;
                    case "ZDBF_IP":
                        ZDBF_IP = item.CSZ;
                        break;
                    case "ZDBF_GZ":
                        ZDBF_GZ = item.CSZ;
                        break;
                    default:
                        break;
                }
                #endregion
            }
        }
        public void Reset()
        {
            _xtcs = null;
        }
        /// <summary>
        /// 营业日期
        /// </summary>
        public string YYRQ { get; set; }
        /// <summary>
        /// 备份文件路径
        /// </summary>
        public string BFWJLJ { get; set; }
        /// <summary>
        /// 是否自动夜审
        /// </summary>
        public bool SF_ZDYS { get; set; }
        /// <summary>
        /// 是否打印押金单
        /// </summary>
        public bool SFDY_YYD { get; set; }
        /// <summary>
        /// 自动夜审IP
        /// </summary>
        public string ZDYS_IP { get; set; }
        /// <summary>
        /// 自动夜审时间
        /// </summary>
        public string ZDYS_SJ { get; set; }
        /// <summary>
        /// 酒店名称
        /// </summary>
        public string JDMC { get; set; }
        /// <summary>
        /// 酒店电话
        /// </summary>
        public string JDDH { get; set; }
        /// <summary>
        /// 加半天房租时间
        /// </summary>
        public string JBTFZ_SJ { get; set; }
        /// <summary>
        /// 加一天房租时间
        /// </summary>
        public string JYTFZ_SJ { get; set; }
        /// <summary>
        /// 是否自动备份
        /// </summary>
        public bool SF_ZDBF { get; set; }
        /// <summary>
        /// 自动备份电脑IP
        /// </summary>
        public string ZDBF_IP { get; set; }
        /// <summary>
        /// 自动备份规则
        /// </summary>
        public string ZDBF_GZ { get; set; }

    }

}
