﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.Win.Common
{
    /// <summary>
    /// 房态中文名
    /// </summary>
    public static class RoomStstusCnName
    {
        public static string SanKe { get { return "散客房"; } }
        public static string TuanDui { get { return "团队房"; } }
        public static string ZhongDian { get { return "钟点房"; } }
        public static string ChangBao { get { return "长包房"; } }
        public static string ZiYong { get { return "自用房"; } }
        public static string MianFei { get { return "免费房"; } }
        public static string Vip { get { return "VIP房"; } }
        public static string BaoMi { get { return "保密房"; } }
        public static string Qingli { get { return "清理房"; } }
        public static string SuoDing { get { return "锁定房"; } }
        public static string MaoBing { get { return "毛病房"; } }
        public static string YuDing { get { return "预订房"; } }
        public static string LiKai { get { return "自用房"; } }
        public static string KongJing { get { return "空净房"; } }
        public static string KongZang { get { return "空脏房"; } }
        public static string WeiXiu { get { return "维修房"; } }
        /// <summary>
        /// 接待可用的所有房态
        /// </summary>
        public static List<string> GetJeiDaiAvailables
        {
            get
            {
                return new List<string>() { SanKe, TuanDui, ZhongDian, ChangBao, MianFei, Vip, ZiYong, BaoMi };
            }
        }
    }
}
