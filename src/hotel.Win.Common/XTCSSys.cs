﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hotel.DAL;
using hotel.Common;
namespace hotel.Win.Common
{
    /// <summary>
    /// 系统参数模型类--系统
    /// </summary>
    public class XTCSSys
    {
        static XTCSSys _xtcs;
        public static XTCSSys GetXTCS
        {
            get
            {
                if (_xtcs == null)
                {
                    _xtcs = new XTCSSys();
                }
                return _xtcs;
            }
            set { _xtcs = null; }
        }
        public XTCSSys()
        {
            hotelEntities _context = MyDataContext.GetDataContext;
            try
            {
                var qry = _context.XTCS.Where(p => p.FL == "系统");
                foreach (var item in qry)
                {
                    #region 所有属性赋值
                    switch (item.CSDM)
                    {
                        case "System_Init_Date":
                            SystemInitDate = item.CSZ;
                            break;
                        default:
                            break;
                    }
                    #endregion
                }
            }
            catch (System.Data.Entity.Core.EntityException ex)
            {
                throw new Exception("请检查数据库是已经打开,错误" + ex.Message + ex.InnerException != null ? ex.InnerException.Message : "");
            }
           
        }
        /// <summary>
        /// 系统初始日期--加密的
        /// </summary>
        public string SystemInitDate { get; set; }
        
    }
}
