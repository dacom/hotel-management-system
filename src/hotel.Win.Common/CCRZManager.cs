﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hotel.Common;

namespace hotel.Win.Common
{
    /// <summary>
    /// 操作日志管理
    /// </summary>
    public class CCRZManager
    {
        public static void Add(string summary,string message,string roomId=null,string rzid=null,string type="一般操作")
        {
            using (var _context = MyDataContext.GetDataContext)
            {
                var ccrz = _context.CCRZ.Create();
                ccrz.IP = Computer.Instance.IpAddress;
                ccrz.MAC = Computer.Instance.MacAddress;
                ccrz.Message = message;
                ccrz.ROOM_ID = roomId;
                ccrz.RZID = rzid;
                ccrz.Type = type;
                ccrz.USERNAME = UserLoginInfo.FullName;
                ccrz.RQ = DateTime.Now;
                ccrz.Abstract = summary;
                _context.CCRZ.Add(ccrz);
                _context.SaveChanges();
            }
        }
        public static void Add(CcrzInfo info)
        {
            if (info == null ||string.IsNullOrEmpty(info.Message))
                return;
            using (var _context = MyDataContext.GetDataContext)
            {
                var ccrz = _context.CCRZ.Create();
                ccrz.IP = Computer.Instance.IpAddress;
                ccrz.MAC = Computer.Instance.MacAddress;
                ccrz.Message = info.Message;
                ccrz.ROOM_ID = info.RoomId;
                ccrz.RZID = info.Rzid;
                ccrz.Type = info.Type.ToString();
                ccrz.USERNAME = UserLoginInfo.FullName;
                ccrz.RQ = DateTime.Now;
                ccrz.Abstract = info.Abstract.ToString();
                _context.CCRZ.Add(ccrz);
                _context.SaveChanges();
            }
        }
    }
    public enum LogTypeEnum { 一般操作, 涉及金额 };
    public class CcrzInfo
    {
        public CcrzInfo()
        {
            Type = LogTypeEnum.涉及金额;
        }
        public string RoomId { get; set; }
        public string Rzid { get; set; }
        public string Message { get; set; }
        public LogTypeEnum Type { get; set; }
        public LogAbstractEnum Abstract { get; set; }
        public void Save()
        {
            CCRZManager.Add(this);
        }
    }
}
