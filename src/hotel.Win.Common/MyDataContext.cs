﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using hotel.DAL;
namespace hotel.Win.Common
{
    /// <summary>
    /// 我的数据源
    /// </summary>
    public class MyDataContext
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["hotelEntities"].ConnectionString;
        /// <summary>
        /// 获得数据源
        /// </summary>
        public static hotelEntities GetDataContext
        {
            get
            {
                return new hotelEntities();
            }
        }       
    }
}
