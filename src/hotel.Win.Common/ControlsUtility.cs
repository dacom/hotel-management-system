﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;
namespace hotel.Win.Common
{
    /// <summary>
    /// 常用控件工具类
    /// </summary>
    public class ControlsUtility
    {
        /// <summary>
        /// 按列的名称获得值，适合行或列选择
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <returns></returns>
        public static string GetActiveGridValue(DataGridView grid, string columnName)
        {
            object obj = null;
            if (grid.CurrentCell != null)
            {
                obj = grid.Rows[grid.CurrentCell.RowIndex].Cells[columnName].Value;
                return obj == null ? "" : obj.ToString();
            }

            else if (grid.CurrentRow != null)
            {
                obj = grid.CurrentRow.Cells[columnName].Value;
                return obj == null ? "" : obj.ToString();
            }

            else
                return "";
        }

        public static string FindParentFormName(DataGridView grid)
        {
            Control parent = grid.Parent;
            while ((parent.Parent != null) && !string.IsNullOrEmpty(parent.Parent.Name))
            {
                parent = parent.Parent;
            }
            return parent.Name;
        }

        public static Control FindForm(Control control)
        {
            Control parent = control.Parent;
            while ((parent.Parent != null) && !string.IsNullOrEmpty(parent.Parent.Name))
            {
                parent = parent.Parent;
            }
            return parent;
        }

        public static void ResetColumnFromXML(DataGridView grid)
        {
            string path = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\\")) + "\\DGVFormatFiles\\";
            grid.Columns.Clear();
            XmlDocument love = new XmlDocument();
            love.Load(path + @"\" + FindParentFormName(grid) + "." + grid.Name + ".xml");
            XmlNodeList list = love.DocumentElement.SelectNodes("Column");
            foreach (XmlNode node in list)
            {
                DataGridViewColumn column;
                switch (node.Attributes["ColumnType"].Value)
                {
                    case "System.Windows.Forms.DataGridViewButtonColumn":
                        column = new DataGridViewButtonColumn();
                        break;

                    case "System.Windows.Forms.DataGridViewCheckBoxColumn":
                        column = new DataGridViewCheckBoxColumn();
                        break;

                    case "System.Windows.Forms.DataGridViewComboBoxColumn":
                        column = new DataGridViewComboBoxColumn();
                        break;

                    case "System.Windows.Forms.DataGridViewIamgeColumn":
                        column = new DataGridViewImageColumn();
                        break;

                    case "System.Windows.Forms.DataGridViewLinkColumn":
                        column = new DataGridViewLinkColumn();
                        break;
                    default:
                        column = new DataGridViewTextBoxColumn();
                        break;
                }
                column.Name = node.Attributes["Name"].Value;
                column.HeaderText = node.Attributes["HeaderText"].Value;
                column.DataPropertyName = node.Attributes["DataPropertyName"].Value;
                column.DefaultCellStyle.Format = node.Attributes["Format"].Value;
                column.DisplayIndex = int.Parse(node.Attributes["DisplayIndex"].Value);
                column.Width = int.Parse(node.Attributes["Width"].Value);
                column.ReadOnly = bool.Parse(node.Attributes["ReadOnly"].Value);
                column.Visible = bool.Parse(node.Attributes["Visible"].Value);
                grid.Columns.Add(column);
            }
        }
    }
}
