﻿namespace hotel.Win.Common
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtzcm = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtyzm = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.btncopy1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtzcm
            // 
            this.txtzcm.Location = new System.Drawing.Point(70, 50);
            this.txtzcm.Multiline = true;
            this.txtzcm.Name = "txtzcm";
            this.txtzcm.Size = new System.Drawing.Size(359, 70);
            this.txtzcm.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "注册码：";
            // 
            // txtyzm
            // 
            this.txtyzm.Location = new System.Drawing.Point(70, 12);
            this.txtyzm.Name = "txtyzm";
            this.txtyzm.Size = new System.Drawing.Size(307, 21);
            this.txtyzm.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "验证码：";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(289, 126);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 35);
            this.button2.TabIndex = 9;
            this.button2.Text = "退出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(192, 126);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 35);
            this.btnok.TabIndex = 8;
            this.btnok.Text = "注册";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // btncopy1
            // 
            this.btncopy1.Location = new System.Drawing.Point(383, 12);
            this.btncopy1.Name = "btncopy1";
            this.btncopy1.Size = new System.Drawing.Size(46, 21);
            this.btncopy1.TabIndex = 8;
            this.btncopy1.Text = "复制";
            this.btncopy1.UseVisualStyleBackColor = true;
            this.btncopy1.Click += new System.EventHandler(this.btncopy1_Click);
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 173);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btncopy1);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.txtzcm);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtyzm);
            this.Controls.Add(this.label1);
            this.Name = "RegisterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "软件注册";
            this.Load += new System.EventHandler(this.RegisterForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtzcm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtyzm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Button btncopy1;
    }
}