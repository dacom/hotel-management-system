//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace hotel.DAL
{
    public partial class ROOM_MAINTAIN
    {
        public int ID { get; set; }
        public string ROOM_ID { get; set; }
        public string WXNR { get; set; }
        public string WCBZ { get; set; }
        public string SQRY { get; set; }
        public Nullable<System.DateTime> SQRQ { get; set; }
        public string WXRY { get; set; }
        public Nullable<System.DateTime> WXRQ { get; set; }
        public string BZSM { get; set; }
    
        public virtual ROOM ROOM { get; set; }
    }
    
}
