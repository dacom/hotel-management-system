//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace hotel.DAL
{
    public partial class CCRZ
    {
        public int ID { get; set; }
        public string RZID { get; set; }
        public string ROOM_ID { get; set; }
        public string USERNAME { get; set; }
        public string IP { get; set; }
        public string MAC { get; set; }
        public string Type { get; set; }
        public string Abstract { get; set; }
        public string Message { get; set; }
        public Nullable<System.DateTime> RQ { get; set; }
    }
    
}
