﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using hotel.Common;
namespace hotel.DAL
{
    public class RoomTypeDal
    {
        /// <summary>
        /// 所有房间类型
        /// </summary>
        /// <returns></returns>
        public static List<ROOM_TYPE> GetAll()
        {
            return new hotelEntities().ROOM_TYPE.OrderBy(p => p.XH).ToList();
        }
        /// <summary>
        /// 所有房间类型-增加一个"全部..."过滤项目
        /// </summary>
        /// <returns></returns>
        public static List<ROOM_TYPE> GetAll2()
        {
            List<ROOM_TYPE> lst = new hotelEntities().ROOM_TYPE.OrderBy(p => p.XH).ToList();
            lst.Insert(0,new ROOM_TYPE(){ ID="000", MC = SysConsts.AllItem});
            return lst ;
        }
    }
}
