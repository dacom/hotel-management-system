﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.DAL
{
    //特殊的处理
    public partial class hotelEntities
    {
        public string GetAdminId()
        {
            var query = this.Database.SqlQuery<string>("select [dbo].[GetAdminId]()");
            return query.FirstOrDefault();
        }
        /// <summary>
        /// 获取下一个联房号
        /// </summary>
        /// <returns></returns>
        public string GetLfh()
        {
            var query = this.Database.SqlQuery<string>("select [dbo].[GetLfh]()");
            return query.FirstOrDefault();
        }
    }
}
