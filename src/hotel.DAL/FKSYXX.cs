//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace hotel.DAL
{
    public partial class FKSYXX
    {
        public int ID { get; set; }
        public string RZJL_ID { get; set; }
        public string KH { get; set; }
        public System.DateTime KSSJ { get; set; }
        public System.DateTime JSSJ { get; set; }
        public string CJRY { get; set; }
        public System.DateTime CJRQ { get; set; }
        public string BZSM { get; set; }
        public string SFGH { get; set; }
    
        public virtual RZJL RZJL { get; set; }
    }
    
}
