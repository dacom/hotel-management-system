﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.DAL
{
    public class XTCYDMDal
    {
        /// <summary>
        /// 指定类别下所有项目数据
        /// </summary>
        /// <param name="lbdm"></param>
        /// <returns></returns>
        public static List<XTCYDM> GetAllByLBDM(string lbdm)
        {
            return new hotelEntities().XTCYDM.Where(p=>p.LBDM == lbdm).OrderBy(p=>p.XH).ToList();
        }
    }
}
