//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace hotel.DAL
{
    public partial class RZJL_ARC
    {
        public RZJL_ARC()
        {
            this.RZRY_ARC = new HashSet<RZRY_ARC>();
            this.XFJL_ARC = new HashSet<XFJL_ARC>();
        }
    
        public string ID { get; set; }
        public string F_ID { get; set; }
        public Nullable<decimal> SKFJ { get; set; }
        public Nullable<decimal> ZK { get; set; }
        public Nullable<decimal> SJFJ { get; set; }
        public Nullable<int> YJ { get; set; }
        public string ZFFS { get; set; }
        public Nullable<int> RS { get; set; }
        public Nullable<System.DateTime> DDRQ { get; set; }
        public Nullable<int> TS { get; set; }
        public Nullable<System.DateTime> LDRQ { get; set; }
        public string BMBZ { get; set; }
        public string KRLB { get; set; }
        public string BZSM { get; set; }
        public string XYDW_DM { get; set; }
        public string XYDW_MC { get; set; }
        public string TDMC { get; set; }
        public string TDDM { get; set; }
        public string ROOM_ID { get; set; }
        public string YDBZ { get; set; }
        public string XGRY { get; set; }
        public Nullable<System.DateTime> XGRQ { get; set; }
        public string CJRY { get; set; }
        public Nullable<System.DateTime> CJRQ { get; set; }
        public Nullable<System.DateTime> JZRQ { get; set; }
        public string JZRY { get; set; }
        public string JZBZ { get; set; }
        public string LFH { get; set; }
        public string XYJG_MC { get; set; }
    
        public virtual ICollection<RZRY_ARC> RZRY_ARC { get; set; }
        public virtual ICollection<XFJL_ARC> XFJL_ARC { get; set; }
    }
    
}
