//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace hotel.DAL
{
    public partial class KSDA
    {
        public int ID { get; set; }
        public string ZJDM { get; set; }
        public string ZJLX { get; set; }
        public string KRXM { get; set; }
        public string KRXB { get; set; }
        public string DZ { get; set; }
        public string DW { get; set; }
        public string LXDH { get; set; }
        public string KRZT { get; set; }
        public string GBKH { get; set; }
        public string GJ { get; set; }
        public string SF { get; set; }
        public string BZSM { get; set; }
        public string CJRY { get; set; }
        public Nullable<System.DateTime> CJRQ { get; set; }
    }
    
}
