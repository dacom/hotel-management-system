﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using hotel.DAL;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.Objects.SqlClient;
using hotel.Win.Common;
using hotel.Common;
using System.Security.Cryptography;

namespace TestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void test3()
        {
            var str = @"c:\ddd\\\";
            var path=  System.IO.Path.Combine(str) + "hotel" + DateTime.Now.ToString("yyyyMMddHHmm") + ".bak" ;
            Assert.Inconclusive(path);
        }
        [TestMethod]
        public void test2()
        {
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.打印;
            logInfo.RoomId = "0";
            logInfo.Rzid = "99";
            logInfo.Message = "打印人员登记表";
            logInfo.Save();
        }
        [TestMethod]
        public void TestMethod2()
        {
            XTCYDMDal.GetAllByLBDM("1");

            using (var _context = new hotelEntities())
            {
                //Assert.Inconclusive(_context.GetRZXH().Single());
                /*var ywmc ="F";
               var qry = _context.XFXM.Where(p => p.YWMC.Contains(ywmc));
                Assert.Inconclusive(qry.Count().ToString());
                */
               // var rzjlmodel = _context.RZJL.Find("A201402080030");
                //Assert.Inconclusive(rzjlmodel.ROOM.ID + rzjlmodel.ROOM.ROOM_MAINTAIN.Count.ToString());                    
               Assert.Inconclusive( _context.GetLfh());
            }
            
        }
        [TestMethod]
        public void TestMethod1()
        {
            using (var _context = new hotelEntities())
            {
                var rq = DateTime.Now.Date;
                var query = from q in _context.UV_RZJL_RZRY_Single
                             //where EntityFunctions.TruncateTime(q.LDRQ) >=rq
                             where SqlFunctions.DateDiff("day",rq,q.LDRQ)>0
                             select q;
                Assert.Inconclusive(query.Count().ToString());              
                
            }    
        }
        [TestMethod]
        public void TestMethodIDCard()
        {
            var id = Utility.CreateIDCard();
            Assert.IsNotNull(id);
            var d = id.ReadCardInfo();
            Assert.IsNotNull(d);
        }
        [TestMethod]
        public void T1()
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

            //生成公钥XML字符串  
            string publicKeyXmlString = rsa.ToXmlString(false);
            
            //生成私钥XML字符串  
            string privateKeyXmlString = rsa.ToXmlString(true);
            Assert.Inconclusive(publicKeyXmlString + "," +privateKeyXmlString);
        }
    }
}
