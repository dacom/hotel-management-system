﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace hotel.MyUserControl
{
    public partial class NoModalDialog : Form
    {
        public NoModalDialog(string title, string message)
        {
            InitializeComponent();
            this.Text = title;
            textBoxDescription.Text = message;
        }

        public NoModalDialog(string message)
            : this("系统消息", message)
        {
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
