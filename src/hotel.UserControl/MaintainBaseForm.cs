﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace hotel.MyUserControl
{
    /// <summary>
    /// 维护窗体基类
    /// </summary>
    public partial class MaintainBaseForm : Form
    {
        #region 代理方法声明
        protected Func<bool> OnAdd;
        protected Func<bool> OnDelete;
        protected Func<bool> OnPost;
        protected Func<bool> OnClose;
        /// <summary>
        /// 当窗体关闭时，回调方法,bool表示是否有数据更改
        /// </summary>
        public Action<bool> CallBack;
        #endregion

        public MaintainBaseForm()
        {
            InitializeComponent();
            IsDataChange = false;
            IsEdit = false;
        }

        #region 属性
        /// <summary>
        /// 是否在编辑状态
        /// </summary>
        protected bool IsEdit { get; set; }

        /// <summary>
        /// 是否有数据更改
        /// </summary>
        protected bool IsDataChange { get; set; }

        protected string StatusMessage
        {
            get { return statusStrip.Items[0].Text; }
            set { statusStrip.Items[0].Text = value; }
        }

        #region button状态控制
        protected bool PostButtonEnabled
        {
            get { return btnPost.Enabled; }
            set { btnPost.Enabled = value; }
        }
        protected bool AddButtonEnabled
        {
            get { return btnAdd.Enabled; }
            set { btnAdd.Enabled = value; }
        }
        protected bool DeleteButtonEnabled
        {
            get { return btnDel.Enabled; }
            set { btnDel.Enabled = value; }
        #endregion
        }
        #endregion

        #region 公共方法

        #endregion


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (OnAdd != null && !OnAdd())
                return;
            btnDel.Enabled = false;
            IsEdit = false;
            btnPost.Enabled = true;
            statusStrip.Items[0].Text = "数据处于增加状态";
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("你确定删除当前数据吗?","系统消息",MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;
            if (OnDelete != null && !OnDelete())
                return;
            btnPost.Enabled = false;
            IsDataChange = true;
            statusStrip.Items[0].Text = "数据已经删除，你可以继续增加";
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            if (OnPost != null && !OnPost())
                return;
            IsEdit = true;
            btnDel.Enabled = true;
            IsDataChange = true;
            statusStrip.Items[0].Text = "数据保持成功，目前处于修改状态";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MaintainBaseForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (OnClose != null)
                OnClose();
            if (CallBack != null)
                CallBack(IsDataChange);
        }


    }
}
