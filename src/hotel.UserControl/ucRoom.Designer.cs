﻿namespace hotel.MyUserControl
{
    partial class ucRoom
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucRoom));
            this.lbid = new System.Windows.Forms.Label();
            this.lb_rzzt = new System.Windows.Forms.Label();
            this.lb_ydzt = new System.Windows.Forms.Label();
            this.lb_lkzt = new System.Windows.Forms.Label();
            this.lbfl = new System.Windows.Forms.Label();
            this.lbfjwt = new System.Windows.Forms.Label();
            this.lbfjms = new System.Windows.Forms.Label();
            this.lblfbz = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbid
            // 
            this.lbid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbid.AutoSize = true;
            this.lbid.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbid.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbid.Location = new System.Drawing.Point(18, 17);
            this.lbid.Margin = new System.Windows.Forms.Padding(0);
            this.lbid.Name = "lbid";
            this.lbid.Size = new System.Drawing.Size(44, 16);
            this.lbid.TabIndex = 11;
            this.lbid.Text = "8888";
            this.lbid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbid.Click += new System.EventHandler(this.ucRoom_Click);
            this.lbid.DragDrop += new System.Windows.Forms.DragEventHandler(this.ucRoom_DragDrop);
            this.lbid.DragOver += new System.Windows.Forms.DragEventHandler(this.ucRoom_DragOver);
            this.lbid.DoubleClick += new System.EventHandler(this.ucRoom_DoubleClick);
            this.lbid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ucRoom_MouseDown);
            this.lbid.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ucRoom_MouseMove);
            this.lbid.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ucRoom_MouseUp);
            // 
            // lb_rzzt
            // 
            this.lb_rzzt.Image = ((System.Drawing.Image)(resources.GetObject("lb_rzzt.Image")));
            this.lb_rzzt.Location = new System.Drawing.Point(2, 2);
            this.lb_rzzt.Margin = new System.Windows.Forms.Padding(0);
            this.lb_rzzt.Name = "lb_rzzt";
            this.lb_rzzt.Size = new System.Drawing.Size(16, 16);
            this.lb_rzzt.TabIndex = 16;
            this.lb_rzzt.DoubleClick += new System.EventHandler(this.ucRoom_DoubleClick);
            // 
            // lb_ydzt
            // 
            this.lb_ydzt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_ydzt.Image = ((System.Drawing.Image)(resources.GetObject("lb_ydzt.Image")));
            this.lb_ydzt.Location = new System.Drawing.Point(2, 47);
            this.lb_ydzt.Margin = new System.Windows.Forms.Padding(0);
            this.lb_ydzt.Name = "lb_ydzt";
            this.lb_ydzt.Size = new System.Drawing.Size(16, 16);
            this.lb_ydzt.TabIndex = 17;
            // 
            // lb_lkzt
            // 
            this.lb_lkzt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_lkzt.Image = ((System.Drawing.Image)(resources.GetObject("lb_lkzt.Image")));
            this.lb_lkzt.Location = new System.Drawing.Point(41, 47);
            this.lb_lkzt.Margin = new System.Windows.Forms.Padding(0);
            this.lb_lkzt.Name = "lb_lkzt";
            this.lb_lkzt.Size = new System.Drawing.Size(16, 16);
            this.lb_lkzt.TabIndex = 18;
            // 
            // lbfl
            // 
            this.lbfl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbfl.AutoSize = true;
            this.lbfl.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfl.Location = new System.Drawing.Point(55, 4);
            this.lbfl.Margin = new System.Windows.Forms.Padding(0);
            this.lbfl.Name = "lbfl";
            this.lbfl.Size = new System.Drawing.Size(17, 11);
            this.lbfl.TabIndex = 19;
            this.lbfl.Text = "BS";
            // 
            // lbfjwt
            // 
            this.lbfjwt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbfjwt.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfjwt.Location = new System.Drawing.Point(57, 52);
            this.lbfjwt.Margin = new System.Windows.Forms.Padding(0);
            this.lbfjwt.Name = "lbfjwt";
            this.lbfjwt.Size = new System.Drawing.Size(17, 11);
            this.lbfjwt.TabIndex = 20;
            this.lbfjwt.Text = "??";
            this.lbfjwt.Click += new System.EventHandler(this.lbfjwt_Click);
            // 
            // lbfjms
            // 
            this.lbfjms.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbfjms.Font = new System.Drawing.Font("SimSun", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfjms.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbfjms.Location = new System.Drawing.Point(4, 35);
            this.lbfjms.Margin = new System.Windows.Forms.Padding(0);
            this.lbfjms.Name = "lbfjms";
            this.lbfjms.Size = new System.Drawing.Size(70, 16);
            this.lbfjms.TabIndex = 21;
            this.lbfjms.Text = "标双";
            this.lbfjms.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbfjms.Click += new System.EventHandler(this.ucRoom_Click);
            this.lbfjms.DragDrop += new System.Windows.Forms.DragEventHandler(this.ucRoom_DragDrop);
            this.lbfjms.DragOver += new System.Windows.Forms.DragEventHandler(this.ucRoom_DragOver);
            this.lbfjms.DoubleClick += new System.EventHandler(this.ucRoom_DoubleClick);
            // 
            // lblfbz
            // 
            this.lblfbz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblfbz.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblfbz.Location = new System.Drawing.Point(22, 4);
            this.lblfbz.Margin = new System.Windows.Forms.Padding(0);
            this.lblfbz.Name = "lblfbz";
            this.lblfbz.Size = new System.Drawing.Size(29, 13);
            this.lblfbz.TabIndex = 22;
            this.lblfbz.Text = "L1";
            // 
            // ucRoom
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Pink;
            this.Controls.Add(this.lblfbz);
            this.Controls.Add(this.lbfjms);
            this.Controls.Add(this.lbfjwt);
            this.Controls.Add(this.lbfl);
            this.Controls.Add(this.lb_lkzt);
            this.Controls.Add(this.lb_ydzt);
            this.Controls.Add(this.lb_rzzt);
            this.Controls.Add(this.lbid);
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "ucRoom";
            this.Size = new System.Drawing.Size(77, 66);
            this.Click += new System.EventHandler(this.ucRoom_Click);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.ucRoom_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.ucRoom_DragOver);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ucRoom_Paint);
            this.DoubleClick += new System.EventHandler(this.ucRoom_DoubleClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ucRoom_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ucRoom_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ucRoom_MouseUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbid;
        private System.Windows.Forms.Label lb_rzzt;
        private System.Windows.Forms.Label lb_ydzt;
        private System.Windows.Forms.Label lb_lkzt;
        private System.Windows.Forms.Label lbfl;
        private System.Windows.Forms.Label lbfjwt;
        private System.Windows.Forms.Label lbfjms;
        private System.Windows.Forms.Label lblfbz;


    }
}
