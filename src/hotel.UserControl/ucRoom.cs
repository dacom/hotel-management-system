﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Common;
using hotel.DAL;
using System.Data.Objects;
namespace hotel.MyUserControl
{
    /// <summary>
    /// 房态,注意顺序
    /// </summary>
    public enum RoomStstusEnum { SanKe, TuanDui, ZhongDian, ChangBao, ZiYong, MianFei, Vip, BaoMi, LiDian, Qingli, SuoDing, MaoBing,YuDing, LiKai }
    /// <summary>
    /// 房间用户控件
    /// </summary>
    public partial class ucRoom : UserControl
    {
        private static ImageList imgList;
        private Rectangle dragBoxFromMouseDown;
        private HoverBalloon m_hb = new HoverBalloon();
        public ROOM Room { get; private set; }
        /// <summary>
        /// 联房号
        /// </summary>
        public string LFH { get; set; }
        /// <summary>
        /// 双击事件回调
        /// </summary>
        public Action<ucRoom> DoubleClickCallBack;
        /// <summary>
        /// 单击事件
        /// </summary>
        public Action<ucRoom> ClickCallBack;
        /// <summary>
        /// 拖动正常接收后回调
        /// </summary>
        public Action<string, string> DragDropCallBack;

        /// <summary>
        /// 是否选择
        /// </summary>
        public bool Selected { get; set; }
        /// <summary>
        /// 是否入住
        /// </summary>
        public bool IsRuZhu { get; set; }
        /// <summary>
        /// 房号标签字体
        /// </summary>
        public Font LabelRoomFont
        {
            get { return lbid.Font; }
            set { lbid.Font = value; }
        }
        #region 共用属性

        //房间最大Width，Height
        public static int MaxWidth = 54;
        public static int MaxHeight = 58;
        #endregion
        public ucRoom(ROOM room)
        {
            InitializeComponent();
            if (room == null)
                return;
            IsRuZhu = false;
            this.SuspendLayout();
            if (imgList == null)
                imgList = RoomStatusImage.GetImageList;
            lb_lkzt.ImageList = imgList;
            lb_rzzt.ImageList = imgList;
            lb_ydzt.ImageList = imgList;
            lb_ydzt.ImageIndex = (int)RoomStstusEnum.YuDing;
            lb_lkzt.ImageIndex = (int)RoomStstusEnum.LiKai;           
            lbfl.Text = room.ROOM_TYPE.ID;
            lbfjms.Text = room.ROOM_TYPE.JC;
            this.Room = room;
            RefreshRoom();
            this.ResumeLayout();
        }

        public override string ToString()
        {
            return Room.ID;
        }
        
        /// <summary>
        /// 更新房间显现信息
        /// </summary>
        public void RefreshRoom()
        {
            var _context = new hotelEntities();
            lbid.Text = Room.ID;           
            
            lb_lkzt.Visible = false; //现在不启用
            this.ForeColor = RoomStatusColor.ForeColor;
            //lb_rzzt.Visible = Room.RZBZ == "是";
            
            lb_rzzt.ImageIndex = GetImageIndex(Room.ZT);
            bool hasZT = false; //有房态 用来处理预订
            bool hasrzzt = false;//是否显示入住状态，因为多线程lb_rzzt.Visible 只能修改一次
            bool hasydbz = _context.RZJL.AsNoTracking().Count(p => p.ROOM_ID == Room.ID & p.YDBZ == "是") > 0;//预定标志
           
            //主房态颜色
            switch (Room.ZT)
            {
                case "空净房":
                    this.BackColor = RoomStatusColor.KongJinFang;
                    hasrzzt = false;
                    break;
                //case "预订房":
                //    this.BackColor = RoomStatusColor.YuDingFang;
                //    break;
                case "空脏房":
                    this.BackColor = RoomStatusColor.KongZangFang;
                    hasrzzt = false;
                    hasZT = true;
                    break;
                case "维修房":
                    this.BackColor = RoomStatusColor.WeiXiuFang;
                    hasrzzt = false;
                    hasZT = true;
                    break;
                case "锁定房":
                    this.BackColor = RoomStatusColor.KongJinFang;//空净房一致
                    hasrzzt = true;
                    break;
                case "毛病房":
                    this.BackColor = RoomStatusColor.KongJinFang;//空净房一致
                    hasrzzt = true;
                    break;
                case "清理房":
                    this.BackColor = RoomStatusColor.KongJinFang;//空净房一致
                    hasrzzt = true;
                    break;
                case "离店房":
                    this.BackColor = RoomStatusColor.KongJinFang;//空净房一致      
                    hasrzzt = true;
                    break;
            }
            lbfjms.Text = Room.ROOM_TYPE.JC;
            //显示联房号
            bool hasLfbz = false;
            //住房，优先级高
            if (Room.RZBZ == "是")
            {
                hasrzzt = true;
                this.BackColor = RoomStatusColor.ZhuFang;
                hasZT = true;
                IsRuZhu = true;
                var rzryModel = _context.UV_RZJL_RZRY_Single.AsNoTracking().FirstOrDefault(p => p.ROOM_ID == Room.ID && p.RZBZ == "是");
                if (rzryModel != null)
                {
                    lbfjms.Text = rzryModel.KRXM;
                    LFH = rzryModel.LFH;
                    if (!string.IsNullOrEmpty(LFH))
                    {
                        if (string.IsNullOrEmpty(rzryModel.F_ID))
                            lblfbz.Text = LFH + "(主)";
                        else
                            lblfbz.Text = LFH;
                        hasLfbz = true;
                    }
                }
            }


            //预订房,纯预订房，才显示颜色
            if (!hasZT && hasydbz)
            {
                this.BackColor = RoomStatusColor.YuDingFang;
                hasrzzt = false;
                hasydbz = false;
            }

            lb_rzzt.Visible = hasrzzt;
            lb_ydzt.Visible = hasydbz;
            //房间问题lbfjwt
            //Room.ROOM_MAINTAIN.Load();
            lbfjwt.Visible = _context.ROOM_MAINTAIN.AsNoTracking().Count(p => p.ROOM_ID == Room.ID && p.WCBZ == "否") > 0;
            
            if (Room.RZBZ=="是")
            {
               
            }
            lblfbz.Visible = hasLfbz;
            DrawSelectedRegion();
            _context.Dispose();
        }

        #region 拖动
        private void ucRoom_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(System.String)))
            {

                string sourceNo = (string)e.Data.GetData(typeof(System.String));
                ucRoom r1 = sender as ucRoom;
                if (sourceNo == r1.Room.ID)
                    return;
                if (e.Effect == DragDropEffects.Copy || e.Effect == DragDropEffects.Move)
                {
                    if (DragDropCallBack != null)
                        DragDropCallBack(sourceNo, r1.Room.ID);
                    //MessageBox.Show("原No=" + sourceNo + "目标No=" + r1.Room.ID);
                }
            }

        }



        private void ucRoom_MouseDown(object sender, MouseEventArgs e)
        {
            Size dragSize = SystemInformation.DragSize;
            dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
        }

        private void ucRoom_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {

                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    DragDropEffects dropEffect = this.DoDragDrop(this.Room.ID, DragDropEffects.All | DragDropEffects.Link);

                }
            }

        }

        private void ucRoom_MouseUp(object sender, MouseEventArgs e)
        {
            dragBoxFromMouseDown = Rectangle.Empty;
        }


        private void ucRoom_DragOver(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(typeof(System.String)))
            {

                e.Effect = DragDropEffects.None;
                return;
            }

            if ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
            {

                // By default, the drop action should be move, if allowed.
                e.Effect = DragDropEffects.Move;

            }
            else
                e.Effect = DragDropEffects.None;

        }


        #endregion

        private int GetImageIndex(string zt)
        {
            switch (zt)
            {                  
                case "散客房":
                    return (int)RoomStstusEnum.SanKe;
                case "团队房":
                    return (int)RoomStstusEnum.TuanDui;
                case "钟点房":
                    return (int)RoomStstusEnum.ZhongDian;
                case "长包房":
                    return (int)RoomStstusEnum.ChangBao;
                case "免费房":
                    return (int)RoomStstusEnum.MianFei;
                case "VIP房":
                    return (int)RoomStstusEnum.Vip;
                case "自用房":
                    return (int)RoomStstusEnum.ZiYong;
                case "保密房":
                    return (int)RoomStstusEnum.BaoMi;
                case "离店房":
                    return (int)RoomStstusEnum.LiDian;
                case "清理房":
                    return (int)RoomStstusEnum.Qingli;
                case "锁定房":
                    return (int)RoomStstusEnum.SuoDing;
                case "毛病房":
                    return (int)RoomStstusEnum.MaoBing;
                //case "预订房":
                //    return (int)RoomStstusEnum.YuDing;
                default:
                    return 0;
            }
        }

        private Image GetImage(string zt)
        {
            switch (zt)
            {
                case "预订房":
                    return RoomStatusImage.GetYDImage;
                case "散客房":
                    return RoomStatusImage.GetSKImage;
                case "团队房":
                    return RoomStatusImage.GetTTImage;
                case "空净房":
                    return RoomStatusImage.GetKJImage;
                default:
                    return RoomStatusImage.GetSKImage;
            }
        }

        public void SetContextMenu(ContextMenuStrip contextMenuStrip)
        {
            this.ContextMenuStrip = contextMenuStrip;
            //this.pBox.ContextMenuStrip = contextMenuStrip;
        }

        private void ucRoom_DoubleClick(object sender, EventArgs e)
        {
            if (DoubleClickCallBack != null)
                DoubleClickCallBack(this);
        }

        private void ucRoom_Click(object sender, EventArgs e)
        {           
            Selected = true;
            this.DrawSelectedRegion();
            if (ClickCallBack != null)
                ClickCallBack(this);
        }

        private void ucRoom_Paint(object sender, PaintEventArgs e)
        {
            if (Selected)
            {
                DrawSelectedRegion();
            }
            else
            {
                Graphics g = e.Graphics;
                g.DrawRectangle(Pens.DarkGray, 0, 0, this.Width, this.Height);
            }
        }
        /// <summary>
        /// 绘制选择的区域
        /// </summary>
        public void DrawSelectedRegion()
        {
            if (Selected)
            {
                System.Drawing.Pen myPen = new System.Drawing.Pen(System.Drawing.Color.Red);
                System.Drawing.Graphics formGraphics;
                formGraphics = this.CreateGraphics();
                formGraphics.DrawRectangle(Pens.Red, 1, 1, this.Width - 2, this.Height - 2);
                //formGraphics.DrawLine(myPen, 0, 0, this.Size.Width, 0);
                //formGraphics.DrawLine(myPen, 0, 0, 0, this.Size.Height);
                //formGraphics.DrawLine(myPen, 0, this.Size.Height - 1, this.Size.Width - 1, this.Size.Height - 1);
                //formGraphics.DrawLine(myPen, this.Size.Width - 1, this.Size.Height - 1, this.Size.Width - 1, 0);

                myPen.Dispose();
                formGraphics.Dispose();
            }
        }
        //房间问题
        private void lbfjwt_Click(object sender, EventArgs e)
        {
            var _context = new hotelEntities();
            var qry = _context.ROOM_MAINTAIN.Where(p => p.ROOM_ID == Room.ID && p.WCBZ == "否");
            var str = "";
            foreach (var item in qry)
            {
                str += item.WXNR + "\r\n";
            }
            _context.Dispose();
            var frm = new NoModalDialog("房间存在的问题",str);
            frm.ShowDialog();

        }
        
    }

    /// <summary>
    /// 主房态背景颜色及字体颜色
    /// </summary>
    public static  class RoomStatusColor
    {
        static RoomStatusColor()
        {
            All = Color.Aqua;
            KongJinFang = Color.LightYellow;
            YuDingFang = Color.CadetBlue;
            ZhuFang = Color.LightPink;
            KongZangFang = Color.MediumOrchid;
            WeiXiuFang = Color.DarkGray;
            ForeColor = System.Drawing.SystemColors.ControlText;
        }
        public static Color All { get; private set; }
        public static Color KongJinFang { get; private set; }
        public static Color YuDingFang { get; private set; }
        public static Color ZhuFang { get; private set; }
        public static Color KongZangFang { get; private set; }
        public static Color WeiXiuFang { get; private set; }
        /// <summary>
        /// 字体颜色
        /// </summary>
        public static Color ForeColor { get; private set; }
    }
    #region 房屋状态图片

    /// <summary>
    /// 房屋状态图片
    /// </summary>
    public class RoomStatusImage
    {
        private static string path = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\\")) + "\\Images\\";
        private  static ImageList imgList;
        /// <summary>
        /// 房态ImageList
        /// </summary>
        public static ImageList GetImageList
        {
            get
            {
                if (imgList == null)
                {
                    CreateImageList();
                }
                return imgList;
            }
        }
        #region 从文件中加载状态图片
        private static void CreateImageList()
        {           
            string defaultFilePath = path + "default.png";
            imgList = new ImageList();
            imgList.ImageSize = new Size(16, 16);
            //注意增加的顺序,与mainControl窗体的状态图标一致，与RoomStstusEnum一致
            string fileName = path + "default.png";
            if (!File.Exists(fileName))
                throw new Exception("没有找到" + fileName + ",请正确安装");
            var imgDefault = Image.FromFile(fileName);
            //散客0
            fileName = path + RoomStstusEnum.SanKe.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //团队1
            fileName = path + RoomStstusEnum.TuanDui.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //钟点2
            fileName = path + RoomStstusEnum.ZhongDian.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //长包3
            fileName = path + RoomStstusEnum.ChangBao.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //自用4
            fileName = path + RoomStstusEnum.ZiYong.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //免费5
            fileName = path + RoomStstusEnum.MianFei.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //VIP
            fileName = path + RoomStstusEnum.Vip.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //保密
            fileName = path + RoomStstusEnum.BaoMi.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //离店
            fileName = path + RoomStstusEnum.LiDian.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //清理
            fileName = path + RoomStstusEnum.Qingli.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //锁定
            fileName = path + RoomStstusEnum.SuoDing.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //毛病
            fileName = path + RoomStstusEnum.MaoBing.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //预订
            fileName = path + RoomStstusEnum.YuDing.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
            //离开标志,左右下角
            fileName = path + RoomStstusEnum.LiKai.ToString() + ".png";
            if (File.Exists(fileName))
                imgList.Images.Add(Image.FromFile(fileName));
            else
                imgList.Images.Add(imgDefault);
        }
        #endregion
       

        private static Image kjImage;
        /// <summary>
        /// 空静image
        /// </summary>
        public static Image GetKJImage
        {
            get
            {
                if (kjImage == null)
                {
                    kjImage = Image.FromFile(path + "room_kj.jpg");
                }
                return kjImage;
            }
        }

        private static Image ydImage;
        /// <summary>
        /// 预订image
        /// </summary>
        public static Image GetYDImage
        {
            get
            {
                if (ydImage == null)
                {
                    ydImage = Image.FromFile(path + "room_yd.jpg");
                }
                return ydImage;
            }
        }
        private static Image skImage;
        /// <summary>
        /// 散客image
        /// </summary>
        public static Image GetSKImage
        {
            get
            {
                if (skImage == null)
                {
                    skImage = Image.FromFile(path + "room_sk.jpg");
                }
                return skImage;
            }
        }

        private static Image ttImage;
        /// <summary>
        /// 团体image
        /// </summary>
        public static Image GetTTImage
        {
            get
            {
                if (ttImage == null)
                {
                    ttImage = Image.FromFile(path + "room_tt.jpg");
                }
                return ttImage;
            }
        }
    }

    #endregion

   
}
