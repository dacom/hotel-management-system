﻿namespace hotel.MyUserControl
{
    partial class uckrxx
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtxm = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cobxxb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cobxzjlx = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtzjhm = new System.Windows.Forms.TextBox();
            this.txtzz = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdw = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtdh = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cobxgj = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cobxlz = new System.Windows.Forms.ComboBox();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnfind = new System.Windows.Forms.Button();
            this.btnreadid = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "姓名：";
            // 
            // txtxm
            // 
            this.txtxm.Location = new System.Drawing.Point(91, 17);
            this.txtxm.Name = "txtxm";
            this.txtxm.Size = new System.Drawing.Size(92, 23);
            this.txtxm.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "性别：";
            // 
            // cobxxb
            // 
            this.cobxxb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxxb.FormattingEnabled = true;
            this.cobxxb.Items.AddRange(new object[] {
            "女",
            "男",
            "未知"});
            this.cobxxb.Location = new System.Drawing.Point(334, 21);
            this.cobxxb.Name = "cobxxb";
            this.cobxxb.Size = new System.Drawing.Size(68, 21);
            this.cobxxb.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "证件类型：";
            // 
            // cobxzjlx
            // 
            this.cobxzjlx.FormattingEnabled = true;
            this.cobxzjlx.Location = new System.Drawing.Point(91, 51);
            this.cobxzjlx.Name = "cobxzjlx";
            this.cobxzjlx.Size = new System.Drawing.Size(140, 21);
            this.cobxzjlx.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "证件号码：";
            // 
            // txtzjhm
            // 
            this.txtzjhm.Location = new System.Drawing.Point(91, 83);
            this.txtzjhm.Name = "txtzjhm";
            this.txtzjhm.Size = new System.Drawing.Size(347, 23);
            this.txtzjhm.TabIndex = 1;
            this.txtzjhm.Leave += new System.EventHandler(this.txtzjhm_Leave);
            // 
            // txtzz
            // 
            this.txtzz.Location = new System.Drawing.Point(91, 117);
            this.txtzz.Name = "txtzz";
            this.txtzz.Size = new System.Drawing.Size(347, 23);
            this.txtzz.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "住址：";
            // 
            // txtdw
            // 
            this.txtdw.Location = new System.Drawing.Point(91, 151);
            this.txtdw.Name = "txtdw";
            this.txtdw.Size = new System.Drawing.Size(347, 23);
            this.txtdw.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "单位：";
            // 
            // txtdh
            // 
            this.txtdh.Location = new System.Drawing.Point(91, 185);
            this.txtdh.Name = "txtdh";
            this.txtdh.Size = new System.Drawing.Size(347, 23);
            this.txtdh.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 189);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "联系电话：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 222);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "国籍：";
            // 
            // cobxgj
            // 
            this.cobxgj.FormattingEnabled = true;
            this.cobxgj.Location = new System.Drawing.Point(91, 219);
            this.cobxgj.Name = "cobxgj";
            this.cobxgj.Size = new System.Drawing.Size(140, 21);
            this.cobxgj.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(247, 223);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 14);
            this.label9.TabIndex = 0;
            this.label9.Text = "来自：";
            // 
            // cobxlz
            // 
            this.cobxlz.FormattingEnabled = true;
            this.cobxlz.Location = new System.Drawing.Point(297, 219);
            this.cobxlz.Name = "cobxlz";
            this.cobxlz.Size = new System.Drawing.Size(140, 21);
            this.cobxlz.TabIndex = 2;
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(91, 251);
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(347, 23);
            this.txtbzsm.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 255);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 14);
            this.label10.TabIndex = 0;
            this.label10.Text = "备注说明：";
            // 
            // btnfind
            // 
            this.btnfind.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnfind.Location = new System.Drawing.Point(189, 16);
            this.btnfind.Name = "btnfind";
            this.btnfind.Size = new System.Drawing.Size(84, 28);
            this.btnfind.TabIndex = 3;
            this.btnfind.Text = "查找...";
            this.btnfind.UseVisualStyleBackColor = true;
            this.btnfind.Click += new System.EventHandler(this.btnfind_Click);
            // 
            // btnreadid
            // 
            this.btnreadid.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnreadid.Location = new System.Drawing.Point(303, 51);
            this.btnreadid.Name = "btnreadid";
            this.btnreadid.Size = new System.Drawing.Size(134, 28);
            this.btnreadid.TabIndex = 4;
            this.btnreadid.Text = "读取身份证";
            this.btnreadid.UseVisualStyleBackColor = true;
            this.btnreadid.Click += new System.EventHandler(this.btnreadid_Click);
            // 
            // uckrxx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnreadid);
            this.Controls.Add(this.btnfind);
            this.Controls.Add(this.cobxlz);
            this.Controls.Add(this.cobxgj);
            this.Controls.Add(this.cobxzjlx);
            this.Controls.Add(this.cobxxb);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtbzsm);
            this.Controls.Add(this.txtdh);
            this.Controls.Add(this.txtdw);
            this.Controls.Add(this.txtzz);
            this.Controls.Add(this.txtzjhm);
            this.Controls.Add(this.txtxm);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "uckrxx";
            this.Size = new System.Drawing.Size(492, 299);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtxm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cobxxb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cobxzjlx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtzjhm;
        private System.Windows.Forms.TextBox txtzz;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtdw;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtdh;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cobxgj;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cobxlz;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnfind;
        private System.Windows.Forms.Button btnreadid;
    }
}
