﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Common;

namespace hotel.MyUserControl
{
    /// <summary>
    /// 客户入住信息
    /// </summary>
    public partial class uckrxx : UserControl
    {
        //private bool IsNew;
        public  RZRY Rzry;
        /// <summary>
        /// 查询调用方法
        /// </summary>
        public Func<string, UV_ALLKR> FindCallBack;
        public uckrxx(RZRY rzry)
        {
            InitializeComponent();
            //btnfind.Visible = (FindCallBack != null);
            Rzry = rzry;
            //IsNew = _rzry.ID == 0;//0为新增
            cobxzjlx.DataSource = XTCYDMDal.GetAllByLBDM(EnumXtcydmLB.ZJLX.ToString());
            cobxzjlx.DisplayMember = "XMMC";
            cobxzjlx.DisplayMember = "XMMC";
            txtbzsm.Text = Rzry.BZSM;
            txtzz.Text = Rzry.DZ;
            txtdw.Text = Rzry.DW;
            cobxgj.Text = Rzry.GJ;
            cobxxb.Text = Rzry.KRXB;
            txtxm.Text = Rzry.KRXM;
            txtdh.Text = Rzry.LXDH;
            cobxlz.Text = Rzry.SF;
            txtzjhm.Text = Rzry.ZJDM;
            cobxzjlx.Text = Rzry.ZJLX;
        }

        public void UpdateRZRY()
        {
            Rzry.BZSM = txtbzsm.Text;
            Rzry.DZ = txtzz.Text;
            Rzry.DW = txtdw.Text;
            Rzry.GJ = cobxgj.Text;
            Rzry.KRXB = cobxxb.Text;
            Rzry.KRXM = string.IsNullOrEmpty(txtxm.Text) ? "新客人" : txtxm.Text;
            Rzry.LXDH = txtdh.Text;
            Rzry.SF = cobxlz.Text;
            Rzry.ZJDM = txtzjhm.Text;
            Rzry.ZJLX = cobxzjlx.Text;
        }

        /// <summary>
        /// 重新显现--根据历史客人信息
        /// </summary>
        /// <param name="rzry">要显示的数据，与构造函数的不同</param>
        public void ReDisplay(UV_ALLKR rzry)
        {
            if (rzry == null)
                return;
            txtbzsm.Text = "";
            txtzz.Text = rzry.DZ;
            txtdw.Text = rzry.DW;
            cobxgj.Text = rzry.GJ;
            cobxxb.Text = rzry.KRXB;
            txtxm.Text = rzry.KRXM;
            txtdh.Text = rzry.LXDH;
            cobxlz.Text = rzry.SF;
            txtzjhm.Text = rzry.ZJDM;
            cobxzjlx.Text = rzry.ZJLX;
            //贵宾卡号
            if (rzry.BMC == "HYK")
                Rzry.GBKH = rzry.ID;
        }

        private void btnfind_Click(object sender, EventArgs e)
        {
            btnfind.Visible = (FindCallBack != null);
            if (FindCallBack == null)
                return;
            ReDisplay(FindCallBack(txtxm.Text));
        }

        private void txtzjhm_Leave(object sender, EventArgs e)
        {
            if (cobxzjlx.Text == "身份证" && txtzjhm.Text.Length >0)
            {
                try
                {
                    var b = IdentityCard.Check(txtzjhm.Text);
                    if (!b)
                    {
                        MessageBox.Show("身份证号码不正确，请查证");
                        return;
                    }
                }
                catch (Exception ex )
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
                
                string areaCode, areaName;
                int sex;
                DateTime birthDate;
                IdentityCard.GetInfoByIdentityCard(txtzjhm.Text, out areaCode, out  areaName, out  sex, out  birthDate);
                txtzz.Text = areaName;
                cobxxb.SelectedIndex = sex;
            }
            
        }

        private void btnreadid_Click(object sender, EventArgs e)
        {
            try
            {
                var id = Utility.CreateIDCard();
                
                var info = id.ReadCardInfo();
                if (info == null)
                    return;
                txtxm.Text = info.Name;
                txtzz.Text = info.ADDRESS;
                cobxgj.Text = "中国";
                cobxxb.Text = info.Sex_CName;
                txtzjhm.Text = info.IDC;
                cobxzjlx.Text = "身份证";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
