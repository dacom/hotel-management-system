using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;
namespace hotel.Common
{
	/// <summary>
	/// Encryption64 的摘要说明。
	/// Base64 加密/解密
	/// </summary>
	public class Encryption64
	{
		private Byte[] key = {};
		private Byte[] IV  = {0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF};
		public Encryption64()
		{
			//
			// TODO: 在此处添加构造函数逻辑
			//
		}
		///<summary>
		///<para>加密</para>		
		///</summary>
		
		public string Decrypt(string stringToDecrypt ,string sEncryptionKey ) 
		{
			
			Byte[] inputByteArray;
			inputByteArray=new byte[stringToDecrypt.Length];
			try
			{
				key = System.Text.Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
				DESCryptoServiceProvider des=new DESCryptoServiceProvider();
				inputByteArray = Convert.FromBase64String(stringToDecrypt);
				MemoryStream  ms = new MemoryStream();
				CryptoStream cs =new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
				cs.Write(inputByteArray, 0, inputByteArray.Length);
				cs.FlushFinalBlock();
				System.Text.Encoding encoding   = System.Text.Encoding.UTF8;
				return encoding.GetString(ms.ToArray());
			}
			catch (Exception e)
			{
				return e.Message;
			}																																			 
		}

		///<summary>
		///解密
		/// <param name="stringToEncrypt">要加密的字符串.</param>
		/// /// <param name="SEncryptionKey">密钥.</param>
		///</summary>
		public string Encrypt(string stringToEncrypt ,string SEncryptionKey )
		{
			try
			{
				key = System.Text.Encoding.UTF8.GetBytes(SEncryptionKey.Substring(0, 8));
				DESCryptoServiceProvider des =new DESCryptoServiceProvider();
				Byte[] inputByteArray  = Encoding.UTF8.GetBytes(stringToEncrypt);
				MemoryStream ms =new MemoryStream();
				CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
				cs.Write(inputByteArray, 0, inputByteArray.Length);
				cs.FlushFinalBlock();
				return Convert.ToBase64String(ms.ToArray());
			}
			catch ( Exception e)
			{
				return e.Message;
			}
		}
		
	}
}

/*
' VB.NET

Imports System
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Security.Cryptography

Public Class Encryption64
    Private key() As Byte = {}
    Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}

    Public Function Decrypt(ByVal stringToDecrypt As String, _
        ByVal sEncryptionKey As String) As String
        Dim inputByteArray(stringToDecrypt.Length) As Byte
         Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(sEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(stringToDecrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function

    Public Function Encrypt(ByVal stringToEncrypt As String, _
        ByVal SEncryptionKey As String) As String
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(SEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes( _
                stringToEncrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function

End Class
*/
