﻿/******************************************************************************
 ----------------------------------------------------------------
  模块名称: 数字与字符串验证
  编者    : 李春雷			     创建日期: 2008年10月14日
  功能描述: 用法:
 *          //1)
            //var  info = ValidateHelper.StartVerify("")
            //.IsNotNullOrEmpty("输入为空")
            //.IsInt("不能格式化为Int类型")
            //.Min(0, "输入小于0")
            //.Max(100, "输入大于100")
            //.EndVerify();
            //Assert.AreEqual(false, info.Status);
            //Assert.AreEqual("00",info.Message);
 * 
            //2)
            decimal dd = 234234.23423M;
            var info = ValidateHelper.StartVerify(dd)
           .IsNotNullOrEmpty("输入为空")
           .IsInt("不是int")
           .Min(0, "输入小于0")
           .Max(100, "输入大于100")
           .EndVerify();
           if (!info.Status)
             Response.Write(info.Message);
 * 
            //3)
           var info = ValidateHelper.StartVerify(txtSJ_CLPH.Text)
           .IsNotNullOrEmpty("实际材料牌号不能为空,请查证!")
           .Max(25, "实际材料牌号不能大于25")
           .EndVerify();
            if (!info.Status)
            {
                HiddenMsg.Value = info.Message;
                return false;
            }
 ----------------------------------------------------------------
  修改日期:					 修改人: 
  修改内容: 
 ----------------------------------------------------------------
 *******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.Common
{
    /// <summary>
    /// 检验帮助类
    /// </summary>
    public class ValidataHelper
    {        
        public static ValidataNumber<int> StartVerify(int num)
        {
            return new ValidataNumber<int>(num);
        }
        public static ValidataNumber<double> StartVerify(double num)
        {
            return new ValidataNumber<double>(num);
        }
        public static ValidataNumber<decimal> StartVerify(decimal num)
        {
            return new ValidataNumber<decimal>(num);
        }
        public static ValidataString StartVerify(string T)
        {
            return new ValidataString(T);
        }

    }
    
    /// <summary>
    /// 检验信息类
    /// </summary>
    public class ValidataInfo
    {
        private bool _Status;

        /// <summary>
        /// 是否通过
        /// </summary>
        public bool Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        private string _Message;

        /// <summary>
        /// 错误提示
        /// </summary>
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
    }

    /// <summary>
    /// 检验基类
    /// </summary>
    public class ValidataBase
    {
        protected bool Status;
        protected string Message;

        public ValidataInfo EndVerify()
        {
            ValidataInfo info = new ValidataInfo();
            info.Status = Status;
            info.Message = Message;
            return info;
        }
    }

    /// <summary>
    /// 数值检验类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ValidataNumber<T> : ValidataBase where T : IComparable<T>
    {
        T m_num = default(T);

        public ValidataNumber(T num)
        {
            m_num = num;
            Status = true;
            Message = string.Empty;
        }

        public ValidataNumber<T> IsNotNullOrEmpty(string msg)
        {
            if (m_num == null)
            {
                Status = false;
                Message = msg;
            }
           
           
            return this;
        }

        public ValidataNumber<T> IsInt(string msg)
        {
            if (!Status)
                return this;
            
            Status = Utility.IsInt32(m_num.ToString());
            if (!Status)
                Message = msg;

            return this;
        }

        public ValidataNumber<T> Min(T min, string msg)
        {
            if (!Status)
                return this;

            if (m_num.CompareTo(min) < 0)
            {
                Status = false;
                Message = msg;
            }

            return this;
        }
        public ValidataNumber<T> Max(T max, string msg)
        {
            if (!Status)
                return this;

            if (m_num.CompareTo(max) > 0)
            {
                Status = false;
                Message = msg;
            }

            return this;
        }
    }
    
    /// <summary>
    /// 字符串检验类
    /// 因字符串可能还有其它特殊的检验，所有单独成一个类
    /// </summary>
    public class ValidataString : ValidataBase
    {
        string stringT = null;

        public ValidataString(string T)
        {
            stringT = T;
            Status = true;
            Message = string.Empty;
        }

        public ValidataString IsNotNullOrEmpty(string msg)
        {
            if (string.IsNullOrEmpty(stringT))
            {
                Status = false;
                Message = msg;
            }

            return this;
        }

        public ValidataString IsInt(string msg)
        {
            if (!Status)
                return this;

            int intTemp = 0;
            Status = int.TryParse(stringT, out intTemp);
            if (!Status)
                Message = msg;

            return this;
        }

        public ValidataString Min(int min, string msg)
        {
            if (!Status)
                return this;

            if (Utility.StringLength(stringT) < min)
            {
                Status = false;
                Message = msg;
            }

            return this;
        }

        public ValidataString Max(int max, string msg)
        {
            if (!Status)
                return this;

            if (Utility.StringLength(stringT) > max)
            {
                Status = false;
                Message = msg;
            }

            return this;
        }
    }
}
