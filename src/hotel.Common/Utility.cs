using System; 
using System.Text; 
using System.Data; 
using System.Collections.Generic; 
using System.Collections; 
using System.ComponentModel;
using System.IO;
using System.Globalization; 
using System.Configuration; 
using System.Security.Cryptography;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;
using System.Reflection;
namespace hotel.Common
{
	/// <summary>
	/// Pub_Funs 公共字符串处理类。
	/// </summary>
	public class Utility 
	{ 

		/// <summary>
		/// 对请求的参数值进行解密,主要解决中文在URL中的传输问题
		/// </summary>
		/// <param name="str"></param>
		/// <param name="split"></param>
		/// <returns></returns>
		public static string DecodeQueryParam(string str,string split) 
		{ 
			if (!string.IsNullOrEmpty(str)) 
			{ 
				str = str.Replace(split, ""); 
				return (str); 
			} 
			else 
			{ 
				return null; 
			} 
		} 

		public static string DecodeQueryParam(string str) 
		{ 
			if (str!=null) 
			{ 
				str = str.Replace("||||", ""); 
				return (str); 
			} 
			else 
			{ 
				return null; 
			} 
		} 
		public static string QuotedStr(string str) 
		{   
			string QuotedStr;
			int i; 
			if (str==null || str == "") 
			{ 
				return "\'\'"; 
			} 
			QuotedStr = str; 
			for ( i = QuotedStr.Length; i >= 1; i--) 
			{ 
				if (QuotedStr.Substring(i - 1, 1) == "'") 
				{ 
					QuotedStr = QuotedStr.Insert(i - 1, "'"); 
				} 
			} 
			QuotedStr = "\'" + QuotedStr + "\'"; 
			return QuotedStr;
		} 

		public static string get_query_tablename(string sqlstr) 
		{ 
			int pos1; 
			int pos2; 
			string get_query_tablename;
			string lsstr; 
			sqlstr = sqlstr.ToUpper(); 
			if (sqlstr == "") 
			{ 
				return ""; 
			} 
			pos1 = sqlstr.IndexOf(" FROM "); 
			pos2 = sqlstr.IndexOf(" WHERE "); 
			
			get_query_tablename = ""; 
			if (pos2 < 0) 
			{ 
				lsstr = sqlstr.Substring(pos1 + " FROM ".Length, sqlstr.Length - (pos1 + " FROM ".Length)); 
			} 
			else 
			{ 
				lsstr = sqlstr.Substring(pos1 + " FROM ".Length, pos2 - (pos1 + " FROM ".Length)); 
			} 
			if (lsstr.IndexOf(",") > 0) 
			{ 
				lsstr = lsstr.Substring(0, lsstr.IndexOf(",")).Trim(); 
			} 
			if (lsstr.IndexOf(" ") > 0) 
			{ 
				lsstr = lsstr.Substring(0, lsstr.IndexOf(" ")).Trim(); 
			} 
			get_query_tablename = lsstr; 
			return get_query_tablename;
		} 
		//'根据sql语句找到第一个表名,为主表的名称,
		public static string GetTableName(string sqlstr) 
		{
			string str,res;
			res = "";
			int i;
			sqlstr = sqlstr.Trim().ToUpper();
			str="";
			for( i = 0 ;i< sqlstr.Length;i++)
			{
				if (sqlstr.Substring(i,4)=="FROM" && sqlstr.Substring(i + 4, 1) == " ")
				{
					str = sqlstr.Substring(i + 5, sqlstr.Length - i - 5).Trim();
					break;
				}
			}
        
			for( i=0;i<str.Length;i++)
			{
				if (str.Substring(i,1)==" " || str.Substring(i, 1) == "\n")
					break;
				res += str.Substring(i, 1).ToUpper();
			}																			
			return res;
		}
		static string fill_string(string string_in, int len_in) 
		{ 
			return fill_string(string_in, len_in, '0'); 
		} 

		static string fill_string(string string_in, int len_in, char fill_char) 
		{ 
			string temp; 
			int i; 
			int old_len; 
			temp = string_in.ToString(); 
			old_len = temp.Length; 
			if (old_len >= len_in) 
			{ 
				return temp; 
			} 
			else 
			{ 
				for ( i = 0; i <= len_in - old_len - 1; i++) 
				{ 
					temp = fill_char + temp; 
				} 
				return temp; 
			} 
		} 
		//获得关闭窗口脚本(非模态)
		public static string GetCloseWindowScript() 
		{ 
			
			string scriptString = "<script language=JavaScript>"; 
			scriptString += "window.opener=null;"; 
			scriptString += "window.close();"; 
			scriptString += "</script>"; 
			return scriptString;
		} 
		//获得关闭模态窗口脚本
		public static string GetCloseModalWindowScript(string res_val) 
		{ 
			//res_val为返回的信息
			string scriptString = "<script language=JavaScript>"; 
			scriptString += "window.opener=null;"; 
			scriptString += "window.parent.returnValue='"+ res_val+ "'" + "\r\n"; 
			scriptString += "window.close();"; 
			scriptString += "</script>"; 
			return scriptString;
		} 

		//格式化字符串为"yyyy-mm-dd"
		public static string FormatRQ(string value)
		{
			return FormatRQ(value,"yyyy-MM-dd");
		}

		public static string FormatRQ(string value,string format)
		{
            if (string.IsNullOrEmpty(value))
                return "";
			return Convert.ToDateTime(value).ToString(format);
		}

		//对字符串进行加密/解密
		///<summary>
		///对字符串进行Base64加密
		///</summary>
		public static string encryptQueryString(string strQueryString) 
		{
			Encryption64 oES = new Encryption64();
			return oES.Encrypt(strQueryString,"!#$a54?3");
		}

		///<summary>
		///对字符串进行Base64解密
		///</summary>
		public static string decryptQueryString(string strQueryString) 
		{
			Encryption64 oES = new Encryption64();
			return oES.Decrypt(strQueryString,"!#$a54?3");
		}

        /// <summary>
        /// 写调试日志文件
        /// </summary>
        /// <param name="filename">The 文件名称.</param>
        /// <param name="msg">The 消息内容.</param>
        public static void WriteDebugMsg(string filename,string msg)
        {
            if (!System.IO.File.Exists(filename))
                return;
            StreamWriter buglog = new StreamWriter(filename, true, System.Text.Encoding.GetEncoding("GB2312"));  //'字体转换,要不是乱码	  
            buglog.WriteLine(System.DateTime.Now.ToString() + "\t Message = " + msg);
            buglog.Flush();
            buglog.Close();
        }

		/// <summary>
		/// MD5加密
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string EncryptMD5(string str)
		{            
            string str2 = encryptQueryString(str);  //因为MD5现在已经可以破译,所以在次加密                     
            str2 = str2.Insert(Math.Min(str.Length - 1, str2.Length - 1), "-1F-6F");
			Byte[] clearBytes;
			Byte[] hashedBytes;
			System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("UTF-8");
            clearBytes = encoding.GetBytes(str2);
			hashedBytes = ((HashAlgorithm) CryptoConfig.CreateFromName("MD5")).ComputeHash(clearBytes);
			return  BitConverter.ToString(hashedBytes);
        }

        #region 数据类型判定
        
        
        /// <summary>
		/// 判断一个字符串是否为数字
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static bool IsNumeric(string str)  
		{

            if (string.IsNullOrEmpty(str))
			{
				return false;
			}
			else
			{
				Regex reg = new Regex("^(-?\\d+)(\\.\\d+)?$"); 
				return reg.IsMatch(str);  
			}
		}
        
        /// <summary>
        /// 验证字符串是否整数
        /// </summary>
        /// <param name="input">要验证的字符串</param>
        /// <returns></returns>
        public static bool IsInt32(string input)
        {
            if (string.IsNullOrEmpty(input))
                return false;
            bool bl = false;
            string regexString = "^-?\\d+$";
            bl = Regex.IsMatch(input, regexString);
            //是否越界
            int result;
            if (bl)
                bl = int.TryParse(input, out result);
            return bl;
        }

        /// <summary>
        /// 验证字符串是否浮点数字
        /// </summary>
        /// <param name="v">要验证的字符串</param>
        /// <returns></returns>
        public static bool IsDouble(string input)
        {
            if (string.IsNullOrEmpty(input))
                return false;
            string regexString = "^(-?\\d+)(\\.\\d+)?$";
            bool bl = Regex.IsMatch(input, regexString);
            //是否越界
            double result;
            if (bl)
                bl = double.TryParse(input, out result);
            return bl;
        }

		/// <summary>
		/// 是否是日期
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public	static bool IsDate(string str)
		{
            if (string.IsNullOrEmpty(str))
                return false;
            DateTime dt;
            return DateTime.TryParse(str, out dt);            
            //try
            //{
            //    DateTime dt;
            //    dt = Convert.ToDateTime(str);
            //    return true;
            //}
            //catch
            //{
            //    return false;
            //}
		}

        /// <summary>
        /// 验证字符串是否日期[2008-08-08|||2008-02-29 10:29:39 pm|||2004/12/31]
        /// </summary>
        /// <param name="v">要验证的字符串</param>
        /// <returns></returns>
        public static bool IsDate2(string input)
        {
            if (string.IsNullOrEmpty(input))
                return false;
            string regexString = @"^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$";

            bool bl = Regex.IsMatch(input, regexString);
            DateTime dt;
            if (bl)
                bl = DateTime.TryParse(input,out dt);
            return bl;
            #region Description
            /*
			Expression:  ^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578]
							)|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[4
							69])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\
							s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([1
							3579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((
							0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((
							0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9]
							)|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(
							\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$
 
			Author:  Sung Lee 
			Sample Matches:  
			2004-2-29|||2004-02-29 10:29:39 pm|||2004/12/31 
			Sample Non-Matches:  
			2003-2-29|||2003-13-02|||2003-2-2 10:72:30 am 
			Description:  Matches ANSI SQL date format YYYY-mm-dd hh:mi:ss am/pm. You can use / - or space for date delimiters, so 2004-12-31 works just as well as 2004/12/31. Checks leap year from 1901 to 2099. 
			 */
            #endregion
        }


        /// <summary>
        /// 是否是邮件地址
        /// </summary>
        /// <param name="inputData">输入字符串</param>
        /// <returns></returns>
        public static bool IsEmail(string input)
        {
            if (string.IsNullOrEmpty(input))
                return false;
            string regexString = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";            
            return Regex.IsMatch(input, regexString);
        }

        /// <summary>
        /// 是否邮政编码
        /// </summary>
        /// <param name="PostCode"></param>
        /// <returns></returns>
        public static bool IsPostCode(string input)
        {
            if (string.IsNullOrEmpty(input))
                return false;
            string regexString = @"^[1-9]\d{5}$";
            return Regex.IsMatch(input, regexString);
        }

        /// <summary>
        /// 是否电话号码
        /// </summary>
        /// <param name="tel"></param>
        /// <returns></returns>
        public static bool IsTelphone(string input)
        {
            string telaval = @"^0[1-9]\d{1,2}-[1-9]\d{6,7}$";
            string telval = @"^[1-9]\d{6,7}$";
            string modval = @"^(159|156|153|130|131|132|133|134|135|136|137|138|139)\d{8}$";
            return Regex.IsMatch(input, telval) || Regex.IsMatch(input, modval) || Regex.IsMatch(input, telaval);
        }

        /// <summary>
        /// 身份证验证
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>
        public static bool IsPersonID(string input)
        {
            string idval = @"^(\d{15})(\d{2}[Xx0-9])?$";
            return Regex.IsMatch(input, idval);
        }

        /// <summary>
        /// 类似SQL数据库的isnull,使用指定的替换值替换 NULL或""
        /// </summary>
        /// <param name="value_in"></param>
        /// <param name="def_val"></param>
        /// <returns></returns>
        public static string IsNull(object value_in, string def_val)
        {
            if (value_in == null || value_in == System.DBNull.Value || value_in.ToString() == "")
                return def_val;
            else
                return value_in.ToString();
        }

        #endregion

        /// <summary>
        /// 写调试日志
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="msg"></param>
        /// <param name="append"></param>
        public void WriteDebugMsg(string filename,string msg, bool append)
        {
            StreamWriter buglog = new StreamWriter(filename, append, System.Text.Encoding.GetEncoding("GB2312"));
            buglog.WriteLine(System.DateTime.Now + "\t" + msg);
            buglog.Flush();
            buglog.Close();
        }        

        /**/
        /// <summary> 
        /// 转换人民币大小金额 
        /// </summary> 
        /// <param name="num">金额</param> 
        /// <returns>返回大写形式</returns> 
        public static string RMBToUpper(decimal num)
        {
            string str1 = "零壹贰叁肆伍陆柒捌玖";            //0-9所对应的汉字 
            string str2 = "万仟佰拾亿仟佰拾万仟佰拾元角分"; //数字位所对应的汉字 
            string str3 = "";    //从原num值中取出的值 
            string str4 = "";    //数字的字符串形式 
            string str5 = "";  //人民币大写金额形式 
            int i;    //循环变量 
            int j;    //num的值乘以100的字符串长度 
            string ch1 = "";    //数字的汉语读法 
            string ch2 = "";    //数字位的汉字读法 
            int nzero = 0;  //用来计算连续的零值是几个 
            int temp;            //从原num值中取出的值 

            num = Math.Round(Math.Abs(num), 2);    //将num取绝对值并四舍五入取2位小数 
            str4 = ((long)(num * 100)).ToString();        //将num乘100并转换成字符串形式 
            j = str4.Length;      //找出最高位 
            if (j > 15) { return "溢出"; }
            str2 = str2.Substring(15 - j);   //取出对应位数的str2的值。如：200.55,j为5所以str2=佰拾元角分 

            //循环取出每一位需要转换的值 
            for (i = 0; i < j; i++)
            {
                str3 = str4.Substring(i, 1);          //取出需转换的某一位的值 
                temp = Convert.ToInt32(str3);      //转换为数字 
                if (i != (j - 3) && i != (j - 7) && i != (j - 11) && i != (j - 15))
                {
                    //当所取位数不为元、万、亿、万亿上的数字时 
                    if (str3 == "0")
                    {
                        ch1 = "";
                        ch2 = "";
                        nzero = nzero + 1;
                    }
                    else
                    {
                        if (str3 != "0" && nzero != 0)
                        {
                            ch1 = "零" + str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                        else
                        {
                            ch1 = str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                    }
                }
                else
                {
                    //该位是万亿，亿，万，元位等关键位 
                    if (str3 != "0" && nzero != 0)
                    {
                        ch1 = "零" + str1.Substring(temp * 1, 1);
                        ch2 = str2.Substring(i, 1);
                        nzero = 0;
                    }
                    else
                    {
                        if (str3 != "0" && nzero == 0)
                        {
                            ch1 = str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                        else
                        {
                            if (str3 == "0" && nzero >= 3)
                            {
                                ch1 = "";
                                ch2 = "";
                                nzero = nzero + 1;
                            }
                            else
                            {
                                if (j >= 11)
                                {
                                    ch1 = "";
                                    nzero = nzero + 1;
                                }
                                else
                                {
                                    ch1 = "";
                                    ch2 = str2.Substring(i, 1);
                                    nzero = nzero + 1;
                                }
                            }
                        }
                    }
                }
                if (i == (j - 11) || i == (j - 3))
                {
                    //如果该位是亿位或元位，则必须写上 
                    ch2 = str2.Substring(i, 1);
                }
                str5 = str5 + ch1 + ch2;

                if (i == j - 1 && str3 == "0")
                {
                    //最后一位（分）为0时，加上“整” 
                    str5 = str5 + '整';
                }
            }
            if (num == 0)
            {
                str5 = "零元整";
            }
            return str5;
        }
        
        /// <summary> 
        /// 一个重载，将字符串先转换成数字在调用RMBToUpper(decimal num) 
        /// </summary> 
        /// <param name="num">用户输入的金额，字符串形式未转成decimal</param> 
        /// <returns></returns> 
        public static string RMBToUpper(string numstr)
        {
            try
            {
                decimal num = Convert.ToDecimal(numstr);
                return RMBToUpper(num);
            }
            catch
            {
                return "非数字形式！";
            }
        }
              
        #region 中文汉字
        
        
        /// <summary>
        /// 中文字符和英文字符长度 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int StringLength(string str)
        {                        
            return System.Text.Encoding.Default.GetByteCount(str);             
        }

        /// <summary>
        /// 判定是否是汉字(单字处理)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsChineseLetter(string input)
        {
            Regex rx = new Regex("^[\u4e00-\u9fa5]$");
            return rx.IsMatch(input);
        }
        /// <summary>
        /// 判断句子中是否含有中文
        /// </summary>
        /// <param name="words"></param>
        /// <returns></returns>
        public static bool WordsIScn(string words)
        {
            
            for (int i = 0; i < words.Length; i++)
            {
                Regex rx = new Regex("^[\u4e00-\u9fa5]$");
                if (rx.IsMatch(words[i].ToString()))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// 给定一个字符串，判断其是否只包含有汉字,不能代标点英文符号
        /// </summary>
        /// <param name="testStr"></param>
        /// <returns></returns>
        public static bool IsOnlyContainsChinese(string testStr)
        {
            char[] words = testStr.ToCharArray();
            foreach (char word in words)
            {
                if (IsGBCode(word.ToString()) || IsGBKCode(word.ToString()))  // it is a GB2312 or GBK chinese word
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        
        /// <summary>
        /// 判断一个word是否为GB2312编码的汉字
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static bool IsGBCode(string word)
        {
            byte[] bytes = Encoding.GetEncoding("GB2312").GetBytes(word);
            if (bytes.Length <= 1)  // if there is only one byte, it is ASCII code or other code
            {
                return false;
            }
            else
            {
                byte byte1 = bytes[0];
                byte byte2 = bytes[1];
                if (byte1 >= 176 && byte1 <= 247 && byte2 >= 160 && byte2 <= 254)    //判断是否是GB2312
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        
        /// <summary>
        /// 判断一个word是否为GBK编码的汉字
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static bool IsGBKCode(string word)
        {
            byte[] bytes = Encoding.GetEncoding("GBK").GetBytes(word.ToString());
            if (bytes.Length <= 1)  // if there is only one byte, it is ASCII code
            {
                return false;
            }
            else
            {
                byte byte1 = bytes[0];
                byte byte2 = bytes[1];
                if (byte1 >= 129 && byte1 <= 254 && byte2 >= 64 && byte2 <= 254)     //判断是否是GBK编码
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        
        /// <summary>
        /// 判断一个word是否为Big5编码的汉字
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static bool IsBig5Code(string word)
        {
            byte[] bytes = Encoding.GetEncoding("Big5").GetBytes(word.ToString());
            if (bytes.Length <= 1)  // if there is only one byte, it is ASCII code
            {
                return false;
            }
            else
            {
                byte byte1 = bytes[0];
                byte byte2 = bytes[1];
                if ((byte1 >= 129 && byte1 <= 254) && ((byte2 >= 64 && byte2 <= 126) || (byte2 >= 161 && byte2 <= 254)))  //判断是否是Big5编码
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion

        /// <summary>
        /// 返回字符串中指字子字符串,指定出现位置指定长度的字符串
        /// </summary>
        /// <param name="str">原字符串</param>
        /// <param name="searchstr">要搜索的字符串</param>
        /// <param name="lengthnum">返回字符串的长度,0或-1返回到尾部所有字符串</param>
        /// <param name="startnum">字符串出现的位置</param>
        /// <returns></returns>
        public static string Substr(string str, string searchstr, int lengthnum, int startnum)
        {
            if (string.IsNullOrEmpty(str))
                return "";
            if (string.IsNullOrEmpty(searchstr))
            {
                if (lengthnum > 0 && str.Length > lengthnum)
                    return str.Substring(0, lengthnum);
                else
                    return str;
            }

            string result = "";
            if (IndexOfByNum(str, searchstr, 1) < 0)
                return "";

            if (startnum < 1)
                startnum = 1;

            int searchstrlength = searchstr.Length;     //要查找字符串的长度

            if (startnum <= 1 && lengthnum > 0)
                result = str.Substring(str.IndexOf(searchstr) + searchstrlength, lengthnum);
            else if (startnum <= 1)
                result = str.Substring(str.IndexOf(searchstr) + searchstrlength);
            else
            {
                int i = 1;
                while (i <= startnum)
                {                                      
                    str = str.Substring(str.IndexOf(searchstr) + searchstrlength);
                    if (i == startnum)
                    {
                        if (lengthnum > 0 && str.Length > lengthnum)
                            result = str.Substring(0, lengthnum);
                        else
                            result = str;
                        break;
                    }
                    if (str.IndexOf(searchstr) < 0)
                        return "";
                    i++;
                }
            }

            return result;
        }

        /// <summary>
        /// 返回字符串中指字子字符串,指定出现次数的位置索引
        /// </summary>
        /// <param name="str">原字符串</param>
        /// <param name="searchstr">要搜索的字符串</param>
        /// <param name="startnum">字符串出现的位置</param>
        /// <returns></returns>
        public static int IndexOfByNum(string str, string searchstr, int startnum)
        {
            if (startnum < 1)
                startnum = 1;

            if (str.IndexOf(searchstr) == -1)
                return -1;
            else if (CountNum(str, searchstr) < startnum)
                return -1;

            int searchstrlength = searchstr.Length;     //要查找字符串的长度

            if (startnum == 1)
                return str.IndexOf(searchstr);
            else
            {
                return str.Substring(0, str.Length - Substr(str, searchstr, 0, startnum).Length).Length;
            }
        }

        /// <summary>
        /// 统计字符串出现的次数
        /// </summary>
        /// <param name="text1">原字符串</param>
        /// <param name="text2">要查找的字符串</param>
        /// <returns>出现的次数</returns>
        public static int CountNum(string text1, string text2)
        {
            int total = 0;
            int loact = text1.IndexOf(text2);
            while (loact != -1)
            {
                int loc = text1.IndexOf(text2) + text2.Length;
                int len = text1.Length - loc;
                if (loc != -1)
                {
                    text1 = text1.Substring(loc, len);
                }
                loact = text1.IndexOf(text2);
                total++;
            }
            return total;
        }
        /// <summary>
        /// 创建房卡读卡器接口
        /// </summary>
        /// <returns></returns>
        public static IKeycard CreateKeycarder()
        {
            Hashtable ht = (Hashtable)ConfigurationManager.GetSection("KeycardInterface");
            if (ht == null)
                throw new Exception("App.config中没有定义KeycardInterface");
            string typestr= ht["type"].ToString();
            var path = typestr.Split(',')[0];
            var className = typestr.Split(',')[1];
            return (IKeycard)Assembly.Load(path).CreateInstance(className);
        }
        /// <summary>
        /// 创建二代身份证读卡器接口
        /// </summary>
        /// <returns></returns>
        public static IIDCard CreateIDCard()
        {
            Hashtable ht = (Hashtable)ConfigurationManager.GetSection("IDCardInterface");
            if (ht == null)
                throw new Exception("App.config中没有定义IDCardInterface");
            string typestr = ht["type"].ToString();
            var path = typestr.Split(',')[0];
            var className = typestr.Split(',')[1];
            var obj= (IIDCard)Assembly.Load(path).CreateInstance(className);
            if (obj == null)
                throw new Exception("无法实例化对象," + className);
            return obj;
        }

        //		public static bool IsDate(string str)
		//		{
		//			Regex reg = new System.Text.RegularExpressions.Regex(@"^((((1[6-9]|[2-9]\d)\d{2})[- ](0?[13578]|1[02])[- ](0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})[- ](0?[13456789]|1[012])[- ](0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})[- ]0?2[- ](0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29))( (0?\d|1\d|2[0-4]):(0?\d|[1-5]\d):(0?\d|[1-5]\d))$");
		//			return reg.IsMatch(str); 
		//		}

		//		public static string encryptQueryString(string strQueryString,string strKey) 
		//		{
		//			Encryption64 oES = new Encryption64();
		//			return oES.Encrypt(strQueryString,strKey);
		//		}
		//
		//		public static string decryptQueryString(string strQueryString,string strKey) 
		//		{
		//			Encryption64 oES = new Encryption64();
		//			return oES.Decrypt(strQueryString,strKey);
		//		}
        /*
                    static string encode_pass(System.String original_pass) 
                    { 
                        System.String str; 
                        System.Decimal temp_val; 
                        string str1; 
                        string str2; 
                        decimal sum; 
                        string return_val; 
                        const int maxchar = 32; 
                        str = original_pass; 
                        str2 = fill_string(Hex(str.Length), 2); 
                        for (int i = 0; i <= str.Length - 1; i++) 
                        { 
                            str1 = fill_string(Hex(ASCIIEncoding.(str[i])), 2); 
                            str2 = str2 + str1; 
                        } 
                        sum = 0; 
                        for (int i = 0; i <= str2.Length - 1; i++) 
                        { 
                            sum = sum + Val("&H" + str2[i]); 
                        } 
                        str2 = fill_string(Hex(sum + str2.Length), 2) + str2; 
                        if (str2.Length < maxchar) 
                        { 
                            while (str2.Length < maxchar) 
                            { 
                                str2 = str2 + fill_string(Hex(sum + str2.Length + 1), 2); 
                            } 
                        } 
                        else 
                        { 
                            str2 = str2.Substring(0, maxchar); 
                        } 
                        return_val = ""; 
                        for (int i = 0; i <= str2.Length - 1; i++) 
                        { 
                            return_val = return_val + Hex(Val("&H" + str2[i]) * (i + 1) % 16); 
                        } 
                        return return_val; 
                    } 
                } 
            */

      
    }
}