﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace hotel.Common
{
    public class MF1Interface : IKeycard
    {

        [DllImport("MF1_Key2007.dll", CharSet = CharSet.Ansi)]
        private static extern string ReadCardInfo(int CommID, int CardType, Byte MF1_Area);
        [DllImport("MF1_Key2007.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        private static extern string ClearCardInfo(int CommID, int CardType, Byte MF1_Area);
        [DllImport("MF1_Key2007.dll", CharSet = CharSet.Ansi)]
        private static extern int WriteGuestCard(int CommID, int CardType, int OldKeyEnd,
            int BAKKey, int TSRight, int FloorCode, int FloorLayCode, int RoomCode, int iSubRoomCode,
            string StartDateTime, string EndDateTime, Byte MF1_Area);

        private static int CommID;
        private static int CardType;
        private static Byte MF1_Area;
        static MF1Interface()
        {
            CardType = 0;
            MF1_Area = 15;
            CommID = Convert.ToInt32(ConfigurationManager.AppSettings["KeycardComNo"]);
        }

        public CardInfo ReadGuestCardInfo()
        {
             //var str ="234D833A78,1,2,10,10101,3,2007-10-10 12:20:00,2007-10-11 12:00:00,1	";
            var str = ReadCardInfo(CommID, CardType, MF1_Area);
            if (str == null)
                return null;
            if (str == "1")
            {
                System.Windows.Forms.MessageBox.Show("不能正常读取数据");
                return null;
            }
            else if (str == "-1")
            {
                System.Windows.Forms.MessageBox.Show("串口打开失败");
                return null;
            }
            else if (str == "-2")
            {
                System.Windows.Forms.MessageBox.Show("卡片无效");
                return null;
            }
            else if (str == "0")
            {
                System.Windows.Forms.MessageBox.Show("读数据失败");
                return null;
            }

            var arr = str.Split(',');
            if (arr.Length < 9)
                throw new Exception("读初的卡信息不符合规范:" + str);

            var info = new CardInfo();
            info.OriginalInfo = str;
            info.CardId = arr[0];
            info.CardType = Convert.ToInt32(arr[1]);
            info.CardTypeStr = "";
            info.FloorCode = Convert.ToInt32(arr[2]);
            info.FloorLayCode = Convert.ToInt32(arr[3]);
            info.RoomCode = Convert.ToInt32(arr[4]);
            info.SubRoomCode = Convert.ToInt32(arr[5]);
            info.StartDateTime = Convert.ToDateTime(arr[6]);
            info.EndDateTime = Convert.ToDateTime(arr[7]);
            info.IsRight = arr[8].Contains('1');
            return info;
        }
        public CardInfo WriteGuestCardInfo(CardInfo info)
        {
            //return ReadGuestCardInfo(); 
            if (info.CardType == 0)
                info.MF1Area = 15;
            if (info.SubRoomCode == 0)
                info.SubRoomCode = 255;
            var str = WriteGuestCard(CommID, 1, 0, info.IsRight ? 1 : 0, info.CardType,
                info.FloorCode, info.FloorLayCode, info.RoomCode, info.SubRoomCode,
                info.StartDateTime.ToString("yyyy-MM-dd HH:mm:ss"), info.EndDateTime.ToString("yyyy-MM-dd HH:mm:ss"), info.MF1Area);
            if (str == 1)
            {
                var rcard = ReadGuestCardInfo();
                if (rcard ==null)
                {
                     MessageBox.Show("制卡时，读取卡信息时出错，请重试!");
                    return null; 
                }
               
                MessageBox.Show("房间:" + info.FloorCode.ToString() + info.FloorLayCode.ToString() + info.RoomCode.ToString() + "写卡成功");
                return rcard;
            }
            else if (str == -1)
            {
                System.Windows.Forms.MessageBox.Show("串口打开失败");
            }
            else if (str == -2)
            {
                System.Windows.Forms.MessageBox.Show("卡片无效,请重试");
            }
            else if (str == -3)
            {
                System.Windows.Forms.MessageBox.Show("软件未注册,请与门锁商联系");
            }
            else if (str == 0)
            {
                System.Windows.Forms.MessageBox.Show("写卡失败,请重试");
            }
            return null;
        }
        public string ClearGuestCardInfo()
        {
            return ClearCardInfo(CommID, CardType, MF1_Area);
        }
    }
}
