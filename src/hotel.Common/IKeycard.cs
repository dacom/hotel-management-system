﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.Common
{
    //门卡接口
    public interface IKeycard
    {
        CardInfo ReadGuestCardInfo();
        CardInfo WriteGuestCardInfo(CardInfo info);
        string ClearGuestCardInfo();
    }

    /// <summary>
    /// 门卡模型
    /// </summary>
    public class CardInfo
    {
        // 卡号,卡类型,楼栋号,楼层号,房间号,套间号,开始时间,结束时间,开特权门标记 ---- 读取宾客卡时的返回值
        public int CommId { get; set; }
        public string CardId { get; set; }
        public int CardType { get; set; }
        public string CardTypeStr { get; set; }
        public int FloorCode { get; set; }
        public int FloorLayCode { get; set; }
        public int RoomCode { get; set; }
        public int SubRoomCode { get; set; }
        public bool IsRight { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public Byte MF1Area { get; set; }
        /// <summary>
        /// 原始读出的信息
        /// </summary>
        public string OriginalInfo { get; set; }
    }
}
