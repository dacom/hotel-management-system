﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.Common
{
    /// <summary>
    /// LDH=楼栋号，ZFFS=支付方式，ZJLX=证件类型,XYDWFL=协议单位分类
    /// </summary>
    public enum EnumXtcydmLB { LDH, ZFFS, ZJLX,XYDWFL }
    /// <summary>
    /// 主要用于绑定控件用
    /// </summary>
    public class NameValue
    {
        /// <summary>
        /// 显示的名字
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 不显示的ID
        /// </summary>
        public string Value { get; set; }

        public object Tag { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
