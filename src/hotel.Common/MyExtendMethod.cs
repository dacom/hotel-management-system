﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hotel.Common
{
    public static class MyExtendMethod
    {
        #region 扩展方法
        public static string ToString<T>(this List<T> source, string split)
        {
            string str = "";
            foreach (var item in source)
            {
                if (str != "")
                    str += split;
                str += item.ToString();
            }
            return str;
        }
        #endregion
    }
}
