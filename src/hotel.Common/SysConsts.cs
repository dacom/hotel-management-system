﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace hotel.Common
{
    /// <summary>
    /// 系统常量定义
    /// </summary>
    public class SysConsts
    {
        /// <summary>
        /// 所以项目,用于过滤项目
        /// </summary>
        public static string AllItem { get { return "全部..."; } }

        /// <summary>
        /// 系统管理员ID标识
        /// </summary>
        public static string SystemAdminId { get { return "admin"; } }
        /// <summary>
        /// 系统警告标题，用于MessageBox
        /// </summary>
        public static string SysWarningCaption { get { return "系统警告"; } }
        /// <summary>
        /// 系统提示标题，用于MessageBox
        /// </summary>
        public static string SysInformationCaption { get { return "系统提示"; } }

        /// <summary>
        /// 虚拟的99999房间标识，用于结帐等特殊使用
        /// </summary>
        public static string Room9999 { get { return "99999"; } }
        /// <summary>
        /// Room9999的入住记录ID标识
        /// </summary>
        public static string Room9999RZID { get { return "0"; } }

        /// <summary>
        /// 试用版提示消息
        /// </summary>
        public static string TrialMessage { get { return "试用版不提供此功能，请申请注册版!"; } }

        #region 报表文件配置

        /// <summary>
        /// 入住登记表
        /// </summary>
        public static string ReportRuZhuDengJiBill { get { return ConfigurationManager.AppSettings["ReportRuZhuDengJiBill"]; } }
        /// <summary>
        /// 消费结账明细单
        /// </summary>
        public static string XiaoFeiMingXiBill { get { return ConfigurationManager.AppSettings["XiaoFeiMingXiBill"]; } }
        #endregion
    }
}
