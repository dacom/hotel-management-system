using System;
using System.Collections.Generic;
using System.Text;

namespace hotel.Common
{

    public enum LogAbstractEnum { 未知, 删除数据, 修改数据, 增加数据, 登录系统, 退出系统, 
        结账退房, 不结账退房, 单位挂账退房, 客人登记, 修改登记, 客人预订, 客人预订加房, 客人入住,
        修改预订, 预订取消, 预订删除,客人换房, 删除客单, 删除客人, 补交押金, 房态修改, 
        消费入单, 消费退单, 消费转单, 恢复入住, 恢复结账, 合并帐单, 拆分帐单, 
        手工夜审, 员工交班, 打印, 制卡 };
    public class LogMessage
    {
        public LogMessage()
        {
            Computer cp = Computer.Instance;
            Message = "";
            IP = cp.IpAddress;
            MAC = cp.MacAddress;
            Type = "用户";
            Abstract = LogAbstractEnum.未知;
        }
        public LogMessage(string message)
            : this()
        {
            Message = message;
        }
        public string UserName { get; set; }
        public string IP { get; set; }
        public string MAC { get; set; }
        /// <summary>
        /// 用户或系统分类
        /// </summary>
        public string Type { get; set; }
        public string Message { get; set; }
        /// <summary>
        /// 摘要
        /// </summary>
        public LogAbstractEnum Abstract { get; set; }

        public override string ToString()
        {
            return Message.ToString();
        }
    }
}
