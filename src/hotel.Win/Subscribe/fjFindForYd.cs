﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
using System.Data.Common;

namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 房间查询--预订
    /// </summary>
    public partial class fjFindForYd : Form
    {
        hotelEntities _context;
        List<string> _rooms = new List<string>();
        public fjFindForYd()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            var qry = from r in _context.ROOM.AsNoTracking()
                      join t in _context.ROOM_TYPE.AsNoTracking() on r.ROOM_TYPE.ID equals t.ID
                      //where r.LD == ld
                      orderby r.ID
                      select new
                      {
                          ROOMID = r.ID,
                          FJLX = t.JC,
                          ZT = r.ZT,
                          FJ = r.SKFJ
                          //YDBZ = r.YDBZ,
                      };
            gridfj.AutoGenerateColumns = false;
            gridfj.DataSource = qry.ToList();
        }
        /// <summary>
        /// 所有选择的房间号
        /// </summary>
        public List<string> Rooms { get { return _rooms; } }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (gridfj.CurrentRow == null)
                return;
            foreach (DataGridViewRow item in gridfj.Rows)
            {
                var obj = item.Cells["select"].Value;
                if (obj != null && (bool)obj == true)
                    _rooms.Add(item.Cells["ROOMID"].Value.ToString());
            }
            this.DialogResult = DialogResult.OK;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
