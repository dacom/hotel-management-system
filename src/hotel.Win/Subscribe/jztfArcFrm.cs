﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
namespace hotel.Win.Subscribe
{
    public partial class jztfArcFrm : Form
    {
        hotelEntities _context;
        RZJL_ARC rzjlModel;

        public jztfArcFrm(string rzzh)
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;           
            rzjlModel = _context.RZJL_ARC.First(p => p.ID == rzzh);
            decimal sumYj = 0, sumXf = 0;
            decimal? je;
            if (string.IsNullOrEmpty(rzjlModel.F_ID))
            {
                //一般或父单
                je = _context.XFJL_ARC.Where(p => p.RZJL_ID == rzjlModel.ID && p.XFXM_MC == "押金").Sum(p => p.YJ);
                sumYj = je ?? 0;
                je = _context.XFJL_ARC.Where(p => p.RZJL_ID == rzjlModel.ID && p.XFXM_MC != "押金").Sum(p => p.JE);
                sumXf = je ?? 0;
                
                //如果有子帐单
                var qry = from r in _context.RZJL_ARC
                      join x in _context.XFJL_ARC on r.ID equals x.RZJL_ID
                      where r.F_ID == rzjlModel.ID
                      select x;
                if (qry.Count() > 0)
                {
                    je = qry.Where(p => p.XFXM_MC == "押金").Sum(p => p.YJ);
                    sumYj += je??0;
                     je = qry.Where(p => p.XFXM_MC != "押金").Sum(p => p.JE);
                    sumXf +=je??0;
                }
            }
            else
            {
                //子单
                //自己及兄弟
                var qry = from r in _context.RZJL_ARC
                          join x in _context.XFJL_ARC on r.ID equals x.RZJL_ID
                          where r.F_ID == rzjlModel.F_ID
                          select x;
                if (qry.Count() > 0)
                {
                    je = qry.Where(p => p.XFXM_MC == "押金").Sum(p => p.YJ);
                    sumYj += je ?? 0;
                    je = qry.Where(p => p.XFXM_MC != "押金").Sum(p => p.JE);
                    sumXf += je ?? 0;
                }
                //父单自身
                qry = _context.XFJL_ARC.Where(p => p.RZJL_ID == rzjlModel.F_ID);
                je = qry.Where(p => p.XFXM_MC == "押金").Sum(p => p.YJ);
                sumYj += je ?? 0;
                je = qry.Where(p => p.XFXM_MC != "押金").Sum(p => p.JE);
                sumXf += je ?? 0;
            }
           
            //帐务信息
            txtysje.Value = sumYj;
            txtljxf.Value = sumXf;
            txtkrzf.Value = sumYj - sumXf;            
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (txtkrzf.Value < 0)
            {
                MessageBox.Show("客人押金不足，可以恢复入住，增加押金");
                return;
            }

            btnok.Enabled = false;
            rzjlModel.JZBZ = "是";
            rzjlModel.JZRQ = DateTime.Now;
            _context.SaveChanges();
            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.恢复结账;
            logInfo.RoomId = rzjlModel.ROOM_ID;
            logInfo.Rzid = rzjlModel.ID;
            logInfo.Message = "消费总金额:" + txtljxf.Value.ToString() + ",押金总额:" + txtysje.Value.ToString();
            logInfo.Save();            
            #endregion
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
        private void btnclose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

       
    }
}
