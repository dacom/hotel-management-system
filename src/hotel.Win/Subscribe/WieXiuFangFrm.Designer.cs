﻿namespace hotel.Win.Subscribe
{
    partial class WieXiuFangFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.maingrid = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnqdrz = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnclose = new System.Windows.Forms.Button();
            this.btnyd = new System.Windows.Forms.Button();
            this.btnquery = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // maingrid
            // 
            this.maingrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.maingrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maingrid.Location = new System.Drawing.Point(0, 102);
            this.maingrid.Name = "maingrid";
            this.maingrid.ReadOnly = true;
            this.maingrid.RowHeadersWidth = 25;
            this.maingrid.RowTemplate.Height = 23;
            this.maingrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.maingrid.Size = new System.Drawing.Size(764, 440);
            this.maingrid.TabIndex = 8;
            this.maingrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.maingrid_CellClick);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnqdrz);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(0, 56);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(764, 46);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "操作区域";
            // 
            // btnqdrz
            // 
            this.btnqdrz.Location = new System.Drawing.Point(394, 15);
            this.btnqdrz.Name = "btnqdrz";
            this.btnqdrz.Size = new System.Drawing.Size(167, 26);
            this.btnqdrz.TabIndex = 10;
            this.btnqdrz.Tag = "0";
            this.btnqdrz.Text = "确定[选中]房间维修好";
            this.btnqdrz.UseVisualStyleBackColor = true;
            this.btnqdrz.Click += new System.EventHandler(this.btnqdrz_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnclose);
            this.groupBox1.Controls.Add(this.btnyd);
            this.groupBox1.Controls.Add(this.btnquery);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(764, 56);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询条件";
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(640, 21);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(74, 25);
            this.btnclose.TabIndex = 9;
            this.btnclose.Tag = "0";
            this.btnclose.Text = "关闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btnyd
            // 
            this.btnyd.Location = new System.Drawing.Point(491, 22);
            this.btnyd.Name = "btnyd";
            this.btnyd.Size = new System.Drawing.Size(91, 25);
            this.btnyd.TabIndex = 9;
            this.btnyd.Tag = "0";
            this.btnyd.Text = "所有维修";
            this.btnyd.UseVisualStyleBackColor = true;
            this.btnyd.Click += new System.EventHandler(this.btnyd_Click);
            // 
            // btnquery
            // 
            this.btnquery.Location = new System.Drawing.Point(394, 22);
            this.btnquery.Name = "btnquery";
            this.btnquery.Size = new System.Drawing.Size(91, 25);
            this.btnquery.TabIndex = 9;
            this.btnquery.Tag = "0";
            this.btnquery.Text = "开始查询";
            this.btnquery.UseVisualStyleBackColor = true;
            this.btnquery.Click += new System.EventHandler(this.btnquery_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(258, 22);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(130, 23);
            this.dateTimePicker2.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(231, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 14);
            this.label2.TabIndex = 7;
            this.label2.Text = "到";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(98, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 23);
            this.dateTimePicker1.TabIndex = 6;
            this.dateTimePicker1.Value = new System.DateTime(2010, 12, 30, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "申请日期从";
            // 
            // WieXiuFangFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 542);
            this.Controls.Add(this.maingrid);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "WieXiuFangFrm";
            this.Text = "房间维修记录";
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView maingrid;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnqdrz;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnyd;
        private System.Windows.Forms.Button btnquery;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnclose;
    }
}