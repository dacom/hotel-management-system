﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
using System.Data.Common;
using hotel.Win.Query;
namespace hotel.Win.Subscribe
{
    public partial class rzhfMaintain : Form
    {
        List<string> roomIds;
        /// <summary>
        /// 不结帐客人恢复入住
        /// </summary>
        public rzhfMaintain()
        {
            InitializeComponent();
            roomIds = new List<string>();
            BindData();
        }

        /// <summary>
        /// 恢复入住的房间ID
        /// </summary>
        public List<string> RoomIds
        {
            get { return roomIds; }
        }
        private void BindData()
        {
            gridfj.AutoGenerateColumns = false;
            using (var _context = MyDataContext.GetDataContext)
            {
                var qry = from rz in _context.RZJL_ARC.AsNoTracking()
                          where rz.JZBZ == "否" && string.IsNullOrEmpty(rz.F_ID)
                          select new
                          {
                              ROOMID = rz.ROOM_ID,
                              KRXM = rz.RZRY_ARC.FirstOrDefault().KRXM,
                              rz.JZRQ,
                              RZZH = rz.ID,
                              BZSM = rz.BZSM
                          };
                gridfj.DataSource = qry.ToList();
            }
           
            btnok.Enabled = gridfj.Rows.Count > 0;
            btnjz.Enabled = btnok.Enabled;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {            
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            //恢复入住，不支持有子单的情况
            if (gridfj.CurrentRow == null)
                return;
            string roomid = gridfj.CurrentRow.Cells["ROOMID"].Value.ToString();
            if (MessageBox.Show("你确定房间" + roomid + "恢复入住吗?", SysConsts.SysInformationCaption, MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;
           
            string rzzh = gridfj.CurrentRow.Cells["RZZH"].Value.ToString();
            var _context = MyDataContext.GetDataContext;
            var rzjl = _context.RZJL_ARC.FirstOrDefault(p => p.ID == rzzh);
            if (rzjl == null)
                return;
             //合单的情况，不支持换房
            if (_context.RZJL_ARC.Count(p => p.F_ID == rzjl.ID) > 0)
            {
                //frm2中直接操作数据库
                rzhfMaintain2 frm2 = new rzhfMaintain2(rzjl.ID);
                if (frm2.ShowDialog() == DialogResult.OK)
                {
                    roomIds.AddRange(frm2.RoomIds);
                    this.DialogResult = DialogResult.OK;
                }
                return;
            }
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.恢复入住;
            //非联房情况的处理，支持换房          
            if (_context.RZJL.Count(p => p.ROOM.ID == roomid && p.RZBZ == "是") > 0)
            {
                if (MessageBox.Show("房间" + rzjl.ROOM_ID + "已经有人入住，是否换房？", SysConsts.SysInformationCaption, MessageBoxButtons.OKCancel) != DialogResult.OK)
                    return;
                //住在新的房间，
                kyhfMaintain frm = new kyhfMaintain(rzjl.ROOM_ID, (decimal)rzjl.SJFJ, "", 0);
                if (frm.ShowDialog() != DialogResult.OK)
                    return;
                string targetRoomId = frm.RoomId;
                decimal targetRoomFJ = frm.NewFJ;
                var targetRoom = _context.ROOM.FirstOrDefault(p => p.ID == targetRoomId);
                if (targetRoom == null)
                {
                    MessageBox.Show("新房间" + targetRoomId + "不是有效的房间号,请查证！");
                    return;
                }
                //目标房不能是在住
                if (_context.RZJL.FirstOrDefault(p => p.ROOM.ID == targetRoomId && p.RZBZ == "是") != null)
                {
                    MessageBox.Show("新房间" + targetRoomId + "已经有人在住,请查证！");
                    return;
                }
                _context.RZJL_RestoreHF(rzjl.ID, UserLoginInfo.FullName, targetRoomId, targetRoomFJ);
                roomIds.Add(targetRoomId);
                logInfo.RoomId = targetRoomId;
                logInfo.Rzid = rzjl.ID;
                logInfo.Message = "换新房，原房号" + roomid + ",新房号:" + targetRoomId;
            }
            else
            {
                //还住在原来的房间
                _context.RZJL_Restore(string.IsNullOrEmpty(rzjl.F_ID) ? rzjl.ID : rzjl.F_ID, UserLoginInfo.FullName);
                roomIds.Add(rzjl.ROOM_ID);
                logInfo.RoomId = rzjl.ROOM_ID;
                logInfo.Rzid = rzjl.ID;
                logInfo.Message = "原房间入住";
            }
             _context.Dispose();          
            logInfo.Save();
            this.DialogResult = DialogResult.OK;
        }

        private void btnjz_Click(object sender, EventArgs e)
        {
            //恢复入住，不支持有子单的情况
            if (gridfj.CurrentRow == null)
                return;             
            string rzzh = gridfj.CurrentRow.Cells["RZZH"].Value.ToString();
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjl = _context.RZJL_ARC.FirstOrDefault(p => p.ID == rzzh);
                if (rzjl == null)
                    return;
            }
            
            var frm = new jztfArcFrm(rzzh);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                BindData();
        }

        private void btnviewzw_Click(object sender, EventArgs e)
        {
            if (gridfj.CurrentRow == null)
                return;
            string rzzh = gridfj.CurrentRow.Cells["RZZH"].Value.ToString();
            var frm = new XiaoFeiDialog(rzzh);
            frm.ShowDialog();
        }
    }
}
