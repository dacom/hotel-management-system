﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Common;
using hotel.DAL;
namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 协议单位挂账
    /// </summary>
    public partial class xydwGzMaintain : Form
    {
        private hotelEntities _context;
        private RZJL rzjlModel;//入住记录模型
        decimal maxGzje;
        public xydwGzMaintain(RZJL rzjlModel,hotelEntities context,decimal maxGzje)
        {
            InitializeComponent();
            this.rzjlModel = rzjlModel;
            this._context = context;
            cobxxydw.DisplayMember = "DWMC";
            cobxxydw.DataSource = _context.XYDW.AsNoTracking().OrderBy(p => p.DWMC).ToList();
            txtgzje.Increment = maxGzje;
            lbzdgzje.Text = "最大挂账额:" + maxGzje.ToString();
            this.maxGzje = maxGzje;
        }
        public decimal Gzje { get; set; }
        public string Xgdm { get; set; }
        public string Bzsm { get; set; }
        public XYDW SelectedXYDW { get; private set; }

        private void btncose_Click(object sender, EventArgs e)            
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtbzsm.Focus();
            this.SelectedXYDW = cobxxydw.SelectedValue as XYDW;
            if (SelectedXYDW == null)
            {
                MessageBox.Show("请选择一个协议单位");
                return;
            }
            if (txtgzje.Value<=0)
            {
                MessageBox.Show("挂账金额不能小于0");
                return;
            }
            if (txtgzje.Value > maxGzje)
            {
                MessageBox.Show("挂账金额不能大于" + maxGzje.ToString());
                return;
            }
            this.Gzje = txtgzje.Value;
            this.Xgdm = txtxgdm.Text;
            this.Bzsm = txtbzsm.Text;
            
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
