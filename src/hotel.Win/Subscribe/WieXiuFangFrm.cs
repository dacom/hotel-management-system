﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;

namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 房间维修记录
    /// </summary>
    public partial class WieXiuFangFrm : Form
    {
        private hotelEntities _context;
        Action RefreshData;
        public WieXiuFangFrm()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            ControlsUtility.ResetColumnFromXML(maingrid);
            dateTimePicker1.Value = DateTime.Now.AddDays(-7).Date;
            dateTimePicker2.Value = DateTime.Now.Date;
            btnqdrz.Enabled = false;
            btnyd_Click(btnyd,null);
        }

        private void btnquery_Click(object sender, EventArgs e)
        {
            var rq2 = dateTimePicker2.Value.AddHours(23.9);
            var qry = from q in _context.ROOM_MAINTAIN.AsNoTracking()
                      where q.SQRQ >= dateTimePicker1.Value && q.SQRQ <= rq2
                      orderby q.SQRQ descending
                      select q;
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            RefreshData = () => { btnquery_Click(null, null); };
        }

        private void btnyd_Click(object sender, EventArgs e)
        {
            var qry = from q in _context.ROOM_MAINTAIN.AsNoTracking()
                      where q.WCBZ == "否"
                      orderby q.SQRQ descending
                      select q;
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            RefreshData = () => { btnyd_Click(null, null); };
        }

        private void btnqdrz_Click(object sender, EventArgs e)
        {
            var str = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            if (string.IsNullOrEmpty(str))
                return;
            int id = Convert.ToInt32(str);
            var selmodel = _context.ROOM_MAINTAIN.First(p => p.ID == id);
            if (selmodel.WCBZ == "是")
            {               
                return;
            }
            selmodel.WCBZ = "是";
            selmodel.WXRY = UserLoginInfo.FullName;
            selmodel.WXRQ = DateTime.Now;
            var room = _context.ROOM.FirstOrDefault(p => p.ID == selmodel.ROOM_ID);
            if (room != null && room.ZT == RoomStstusCnName.WeiXiu && room.RZBZ =="否")
            {
                room.ZT = RoomStstusCnName.KongJing;
            }
            _context.SaveChanges();

            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.修改数据;
            logInfo.RoomId = room.ID;
            logInfo.Message = "房间:" + room.ID + "维修好";
            logInfo.Save();
            #endregion
            MessageBox.Show("房间维修成功！");
            RefreshData();
        }

        private void maingrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (maingrid.CurrentRow == null)
                return;
            var model = maingrid.CurrentRow.DataBoundItem as ROOM_MAINTAIN;
            btnqdrz.Enabled = model.WCBZ == "否";
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
