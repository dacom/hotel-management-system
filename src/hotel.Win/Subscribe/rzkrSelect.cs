﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.DAL;
using hotel.Common;
using hotel.Win.BaseData;
using hotel.Win.Common;
using hotel.MyUserControl;
namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 入住客人选择
    /// </summary>
    public partial class rzkrSelect : Form
    {
        public rzkrSelect(List<RZJL> rzjls)
        {
            InitializeComponent();
            if (rzjls.Count == 0)
            {
                this.Close();
                return;
            }
            listView1.Items.Clear();
            foreach (var item in rzjls)
            {
                ListViewItem li = new ListViewItem();                
                li.Text = item.RZRY.Max(p => p.KRXM);
                li.Tag = item;
                if (item.RZBZ == "是")
                    li.ImageIndex = 0;
                else
                    li.ImageIndex = 1;
                listView1.Items.Add(li);
            }
            if (listView1.Items.Count > 0)
                listView1.Items[0].Selected = true;
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnsel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        public RZJL SelectedRZJL
        {
            get
            {
                return listView1.SelectedItems[0].Tag as RZJL;
            }
        }       
    }
}
