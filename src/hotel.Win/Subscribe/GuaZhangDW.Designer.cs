﻿namespace hotel.Win.Subscribe
{
    partial class GuaZhangDW
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucQueryToolBar1 = new hotel.Win.Query.ucQueryToolBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.maingrid = new System.Windows.Forms.DataGridView();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).BeginInit();
            this.SuspendLayout();
            // 
            // ucQueryToolBar1
            // 
            this.ucQueryToolBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.ucQueryToolBar1.Location = new System.Drawing.Point(586, 0);
            this.ucQueryToolBar1.Name = "ucQueryToolBar1";
            this.ucQueryToolBar1.Size = new System.Drawing.Size(84, 460);
            this.ucQueryToolBar1.TabIndex = 7;
            this.ucQueryToolBar1.VisibleClose = true;
            this.ucQueryToolBar1.VisibleCombine = false;
            this.ucQueryToolBar1.VisibleExpExcel = true;
            this.ucQueryToolBar1.VisibleFind = false;
            this.ucQueryToolBar1.VisibleRefresh = true;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(131, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 460);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(670, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "ff";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.maingrid);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(586, 460);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "挂帐单位列表";
            // 
            // maingrid
            // 
            this.maingrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.maingrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maingrid.Location = new System.Drawing.Point(3, 19);
            this.maingrid.Name = "maingrid";
            this.maingrid.ReadOnly = true;
            this.maingrid.RowHeadersWidth = 25;
            this.maingrid.RowTemplate.Height = 23;
            this.maingrid.Size = new System.Drawing.Size(580, 438);
            this.maingrid.TabIndex = 1;
            // 
            // GuaZhangDW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 482);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ucQueryToolBar1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "GuaZhangDW";
            this.Text = "挂帐单位帐务";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private hotel.Win.Query.ucQueryToolBar ucQueryToolBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView maingrid;
    }
}