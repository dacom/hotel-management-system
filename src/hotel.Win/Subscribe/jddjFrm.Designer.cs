﻿namespace hotel.Win.Subscribe
{
    partial class jddjFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cobxsyjg = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtfz = new System.Windows.Forms.NumericUpDown();
            this.txtzk = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbfl = new System.Windows.Forms.Label();
            this.lbysj = new System.Windows.Forms.Label();
            this.lbfh = new System.Windows.Forms.Label();
            this.lbzh = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnyc = new System.Windows.Forms.Button();
            this.btnzj = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.grid2 = new System.Windows.Forms.DataGridView();
            this.GRID2_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtFind = new System.Windows.Forms.TextBox();
            this.grid1 = new System.Windows.Forms.DataGridView();
            this.GRID1_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GRID1_FJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GRID1_FL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GRID1_YD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnclose = new System.Windows.Forms.Button();
            this.btnprintJy = new System.Windows.Forms.Button();
            this.plrd = new System.Windows.Forms.Panel();
            this.btnjydf = new System.Windows.Forms.Button();
            this.btncxrd = new System.Windows.Forms.Button();
            this.btnydrz = new System.Windows.Forms.Button();
            this.plrz = new System.Windows.Forms.Panel();
            this.btnyjcl = new System.Windows.Forms.Button();
            this.btnxf = new System.Windows.Forms.Button();
            this.btnjz = new System.Windows.Forms.Button();
            this.cobxzzfs = new System.Windows.Forms.ComboBox();
            this.cobxtdmc = new System.Windows.Forms.ComboBox();
            this.cobxxydw = new System.Windows.Forms.ComboBox();
            this.cobxkrlb = new System.Windows.Forms.ComboBox();
            this.dtpldrq = new System.Windows.Forms.DateTimePicker();
            this.dtpddrq = new System.Windows.Forms.DateTimePicker();
            this.txtrs = new System.Windows.Forms.NumericUpDown();
            this.txtyj = new System.Windows.Forms.NumericUpDown();
            this.txtts = new System.Windows.Forms.NumericUpDown();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabkrxx = new System.Windows.Forms.TabControl();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnclose2 = new System.Windows.Forms.Button();
            this.btndy = new System.Windows.Forms.Button();
            this.btnkrzj = new System.Windows.Forms.Button();
            this.btnkrsc = new System.Windows.Forms.Button();
            this.txtkrld = new System.Windows.Forms.Button();
            this.btnksbc = new System.Windows.Forms.Button();
            this.tabMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtzk)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid1)).BeginInit();
            this.panel1.SuspendLayout();
            this.plrd.SuspendLayout();
            this.plrz.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtrs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtts)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPage1);
            this.tabMain.Controls.Add(this.tabPage2);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(638, 471);
            this.tabMain.TabIndex = 0;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cobxsyjg);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.txtfz);
            this.tabPage1.Controls.Add(this.txtzk);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.cobxzzfs);
            this.tabPage1.Controls.Add(this.cobxtdmc);
            this.tabPage1.Controls.Add(this.cobxxydw);
            this.tabPage1.Controls.Add(this.cobxkrlb);
            this.tabPage1.Controls.Add(this.dtpldrq);
            this.tabPage1.Controls.Add(this.dtpddrq);
            this.tabPage1.Controls.Add(this.txtrs);
            this.tabPage1.Controls.Add(this.txtyj);
            this.tabPage1.Controls.Add(this.txtts);
            this.tabPage1.Controls.Add(this.txtbzsm);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(630, 444);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "房间信息";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cobxsyjg
            // 
            this.cobxsyjg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxsyjg.FormattingEnabled = true;
            this.cobxsyjg.Location = new System.Drawing.Point(90, 78);
            this.cobxsyjg.Name = "cobxsyjg";
            this.cobxsyjg.Size = new System.Drawing.Size(124, 21);
            this.cobxsyjg.TabIndex = 13;
            this.cobxsyjg.SelectedIndexChanged += new System.EventHandler(this.cobxsyjg_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 14);
            this.label15.TabIndex = 12;
            this.label15.Text = "协议价格:";
            // 
            // txtfz
            // 
            this.txtfz.DecimalPlaces = 2;
            this.txtfz.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtfz.Location = new System.Drawing.Point(297, 78);
            this.txtfz.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtfz.Name = "txtfz";
            this.txtfz.Size = new System.Drawing.Size(104, 23);
            this.txtfz.TabIndex = 2;
            this.txtfz.Leave += new System.EventHandler(this.txtfz_Leave);
            // 
            // txtzk
            // 
            this.txtzk.DecimalPlaces = 2;
            this.txtzk.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.txtzk.Location = new System.Drawing.Point(467, 76);
            this.txtzk.Name = "txtzk";
            this.txtzk.Size = new System.Drawing.Size(69, 23);
            this.txtzk.TabIndex = 1;
            this.txtzk.Leave += new System.EventHandler(this.txtzk_Leave);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbfl);
            this.groupBox3.Controls.Add(this.lbysj);
            this.groupBox3.Controls.Add(this.lbfh);
            this.groupBox3.Controls.Add(this.lbzh);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(21, 2);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox3.Size = new System.Drawing.Size(602, 38);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            // 
            // lbfl
            // 
            this.lbfl.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbfl.Location = new System.Drawing.Point(312, 17);
            this.lbfl.Name = "lbfl";
            this.lbfl.Size = new System.Drawing.Size(69, 15);
            this.lbfl.TabIndex = 7;
            this.lbfl.Text = "标双";
            // 
            // lbysj
            // 
            this.lbysj.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbysj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbysj.Location = new System.Drawing.Point(455, 17);
            this.lbysj.Name = "lbysj";
            this.lbysj.Size = new System.Drawing.Size(60, 13);
            this.lbysj.TabIndex = 6;
            this.lbysj.Text = "888";
            // 
            // lbfh
            // 
            this.lbfh.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbfh.Location = new System.Drawing.Point(197, 17);
            this.lbfh.Name = "lbfh";
            this.lbfh.Size = new System.Drawing.Size(56, 12);
            this.lbfh.TabIndex = 5;
            this.lbfh.Text = "8888";
            // 
            // lbzh
            // 
            this.lbzh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbzh.Location = new System.Drawing.Point(51, 17);
            this.lbzh.Name = "lbzh";
            this.lbzh.Size = new System.Drawing.Size(85, 16);
            this.lbzh.TabIndex = 4;
            this.lbzh.Text = "A1005168888";
            this.lbzh.Click += new System.EventHandler(this.lbzh_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 2;
            this.label14.Text = "帐号：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(396, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 2;
            this.label19.Text = "预设价：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(271, 17);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 2;
            this.label18.Text = "房类：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(152, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "房号：";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 246);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(624, 151);
            this.panel2.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnyc);
            this.panel3.Controls.Add(this.btnzj);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(288, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(92, 151);
            this.panel3.TabIndex = 2;
            // 
            // btnyc
            // 
            this.btnyc.Location = new System.Drawing.Point(6, 64);
            this.btnyc.Name = "btnyc";
            this.btnyc.Size = new System.Drawing.Size(67, 23);
            this.btnyc.TabIndex = 22;
            this.btnyc.Text = "<-移除";
            this.btnyc.UseVisualStyleBackColor = true;
            this.btnyc.Click += new System.EventHandler(this.btnyc_Click);
            // 
            // btnzj
            // 
            this.btnzj.Location = new System.Drawing.Point(6, 38);
            this.btnzj.Name = "btnzj";
            this.btnzj.Size = new System.Drawing.Size(67, 23);
            this.btnzj.TabIndex = 21;
            this.btnzj.Text = "增加->";
            this.btnzj.UseVisualStyleBackColor = true;
            this.btnzj.Click += new System.EventHandler(this.btnzj_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.grid2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox2.Location = new System.Drawing.Point(380, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(244, 151);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "已选房间";
            // 
            // grid2
            // 
            this.grid2.AllowUserToAddRows = false;
            this.grid2.AllowUserToDeleteRows = false;
            this.grid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GRID2_ID,
            this.Column3,
            this.Column2});
            this.grid2.Location = new System.Drawing.Point(3, 19);
            this.grid2.Name = "grid2";
            this.grid2.ReadOnly = true;
            this.grid2.RowHeadersWidth = 20;
            this.grid2.RowTemplate.Height = 23;
            this.grid2.Size = new System.Drawing.Size(230, 110);
            this.grid2.TabIndex = 23;
            // 
            // GRID2_ID
            // 
            this.GRID2_ID.DataPropertyName = "ID";
            this.GRID2_ID.HeaderText = "房号";
            this.GRID2_ID.Name = "GRID2_ID";
            this.GRID2_ID.ReadOnly = true;
            this.GRID2_ID.Width = 60;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "FJ";
            this.Column3.HeaderText = "房价";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 60;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "FJZL";
            this.Column2.HeaderText = "房类";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 80;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFind);
            this.groupBox1.Controls.Add(this.grid1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 151);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "可选房间";
            // 
            // txtFind
            // 
            this.txtFind.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtFind.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtFind.Location = new System.Drawing.Point(71, -1);
            this.txtFind.Margin = new System.Windows.Forms.Padding(0);
            this.txtFind.MaxLength = 8;
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(67, 21);
            this.txtFind.TabIndex = 10;
            this.txtFind.TextChanged += new System.EventHandler(this.txtFind_TextChanged);
            // 
            // grid1
            // 
            this.grid1.AllowUserToAddRows = false;
            this.grid1.AllowUserToDeleteRows = false;
            this.grid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GRID1_ID,
            this.GRID1_FJ,
            this.GRID1_FL,
            this.GRID1_YD});
            this.grid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid1.Location = new System.Drawing.Point(3, 17);
            this.grid1.Name = "grid1";
            this.grid1.ReadOnly = true;
            this.grid1.RowHeadersWidth = 20;
            this.grid1.RowTemplate.Height = 23;
            this.grid1.Size = new System.Drawing.Size(282, 131);
            this.grid1.TabIndex = 20;
            // 
            // GRID1_ID
            // 
            this.GRID1_ID.DataPropertyName = "ID";
            this.GRID1_ID.HeaderText = "房号";
            this.GRID1_ID.Name = "GRID1_ID";
            this.GRID1_ID.ReadOnly = true;
            this.GRID1_ID.Width = 60;
            // 
            // GRID1_FJ
            // 
            this.GRID1_FJ.DataPropertyName = "FJ";
            this.GRID1_FJ.HeaderText = "房价";
            this.GRID1_FJ.Name = "GRID1_FJ";
            this.GRID1_FJ.ReadOnly = true;
            this.GRID1_FJ.Width = 60;
            // 
            // GRID1_FL
            // 
            this.GRID1_FL.DataPropertyName = "FJZL";
            this.GRID1_FL.HeaderText = "房类";
            this.GRID1_FL.Name = "GRID1_FL";
            this.GRID1_FL.ReadOnly = true;
            this.GRID1_FL.Width = 70;
            // 
            // GRID1_YD
            // 
            this.GRID1_YD.DataPropertyName = "YDF";
            this.GRID1_YD.HeaderText = "预订";
            this.GRID1_YD.Name = "GRID1_YD";
            this.GRID1_YD.ReadOnly = true;
            this.GRID1_YD.Width = 60;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnsave);
            this.panel1.Controls.Add(this.btnclose);
            this.panel1.Controls.Add(this.btnprintJy);
            this.panel1.Controls.Add(this.plrd);
            this.panel1.Controls.Add(this.plrz);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 397);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(624, 44);
            this.panel1.TabIndex = 6;
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(404, 11);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(82, 25);
            this.btnsave.TabIndex = 30;
            this.btnsave.Text = "保存";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(492, 11);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(91, 25);
            this.btnclose.TabIndex = 31;
            this.btnclose.Text = "关闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btnprintJy
            // 
            this.btnprintJy.Location = new System.Drawing.Point(317, 11);
            this.btnprintJy.Name = "btnprintJy";
            this.btnprintJy.Size = new System.Drawing.Size(82, 25);
            this.btnprintJy.TabIndex = 32;
            this.btnprintJy.Text = "打印押金";
            this.btnprintJy.UseVisualStyleBackColor = true;
            this.btnprintJy.Visible = false;
            this.btnprintJy.Click += new System.EventHandler(this.btnprintJy_Click);
            // 
            // plrd
            // 
            this.plrd.Controls.Add(this.btnjydf);
            this.plrd.Controls.Add(this.btncxrd);
            this.plrd.Controls.Add(this.btnydrz);
            this.plrd.Dock = System.Windows.Forms.DockStyle.Left;
            this.plrd.Location = new System.Drawing.Point(382, 0);
            this.plrd.Name = "plrd";
            this.plrd.Size = new System.Drawing.Size(350, 44);
            this.plrd.TabIndex = 3;
            // 
            // btnjydf
            // 
            this.btnjydf.Location = new System.Drawing.Point(82, 8);
            this.btnjydf.Name = "btnjydf";
            this.btnjydf.Size = new System.Drawing.Size(85, 25);
            this.btnjydf.TabIndex = 27;
            this.btnjydf.Text = "加预订房";
            this.btnjydf.UseVisualStyleBackColor = true;
            this.btnjydf.Click += new System.EventHandler(this.btnjydf_Click);
            // 
            // btncxrd
            // 
            this.btncxrd.Location = new System.Drawing.Point(169, 8);
            this.btncxrd.Name = "btncxrd";
            this.btncxrd.Size = new System.Drawing.Size(84, 25);
            this.btncxrd.TabIndex = 28;
            this.btncxrd.Text = "取消预订";
            this.btncxrd.UseVisualStyleBackColor = true;
            this.btncxrd.Click += new System.EventHandler(this.btncxrd_Click);
            // 
            // btnydrz
            // 
            this.btnydrz.Location = new System.Drawing.Point(255, 8);
            this.btnydrz.Name = "btnydrz";
            this.btnydrz.Size = new System.Drawing.Size(84, 25);
            this.btnydrz.TabIndex = 29;
            this.btnydrz.Text = "预订入住";
            this.btnydrz.UseVisualStyleBackColor = true;
            this.btnydrz.Click += new System.EventHandler(this.btnydrz_Click);
            // 
            // plrz
            // 
            this.plrz.Controls.Add(this.btnyjcl);
            this.plrz.Controls.Add(this.btnxf);
            this.plrz.Controls.Add(this.btnjz);
            this.plrz.Dock = System.Windows.Forms.DockStyle.Left;
            this.plrz.Location = new System.Drawing.Point(0, 0);
            this.plrz.Name = "plrz";
            this.plrz.Size = new System.Drawing.Size(382, 44);
            this.plrz.TabIndex = 2;
            // 
            // btnyjcl
            // 
            this.btnyjcl.Location = new System.Drawing.Point(82, 8);
            this.btnyjcl.Name = "btnyjcl";
            this.btnyjcl.Size = new System.Drawing.Size(85, 25);
            this.btnyjcl.TabIndex = 24;
            this.btnyjcl.Text = "押金处理";
            this.btnyjcl.UseVisualStyleBackColor = true;
            this.btnyjcl.Click += new System.EventHandler(this.btnyjcl_Click);
            // 
            // btnxf
            // 
            this.btnxf.Location = new System.Drawing.Point(169, 8);
            this.btnxf.Name = "btnxf";
            this.btnxf.Size = new System.Drawing.Size(84, 25);
            this.btnxf.TabIndex = 24;
            this.btnxf.Text = "费用入单";
            this.btnxf.UseVisualStyleBackColor = true;
            this.btnxf.Click += new System.EventHandler(this.btnxf_Click);
            // 
            // btnjz
            // 
            this.btnjz.Location = new System.Drawing.Point(255, 8);
            this.btnjz.Name = "btnjz";
            this.btnjz.Size = new System.Drawing.Size(84, 25);
            this.btnjz.TabIndex = 26;
            this.btnjz.Text = "结帐退房";
            this.btnjz.UseVisualStyleBackColor = true;
            this.btnjz.Click += new System.EventHandler(this.btnjz_Click);
            // 
            // cobxzzfs
            // 
            this.cobxzzfs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxzzfs.FormattingEnabled = true;
            this.cobxzzfs.Location = new System.Drawing.Point(90, 140);
            this.cobxzzfs.Name = "cobxzzfs";
            this.cobxzzfs.Size = new System.Drawing.Size(156, 21);
            this.cobxzzfs.TabIndex = 9;
            // 
            // cobxtdmc
            // 
            this.cobxtdmc.FormattingEnabled = true;
            this.cobxtdmc.Location = new System.Drawing.Point(369, 170);
            this.cobxtdmc.Name = "cobxtdmc";
            this.cobxtdmc.Size = new System.Drawing.Size(237, 21);
            this.cobxtdmc.TabIndex = 10;
            // 
            // cobxxydw
            // 
            this.cobxxydw.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxxydw.FormattingEnabled = true;
            this.cobxxydw.Location = new System.Drawing.Point(369, 137);
            this.cobxxydw.Name = "cobxxydw";
            this.cobxxydw.Size = new System.Drawing.Size(237, 21);
            this.cobxxydw.TabIndex = 8;
            this.cobxxydw.DropDown += new System.EventHandler(this.cobxxydw_DropDown);
            // 
            // cobxkrlb
            // 
            this.cobxkrlb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxkrlb.FormattingEnabled = true;
            this.cobxkrlb.Location = new System.Drawing.Point(90, 51);
            this.cobxkrlb.Name = "cobxkrlb";
            this.cobxkrlb.Size = new System.Drawing.Size(124, 21);
            this.cobxkrlb.TabIndex = 0;
            this.cobxkrlb.SelectedIndexChanged += new System.EventHandler(this.cobxkrlb_SelectedIndexChanged);
            // 
            // dtpldrq
            // 
            this.dtpldrq.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dtpldrq.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpldrq.Location = new System.Drawing.Point(445, 107);
            this.dtpldrq.Name = "dtpldrq";
            this.dtpldrq.Size = new System.Drawing.Size(161, 23);
            this.dtpldrq.TabIndex = 7;
            this.dtpldrq.ValueChanged += new System.EventHandler(this.dtpldrq_ValueChanged);
            // 
            // dtpddrq
            // 
            this.dtpddrq.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dtpddrq.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpddrq.Location = new System.Drawing.Point(90, 107);
            this.dtpddrq.Name = "dtpddrq";
            this.dtpddrq.Size = new System.Drawing.Size(152, 23);
            this.dtpddrq.TabIndex = 5;
            this.dtpddrq.ValueChanged += new System.EventHandler(this.dtpldrq_ValueChanged);
            // 
            // txtrs
            // 
            this.txtrs.Location = new System.Drawing.Point(296, 49);
            this.txtrs.Name = "txtrs";
            this.txtrs.Size = new System.Drawing.Size(105, 23);
            this.txtrs.TabIndex = 4;
            // 
            // txtyj
            // 
            this.txtyj.BackColor = System.Drawing.Color.Lime;
            this.txtyj.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtyj.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtyj.Location = new System.Drawing.Point(90, 170);
            this.txtyj.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtyj.Name = "txtyj";
            this.txtyj.Size = new System.Drawing.Size(156, 23);
            this.txtyj.TabIndex = 3;
            this.txtyj.ThousandsSeparator = true;
            // 
            // txtts
            // 
            this.txtts.Location = new System.Drawing.Point(310, 107);
            this.txtts.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtts.Name = "txtts";
            this.txtts.Size = new System.Drawing.Size(46, 23);
            this.txtts.TabIndex = 6;
            this.txtts.Leave += new System.EventHandler(this.txtts_Leave);
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(90, 204);
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(516, 23);
            this.txtbzsm.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(297, 173);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "团队名称：";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(411, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "折扣:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(294, 140);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 14);
            this.label12.TabIndex = 0;
            this.label12.Text = "协议单位：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 14);
            this.label9.TabIndex = 0;
            this.label9.Text = "支付方式:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 207);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 0;
            this.label11.Text = "备注说明：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "客人类别:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 172);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "预收押金：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(221, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 14);
            this.label10.TabIndex = 0;
            this.label10.Text = "入住人数:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(255, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "天数：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(362, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "离店日期：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "抵店日期:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(235, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "房  价:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabkrxx);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(630, 444);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "客人信息";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabkrxx
            // 
            this.tabkrxx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabkrxx.Location = new System.Drawing.Point(3, 3);
            this.tabkrxx.Name = "tabkrxx";
            this.tabkrxx.SelectedIndex = 0;
            this.tabkrxx.Size = new System.Drawing.Size(631, 402);
            this.tabkrxx.TabIndex = 1;
            this.tabkrxx.SelectedIndexChanged += new System.EventHandler(this.tabkrxx_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnclose2);
            this.panel4.Controls.Add(this.btndy);
            this.panel4.Controls.Add(this.btnkrzj);
            this.panel4.Controls.Add(this.btnkrsc);
            this.panel4.Controls.Add(this.txtkrld);
            this.panel4.Controls.Add(this.btnksbc);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 405);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(631, 36);
            this.panel4.TabIndex = 0;
            // 
            // btnclose2
            // 
            this.btnclose2.Location = new System.Drawing.Point(489, 6);
            this.btnclose2.Name = "btnclose2";
            this.btnclose2.Size = new System.Drawing.Size(73, 25);
            this.btnclose2.TabIndex = 4;
            this.btnclose2.Text = "关闭";
            this.btnclose2.UseVisualStyleBackColor = true;
            this.btnclose2.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btndy
            // 
            this.btndy.Location = new System.Drawing.Point(392, 6);
            this.btndy.Name = "btndy";
            this.btndy.Size = new System.Drawing.Size(91, 25);
            this.btndy.TabIndex = 3;
            this.btndy.Text = "打印登记单";
            this.btndy.UseVisualStyleBackColor = true;
            this.btndy.Click += new System.EventHandler(this.btndy_Click);
            // 
            // btnkrzj
            // 
            this.btnkrzj.Location = new System.Drawing.Point(59, 6);
            this.btnkrzj.Name = "btnkrzj";
            this.btnkrzj.Size = new System.Drawing.Size(78, 25);
            this.btnkrzj.TabIndex = 2;
            this.btnkrzj.Text = "增加客人";
            this.btnkrzj.UseVisualStyleBackColor = true;
            this.btnkrzj.Click += new System.EventHandler(this.btnkrzj_Click);
            // 
            // btnkrsc
            // 
            this.btnkrsc.Location = new System.Drawing.Point(143, 6);
            this.btnkrsc.Name = "btnkrsc";
            this.btnkrsc.Size = new System.Drawing.Size(67, 25);
            this.btnkrsc.TabIndex = 2;
            this.btnkrsc.Text = "删除";
            this.btnkrsc.UseVisualStyleBackColor = true;
            this.btnkrsc.Click += new System.EventHandler(this.btnkrsc_Click);
            // 
            // txtkrld
            // 
            this.txtkrld.Location = new System.Drawing.Point(216, 6);
            this.txtkrld.Name = "txtkrld";
            this.txtkrld.Size = new System.Drawing.Size(67, 25);
            this.txtkrld.TabIndex = 2;
            this.txtkrld.Text = "离店";
            this.txtkrld.UseVisualStyleBackColor = true;
            this.txtkrld.Click += new System.EventHandler(this.txtkrld_Click);
            // 
            // btnksbc
            // 
            this.btnksbc.Location = new System.Drawing.Point(298, 6);
            this.btnksbc.Name = "btnksbc";
            this.btnksbc.Size = new System.Drawing.Size(74, 25);
            this.btnksbc.TabIndex = 2;
            this.btnksbc.Text = "保存";
            this.btnksbc.UseVisualStyleBackColor = true;
            this.btnksbc.Click += new System.EventHandler(this.btnksbc_Click);
            // 
            // jddjFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 471);
            this.Controls.Add(this.tabMain);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "jddjFrm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "jddjFrm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.jddjFrm_FormClosing);
            this.Shown += new System.EventHandler(this.jddjFrm_Shown);
            this.tabMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtzk)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.plrd.ResumeLayout(false);
            this.plrz.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtrs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtts)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DateTimePicker dtpddrq;
        private System.Windows.Forms.NumericUpDown txtts;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpldrq;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cobxkrlb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cobxzzfs;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown txtrs;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cobxxydw;
        private System.Windows.Forms.ComboBox cobxtdmc;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnyc;
        private System.Windows.Forms.Button btnzj;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView grid2;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.TabControl tabkrxx;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnksbc;
        private System.Windows.Forms.Button btnkrzj;
        private System.Windows.Forms.Button btnkrsc;
        private System.Windows.Forms.Button txtkrld;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbfl;
        private System.Windows.Forms.Label lbysj;
        private System.Windows.Forms.Label lbfh;
        private System.Windows.Forms.Label lbzh;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel plrz;
        private System.Windows.Forms.Button btnyjcl;
        private System.Windows.Forms.Button btnxf;
        private System.Windows.Forms.Button btnjz;
        private System.Windows.Forms.Panel plrd;
        private System.Windows.Forms.Button btnjydf;
        private System.Windows.Forms.Button btncxrd;
        private System.Windows.Forms.Button btnydrz;
        private System.Windows.Forms.DataGridView grid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn GRID2_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn GRID1_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn GRID1_FJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn GRID1_FL;
        private System.Windows.Forms.DataGridViewTextBoxColumn GRID1_YD;
        private System.Windows.Forms.NumericUpDown txtfz;
        private System.Windows.Forms.NumericUpDown txtzk;
        private System.Windows.Forms.NumericUpDown txtyj;
        private System.Windows.Forms.TextBox txtFind;
        private System.Windows.Forms.Button btndy;
        private System.Windows.Forms.Button btnclose2;
        private System.Windows.Forms.Button btnprintJy;
        private System.Windows.Forms.ComboBox cobxsyjg;
        private System.Windows.Forms.Label label15;
    }
}