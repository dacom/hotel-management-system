﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;

namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 协议单位查找
    /// </summary>
    public partial class xydwFind : Form
    {
        private hotelEntities _context;
        public xydwFind()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            InitDWData();
        }

        private void InitDWData()
        {
            griddw.AutoGenerateColumns = false;
            griddw.DataSource = _context.XYDW.AsNoTracking().OrderBy(p => p.DWMC).ToList();
        }
        private void InitXYFJData(int flid)
        {
            gridfj.AutoGenerateColumns = false;
            var qry = from r in _context.XYFJ.AsNoTracking()
                      join t in _context.ROOM_TYPE.AsNoTracking() on r.ROOM_TYPE.ID equals t.ID
                      where r.XYDW.ID == flid
                      orderby r.ID
                      select new
                      {
                          r.BZSM,
                          r.CJRQ,
                          r.CJRY,
                          r.FJ,
                          r.ID,
                          r.XGRQ,
                          r.XGRY,
                          r.YJ,
                          FL = t.JC
                      };
            gridfj.DataSource = qry.ToList();
        }

        private void griddw_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var dwid = Common.ControlsUtility.GetActiveGridValue(griddw, "DWID");
            if (string.IsNullOrEmpty(dwid))
                return;
            int flid = Convert.ToInt32(dwid);
            InitXYFJData(flid);
        }

        private void btnfind_Click(object sender, EventArgs e)
        {
            griddw.AutoGenerateColumns = false;
            griddw.DataSource = _context.XYDW.AsNoTracking().OrderBy(p => p.DWMC).ToList();
        }

        public XYDW SelectedXYDW { get; private set; }
       
        private void griddw_DoubleClick(object sender, EventArgs e)
        {
            var dwid = Common.ControlsUtility.GetActiveGridValue(griddw, "DWID");
            if (string.IsNullOrEmpty(dwid))
                return;
            int flid = Convert.ToInt32(dwid);
            SelectedXYDW = _context.XYDW.First(p => p.ID == flid);            
            this.DialogResult = DialogResult.OK;
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            griddw_DoubleClick(btnclose ,null);
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
