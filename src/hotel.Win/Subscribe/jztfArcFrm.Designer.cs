﻿namespace hotel.Win.Subscribe
{
    partial class jztfArcFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtysje = new System.Windows.Forms.NumericUpDown();
            this.txtkrzf = new System.Windows.Forms.NumericUpDown();
            this.txtljxf = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.btnclose = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtysje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkrzf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtljxf)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtysje);
            this.groupBox1.Controls.Add(this.txtkrzf);
            this.groupBox1.Controls.Add(this.txtljxf);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(232, 134);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "账务信息";
            // 
            // txtysje
            // 
            this.txtysje.DecimalPlaces = 2;
            this.txtysje.Enabled = false;
            this.txtysje.Location = new System.Drawing.Point(84, 52);
            this.txtysje.Maximum = new decimal(new int[] {
            9000000,
            0,
            0,
            0});
            this.txtysje.Name = "txtysje";
            this.txtysje.ReadOnly = true;
            this.txtysje.Size = new System.Drawing.Size(100, 23);
            this.txtysje.TabIndex = 18;
            this.txtysje.ThousandsSeparator = true;
            // 
            // txtkrzf
            // 
            this.txtkrzf.BackColor = System.Drawing.Color.Lime;
            this.txtkrzf.DecimalPlaces = 2;
            this.txtkrzf.Enabled = false;
            this.txtkrzf.Location = new System.Drawing.Point(84, 81);
            this.txtkrzf.Maximum = new decimal(new int[] {
            90000000,
            0,
            0,
            0});
            this.txtkrzf.Minimum = new decimal(new int[] {
            9000000,
            0,
            0,
            -2147483648});
            this.txtkrzf.Name = "txtkrzf";
            this.txtkrzf.ReadOnly = true;
            this.txtkrzf.Size = new System.Drawing.Size(100, 23);
            this.txtkrzf.TabIndex = 0;
            this.txtkrzf.ThousandsSeparator = true;
            // 
            // txtljxf
            // 
            this.txtljxf.DecimalPlaces = 2;
            this.txtljxf.Enabled = false;
            this.txtljxf.Location = new System.Drawing.Point(84, 22);
            this.txtljxf.Maximum = new decimal(new int[] {
            9000000,
            0,
            0,
            0});
            this.txtljxf.Name = "txtljxf";
            this.txtljxf.ReadOnly = true;
            this.txtljxf.Size = new System.Drawing.Size(100, 23);
            this.txtljxf.TabIndex = 18;
            this.txtljxf.ThousandsSeparator = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 16;
            this.label6.Text = "押金:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 14);
            this.label8.TabIndex = 16;
            this.label8.Text = "客人返还:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "累计消费:";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(39, 140);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(95, 29);
            this.btnok.TabIndex = 19;
            this.btnok.Text = "人民币结账";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(152, 140);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(68, 28);
            this.btnclose.TabIndex = 20;
            this.btnclose.Text = "关闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // jztfArcFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 182);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "jztfArcFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "不结账退房-结转";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtysje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkrzf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtljxf)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown txtkrzf;
        private System.Windows.Forms.NumericUpDown txtysje;
        private System.Windows.Forms.NumericUpDown txtljxf;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Button btnclose;
    }
}