﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
using hotel.DAL;
using hotel.Win.BaseData;
using hotel.Win.Subscribe;
using hotel.Common;

namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 客人换房
    /// </summary>
    public partial class kyhfMaintain : Form
    {
         
        /// <summary>
        /// 
        /// </summary>
        /// <param name="yfh">原房号</param>
        /// <param name="yfj">原房价</param>
        /// <param name="xfh">新房号</param>
        /// <param name="xfj">新房价</param>
        public kyhfMaintain(string yfh,decimal yfj,string xfh,decimal xfj)
        {
            InitializeComponent();
            txtyfh.Text = yfh;
            txtyfj.Text = yfj.ToString();
            txtxfh.Text = xfh;
            txtxfj.Value = xfj;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (txtxfh.Text.Length == 0)
                return;
            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// 新房号
        /// </summary>
        public string RoomId { get { return txtxfh.Text; } }
        /// <summary>
        /// 新房价
        /// </summary>
        public decimal NewFJ { get { return txtxfj.Value; } }
        /// <summary>
        /// 换房原因
        /// </summary>
        public string YY { get { return txthfyy.Text; } }

        private void txtxfh_TextChanged(object sender, EventArgs e)
        {
            if (txtxfh.Text.Length < 4)
                return;
            using (var _context = MyDataContext.GetDataContext)
            {
                var room = _context.ROOM.AsNoTracking().FirstOrDefault(p => p.ID == txtxfh.Text);
                if (room != null)
                    txtxfj.Value = (decimal)room.SKFJ;
                else
                    txtxfj.Value = 0;
            }
           
        }
    }
}
