﻿namespace hotel.Win.Subscribe
{
    partial class fjFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btncancel = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gridfj = new System.Windows.Forms.DataGridView();
            this.ROOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KRXM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RZZH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btncancel);
            this.panel1.Controls.Add(this.btnok);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(546, 50);
            this.panel1.TabIndex = 1;
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(342, 12);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(68, 28);
            this.btncancel.TabIndex = 2;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(261, 12);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(68, 28);
            this.btnok.TabIndex = 1;
            this.btnok.Text = "选定";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "请选择目标房间";
            // 
            // gridfj
            // 
            this.gridfj.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridfj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROOMID,
            this.KRXM,
            this.Column1,
            this.Column2,
            this.RZZH});
            this.gridfj.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridfj.Location = new System.Drawing.Point(0, 50);
            this.gridfj.MultiSelect = false;
            this.gridfj.Name = "gridfj";
            this.gridfj.ReadOnly = true;
            this.gridfj.RowTemplate.Height = 23;
            this.gridfj.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridfj.Size = new System.Drawing.Size(546, 455);
            this.gridfj.TabIndex = 2;
            this.gridfj.DoubleClick += new System.EventHandler(this.btnok_Click);
            // 
            // ROOMID
            // 
            this.ROOMID.DataPropertyName = "ROOMID";
            this.ROOMID.HeaderText = "房间号";
            this.ROOMID.Name = "ROOMID";
            this.ROOMID.ReadOnly = true;
            this.ROOMID.Width = 80;
            // 
            // KRXM
            // 
            this.KRXM.DataPropertyName = "KRXM";
            this.KRXM.HeaderText = "客人姓名";
            this.KRXM.Name = "KRXM";
            this.KRXM.ReadOnly = true;
            this.KRXM.Width = 90;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "BZSM";
            this.Column1.HeaderText = "备注说明";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 140;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "IsZZ";
            this.Column2.HeaderText = "主账";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 45;
            // 
            // RZZH
            // 
            this.RZZH.DataPropertyName = "RZZH";
            this.RZZH.HeaderText = "入住帐号";
            this.RZZH.Name = "RZZH";
            this.RZZH.ReadOnly = true;
            this.RZZH.Width = 130;
            // 
            // fjFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 505);
            this.Controls.Add(this.gridfj);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "fjFind";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "费用转单";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView gridfj;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn KRXM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn RZZH;
    }
}