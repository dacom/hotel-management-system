﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
using System.Data.Common;
using System.Data.SqlClient;
namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 历史客人查询
    /// </summary>
    public partial class rzryFind : Form
    {
        hotelEntities _context;
        public rzryFind(string xm)
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;

            if (!string.IsNullOrEmpty(xm))
            {
                txtxm.Text = xm;
                btnfind_Click(btnfind, null);
            }
        }

        private void btnfind_Click(object sender, EventArgs e)
        {
            //UV_ALLKR,不包括黑名
            dataGridView1.AutoGenerateColumns = false;
            string esql = "select a.* from UV_ALLKR as a "
                    + " where  a.KRXM like @krxm  and a.ZJDM like @zjdm order by a.KRXM desc";

           /* ObjectParameter op = new ObjectParameter("krxm", "%" + txtxm.Text.Trim() + "%");
            ObjectParameter op2 = new ObjectParameter("zjdm", "%" + txtzjdm.Text.Trim() + "%");
             */
            var args = new DbParameter[] {
                new SqlParameter { ParameterName = "krxm", Value = "%" + txtxm.Text.Trim() + "%" },
                new SqlParameter { ParameterName = "zjdm", Value = "%" + txtzjdm.Text.Trim() + "%" } 
            };

            var query = _context.UV_ALLKR.SqlQuery(esql, args);
            dataGridView1.DataSource = query.ToList();
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {           
            this.DialogResult = DialogResult.OK;
        }

        public UV_ALLKR SelectedRZRY
        {
            get
            {
                if (dataGridView1.CurrentRow == null)
                    return null;
                string id = dataGridView1.CurrentRow.Cells["ID"].Value.ToString();
                return _context.UV_ALLKR.First(p => p.ID == id);
            }
        }
    }
}
