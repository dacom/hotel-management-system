﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
using System.Data.Common;

namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 处理合单的恢复入住,不支持换房
    /// </summary>
    public partial class rzhfMaintain2 : Form
    {
        hotelEntities _context;
        List<string> roomIds;
        string frzid;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="frzid"><父入住ID/param>
        public rzhfMaintain2(string frzid)
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            roomIds = new List<string>();
            var lst1 = from f in _context.RZJL_ARC
                       where f.ID == frzid
                       select new
                       {
                           RZID = f.ID,
                           ROOM_ID = f.ROOM_ID,
                           ZT = _context.ROOM.FirstOrDefault(p => p.ID == f.ROOM_ID).ZT,
                           FZ = f.SJFJ,
                           ID = f.ROOM_ID
                       };
            var lst2 = from f in _context.RZJL_ARC
                       where f.F_ID == frzid
                       select new
                       {
                           RZID = f.ID,
                           ROOM_ID = f.ROOM_ID,
                           ZT = _context.ROOM.FirstOrDefault(p => p.ID == f.ROOM_ID).ZT,
                           FZ = f.SJFJ,
                           ID = f.ROOM_ID
                       };
            maingrid.AutoGenerateColumns = false;
            //绑定之后无法编辑DataGridView，因此改用逐行添加
            maingrid.AllowUserToAddRows = true;
            var datas = (lst1.Union(lst2)).ToList();
            foreach (var item in datas)
            {
                DataGridViewRow Dgvr = this.maingrid.Rows[this.maingrid.Rows.Add()];
                Dgvr.Cells[0].Value = item.ROOM_ID;
                Dgvr.Cells[1].Value = item.ZT;
                Dgvr.Cells[2].Value = item.FZ;
                Dgvr.Cells[3].Value = item.ID;
                Dgvr.Cells[4].Value = item.ZT;
                Dgvr.Cells[5].Value = item.FZ;
                Dgvr.Cells[6].Value = item.RZID;
                Dgvr.Dispose();
            }
            maingrid.AllowUserToAddRows = false;
            maingrid.AllowUserToDeleteRows = false;
            this.frzid = frzid;
            roomIds = datas.Select(p => p.ROOM_ID).ToList();
        }

        /// <summary>
        /// 恢复入住的房间ID
        /// </summary>
        public List<string> RoomIds
        {
            get { return roomIds; }
        }
        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in maingrid.Rows)
            {
                string id = item.Cells["ID"].Value.ToString();
                if (_context.ROOM.Count(p => p.ID == id && p.ZT == RoomStstusCnName.KongJing) == 0)
                {
                    MessageBox.Show("房间：" + id + "不存在或房态不是" + RoomStstusCnName.KongJing);
                    return;
                }
                if (_context.ROOM.Count(p => p.ID == id && p.RZBZ == "否") == 0)
                {
                    MessageBox.Show("房间：" + id + "已经入住，请查证");
                    return;
                }
          }

            _context.RZJL_Restore(frzid, UserLoginInfo.FullName);

            MessageBox.Show("恢复入住成功！");
            this.DialogResult = DialogResult.OK;
        }

        /*  private void maingrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
          {
              string id = maingrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
              if (id.Length >= 4)
              {
                  var data = _context.ROOM.FirstOrDefault(p => p.ID == id);
                  if (data == null)
                      return;
                  maingrid.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value = data.ZT;
                  maingrid.Rows[e.RowIndex].Cells[e.ColumnIndex + 2].Value = data.SKFJ.ToString(); 
              }
          }*/
    }
}
