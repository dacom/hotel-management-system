﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Report;

namespace hotel.Win.Subscribe
{
    public partial class GuaZhangZhiFu : Form
    {
        hotelEntities _context;
        private string XYDW_DM;
        private XYDW_GZ gzModel;
        public GuaZhangZhiFu(string dwdm)
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            XYDW_DM = dwdm;
            btnjs.Enabled = false;
            BindMaingrid(dwdm);
        }

        private void BindMaingrid(string dwdm)
        {
            var qry = _context.XYDW_GZ.AsNoTracking().Where(p => p.XYDW_DM == dwdm && (p.GZJE - p.ZFJE) > 0);

            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
        }

        private void maingrid_SelectionChanged(object sender, EventArgs e)
        {
            if (maingrid.CurrentRow == null)
            {
                gzModel = null;
                btnjs.Enabled = false;
                return;
            }
            btnjs.Enabled = true;
            var d = maingrid.CurrentRow.DataBoundItem as XYDW_GZ;
            gzModel = _context.XYDW_GZ.Find(d.ID);
            var qry = _context.XYDW_ZF.AsNoTracking().Where(p => p.XYDW_GZ.ID == gzModel.ID).OrderBy(p => p.ZFRQ);

            zfGrid.AutoGenerateColumns = false;
            zfGrid.DataSource = qry.ToList();
            var ye = (decimal)(gzModel.GZJE - gzModel.ZFJE);            
            txtgzje.Text = ye.ToString();
            txtzfje.Maximum = ye;
            txtzfje.Value = 0;

        }

        private void btnjs_Click(object sender, EventArgs e)
        {
            if (txtzfje.Value <= 0)
                return;
            if (gzModel == null)
                return;
           // gzModel.RZJL_ARCReference.Load();
            var model = new XYDW_ZF();
            model.BZSM = txtbz.Text;
            model.XYDW_GZ = gzModel;
            model.ZFFS = "人民币";
            model.ZFJE = txtzfje.Value;
            model.ZFRQ = DateTime.Now;
            model.ZFRY = UserLoginInfo.UserName;
            model.GZ_ID = gzModel.ID;
            _context.XYDW_ZF.Add(model);
            gzModel.ZFJE = gzModel.ZFJE + model.ZFJE;
            _context.SaveChanges();
            MessageBox.Show("支付成功，表格数据将刷新");
            btnjs.Enabled = false;
            BindMaingrid(XYDW_DM);
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.增加数据;
            logInfo.Rzid = gzModel.RZJL_ID;            
            logInfo.Type = LogTypeEnum.涉及金额;
            logInfo.Message = "协议单位账务,本次支付金额=" + model.ZFJE.ToString() + "]";
            logInfo.Save();
            #endregion
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            string str = Common.ControlsUtility.GetActiveGridValue(zfGrid, "ColumnID");
            if (string.IsNullOrEmpty(str))
            {
                MessageBox.Show("请选择一个支付明细纪录");
                return;
            }
            int id = Convert.ToInt32(str);
            var d = _context.XYDW_ZF.FirstOrDefault(p=>p.ID == id);
            if (d == null)
                return;
            var dwmc = _context.XYDW_GZ.First(p => p.ID == d.GZ_ID).XYDW_MC;
            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.打印;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "协议单位结算";      
            #endregion           
            ReportEntry.ShowXieYiDanWeiJSBill(dwmc,d.ZFFS,d.ZFJE,d.BZSM,logInfo);
        }
    }
}
