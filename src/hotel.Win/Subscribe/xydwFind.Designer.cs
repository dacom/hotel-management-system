﻿namespace hotel.Win.Subscribe
{
    partial class xydwFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnfind = new System.Windows.Forms.Button();
            this.griddw = new System.Windows.Forms.DataGridView();
            this.XYDM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DWID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridfj = new System.Windows.Forms.DataGridView();
            this.btnclose = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.griddw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "协议代码：";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(96, 10);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(120, 23);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(281, 10);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(165, 23);
            this.textBox2.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(226, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "单位：";
            // 
            // btnfind
            // 
            this.btnfind.Location = new System.Drawing.Point(452, 8);
            this.btnfind.Name = "btnfind";
            this.btnfind.Size = new System.Drawing.Size(75, 34);
            this.btnfind.TabIndex = 3;
            this.btnfind.Text = "查询";
            this.btnfind.UseVisualStyleBackColor = true;
            this.btnfind.Click += new System.EventHandler(this.btnfind_Click);
            // 
            // griddw
            // 
            this.griddw.AllowUserToAddRows = false;
            this.griddw.AllowUserToDeleteRows = false;
            this.griddw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.griddw.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.XYDM,
            this.Column4,
            this.Column5,
            this.Column2,
            this.Column3,
            this.Column6,
            this.Column12,
            this.Column17,
            this.Column18,
            this.Column19,
            this.DWID});
            this.griddw.Location = new System.Drawing.Point(3, 48);
            this.griddw.Name = "griddw";
            this.griddw.ReadOnly = true;
            this.griddw.RowHeadersWidth = 25;
            this.griddw.RowTemplate.Height = 23;
            this.griddw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.griddw.Size = new System.Drawing.Size(443, 404);
            this.griddw.TabIndex = 4;
            this.griddw.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.griddw_CellClick);
            this.griddw.DoubleClick += new System.EventHandler(this.griddw_DoubleClick);
            // 
            // XYDM
            // 
            this.XYDM.DataPropertyName = "XYDM";
            this.XYDM.HeaderText = "协议代码";
            this.XYDM.Name = "XYDM";
            this.XYDM.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "DWMC";
            this.Column4.HeaderText = "单位名称";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 110;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "LXR";
            this.Column5.HeaderText = "联系人";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "DH";
            this.Column2.HeaderText = "电话";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "DZ";
            this.Column3.HeaderText = "地址";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "KFGZ";
            this.Column6.HeaderText = "可否挂账";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "GZJE";
            this.Column12.HeaderText = "挂账金额";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "ZT";
            this.Column17.HeaderText = "状态";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "XYFL";
            this.Column18.HeaderText = "协议分类";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "BZSM";
            this.Column19.HeaderText = "备注说明";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // DWID
            // 
            this.DWID.DataPropertyName = "ID";
            this.DWID.HeaderText = "协议ID";
            this.DWID.Name = "DWID";
            this.DWID.ReadOnly = true;
            // 
            // gridfj
            // 
            this.gridfj.AllowUserToAddRows = false;
            this.gridfj.AllowUserToDeleteRows = false;
            this.gridfj.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridfj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column10,
            this.Column14,
            this.Column9});
            this.gridfj.Location = new System.Drawing.Point(452, 48);
            this.gridfj.Name = "gridfj";
            this.gridfj.ReadOnly = true;
            this.gridfj.RowHeadersWidth = 25;
            this.gridfj.RowTemplate.Height = 23;
            this.gridfj.Size = new System.Drawing.Size(271, 404);
            this.gridfj.TabIndex = 5;
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(542, 8);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(89, 34);
            this.btnclose.TabIndex = 6;
            this.btnclose.Text = "选定返回";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(654, 8);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(72, 34);
            this.btncancel.TabIndex = 7;
            this.btncancel.Text = "关闭";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "FL";
            this.Column8.HeaderText = "房类";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 110;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "FJ";
            this.Column10.HeaderText = "房价";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "YJ";
            this.Column14.HeaderText = "佣金";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 90;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "BZSM";
            this.Column9.HeaderText = "备注说明";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // xydwFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 455);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.gridfj);
            this.Controls.Add(this.griddw);
            this.Controls.Add(this.btnfind);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "xydwFind";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "协议单位选择";
            ((System.ComponentModel.ISupportInitialize)(this.griddw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnfind;
        private System.Windows.Forms.DataGridView griddw;
        private System.Windows.Forms.DataGridViewTextBoxColumn XYDM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn DWID;
        private System.Windows.Forms.DataGridView gridfj;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
    }
}