﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;
using System.Data.Entity;
namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 房态维护
    /// </summary>
    public partial class ftMaintain : Form
    {
        hotelEntities _context;
        ROOM _room;
        public ftMaintain(string roomId)
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            _room = _context.ROOM.Find(roomId);
            gboxwxxx.Enabled = false;
            this.Text = _room.ID + "房态维护";
            lbfh.Text = _room.ID;
            lbft.Text = _room.ZT;

            //有入住不能修改房态
            if (_room.RZBZ == "是")
            {
                foreach (RadioButton item in gboxft.Controls)
                    item.Enabled = false;
                rbtnwxf.Enabled = true;
            }
            else
            {
                foreach (RadioButton item in gboxft.Controls)
                    item.Enabled = true;
            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            string zt = "";
            foreach (Control item in gboxft.Controls)
            {
                if ((item as RadioButton).Checked)
                {
                    zt = (item as RadioButton).Text;
                    break;
                }
            }
            _room.ZT = zt;
            _context.Entry(_room).State = EntityState.Modified;
            //处理维修记录
            if (rbtnwxf.Checked)
            {
                if (txtwxxx.Text.Length == 0)
                {
                    MessageBox.Show("维修房，请输入需要维修的内容");
                    return;
                }
                var roomMaintain = new ROOM_MAINTAIN { ROOM_ID = _room.ID };
                roomMaintain.WXNR = txtwxxx.Text;
                roomMaintain.SQRQ = DateTime.Now;
                roomMaintain.SQRY = Common.UserLoginInfo.FullName;
                roomMaintain.WCBZ = "否";
                _context.ROOM_MAINTAIN.Add(roomMaintain);
                #region 日志纪录
                CcrzInfo logInfo2 = null;
                logInfo2 = new CcrzInfo();
                logInfo2.Abstract = LogAbstractEnum.增加数据;
                logInfo2.Type = LogTypeEnum.一般操作;
                logInfo2.RoomId = _room.ID;
                logInfo2.Message = "增加维修，房号:" + lbfh.Text;
                logInfo2.Save();
                #endregion
            }
            _context.SaveChanges();

            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.房态修改;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.RoomId = _room.ID;
            logInfo.Message = "房号:" + lbfh.Text + "，原房态:" + lbft.Text + ",新房态:" + zt;
            logInfo.Save();
            #endregion
            this.DialogResult = DialogResult.OK;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void rbtnwxf_CheckedChanged(object sender, EventArgs e)
        {
            gboxwxxx.Enabled = rbtnwxf.Checked;
        }
    }
}
