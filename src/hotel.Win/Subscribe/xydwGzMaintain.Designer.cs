﻿namespace hotel.Win.Subscribe
{
    partial class xydwGzMaintain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cobxxydw = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtgzje = new System.Windows.Forms.NumericUpDown();
            this.txtsyje = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtxgdm = new System.Windows.Forms.TextBox();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btncose = new System.Windows.Forms.Button();
            this.lbzdgzje = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtgzje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsyje)).BeginInit();
            this.SuspendLayout();
            // 
            // cobxxydw
            // 
            this.cobxxydw.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxxydw.FormattingEnabled = true;
            this.cobxxydw.Location = new System.Drawing.Point(103, 11);
            this.cobxxydw.Name = "cobxxydw";
            this.cobxxydw.Size = new System.Drawing.Size(270, 21);
            this.cobxxydw.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 14);
            this.label12.TabIndex = 9;
            this.label12.Text = "协议单位：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "剩余挂账额：";
            // 
            // txtgzje
            // 
            this.txtgzje.BackColor = System.Drawing.Color.Lime;
            this.txtgzje.DecimalPlaces = 2;
            this.txtgzje.Location = new System.Drawing.Point(103, 85);
            this.txtgzje.Maximum = new decimal(new int[] {
            900000000,
            0,
            0,
            0});
            this.txtgzje.Name = "txtgzje";
            this.txtgzje.Size = new System.Drawing.Size(150, 23);
            this.txtgzje.TabIndex = 12;
            // 
            // txtsyje
            // 
            this.txtsyje.Enabled = false;
            this.txtsyje.Location = new System.Drawing.Point(103, 47);
            this.txtsyje.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.txtsyje.Name = "txtsyje";
            this.txtsyje.Size = new System.Drawing.Size(150, 23);
            this.txtsyje.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "挂账金额：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "相关代码：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 14;
            this.label4.Text = "备注说明：";
            // 
            // txtxgdm
            // 
            this.txtxgdm.Location = new System.Drawing.Point(103, 125);
            this.txtxgdm.Name = "txtxgdm";
            this.txtxgdm.Size = new System.Drawing.Size(270, 23);
            this.txtxgdm.TabIndex = 15;
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(103, 157);
            this.txtbzsm.Multiline = true;
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(270, 49);
            this.txtbzsm.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(176, 213);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 34);
            this.button1.TabIndex = 17;
            this.button1.Text = "确 定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btncose
            // 
            this.btncose.Location = new System.Drawing.Point(286, 213);
            this.btncose.Name = "btncose";
            this.btncose.Size = new System.Drawing.Size(87, 34);
            this.btncose.TabIndex = 18;
            this.btncose.Text = "返回";
            this.btncose.UseVisualStyleBackColor = true;
            this.btncose.Click += new System.EventHandler(this.btncose_Click);
            // 
            // lbzdgzje
            // 
            this.lbzdgzje.AutoSize = true;
            this.lbzdgzje.Location = new System.Drawing.Point(259, 87);
            this.lbzdgzje.Name = "lbzdgzje";
            this.lbzdgzje.Size = new System.Drawing.Size(77, 14);
            this.lbzdgzje.TabIndex = 19;
            this.lbzdgzje.Text = "挂账金额：";
            // 
            // xydwGzMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 259);
            this.Controls.Add(this.lbzdgzje);
            this.Controls.Add(this.btncose);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtbzsm);
            this.Controls.Add(this.txtxgdm);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtsyje);
            this.Controls.Add(this.txtgzje);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cobxxydw);
            this.Controls.Add(this.label12);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "xydwGzMaintain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "协议单位挂账";
            ((System.ComponentModel.ISupportInitialize)(this.txtgzje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsyje)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cobxxydw;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtgzje;
        private System.Windows.Forms.NumericUpDown txtsyje;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtxgdm;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btncose;
        private System.Windows.Forms.Label lbzdgzje;
    }
}