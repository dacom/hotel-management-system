﻿namespace hotel.Win.Subscribe
{
    partial class krfyzzMaintain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtxm = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtxfje = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtdqfj = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtmbfj = new System.Windows.Forms.TextBox();
            this.btnfind = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(152, 148);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(68, 28);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(68, 148);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(68, 28);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "确定";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtxm
            // 
            this.txtxm.Location = new System.Drawing.Point(120, 44);
            this.txtxm.Name = "txtxm";
            this.txtxm.ReadOnly = true;
            this.txtxm.Size = new System.Drawing.Size(100, 23);
            this.txtxm.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 31;
            this.label1.Text = "消费项目：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 31;
            this.label2.Text = "转帐金额：";
            // 
            // txtxfje
            // 
            this.txtxfje.Location = new System.Drawing.Point(120, 73);
            this.txtxfje.Name = "txtxfje";
            this.txtxfje.ReadOnly = true;
            this.txtxfje.Size = new System.Drawing.Size(100, 23);
            this.txtxfje.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 31;
            this.label3.Text = "当前房间：";
            // 
            // txtdqfj
            // 
            this.txtdqfj.Location = new System.Drawing.Point(120, 15);
            this.txtdqfj.Name = "txtdqfj";
            this.txtdqfj.ReadOnly = true;
            this.txtdqfj.Size = new System.Drawing.Size(100, 23);
            this.txtdqfj.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 31;
            this.label4.Text = "目标房间：";
            // 
            // txtmbfj
            // 
            this.txtmbfj.Location = new System.Drawing.Point(120, 102);
            this.txtmbfj.Name = "txtmbfj";
            this.txtmbfj.Size = new System.Drawing.Size(86, 23);
            this.txtmbfj.TabIndex = 4;
            // 
            // btnfind
            // 
            this.btnfind.Location = new System.Drawing.Point(210, 102);
            this.btnfind.Name = "btnfind";
            this.btnfind.Size = new System.Drawing.Size(42, 23);
            this.btnfind.TabIndex = 5;
            this.btnfind.Text = "...";
            this.btnfind.UseVisualStyleBackColor = true;
            this.btnfind.Click += new System.EventHandler(this.btnfind_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnfind);
            this.groupBox1.Controls.Add(this.btnOk);
            this.groupBox1.Controls.Add(this.txtdqfj);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtmbfj);
            this.groupBox1.Controls.Add(this.txtxm);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtxfje);
            this.groupBox1.Location = new System.Drawing.Point(4, -3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(279, 206);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            // 
            // krfyzzMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 207);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "krfyzzMaintain";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "客人费用转帐";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtxm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtxfje;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtdqfj;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtmbfj;
        private System.Windows.Forms.Button btnfind;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}