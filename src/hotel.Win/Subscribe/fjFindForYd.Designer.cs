﻿namespace hotel.Win.Subscribe
{
    partial class fjFindForYd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridfj = new System.Windows.Forms.DataGridView();
            this.select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ROOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FJLX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RZZH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btncancel = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridfj
            // 
            this.gridfj.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridfj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.select,
            this.ROOMID,
            this.FJLX,
            this.Column1,
            this.RZZH,
            this.Column3});
            this.gridfj.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridfj.Location = new System.Drawing.Point(0, 50);
            this.gridfj.MultiSelect = false;
            this.gridfj.Name = "gridfj";
            this.gridfj.RowHeadersWidth = 20;
            this.gridfj.RowTemplate.Height = 23;
            this.gridfj.Size = new System.Drawing.Size(453, 473);
            this.gridfj.TabIndex = 4;
            // 
            // select
            // 
            this.select.HeaderText = "选择";
            this.select.Name = "select";
            this.select.Width = 55;
            // 
            // ROOMID
            // 
            this.ROOMID.DataPropertyName = "ROOMID";
            this.ROOMID.HeaderText = "房间号";
            this.ROOMID.Name = "ROOMID";
            this.ROOMID.ReadOnly = true;
            this.ROOMID.Width = 80;
            // 
            // FJLX
            // 
            this.FJLX.DataPropertyName = "FJLX";
            this.FJLX.HeaderText = "房间类型";
            this.FJLX.Name = "FJLX";
            this.FJLX.ReadOnly = true;
            this.FJLX.Width = 90;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "FJ";
            this.Column1.HeaderText = "房价";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // RZZH
            // 
            this.RZZH.DataPropertyName = "ZT";
            this.RZZH.HeaderText = "状态";
            this.RZZH.Name = "RZZH";
            this.RZZH.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "YDBZ";
            this.Column3.HeaderText = "预订否";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            this.Column3.Width = 80;
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(367, 12);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(68, 28);
            this.btncancel.TabIndex = 2;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(286, 12);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(68, 28);
            this.btnok.TabIndex = 1;
            this.btnok.Text = "选定";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btncancel);
            this.panel1.Controls.Add(this.btnok);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(453, 50);
            this.panel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(3, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "请你要增加预订的房间";
            // 
            // fjFindForYd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 523);
            this.Controls.Add(this.gridfj);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "fjFindForYd";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "加预订房";
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridfj;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn select;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FJLX;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RZZH;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}