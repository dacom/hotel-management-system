﻿namespace hotel.Win.Subscribe
{
    partial class ftMaintain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gboxft = new System.Windows.Forms.GroupBox();
            this.rbtnldf = new System.Windows.Forms.RadioButton();
            this.rbtnsdf = new System.Windows.Forms.RadioButton();
            this.rbtnqkf = new System.Windows.Forms.RadioButton();
            this.rbtnwxf = new System.Windows.Forms.RadioButton();
            this.rbtnkzf = new System.Windows.Forms.RadioButton();
            this.rbtnkjf = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lbfh = new System.Windows.Forms.LinkLabel();
            this.btnok = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.gboxwxxx = new System.Windows.Forms.GroupBox();
            this.txtwxxx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbft = new System.Windows.Forms.Label();
            this.gboxft.SuspendLayout();
            this.gboxwxxx.SuspendLayout();
            this.SuspendLayout();
            // 
            // gboxft
            // 
            this.gboxft.Controls.Add(this.rbtnldf);
            this.gboxft.Controls.Add(this.rbtnsdf);
            this.gboxft.Controls.Add(this.rbtnqkf);
            this.gboxft.Controls.Add(this.rbtnwxf);
            this.gboxft.Controls.Add(this.rbtnkzf);
            this.gboxft.Controls.Add(this.rbtnkjf);
            this.gboxft.Location = new System.Drawing.Point(12, 36);
            this.gboxft.Name = "gboxft";
            this.gboxft.Size = new System.Drawing.Size(119, 222);
            this.gboxft.TabIndex = 0;
            this.gboxft.TabStop = false;
            this.gboxft.Text = "可选房态";
            // 
            // rbtnldf
            // 
            this.rbtnldf.AutoSize = true;
            this.rbtnldf.Location = new System.Drawing.Point(16, 184);
            this.rbtnldf.Name = "rbtnldf";
            this.rbtnldf.Size = new System.Drawing.Size(67, 18);
            this.rbtnldf.TabIndex = 1;
            this.rbtnldf.TabStop = true;
            this.rbtnldf.Text = "离店房";
            this.rbtnldf.UseVisualStyleBackColor = true;
            this.rbtnldf.CheckedChanged += new System.EventHandler(this.rbtnwxf_CheckedChanged);
            // 
            // rbtnsdf
            // 
            this.rbtnsdf.AutoSize = true;
            this.rbtnsdf.Location = new System.Drawing.Point(16, 151);
            this.rbtnsdf.Name = "rbtnsdf";
            this.rbtnsdf.Size = new System.Drawing.Size(67, 18);
            this.rbtnsdf.TabIndex = 1;
            this.rbtnsdf.TabStop = true;
            this.rbtnsdf.Text = "锁定房";
            this.rbtnsdf.UseVisualStyleBackColor = true;
            this.rbtnsdf.CheckedChanged += new System.EventHandler(this.rbtnwxf_CheckedChanged);
            // 
            // rbtnqkf
            // 
            this.rbtnqkf.AutoSize = true;
            this.rbtnqkf.Location = new System.Drawing.Point(16, 118);
            this.rbtnqkf.Name = "rbtnqkf";
            this.rbtnqkf.Size = new System.Drawing.Size(67, 18);
            this.rbtnqkf.TabIndex = 1;
            this.rbtnqkf.TabStop = true;
            this.rbtnqkf.Text = "清理房";
            this.rbtnqkf.UseVisualStyleBackColor = true;
            this.rbtnqkf.CheckedChanged += new System.EventHandler(this.rbtnwxf_CheckedChanged);
            // 
            // rbtnwxf
            // 
            this.rbtnwxf.AutoSize = true;
            this.rbtnwxf.Location = new System.Drawing.Point(16, 87);
            this.rbtnwxf.Name = "rbtnwxf";
            this.rbtnwxf.Size = new System.Drawing.Size(67, 18);
            this.rbtnwxf.TabIndex = 1;
            this.rbtnwxf.TabStop = true;
            this.rbtnwxf.Text = "维修房";
            this.rbtnwxf.UseVisualStyleBackColor = true;
            this.rbtnwxf.CheckedChanged += new System.EventHandler(this.rbtnwxf_CheckedChanged);
            // 
            // rbtnkzf
            // 
            this.rbtnkzf.AutoSize = true;
            this.rbtnkzf.Location = new System.Drawing.Point(16, 57);
            this.rbtnkzf.Name = "rbtnkzf";
            this.rbtnkzf.Size = new System.Drawing.Size(67, 18);
            this.rbtnkzf.TabIndex = 1;
            this.rbtnkzf.TabStop = true;
            this.rbtnkzf.Text = "空脏房";
            this.rbtnkzf.UseVisualStyleBackColor = true;
            this.rbtnkzf.CheckedChanged += new System.EventHandler(this.rbtnwxf_CheckedChanged);
            // 
            // rbtnkjf
            // 
            this.rbtnkjf.AutoSize = true;
            this.rbtnkjf.Checked = true;
            this.rbtnkjf.Location = new System.Drawing.Point(16, 29);
            this.rbtnkjf.Name = "rbtnkjf";
            this.rbtnkjf.Size = new System.Drawing.Size(67, 18);
            this.rbtnkjf.TabIndex = 0;
            this.rbtnkjf.TabStop = true;
            this.rbtnkjf.Text = "空净房";
            this.rbtnkjf.UseVisualStyleBackColor = true;
            this.rbtnkjf.CheckedChanged += new System.EventHandler(this.rbtnwxf_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "房号：";
            // 
            // lbfh
            // 
            this.lbfh.AutoSize = true;
            this.lbfh.Font = new System.Drawing.Font("SimSun", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfh.Location = new System.Drawing.Point(67, 9);
            this.lbfh.Name = "lbfh";
            this.lbfh.Size = new System.Drawing.Size(49, 19);
            this.lbfh.TabIndex = 2;
            this.lbfh.TabStop = true;
            this.lbfh.Text = "8888";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(213, 277);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(83, 28);
            this.btnok.TabIndex = 3;
            this.btnok.Text = "确定";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(318, 277);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(68, 28);
            this.btncancel.TabIndex = 3;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // gboxwxxx
            // 
            this.gboxwxxx.Controls.Add(this.txtwxxx);
            this.gboxwxxx.Location = new System.Drawing.Point(137, 36);
            this.gboxwxxx.Name = "gboxwxxx";
            this.gboxwxxx.Size = new System.Drawing.Size(282, 222);
            this.gboxwxxx.TabIndex = 4;
            this.gboxwxxx.TabStop = false;
            this.gboxwxxx.Text = "备注信息：";
            // 
            // txtwxxx
            // 
            this.txtwxxx.BackColor = System.Drawing.SystemColors.Window;
            this.txtwxxx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtwxxx.Location = new System.Drawing.Point(3, 19);
            this.txtwxxx.Multiline = true;
            this.txtwxxx.Name = "txtwxxx";
            this.txtwxxx.Size = new System.Drawing.Size(276, 200);
            this.txtwxxx.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(137, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = "当前房态：";
            // 
            // lbft
            // 
            this.lbft.AutoSize = true;
            this.lbft.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Bold);
            this.lbft.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lbft.Location = new System.Drawing.Point(229, 11);
            this.lbft.Name = "lbft";
            this.lbft.Size = new System.Drawing.Size(42, 16);
            this.lbft.TabIndex = 6;
            this.lbft.Text = "房态";
            // 
            // ftMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 317);
            this.Controls.Add(this.lbft);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gboxwxxx);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.lbfh);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gboxft);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ftMaintain";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "房态修改";
            this.gboxft.ResumeLayout(false);
            this.gboxft.PerformLayout();
            this.gboxwxxx.ResumeLayout(false);
            this.gboxwxxx.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gboxft;
        private System.Windows.Forms.RadioButton rbtnkzf;
        private System.Windows.Forms.RadioButton rbtnkjf;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel lbfh;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.GroupBox gboxwxxx;
        private System.Windows.Forms.TextBox txtwxxx;
        private System.Windows.Forms.RadioButton rbtnwxf;
        private System.Windows.Forms.RadioButton rbtnqkf;
        private System.Windows.Forms.RadioButton rbtnsdf;
        private System.Windows.Forms.RadioButton rbtnldf;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbft;
    }
}