﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
using System.Data.Common;


namespace hotel.Win.Subscribe
{
    public partial class krfytdMaintain : Form
    {
        public krfytdMaintain(XFJL xfjl)
        {
            InitializeComponent();
            txtxm.Text = xfjl.XFXM_MC;
            txtxfsl.Value = 1;
            txtxfsl.Maximum = (decimal)xfjl.SL;
            txtmsl.Text = xfjl.SL.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtxfsl.Value == 0 || txtxfsl.Value > Convert.ToDecimal(txtmsl.Text))
            {
                MessageBox.Show("退单数量不大于" + txtmsl.Text + ",并且不能为0");
                return;
            }
            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// 退单数量
        /// </summary>
        public int TDSL { get { return (int)txtxfsl.Value; } }
    }
}
