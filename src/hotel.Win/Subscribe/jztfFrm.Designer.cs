﻿namespace hotel.Win.Subscribe
{
    partial class jztfFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(jztfFrm));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbfl = new System.Windows.Forms.Label();
            this.lbyzts = new System.Windows.Forms.Label();
            this.lbldrq = new System.Windows.Forms.Label();
            this.lbddrq = new System.Windows.Forms.Label();
            this.lbfksl = new System.Windows.Forms.Label();
            this.lbfj = new System.Windows.Forms.Label();
            this.lbfh = new System.Windows.Forms.Label();
            this.lbzh = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnbjztf = new System.Windows.Forms.Button();
            this.btnrmbjz = new System.Windows.Forms.Button();
            this.groupBoxjsfz = new System.Windows.Forms.GroupBox();
            this.rbtnmz = new System.Windows.Forms.RadioButton();
            this.rbtnbtz = new System.Windows.Forms.RadioButton();
            this.rbtnqz = new System.Windows.Forms.RadioButton();
            this.txtjsfz = new System.Windows.Forms.NumericUpDown();
            this.gboxZhclq = new System.Windows.Forms.GroupBox();
            this.btnzqtf = new System.Windows.Forms.Button();
            this.btnxfrd = new System.Windows.Forms.Button();
            this.btnxdyj = new System.Windows.Forms.Button();
            this.btnqtjz = new System.Windows.Forms.Button();
            this.gboxdy = new System.Windows.Forms.GroupBox();
            this.btnxfmxb = new System.Windows.Forms.Button();
            this.btndyzd = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtkrzf = new System.Windows.Forms.NumericUpDown();
            this.txtysje = new System.Windows.Forms.NumericUpDown();
            this.txtljxf = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridxf = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnBZSM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XFSL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XFID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.gridzf = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.treefj = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBoxjsfz.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtjsfz)).BeginInit();
            this.gboxZhclq.SuspendLayout();
            this.gboxdy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtkrzf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtysje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtljxf)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridxf)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridzf)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.lbfl);
            this.groupBox3.Controls.Add(this.lbyzts);
            this.groupBox3.Controls.Add(this.lbldrq);
            this.groupBox3.Controls.Add(this.lbddrq);
            this.groupBox3.Controls.Add(this.lbfksl);
            this.groupBox3.Controls.Add(this.lbfj);
            this.groupBox3.Controls.Add(this.lbfh);
            this.groupBox3.Controls.Add(this.lbzh);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox3.Size = new System.Drawing.Size(842, 66);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "客人信息";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "抵店日期：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(477, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 14);
            this.label11.TabIndex = 14;
            this.label11.Text = "房卡数：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(340, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "实际天数：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(174, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 14;
            this.label5.Text = "离店日期：";
            // 
            // lbfl
            // 
            this.lbfl.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbfl.Location = new System.Drawing.Point(393, 20);
            this.lbfl.Name = "lbfl";
            this.lbfl.Size = new System.Drawing.Size(97, 15);
            this.lbfl.TabIndex = 7;
            this.lbfl.Text = "标双";
            // 
            // lbyzts
            // 
            this.lbyzts.Location = new System.Drawing.Point(423, 47);
            this.lbyzts.Name = "lbyzts";
            this.lbyzts.Size = new System.Drawing.Size(50, 15);
            this.lbyzts.TabIndex = 6;
            this.lbyzts.Text = "3";
            // 
            // lbldrq
            // 
            this.lbldrq.Location = new System.Drawing.Point(255, 47);
            this.lbldrq.Name = "lbldrq";
            this.lbldrq.Size = new System.Drawing.Size(90, 15);
            this.lbldrq.TabIndex = 6;
            this.lbldrq.Text = "2010-05-20";
            // 
            // lbddrq
            // 
            this.lbddrq.Location = new System.Drawing.Point(86, 47);
            this.lbddrq.Name = "lbddrq";
            this.lbddrq.Size = new System.Drawing.Size(99, 15);
            this.lbddrq.TabIndex = 6;
            this.lbddrq.Text = "2010-05-20";
            // 
            // lbfksl
            // 
            this.lbfksl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbfksl.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfksl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbfksl.Location = new System.Drawing.Point(546, 44);
            this.lbfksl.Name = "lbfksl";
            this.lbfksl.Size = new System.Drawing.Size(60, 15);
            this.lbfksl.TabIndex = 6;
            this.lbfksl.Text = "888";
            this.lbfksl.Click += new System.EventHandler(this.lbfksl_Click);
            // 
            // lbfj
            // 
            this.lbfj.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbfj.Location = new System.Drawing.Point(545, 20);
            this.lbfj.Name = "lbfj";
            this.lbfj.Size = new System.Drawing.Size(60, 15);
            this.lbfj.TabIndex = 6;
            this.lbfj.Text = "888";
            // 
            // lbfh
            // 
            this.lbfh.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbfh.Location = new System.Drawing.Point(255, 20);
            this.lbfh.Name = "lbfh";
            this.lbfh.Size = new System.Drawing.Size(56, 15);
            this.lbfh.TabIndex = 5;
            this.lbfh.Text = "8888";
            // 
            // lbzh
            // 
            this.lbzh.Location = new System.Drawing.Point(86, 20);
            this.lbzh.Name = "lbzh";
            this.lbzh.Size = new System.Drawing.Size(109, 15);
            this.lbzh.TabIndex = 4;
            this.lbzh.Text = "A1005168888";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(36, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 14);
            this.label14.TabIndex = 2;
            this.label14.Text = "账号：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(491, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 14);
            this.label19.TabIndex = 2;
            this.label19.Text = "房价：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(339, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 14);
            this.label18.TabIndex = 2;
            this.label18.Text = "房类：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(202, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "房号：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBoxjsfz);
            this.groupBox1.Controls.Add(this.gboxZhclq);
            this.groupBox1.Controls.Add(this.gboxdy);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtkrzf);
            this.groupBox1.Controls.Add(this.txtysje);
            this.groupBox1.Controls.Add(this.txtljxf);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(842, 132);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "账务信息";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnbjztf);
            this.groupBox4.Controls.Add(this.btnrmbjz);
            this.groupBox4.Location = new System.Drawing.Point(350, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(140, 106);
            this.groupBox4.TabIndex = 31;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "结账方式";
            // 
            // btnbjztf
            // 
            this.btnbjztf.Font = new System.Drawing.Font("SimSun", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnbjztf.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnbjztf.Location = new System.Drawing.Point(6, 58);
            this.btnbjztf.Name = "btnbjztf";
            this.btnbjztf.Size = new System.Drawing.Size(122, 36);
            this.btnbjztf.TabIndex = 2;
            this.btnbjztf.Text = "不结账退房";
            this.btnbjztf.UseVisualStyleBackColor = false;
            this.btnbjztf.Click += new System.EventHandler(this.btnbjztf_Click);
            // 
            // btnrmbjz
            // 
            this.btnrmbjz.Font = new System.Drawing.Font("SimSun", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnrmbjz.Location = new System.Drawing.Point(6, 19);
            this.btnrmbjz.Name = "btnrmbjz";
            this.btnrmbjz.Size = new System.Drawing.Size(122, 36);
            this.btnrmbjz.TabIndex = 2;
            this.btnrmbjz.Text = "人民币结账";
            this.btnrmbjz.UseVisualStyleBackColor = false;
            this.btnrmbjz.Click += new System.EventHandler(this.btnrmbjz_Click);
            // 
            // groupBoxjsfz
            // 
            this.groupBoxjsfz.Controls.Add(this.rbtnmz);
            this.groupBoxjsfz.Controls.Add(this.rbtnbtz);
            this.groupBoxjsfz.Controls.Add(this.rbtnqz);
            this.groupBoxjsfz.Controls.Add(this.txtjsfz);
            this.groupBoxjsfz.Location = new System.Drawing.Point(11, 76);
            this.groupBoxjsfz.Name = "groupBoxjsfz";
            this.groupBoxjsfz.Size = new System.Drawing.Size(219, 52);
            this.groupBoxjsfz.TabIndex = 26;
            this.groupBoxjsfz.TabStop = false;
            this.groupBoxjsfz.Text = "加收房租";
            // 
            // rbtnmz
            // 
            this.rbtnmz.AutoSize = true;
            this.rbtnmz.Checked = true;
            this.rbtnmz.Location = new System.Drawing.Point(160, 28);
            this.rbtnmz.Name = "rbtnmz";
            this.rbtnmz.Size = new System.Drawing.Size(47, 16);
            this.rbtnmz.TabIndex = 31;
            this.rbtnmz.TabStop = true;
            this.rbtnmz.Text = "免租";
            this.rbtnmz.UseVisualStyleBackColor = true;
            // 
            // rbtnbtz
            // 
            this.rbtnbtz.AutoSize = true;
            this.rbtnbtz.Location = new System.Drawing.Point(101, 27);
            this.rbtnbtz.Name = "rbtnbtz";
            this.rbtnbtz.Size = new System.Drawing.Size(47, 16);
            this.rbtnbtz.TabIndex = 31;
            this.rbtnbtz.Text = "半天";
            this.rbtnbtz.UseVisualStyleBackColor = true;
            // 
            // rbtnqz
            // 
            this.rbtnqz.AutoSize = true;
            this.rbtnqz.Location = new System.Drawing.Point(42, 26);
            this.rbtnqz.Name = "rbtnqz";
            this.rbtnqz.Size = new System.Drawing.Size(47, 16);
            this.rbtnqz.TabIndex = 31;
            this.rbtnqz.Text = "全租";
            this.rbtnqz.UseVisualStyleBackColor = true;
            // 
            // txtjsfz
            // 
            this.txtjsfz.DecimalPlaces = 2;
            this.txtjsfz.Enabled = false;
            this.txtjsfz.Location = new System.Drawing.Point(73, 0);
            this.txtjsfz.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtjsfz.Name = "txtjsfz";
            this.txtjsfz.Size = new System.Drawing.Size(140, 23);
            this.txtjsfz.TabIndex = 30;
            this.txtjsfz.ThousandsSeparator = true;
            // 
            // gboxZhclq
            // 
            this.gboxZhclq.Controls.Add(this.btnzqtf);
            this.gboxZhclq.Controls.Add(this.btnxfrd);
            this.gboxZhclq.Controls.Add(this.btnxdyj);
            this.gboxZhclq.Controls.Add(this.btnqtjz);
            this.gboxZhclq.Location = new System.Drawing.Point(495, 13);
            this.gboxZhclq.Name = "gboxZhclq";
            this.gboxZhclq.Size = new System.Drawing.Size(222, 107);
            this.gboxZhclq.TabIndex = 30;
            this.gboxZhclq.TabStop = false;
            this.gboxZhclq.Text = "账务处理区";
            // 
            // btnzqtf
            // 
            this.btnzqtf.Location = new System.Drawing.Point(105, 53);
            this.btnzqtf.Name = "btnzqtf";
            this.btnzqtf.Size = new System.Drawing.Size(112, 25);
            this.btnzqtf.TabIndex = 26;
            this.btnzqtf.Text = "转其他房退房";
            this.btnzqtf.UseVisualStyleBackColor = true;
            this.btnzqtf.Click += new System.EventHandler(this.btnzqtf_Click);
            // 
            // btnxfrd
            // 
            this.btnxfrd.Location = new System.Drawing.Point(7, 22);
            this.btnxfrd.Name = "btnxfrd";
            this.btnxfrd.Size = new System.Drawing.Size(95, 25);
            this.btnxfrd.TabIndex = 25;
            this.btnxfrd.Text = "费用入单";
            this.btnxfrd.UseVisualStyleBackColor = true;
            this.btnxfrd.Click += new System.EventHandler(this.btnxfrd_Click);
            // 
            // btnxdyj
            // 
            this.btnxdyj.Location = new System.Drawing.Point(6, 53);
            this.btnxdyj.Name = "btnxdyj";
            this.btnxdyj.Size = new System.Drawing.Size(95, 25);
            this.btnxdyj.TabIndex = 25;
            this.btnxdyj.Text = "续订押金";
            this.btnxdyj.UseVisualStyleBackColor = true;
            this.btnxdyj.Click += new System.EventHandler(this.btnxdyj_Click);
            // 
            // btnqtjz
            // 
            this.btnqtjz.Location = new System.Drawing.Point(105, 22);
            this.btnqtjz.Name = "btnqtjz";
            this.btnqtjz.Size = new System.Drawing.Size(112, 25);
            this.btnqtjz.TabIndex = 3;
            this.btnqtjz.Text = "签单挂账";
            this.btnqtjz.UseVisualStyleBackColor = true;
            this.btnqtjz.Click += new System.EventHandler(this.btnqtjz_Click);
            // 
            // gboxdy
            // 
            this.gboxdy.Controls.Add(this.btnxfmxb);
            this.gboxdy.Controls.Add(this.btndyzd);
            this.gboxdy.Location = new System.Drawing.Point(720, 13);
            this.gboxdy.Name = "gboxdy";
            this.gboxdy.Size = new System.Drawing.Size(115, 107);
            this.gboxdy.TabIndex = 27;
            this.gboxdy.TabStop = false;
            this.gboxdy.Text = "打印功能区";
            // 
            // btnxfmxb
            // 
            this.btnxfmxb.Location = new System.Drawing.Point(6, 53);
            this.btnxfmxb.Name = "btnxfmxb";
            this.btnxfmxb.Size = new System.Drawing.Size(95, 25);
            this.btnxfmxb.TabIndex = 5;
            this.btnxfmxb.Text = "消费明细表";
            this.btnxfmxb.UseVisualStyleBackColor = true;
            this.btnxfmxb.Click += new System.EventHandler(this.btnxfmxb_Click);
            // 
            // btndyzd
            // 
            this.btndyzd.Location = new System.Drawing.Point(6, 22);
            this.btndyzd.Name = "btndyzd";
            this.btndyzd.Size = new System.Drawing.Size(95, 25);
            this.btndyzd.TabIndex = 5;
            this.btndyzd.Text = "消费汇总表";
            this.btndyzd.UseVisualStyleBackColor = true;
            this.btndyzd.Click += new System.EventHandler(this.btndyzd_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 14);
            this.label6.TabIndex = 16;
            this.label6.Text = "押金合计:";
            // 
            // txtkrzf
            // 
            this.txtkrzf.BackColor = System.Drawing.Color.Lime;
            this.txtkrzf.DecimalPlaces = 2;
            this.txtkrzf.Enabled = false;
            this.txtkrzf.Font = new System.Drawing.Font("SimSun", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtkrzf.Location = new System.Drawing.Point(209, 35);
            this.txtkrzf.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtkrzf.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.txtkrzf.Name = "txtkrzf";
            this.txtkrzf.Size = new System.Drawing.Size(136, 29);
            this.txtkrzf.TabIndex = 0;
            this.txtkrzf.ThousandsSeparator = true;
            this.txtkrzf.DoubleClick += new System.EventHandler(this.txtkrzf_DoubleClick);
            // 
            // txtysje
            // 
            this.txtysje.DecimalPlaces = 2;
            this.txtysje.Enabled = false;
            this.txtysje.Location = new System.Drawing.Point(84, 47);
            this.txtysje.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtysje.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.txtysje.Name = "txtysje";
            this.txtysje.Size = new System.Drawing.Size(116, 23);
            this.txtysje.TabIndex = 18;
            this.txtysje.ThousandsSeparator = true;
            // 
            // txtljxf
            // 
            this.txtljxf.DecimalPlaces = 2;
            this.txtljxf.Enabled = false;
            this.txtljxf.Location = new System.Drawing.Point(84, 17);
            this.txtljxf.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.txtljxf.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.txtljxf.Name = "txtljxf";
            this.txtljxf.Size = new System.Drawing.Size(116, 23);
            this.txtljxf.TabIndex = 18;
            this.txtljxf.ThousandsSeparator = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(206, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 14);
            this.label8.TabIndex = 16;
            this.label8.Text = "客人返还:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "消费合计:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl2);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 198);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 328);
            this.panel1.TabIndex = 18;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(150, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(692, 328);
            this.tabControl2.TabIndex = 20;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridxf);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(684, 301);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "消费记录";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridxf
            // 
            this.gridxf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridxf.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.ColumnBZSM,
            this.XFSL,
            this.Column6,
            this.dataGridViewTextBoxColumn4,
            this.XFID});
            this.gridxf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridxf.Location = new System.Drawing.Point(3, 3);
            this.gridxf.Name = "gridxf";
            this.gridxf.RowHeadersWidth = 20;
            this.gridxf.RowTemplate.Height = 23;
            this.gridxf.Size = new System.Drawing.Size(678, 295);
            this.gridxf.TabIndex = 27;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ROOM_ID";
            this.Column5.HeaderText = "房号";
            this.Column5.Name = "Column5";
            this.Column5.Width = 60;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "XFXM_MC";
            this.dataGridViewTextBoxColumn1.HeaderText = "项目名称";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 90;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "JE";
            this.dataGridViewTextBoxColumn2.HeaderText = "金额";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 60;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CJRQ";
            this.dataGridViewTextBoxColumn3.HeaderText = "入帐日期";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // ColumnBZSM
            // 
            this.ColumnBZSM.DataPropertyName = "BZSM";
            this.ColumnBZSM.HeaderText = "备注说明";
            this.ColumnBZSM.Name = "ColumnBZSM";
            // 
            // XFSL
            // 
            this.XFSL.DataPropertyName = "SL";
            this.XFSL.HeaderText = "数量";
            this.XFSL.Name = "XFSL";
            this.XFSL.Width = 60;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "DJ";
            this.Column6.HeaderText = "单价";
            this.Column6.Name = "Column6";
            this.Column6.Width = 60;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CJRY";
            this.dataGridViewTextBoxColumn4.HeaderText = "入帐人";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 75;
            // 
            // XFID
            // 
            this.XFID.DataPropertyName = "ID";
            this.XFID.HeaderText = "ID";
            this.XFID.Name = "XFID";
            this.XFID.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.gridzf);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(684, 301);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "支付记录";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // gridzf
            // 
            this.gridzf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridzf.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.gridzf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridzf.Location = new System.Drawing.Point(3, 3);
            this.gridzf.Name = "gridzf";
            this.gridzf.RowHeadersWidth = 20;
            this.gridzf.RowTemplate.Height = 23;
            this.gridzf.Size = new System.Drawing.Size(553, 295);
            this.gridzf.TabIndex = 24;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "ROOM_ID";
            this.Column7.HeaderText = "房间号";
            this.Column7.Name = "Column7";
            this.Column7.Width = 85;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "YJ";
            this.Column1.HeaderText = "押金";
            this.Column1.Name = "Column1";
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "CJRQ";
            this.Column2.HeaderText = "入帐日期";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "CJRY";
            this.Column3.HeaderText = "入帐人";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "ZFFS";
            this.Column4.HeaderText = "支付方式";
            this.Column4.Name = "Column4";
            this.Column4.Width = 90;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(150, 328);
            this.tabControl1.TabIndex = 19;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.treefj);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(142, 301);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "结帐房间";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // treefj
            // 
            this.treefj.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treefj.ImageIndex = 0;
            this.treefj.ImageList = this.imageList1;
            this.treefj.Location = new System.Drawing.Point(3, 3);
            this.treefj.Name = "treefj";
            this.treefj.SelectedImageIndex = 1;
            this.treefj.Size = new System.Drawing.Size(136, 295);
            this.treefj.TabIndex = 0;
            this.treefj.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treefj_AfterSelect);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "ZiYong.png");
            this.imageList1.Images.SetKeyName(1, "tool4.png");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(137, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem1.Text = "消费汇总单";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem2.Text = "消费明细单";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // jztfFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 526);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "jztfFrm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "结账退房";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.jztfFrm_FormClosed);
            this.Shown += new System.EventHandler(this.jztfFrm_Shown);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBoxjsfz.ResumeLayout(false);
            this.groupBoxjsfz.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtjsfz)).EndInit();
            this.gboxZhclq.ResumeLayout(false);
            this.gboxdy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtkrzf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtysje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtljxf)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridxf)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridzf)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbfl;
        private System.Windows.Forms.Label lbyzts;
        private System.Windows.Forms.Label lbldrq;
        private System.Windows.Forms.Label lbddrq;
        private System.Windows.Forms.Label lbfj;
        private System.Windows.Forms.Label lbfh;
        private System.Windows.Forms.Label lbzh;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown txtljxf;
        private System.Windows.Forms.NumericUpDown txtysje;
        private System.Windows.Forms.Button btnrmbjz;
        private System.Windows.Forms.Button btndyzd;
        private System.Windows.Forms.Button btnqtjz;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TreeView treefj;
        private System.Windows.Forms.DataGridView gridzf;
        private System.Windows.Forms.DataGridView gridxf;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbfksl;
        private System.Windows.Forms.Button btnxfrd;
        private System.Windows.Forms.Button btnxdyj;
        private System.Windows.Forms.GroupBox gboxdy;
        private System.Windows.Forms.Button btnxfmxb;
        private System.Windows.Forms.NumericUpDown txtkrzf;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox gboxZhclq;
        private System.Windows.Forms.GroupBox groupBoxjsfz;
        private System.Windows.Forms.RadioButton rbtnmz;
        private System.Windows.Forms.RadioButton rbtnbtz;
        private System.Windows.Forms.RadioButton rbtnqz;
        private System.Windows.Forms.NumericUpDown txtjsfz;
        private System.Windows.Forms.Button btnzqtf;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnBZSM;
        private System.Windows.Forms.DataGridViewTextBoxColumn XFSL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn XFID;
        private System.Windows.Forms.Button btnbjztf;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}