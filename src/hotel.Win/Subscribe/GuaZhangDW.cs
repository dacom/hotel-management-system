﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;

namespace hotel.Win.Subscribe
{
    public partial class GuaZhangDW : Form
    {
        /// <summary>
        /// 挂帐单位-帐务
        /// </summary>
        public GuaZhangDW()
        {
            InitializeComponent();
            ControlsUtility.ResetColumnFromXML(maingrid);
            BindData();
            ucQueryToolBar1.TargetDataGrid = maingrid;            
            ucQueryToolBar1.RefreshFunction = () => { BindData(); };
            AddButton();
        }

        private void BindData()
        {
            var _context = MyDataContext.GetDataContext;
            var qry = _context.UV_XYDW_GZ_SUM.AsNoTracking().Where(p => p.SUM_GZJE > 0);
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
        }

        private void AddButton()
        {
            ToolStripButton item = new ToolStripButton();
            //item.Image = global::hotel.Win.Query.Properties.Resources.excel1;
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnzwcl";
            item.Size = new System.Drawing.Size(77, 30);
            item.Text = "帐务处理";
            item.Click += new System.EventHandler(ZhangWu_Click);
            ucQueryToolBar1.Insert(item);
        }

        private void ZhangWu_Click(object sender, EventArgs e)
        {
            string dwdm = ControlsUtility.GetActiveGridValue(maingrid, "XYDW_DM");
            if (dwdm == "")
                return;
            var frm = new GuaZhangZhiFu(dwdm);
            frm.ShowDialog();
        }
    }
}
