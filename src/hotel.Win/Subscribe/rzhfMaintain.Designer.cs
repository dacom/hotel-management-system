﻿namespace hotel.Win.Subscribe
{
    partial class rzhfMaintain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridfj = new System.Windows.Forms.DataGridView();
            this.ROOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KRXM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLDRQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RZZH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btncancel = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnjz = new System.Windows.Forms.Button();
            this.btnviewzw = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridfj
            // 
            this.gridfj.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridfj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROOMID,
            this.KRXM,
            this.ColumnLDRQ,
            this.Column1,
            this.RZZH});
            this.gridfj.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridfj.Location = new System.Drawing.Point(0, 50);
            this.gridfj.MultiSelect = false;
            this.gridfj.Name = "gridfj";
            this.gridfj.ReadOnly = true;
            this.gridfj.RowTemplate.Height = 23;
            this.gridfj.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridfj.Size = new System.Drawing.Size(609, 484);
            this.gridfj.TabIndex = 4;
            // 
            // ROOMID
            // 
            this.ROOMID.DataPropertyName = "ROOMID";
            this.ROOMID.HeaderText = "房间号";
            this.ROOMID.Name = "ROOMID";
            this.ROOMID.ReadOnly = true;
            this.ROOMID.Width = 80;
            // 
            // KRXM
            // 
            this.KRXM.DataPropertyName = "KRXM";
            this.KRXM.HeaderText = "客人姓名";
            this.KRXM.Name = "KRXM";
            this.KRXM.ReadOnly = true;
            this.KRXM.Width = 90;
            // 
            // ColumnLDRQ
            // 
            this.ColumnLDRQ.DataPropertyName = "JZRQ";
            this.ColumnLDRQ.HeaderText = "结账日期";
            this.ColumnLDRQ.Name = "ColumnLDRQ";
            this.ColumnLDRQ.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "BZSM";
            this.Column1.HeaderText = "备注说明";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 140;
            // 
            // RZZH
            // 
            this.RZZH.DataPropertyName = "RZZH";
            this.RZZH.HeaderText = "入住帐号";
            this.RZZH.Name = "RZZH";
            this.RZZH.ReadOnly = true;
            this.RZZH.Width = 130;
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(451, 12);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(68, 28);
            this.btncancel.TabIndex = 1;
            this.btncancel.Text = "关闭";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(113, 12);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(78, 28);
            this.btnok.TabIndex = 1;
            this.btnok.Text = "恢复入住";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnjz);
            this.panel1.Controls.Add(this.btncancel);
            this.panel1.Controls.Add(this.btnviewzw);
            this.panel1.Controls.Add(this.btnok);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(609, 50);
            this.panel1.TabIndex = 3;
            // 
            // btnjz
            // 
            this.btnjz.Location = new System.Drawing.Point(29, 12);
            this.btnjz.Name = "btnjz";
            this.btnjz.Size = new System.Drawing.Size(78, 28);
            this.btnjz.TabIndex = 2;
            this.btnjz.Text = "结账退房";
            this.btnjz.UseVisualStyleBackColor = true;
            this.btnjz.Click += new System.EventHandler(this.btnjz_Click);
            // 
            // btnviewzw
            // 
            this.btnviewzw.Location = new System.Drawing.Point(224, 12);
            this.btnviewzw.Name = "btnviewzw";
            this.btnviewzw.Size = new System.Drawing.Size(78, 28);
            this.btnviewzw.TabIndex = 1;
            this.btnviewzw.Text = "查看账务";
            this.btnviewzw.UseVisualStyleBackColor = true;
            this.btnviewzw.Click += new System.EventHandler(this.btnviewzw_Click);
            // 
            // rzhfMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 534);
            this.Controls.Add(this.gridfj);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "rzhfMaintain";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "不结帐客人账务处理";
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridfj;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnjz;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn KRXM;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLDRQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RZZH;
        private System.Windows.Forms.Button btnviewzw;
    }
}