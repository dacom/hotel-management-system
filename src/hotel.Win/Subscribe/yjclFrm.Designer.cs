﻿namespace hotel.Win.Subscribe
{
    partial class yjclFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblfbz = new System.Windows.Forms.Label();
            this.lbye = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbfl = new System.Windows.Forms.Label();
            this.lbyzts = new System.Windows.Forms.Label();
            this.lbldrq = new System.Windows.Forms.Label();
            this.lbddrq = new System.Windows.Forms.Label();
            this.lbfj = new System.Windows.Forms.Label();
            this.lbfh = new System.Windows.Forms.Label();
            this.lbxf = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbyj = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtts = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.txtyj = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnprint = new System.Windows.Forms.Button();
            this.gridyj = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.cobxzzfs = new System.Windows.Forms.ComboBox();
            this.btnclose = new System.Windows.Forms.Button();
            this.btnctxz = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridyj)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblfbz);
            this.groupBox3.Controls.Add(this.lbye);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.lbfl);
            this.groupBox3.Controls.Add(this.lbyzts);
            this.groupBox3.Controls.Add(this.lbldrq);
            this.groupBox3.Controls.Add(this.lbddrq);
            this.groupBox3.Controls.Add(this.lbfj);
            this.groupBox3.Controls.Add(this.lbfh);
            this.groupBox3.Controls.Add(this.lbxf);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.lbyj);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(6, 9);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox3.Size = new System.Drawing.Size(198, 266);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "客人信息";
            // 
            // lblfbz
            // 
            this.lblfbz.BackColor = System.Drawing.Color.Lime;
            this.lblfbz.Location = new System.Drawing.Point(131, 141);
            this.lblfbz.Name = "lblfbz";
            this.lblfbz.Size = new System.Drawing.Size(63, 15);
            this.lblfbz.TabIndex = 19;
            this.lblfbz.Text = "联房:2";
            // 
            // lbye
            // 
            this.lbye.BackColor = System.Drawing.Color.Lime;
            this.lbye.Location = new System.Drawing.Point(86, 224);
            this.lbye.Name = "lbye";
            this.lbye.Size = new System.Drawing.Size(85, 16);
            this.lbye.TabIndex = 18;
            this.lbye.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 224);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 14);
            this.label10.TabIndex = 17;
            this.label10.Text = "余额：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "抵店日期：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "预住天数：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 14;
            this.label5.Text = "离店日期：";
            // 
            // lbfl
            // 
            this.lbfl.Location = new System.Drawing.Point(87, 47);
            this.lbfl.Name = "lbfl";
            this.lbfl.Size = new System.Drawing.Size(69, 15);
            this.lbfl.TabIndex = 7;
            this.lbfl.Text = "标双";
            // 
            // lbyzts
            // 
            this.lbyzts.Location = new System.Drawing.Point(90, 141);
            this.lbyzts.Name = "lbyzts";
            this.lbyzts.Size = new System.Drawing.Size(35, 15);
            this.lbyzts.TabIndex = 6;
            this.lbyzts.Text = "11";
            // 
            // lbldrq
            // 
            this.lbldrq.Location = new System.Drawing.Point(87, 117);
            this.lbldrq.Name = "lbldrq";
            this.lbldrq.Size = new System.Drawing.Size(90, 15);
            this.lbldrq.TabIndex = 6;
            this.lbldrq.Text = "2010-05-20";
            // 
            // lbddrq
            // 
            this.lbddrq.Location = new System.Drawing.Point(87, 92);
            this.lbddrq.Name = "lbddrq";
            this.lbddrq.Size = new System.Drawing.Size(96, 15);
            this.lbddrq.TabIndex = 6;
            this.lbddrq.Text = "2010-05-20";
            // 
            // lbfj
            // 
            this.lbfj.Location = new System.Drawing.Point(86, 71);
            this.lbfj.Name = "lbfj";
            this.lbfj.Size = new System.Drawing.Size(60, 13);
            this.lbfj.TabIndex = 6;
            this.lbfj.Text = "888";
            // 
            // lbfh
            // 
            this.lbfh.Location = new System.Drawing.Point(87, 26);
            this.lbfh.Name = "lbfh";
            this.lbfh.Size = new System.Drawing.Size(56, 12);
            this.lbfh.TabIndex = 5;
            this.lbfh.Text = "8888";
            // 
            // lbxf
            // 
            this.lbxf.Location = new System.Drawing.Point(87, 198);
            this.lbxf.Name = "lbxf";
            this.lbxf.Size = new System.Drawing.Size(85, 16);
            this.lbxf.TabIndex = 4;
            this.lbxf.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "消费合计：";
            // 
            // lbyj
            // 
            this.lbyj.Location = new System.Drawing.Point(86, 172);
            this.lbyj.Name = "lbyj";
            this.lbyj.Size = new System.Drawing.Size(85, 16);
            this.lbyj.TabIndex = 4;
            this.lbyj.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 172);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 14);
            this.label14.TabIndex = 2;
            this.label14.Text = "押金合计：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(35, 71);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 14);
            this.label19.TabIndex = 2;
            this.label19.Text = "房价：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(35, 48);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 14);
            this.label18.TabIndex = 2;
            this.label18.Text = "房类：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "房号：";
            // 
            // txtts
            // 
            this.txtts.Location = new System.Drawing.Point(274, 309);
            this.txtts.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtts.Name = "txtts";
            this.txtts.Size = new System.Drawing.Size(58, 23);
            this.txtts.TabIndex = 3;
            this.txtts.ValueChanged += new System.EventHandler(this.txtts_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(198, 310);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 14);
            this.label6.TabIndex = 17;
            this.label6.Text = "续订天数:";
            // 
            // txtyj
            // 
            this.txtyj.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtyj.Location = new System.Drawing.Point(81, 306);
            this.txtyj.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.txtyj.Name = "txtyj";
            this.txtyj.Size = new System.Drawing.Size(114, 23);
            this.txtyj.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 311);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 14);
            this.label8.TabIndex = 20;
            this.label8.Text = "支付押金:";
            // 
            // btnsave
            // 
            this.btnsave.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnsave.Location = new System.Drawing.Point(339, 306);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(91, 30);
            this.btnsave.TabIndex = 4;
            this.btnsave.Text = "新增";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnprint
            // 
            this.btnprint.Location = new System.Drawing.Point(453, 306);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(71, 30);
            this.btnprint.TabIndex = 5;
            this.btnprint.Text = "打印";
            this.btnprint.UseVisualStyleBackColor = true;
            this.btnprint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // gridyj
            // 
            this.gridyj.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridyj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column_ID});
            this.gridyj.Location = new System.Drawing.Point(207, 12);
            this.gridyj.Name = "gridyj";
            this.gridyj.RowHeadersWidth = 20;
            this.gridyj.RowTemplate.Height = 23;
            this.gridyj.Size = new System.Drawing.Size(490, 282);
            this.gridyj.TabIndex = 23;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "YJ";
            this.Column1.HeaderText = "押金";
            this.Column1.Name = "Column1";
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "CJRQ";
            this.Column2.HeaderText = "入帐日期";
            this.Column2.Name = "Column2";
            this.Column2.Width = 130;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "CJRY";
            this.Column3.HeaderText = "入帐人";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "ZFFS";
            this.Column4.HeaderText = "支付方式";
            this.Column4.Name = "Column4";
            this.Column4.Width = 90;
            // 
            // Column_ID
            // 
            this.Column_ID.DataPropertyName = "ID";
            this.Column_ID.HeaderText = "ID";
            this.Column_ID.Name = "Column_ID";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 280);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 14);
            this.label9.TabIndex = 16;
            this.label9.Text = "支付方式:";
            // 
            // cobxzzfs
            // 
            this.cobxzzfs.FormattingEnabled = true;
            this.cobxzzfs.Location = new System.Drawing.Point(81, 278);
            this.cobxzzfs.Name = "cobxzzfs";
            this.cobxzzfs.Size = new System.Drawing.Size(111, 21);
            this.cobxzzfs.TabIndex = 1;
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(626, 306);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(71, 30);
            this.btnclose.TabIndex = 24;
            this.btnclose.Text = "关闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btnctxz
            // 
            this.btnctxz.ForeColor = System.Drawing.Color.Red;
            this.btnctxz.Location = new System.Drawing.Point(530, 306);
            this.btnctxz.Name = "btnctxz";
            this.btnctxz.Size = new System.Drawing.Size(90, 30);
            this.btnctxz.TabIndex = 25;
            this.btnctxz.Text = "冲调选中";
            this.btnctxz.UseVisualStyleBackColor = true;
            this.btnctxz.Click += new System.EventHandler(this.btnctxz_Click);
            // 
            // yjclFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 344);
            this.Controls.Add(this.btnctxz);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.gridyj);
            this.Controls.Add(this.btnprint);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.txtyj);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cobxzzfs);
            this.Controls.Add(this.txtts);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox3);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "yjclFrm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "客人押金处理";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.yjclFrm_FormClosed);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridyj)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbfl;
        private System.Windows.Forms.Label lbfj;
        private System.Windows.Forms.Label lbfh;
        private System.Windows.Forms.Label lbyj;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbldrq;
        private System.Windows.Forms.Label lbddrq;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown txtts;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown txtyj;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnprint;
        private System.Windows.Forms.DataGridView gridyj;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbyzts;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cobxzzfs;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_ID;
        private System.Windows.Forms.Label lbye;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbxf;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblfbz;
        private System.Windows.Forms.Button btnctxz;
    }
}