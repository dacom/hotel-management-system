﻿namespace hotel.Win.Subscribe
{
    partial class ZhiKaMaintain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtfh = new System.Windows.Forms.TextBox();
            this.btncancel = new System.Windows.Forms.Button();
            this.dtpldrq = new System.Windows.Forms.DateTimePicker();
            this.dtpddrq = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtpldrq);
            this.groupBox1.Controls.Add(this.dtpddrq);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnok);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtfh);
            this.groupBox1.Controls.Add(this.btncancel);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(304, 212);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "开始时间:";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(100, 161);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(95, 35);
            this.btnok.TabIndex = 5;
            this.btnok.Text = "确定";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "结束时间:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "房号:";
            // 
            // txtfh
            // 
            this.txtfh.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtfh.Location = new System.Drawing.Point(100, 23);
            this.txtfh.Name = "txtfh";
            this.txtfh.ReadOnly = true;
            this.txtfh.Size = new System.Drawing.Size(136, 26);
            this.txtfh.TabIndex = 1;
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(201, 160);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(68, 36);
            this.btncancel.TabIndex = 6;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // dtpldrq
            // 
            this.dtpldrq.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dtpldrq.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpldrq.Location = new System.Drawing.Point(100, 104);
            this.dtpldrq.Name = "dtpldrq";
            this.dtpldrq.Size = new System.Drawing.Size(154, 23);
            this.dtpldrq.TabIndex = 9;
            // 
            // dtpddrq
            // 
            this.dtpddrq.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dtpddrq.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpddrq.Location = new System.Drawing.Point(100, 64);
            this.dtpddrq.Name = "dtpddrq";
            this.dtpddrq.Size = new System.Drawing.Size(154, 23);
            this.dtpddrq.TabIndex = 8;
            // 
            // ZhiKaMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 212);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ZhiKaMaintain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "制卡";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtfh;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.DateTimePicker dtpldrq;
        private System.Windows.Forms.DateTimePicker dtpddrq;
    }
}