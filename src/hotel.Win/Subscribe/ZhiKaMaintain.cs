﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;

namespace hotel.Win.Subscribe
{
    public partial class ZhiKaMaintain : Form
    {
        private string rzid;
        /// <summary>
        /// 房间制卡
        /// </summary>
        /// <param name="rzjlModel"></param>
        public ZhiKaMaintain(RZJL rzjlModel)
        {
            InitializeComponent();
            txtfh.Text = rzjlModel.ROOM.ID;
            dtpddrq.Value = rzjlModel.DDRQ.Value.AddHours(-1);
            dtpldrq.Value = rzjlModel.LDRQ.Value;
            rzid = rzjlModel.ID;
            if (rzjlModel.YJ.Value <= 0)
            {
                btnok.Enabled = false;
                MessageBox.Show("没交押金不能制卡");
            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            var ICard = Utility.CreateKeycarder();
            var str = txtfh.Text;

            var info = new CardInfo();
            info.CommId = 3;
            info.CardType = 0;
            info.FloorCode = Convert.ToInt32(str[0].ToString());
            info.FloorLayCode = Convert.ToInt32(str[1].ToString());
            info.RoomCode = Convert.ToInt32(str[2].ToString() + str[3].ToString());
            info.SubRoomCode = 255;
            info.IsRight = false;
            info.StartDateTime = dtpddrq.Value;
            info.EndDateTime = dtpldrq.Value;
            info.MF1Area = 15;
            var cardInfo = ICard.WriteGuestCardInfo(info);
            if ( cardInfo!= null)
            {
                using (var _context = MyDataContext.GetDataContext)
                {
                    var model = _context.FKSYXX.Create();
                    model.CJRQ = DateTime.Now;
                    model.CJRY = UserLoginInfo.FullName;
                    model.JSSJ = cardInfo.EndDateTime;
                    model.KSSJ = cardInfo.StartDateTime;
                    model.KH = cardInfo.CardId;
                    model.SFGH = "否";
                    model.RZJL_ID = rzid;
                    _context.FKSYXX.Add(model);
                    _context.SaveChanges();
                }

                #region 日志纪录
                var logInfo = new CcrzInfo();
                logInfo.Abstract = LogAbstractEnum.制卡;
                logInfo.Type = LogTypeEnum.一般操作;
                logInfo.RoomId = txtfh.Text;
                logInfo.Rzid = rzid;
                logInfo.Message = "开始时间:" + cardInfo.StartDateTime.ToShortDateString()
                    + ",结束时间:" + cardInfo.EndDateTime.ToShortDateString();
                logInfo.Save();
                #endregion
                this.DialogResult = DialogResult.OK;
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
