﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
using System.Data.Common;


namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 消费转帐
    /// </summary>
    public partial class krfyzzMaintain : Form
    {
        public krfyzzMaintain(XFJL xfjl)
        {
            InitializeComponent();
            txtdqfj.Text = xfjl.ROOM.ID;
            txtxm.Text = xfjl.XFXM_MC;
            txtxfje.Text = xfjl.JE.ToString();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtmbfj.Text.Length == 0)
            {
                MessageBox.Show("目标房间号不能为空！");
                return;
            }
            if (txtmbfj.Text == txtdqfj.Text)
            {
                MessageBox.Show("目标房间不能与当前房间相同！");
                return;
            }
            this.DialogResult = DialogResult.OK;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// 转帐目标房间号
        /// </summary>
        public string RoomID { get { return txtmbfj.Text; } }

        private void btnfind_Click(object sender, EventArgs e)
        {
            var frm = new fjFind("费用转单");
            if (frm.ShowDialog() != DialogResult.OK)
                return;
            txtmbfj.Text = frm.SelectedROOM;
        }
    }
}
