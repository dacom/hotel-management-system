﻿namespace hotel.Win.Subscribe
{
    partial class kyhfMaintain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtyfh = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtyfj = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtxfh = new System.Windows.Forms.TextBox();
            this.btnok = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.txtxfj = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txthfyy = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtxfj)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "原房号:";
            // 
            // txtyfh
            // 
            this.txtyfh.Location = new System.Drawing.Point(87, 20);
            this.txtyfh.Name = "txtyfh";
            this.txtyfh.ReadOnly = true;
            this.txtyfh.Size = new System.Drawing.Size(110, 23);
            this.txtyfh.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "原房价:";
            // 
            // txtyfj
            // 
            this.txtyfj.Location = new System.Drawing.Point(87, 57);
            this.txtyfj.Name = "txtyfj";
            this.txtyfj.ReadOnly = true;
            this.txtyfj.Size = new System.Drawing.Size(110, 23);
            this.txtyfj.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(207, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "新房号:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(207, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "新房价:";
            // 
            // txtxfh
            // 
            this.txtxfh.Location = new System.Drawing.Point(276, 20);
            this.txtxfh.Name = "txtxfh";
            this.txtxfh.Size = new System.Drawing.Size(110, 23);
            this.txtxfh.TabIndex = 2;
            this.txtxfh.TextChanged += new System.EventHandler(this.txtxfh_TextChanged);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(129, 116);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(68, 28);
            this.btnok.TabIndex = 5;
            this.btnok.Text = "确定";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(229, 116);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(68, 28);
            this.btncancel.TabIndex = 6;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // txtxfj
            // 
            this.txtxfj.DecimalPlaces = 2;
            this.txtxfj.Location = new System.Drawing.Point(276, 60);
            this.txtxfj.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtxfj.Name = "txtxfj";
            this.txtxfj.Size = new System.Drawing.Size(110, 23);
            this.txtxfj.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txthfyy);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnok);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtxfj);
            this.groupBox1.Controls.Add(this.txtxfh);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtyfh);
            this.groupBox1.Controls.Add(this.txtyfj);
            this.groupBox1.Controls.Add(this.btncancel);
            this.groupBox1.Location = new System.Drawing.Point(5, -4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(417, 144);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 14);
            this.label5.TabIndex = 7;
            this.label5.Text = "换房原因:";
            // 
            // txthfyy
            // 
            this.txthfyy.Location = new System.Drawing.Point(87, 86);
            this.txthfyy.Name = "txthfyy";
            this.txthfyy.Size = new System.Drawing.Size(299, 23);
            this.txthfyy.TabIndex = 8;
            // 
            // kyhfMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 152);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "kyhfMaintain";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "客人换房";
            ((System.ComponentModel.ISupportInitialize)(this.txtxfj)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtyfh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtyfj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtxfh;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.NumericUpDown txtxfj;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txthfyy;
        private System.Windows.Forms.Label label5;
    }
}