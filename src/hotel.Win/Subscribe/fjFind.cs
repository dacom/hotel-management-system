﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;

using System.Data.Common;

namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 房间查询
    /// </summary>
    public partial class fjFind : Form
    {
        hotelEntities _context;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title">窗体显示的标题</param>
        public fjFind(string title)
        {
            InitializeComponent();
            this.Text = title;
            _context = MyDataContext.GetDataContext;
            gridfj.AutoGenerateColumns = false;
            var qry = from rz in _context.UV_RZJL_RZRY_Single.AsNoTracking()
                      //join ry in _context.RZRY on rz.ID equals ry.RZJL.ID
                      where rz.RZBZ == "是"
                      orderby rz.ROOM_ID
                      select new
                      {
                          ROOMID = rz.ROOM_ID,
                          KRXM = rz.KRXM,
                          RZZH = rz.ID,
                          BZSM = rz.BZSM,
                          IsZZ = rz.F_ID == null ? "是" : "否" 
                      };
            gridfj.DataSource = qry.ToList();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (gridfj.CurrentRow == null)
                return;
            this.DialogResult = DialogResult.OK;
        }

        public RZJL SelectedRZJL
        {
            get
            {
                if (gridfj.CurrentRow == null)
                    return null;
                var id = gridfj.CurrentRow.Cells["RZZH"].Value.ToString();
                return _context.RZJL.First(p => p.ID == id);
            }
        }


        public string SelectedROOM
        {
            get
            {
                var rzjl = SelectedRZJL;
                if (rzjl == null)
                    return null;
                return rzjl.ROOM.ID;
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
