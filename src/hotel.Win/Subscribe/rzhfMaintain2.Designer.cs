﻿namespace hotel.Win.Subscribe
{
    partial class rzhfMaintain2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btncancel = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.maingrid = new System.Windows.Forms.DataGridView();
            this.ROOM_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RZID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btncancel);
            this.panel1.Controls.Add(this.btnok);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 247);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(596, 41);
            this.panel1.TabIndex = 0;
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(226, 6);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(68, 28);
            this.btncancel.TabIndex = 3;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(145, 6);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(68, 28);
            this.btnok.TabIndex = 2;
            this.btnok.Text = "确定";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.maingrid);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(596, 247);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "客单房间情况";
            // 
            // maingrid
            // 
            this.maingrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.maingrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROOM_ID,
            this.Column2,
            this.Column3,
            this.ID,
            this.ZT,
            this.FZ,
            this.RZID});
            this.maingrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maingrid.Location = new System.Drawing.Point(3, 19);
            this.maingrid.Name = "maingrid";
            this.maingrid.ReadOnly = true;
            this.maingrid.RowHeadersWidth = 25;
            this.maingrid.RowTemplate.Height = 23;
            this.maingrid.Size = new System.Drawing.Size(590, 225);
            this.maingrid.TabIndex = 0;
            // 
            // ROOM_ID
            // 
            this.ROOM_ID.DataPropertyName = "ROOM_ID";
            this.ROOM_ID.HeaderText = "原房间";
            this.ROOM_ID.Name = "ROOM_ID";
            this.ROOM_ID.ReadOnly = true;
            this.ROOM_ID.Width = 80;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "ZT";
            this.Column2.HeaderText = "原房间房态";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "FZ";
            this.Column3.HeaderText = "原房租";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 80;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "新房间";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 80;
            // 
            // ZT
            // 
            this.ZT.DataPropertyName = "ZT";
            this.ZT.HeaderText = "新房间房态";
            this.ZT.Name = "ZT";
            this.ZT.ReadOnly = true;
            // 
            // FZ
            // 
            this.FZ.DataPropertyName = "FZ";
            this.FZ.HeaderText = "新房间房租";
            this.FZ.Name = "FZ";
            this.FZ.ReadOnly = true;
            // 
            // RZID
            // 
            this.RZID.DataPropertyName = "RZID";
            this.RZID.HeaderText = "RZID";
            this.RZID.Name = "RZID";
            this.RZID.ReadOnly = true;
            this.RZID.Visible = false;
            // 
            // rzhfMaintain2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 288);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "rzhfMaintain2";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "合单恢复入住";
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.DataGridView maingrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROOM_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZT;
        private System.Windows.Forms.DataGridViewTextBoxColumn FZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn RZID;
    }
}