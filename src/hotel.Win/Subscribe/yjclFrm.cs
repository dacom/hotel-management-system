﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
namespace hotel.Win.Subscribe
{
    /// <summary>
    /// 增加客户押金
    /// </summary>
    public partial class yjclFrm : Form
    {
        hotelEntities _context;
        RZJL dataModel;
        bool isAdd;
        int yjxm_id;//押金项目ID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rzzh">入住帐号</param>
        public yjclFrm(string rzzh)
        {
            InitializeComponent();
            isAdd = false;
            _context = MyDataContext.GetDataContext;
            yjxm_id = _context.XFXM.First(p => p.DM == "YJ").ID;//系统固定= 押金
            cobxzzfs.DataSource = XTCYDMDal.GetAllByLBDM(EnumXtcydmLB.ZFFS.ToString());
            cobxzzfs.DisplayMember = "XMMC";
            cobxzzfs.ValueMember = "XMDM";
            dataModel = _context.RZJL.First(p => p.ID == rzzh);
            if (dataModel.RZBZ == "是")
                this.Text = "客人押金处理";
            else
                this.Text = "客人押金处理[预定]";
            lbfh.Text = dataModel.ROOM.ID;            
            lbfl.Text = dataModel.ROOM.ROOM_TYPE.JC;
            lbfj.Text = dataModel.SJFJ.ToString();
            lbddrq.Text = dataModel.DDRQ.Value.Date.ToString("yyyy-MM-dd");
            lbldrq.Text = dataModel.LDRQ.Value.Date.ToString("yyyy-MM-dd");
            lbyzts.Text = (dataModel.LDRQ.Value.Date - dataModel.DDRQ.Value.Date).Days.ToString();
            if (string.IsNullOrEmpty(dataModel.LFH))
                lblfbz.Visible = false;
            else
            {
                lblfbz.Visible = true;
                lblfbz.Text = "联房:" + _context.RZJL.Count(p => p.LFH == dataModel.LFH);
            }
                
            decimal? je = _context.XFJL.Where(p => p.RZJL_ID == rzzh && p.JE > 0).Sum(p => p.JE);
            lbxf.Text = (je ?? 0).ToString();
            BindGrid();
        }

        private void BindGrid()
        {
            var qry = from q in _context.XFJL.AsNoTracking()
                                where q.RZJL_ID == dataModel.ID && q.XFXM_ID == yjxm_id
                                orderby q.CJRQ descending
                                select new
                                {
                                    q.CJRQ,
                                    q.CJRY,
                                    q.YJ,
                                    q.ZFFS,
                                    q.ID
                                };
            gridyj.DataSource = qry.ToList();
            lbyj.Text = dataModel.YJ.ToString();
            lbye.Text = (dataModel.YJ - Convert.ToDecimal(lbxf.Text)).ToString();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtyj.Value == 0)
                return;
            var xfjlModel = new XFJL();
            xfjlModel.CJRQ = DateTime.Now;
            xfjlModel.CJRY = UserLoginInfo.FullName;
            xfjlModel.DJ = 0;
            xfjlModel.JE = 0;
            xfjlModel.SL = 1;
            xfjlModel.ROOM_ID = dataModel.ROOM_ID;
            xfjlModel.RZJL_ID = dataModel.ID;
            xfjlModel.XFXM_ID = yjxm_id;
            xfjlModel.XFXM_MC = "押金";
            xfjlModel.YJ = txtyj.Value;
            xfjlModel.ZFFS = cobxzzfs.Text;
            xfjlModel.XFBZ = "押";
            _context.XFJL.Add(xfjlModel);
            if (txtts.Value > 0)
            {
                dataModel.LDRQ = dataModel.LDRQ.Value.AddDays((double)txtts.Value);
                dataModel.TS = Convert.ToInt32(lbyzts.Text);
            }
            //合计押金
            dataModel.YJ += (int)txtyj.Value;
            _context.SaveChanges();
            BindGrid();
            lbyzts.Text = (dataModel.LDRQ.Value.Date - dataModel.DDRQ.Value.Date).Days.ToString();
            //清空相关数据

            txtts.Value = 0;
            txtyj.Value = 0;
            isAdd = true;
            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.补交押金;
            logInfo.RoomId = dataModel.ROOM_ID;
            logInfo.Rzid = dataModel.ID;
            logInfo.Message = "押金:" + txtyj.Value.ToString() + ",续订天数:" + txtts.Value.ToString();
            logInfo.Save();
            #endregion
        }

        private void txtts_ValueChanged(object sender, EventArgs e)
        {
            lbldrq.Text = (dataModel.LDRQ.Value.AddDays((int)txtts.Value)).ToString();
            lbyzts.Text = ((int)txtts.Value + Convert.ToInt32(lbyzts.Text)).ToString();
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
           var xfjlModel = GetSelectXFJL();
            if (xfjlModel == null)
                return;
            #region 日志
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.打印;
            logInfo.RoomId = dataModel.ROOM_ID;
            logInfo.Rzid = dataModel.ID;
            logInfo.Message = "打印押金单,金额:" + xfjlModel.YJ.ToString();
            #endregion

            hotel.Win.Report.ReportEntry.ShowYaJinBill(xfjlModel.ID, logInfo);

        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            _context.Dispose();
            this.Close();
        }

        private void yjclFrm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = isAdd ? DialogResult.OK : DialogResult.Cancel;
        }
        private XFJL GetSelectXFJL()
        {
            string XFID = Common.ControlsUtility.GetActiveGridValue(gridyj, "Column_ID");
            if (XFID == "")
            {
                MessageBox.Show("请选择一个押金记录!");
                return null;
            }
            int id = Convert.ToInt32(XFID);
            return _context.XFJL.FirstOrDefault(p => p.ID == id);
        }
        private void btnctxz_Click(object sender, EventArgs e)
        {
            var xfjlModel = GetSelectXFJL();
            if (xfjlModel == null)
                return;
            if (xfjlModel.YJ.Value < 0)
            {
                MessageBox.Show("押金小于0，操作失败");
                return;
            }
               
            if (MessageBox.Show("押金:" + xfjlModel.YJ.ToString() + "\n你确定押金冲调吗？", "费用冲调", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                return;
            var newModel = new XFJL();
            newModel.CJRQ = DateTime.Now;
            newModel.CJRY = UserLoginInfo.FullName;
            newModel.DJ = 0;
            newModel.JE = 0;
            newModel.SL = 1;
            newModel.ROOM_ID = xfjlModel.ROOM_ID;
            newModel.RZJL_ID = xfjlModel.RZJL_ID;
            newModel.XFXM_ID = yjxm_id;
            newModel.XFXM_MC = "押金";
            newModel.YJ = -xfjlModel.YJ; //负值
            newModel.ZFFS = xfjlModel.ZFFS;
            newModel.XFBZ = "冲";
            newModel.BZSM="冲调";
            _context.XFJL.Add(newModel);
            //合计押金
            dataModel.YJ -= (int)xfjlModel.YJ;
            _context.SaveChanges();
            BindGrid();
            #region 日志
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.消费退单;
            logInfo.RoomId = dataModel.ROOM_ID;
            logInfo.Rzid = dataModel.ID;
            logInfo.Message = "冲调押金，金额:" + xfjlModel.YJ.ToString();
            logInfo.Save();
            #endregion
        }
    }
}
