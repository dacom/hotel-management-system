﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
using hotel.DAL;
using hotel.Win.BaseData;
using hotel.Win.Subscribe;
using hotel.Common;
using hotel.MyUserControl;
using hotel.Win.Report;
using hotel.Win.Query;
namespace hotel.Win
{
    public partial class MainControl
    {
        private bool CheckucRoom(ucRoom ucroom)
        {
            if (ucroom == null)
            {
                MessageBox.Show("操作前，请先选择一个房间！");
                return false;
            }
            return true;
        }
        /// <summary>
        /// 新预订
        /// </summary>
        /// <param name="ucroom"></param>
        public void NewYDMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            if (ucroom.Room.ZT == RoomStstusCnName.SuoDing)
            {
                MessageBox.Show("锁定房不能预定或入住，请先解锁");
                return;
            }
            var frm = new jddjFrm(ucroom.Room.ID, 0);
            frm.ShowDialog();
            if (frm.IsMutilRoom)
            {               
                RefreshRoomFromDataBase(frm.RoomIds);
            }
            else
            {
                RefreshRoomFromDataBase(ucroom);
            }
        }
        public void NewYDMenuMethod()
        {
            NewYDMenuMethod(currentURoom);
        }
        /// <summary>
        /// 客人登记
        /// </summary>
        public void KRTJMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            if (ucroom.Room.ZT == RoomStstusCnName.SuoDing)
            {
                MessageBox.Show("锁定房不能预定或入住，请先解锁");
                return;
            }

            jddjFrm frm;
            rzkrSelect selectFrm;

            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjls = _context.RZJL.Where(p => p.ROOM.ID == ucroom.Room.ID && p.YDBZ == "否").ToList();
                //当前只有一个入住
                if (rzjls.Count == 0)
                    frm = new jddjFrm(ucroom.Room.ID, 1);
                else if (rzjls.Count == 1)
                    frm = new jddjFrm(rzjls.First().ID, 1, false);
                else
                {
                    selectFrm = new rzkrSelect(rzjls);
                    if (selectFrm.ShowDialog() != DialogResult.OK)
                        return;
                    frm = new jddjFrm(selectFrm.SelectedRZJL.ID, 1, false);
                }
            }
            frm.ShowDialog();
            if (frm.IsMutilRoom)
            {
                RefreshRoomFromDataBase(frm.RoomIds);
            }
            else
            {
                RefreshRoomFromDataBase(ucroom);
            }
        }
        public void KRTJMenuMethod()
        {
            KRTJMenuMethod(currentURoom);
        }
        /// <summary>
        /// 客人信息
        /// </summary>
        public void KRXXMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            jddjFrm frm;
            rzkrSelect selectFrm;
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjls = _context.RZJL.Where(p => p.ROOM.ID == ucroom.Room.ID).ToList();
                //当前只有一个入住
                if (rzjls.Count == 0)
                {
                    MessageBox.Show("此房间没有客人入住或预订！");
                    return;
                }
                else if (rzjls.Count == 1)
                    frm = new jddjFrm(rzjls.First().ID, rzjls.First().YDBZ == "是" ? 0 : 1, true);
                else
                {
                    selectFrm = new rzkrSelect(rzjls);
                    if (selectFrm.ShowDialog() != DialogResult.OK)
                        return;
                    frm = new jddjFrm(selectFrm.SelectedRZJL.ID, selectFrm.SelectedRZJL.YDBZ == "是" ? 0 : 1, true);
                }
            }
            frm.ShowDialog();
            RefreshRoomFromDataBase(ucroom);
            if (frm.RoomIds != null)
                RefreshRoomFromDataBase(frm.RoomIds);            
        }

        public void KRXXMenuMethod()
        {
            KRXXMenuMethod(currentURoom);
        }
        /// <summary>
        /// 客人换房
        /// </summary>
        public void KRHFMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            RZJL sourceRzjl;
            kyhfMaintain frm ;
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjls = _context.RZJL.Where(p => p.ROOM.ID == ucroom.Room.ID).ToList();
                if (rzjls.Count == 0)
                {
                    MessageBox.Show("该房间不具备换房条件!只有在住房和预订房才可以!");
                    return;
                }
                else if (rzjls.Count == 1)
                    sourceRzjl = rzjls.First();
                else
                {
                    var selectFrm = new rzkrSelect(rzjls);
                    if (selectFrm.ShowDialog() != DialogResult.OK)
                        return;
                    sourceRzjl = selectFrm.SelectedRZJL;
                }
               frm = new kyhfMaintain(sourceRzjl.ROOM.ID, (decimal)sourceRzjl.SJFJ, "", 0);
                if (frm.ShowDialog() != DialogResult.OK)
                    return;
            }
            SaveKRHF(sourceRzjl.ID, frm.RoomId, frm.NewFJ, frm.YY);
            RefreshRoomFromDataBase(new List<string> {ucroom.Room.ID,frm.RoomId});
        }
        public void KRHFMenuMethod()
        {
            KRHFMenuMethod(currentURoom);
        }
        //制卡
        public void ZhiKaFMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;

            var _context = MyDataContext.GetDataContext;
            var rzjls = _context.RZJL.AsNoTracking().Where(p => p.ROOM.ID == ucroom.Room.ID).ToList();
            if (rzjls.Count == 0)
            {
                MessageBox.Show("只有在住房和预订房才可以制卡!");
                return;
            }
            var frm = new ZhiKaMaintain(rzjls.First());
            frm.ShowDialog();
        }
        //查看房卡
        public void FangKaViewMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            var rzzh = "";
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjls = _context.RZJL.Where(p => p.ROOM.ID == ucroom.Room.ID).ToList();
                if (rzjls.Count == 0)
                {
                    MessageBox.Show("只有在住房和预订房才可以查看!");
                    return;
                }
                rzzh = rzjls.First().ID;
            }
            var frm = new FangKaView(rzzh);
            frm.ShowDialog();
        }
        /// <summary>
        /// 保存客人换房
        /// </summary>
        /// <param name="rzjl"></param>
        /// <param name="targetRommId"></param>
        private void SaveKRHF(string sourceRzjlId, string targetRoomId, decimal targetRoomFJ,string yy)
        {
             var _context = MyDataContext.GetDataContext;
             RZJL sourceRzjl = _context.RZJL.Find(sourceRzjlId);
            string sourceRoomId = sourceRzjl.ROOM_ID;
            if (sourceRoomId == targetRoomId)
            {
                return;
            }
            
            var targetRoom = _context.ROOM.FirstOrDefault(p => p.ID == targetRoomId);
            if (targetRoom == null)
            {
                MessageBox.Show("目标房间" + targetRoomId + "不是有效的房间号");
                return;
            }
            //目标房不能是在住
            if (_context.RZJL.FirstOrDefault(p => p.ROOM.ID == targetRoomId && p.RZBZ == "是") != null)
            {
                MessageBox.Show("目标房间" + targetRoomId + "已经有人在住,不能换房");
                return;
            }
            
            //如果是入住换房，对于预订换房，不改变原/目标的房态
            if (sourceRzjl.RZBZ == "是")
            {
                targetRoom.ZT = sourceRzjl.ROOM.ZT;
                targetRoom.RZBZ = "是";
                sourceRzjl.ROOM.ZT = RoomStstusCnName.KongZang;
                sourceRzjl.ROOM.RZBZ = "否";
            }
            var logmsg = "原房号:" + sourceRoomId + "，目标房号:" + targetRoomId;
            if (sourceRzjl.SJFJ != targetRoomFJ)
                logmsg += "；原房价:" + sourceRzjl.SJFJ.ToString() + ",新房价:" + targetRoomFJ.ToString();
            logmsg += ",原因：" + yy;
           
            var sourceRoomFJ = sourceRzjl.SJFJ.Value;
            sourceRzjl.ROOM = targetRoom;
            sourceRzjl.SJFJ = targetRoomFJ;           
           
            //更新消费记录房号
            foreach (var item in _context.XFJL.Where(p => p.RZJL.ID == sourceRzjl.ID))
            {
                item.ROOM = targetRoom;
            }
            _context.SaveChanges();

            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.客人换房;
            logInfo.RoomId = sourceRoomId;
            logInfo.Rzid = sourceRzjl.ID;
            logInfo.Message = logmsg; ;
            logInfo.Save();
            #endregion
            logInfo.Abstract = LogAbstractEnum.打印;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "打印客人换房表";
            if (MessageBox.Show("你的换房操作成功,是否打印", "操作成功", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                ReportEntry.ShowKeRenHuanFangBill(sourceRzjl.RZRY.First().KRXM, sourceRzjl.DDRQ.Value,
                    sourceRoomId, targetRoomId, sourceRoomFJ, targetRoomFJ, yy, logInfo);
            }
        }

        /// <summary>
        /// 消费入单
        /// </summary>
        public void XFRDMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            var rzzh = "";
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjl = _context.RZJL.FirstOrDefault(p => p.ROOM.ID == ucroom.Room.ID && p.RZBZ == "是");
                if (rzjl == null)
                {
                    MessageBox.Show("该房间还没有客人入住!");
                    return;
                }
                rzzh = rzjl.ID;
            }
            var frm = new krfyMaintain(rzzh);
            frm.ShowDialog();
        }

        public void XFRDMenuMethod()
        {
            XFRDMenuMethod(currentURoom);
        }
        /// <summary>
        /// 押金处理
        /// </summary>
        public void YJCLMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            var rzzh = "";
            using (var _context = MyDataContext.GetDataContext)
            {
                RZJL rzjl = _context.RZJL.FirstOrDefault(p => p.ROOM.ID == ucroom.Room.ID && p.RZBZ == "是");
                if (rzjl == null)
                {
                    rzjl = _context.RZJL.FirstOrDefault(p => p.ROOM.ID == ucroom.Room.ID && p.YDBZ == "是");
                    if (rzjl == null)
                    {
                        MessageBox.Show("该房间还没有客人入住或预定!");
                        return;
                    }
                }
                if (!string.IsNullOrEmpty(rzjl.F_ID))
                    MessageBox.Show("子房间不能处理押金，自动转到主房间");
                rzzh = rzjl.F_ID ?? rzjl.ID;
            }
            
            //押金默认进入父房
            var frm = new yjclFrm(rzzh);
            frm.ShowDialog();
            RefreshRoomFromDataBase(ucroom);
            //_context.Refresh(System.Data.Objects.RefreshMode.StoreWins, ucroom.Room.RZJL);

        }
        public void YJCLMenuMethod()
        {
            YJCLMenuMethod(currentURoom);
        }

        /// <summary>
        /// 结帐退房
        /// </summary>
        public void JZTFMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            var rzzh = "";
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjl = _context.RZJL.FirstOrDefault(p => p.ROOM.ID == ucroom.Room.ID && p.RZBZ == "是");
                if (rzjl == null)
                {
                    MessageBox.Show("该房间还没有客人入住!");
                    return;
                }
                rzzh = rzjl.ID;
            }
            var frm = new jztfFrm(rzzh);
            frm.ShowDialog();
            if (frm.RoomIds != null && frm.RoomIds.Count >1)
            {
                RefreshRoomFromDataBase(frm.RoomIds);
            }
            else
            {
                RefreshRoomFromDataBase(ucroom);
            }
        }
        public void JZTFMenuMethod()
        {
            JZTFMenuMethod(currentURoom);
        }

        /// <summary>
        /// 不结帐退房
        /// </summary>
        public void BJZTFMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            List<string> zroomids;
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjl = _context.RZJL.FirstOrDefault(p => p.ROOM.ID == ucroom.Room.ID && p.RZBZ == "是");
                if (rzjl == null)
                {
                    MessageBox.Show("该房间还没有客人入住!");
                    return;
                }
                string rzid = string.IsNullOrEmpty(rzjl.F_ID) ? rzjl.ID : rzjl.F_ID;
                string msg = "";
                var sumyj = _context.XFJL.Where(p => p.RZJL_ID == rzid && p.XFXM_MC == "押金").Sum(p => p.YJ);
                var sumxf = _context.XFJL.Where(p => p.RZJL_ID == rzid && p.XFXM_MC != "押金").Sum(p => p.JE);
                msg = "你确定对" + ucroom.Room.ID + "房间的客人进行不结帐退房？\n"
                    + "客人押金总额：" + sumyj.ToString() + ",消费总额：" + sumxf.ToString() + ",余额:" + (sumyj - sumxf).ToString()
                    + "\n注意：如果有合单，则一起进行不结帐退房";
                if (MessageBox.Show(msg, SysConsts.SysWarningCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK)
                    return;

                zroomids = _context.RZJL.Where(p => p.F_ID == rzid).Select(p => p.ROOM.ID).ToList();//所有子入住记录,room_id,刷新房态用

                _context.RZJL_JZTF_PROC(rzid, UserLoginInfo.FullName, "否", 0, 0);
                #region 日志纪录
                var logInfo = new CcrzInfo();
                logInfo.Abstract = LogAbstractEnum.不结账退房;
                logInfo.RoomId = rzjl.ROOM_ID;
                logInfo.Rzid = rzid;
                logInfo.Message = "押金:" + sumyj.ToString() + ",消费:" + sumxf.ToString() + ",余额:" + (sumyj - sumxf).ToString();
                logInfo.Save();
                #endregion
            }
            RefreshRoomFromDataBase(ucroom);
            if (zroomids != null && zroomids.Count > 0)
                RefreshRoomFromDataBase(zroomids);
            MessageBox.Show("不结帐退房，成功");
        }
        //不结账退房
        public void BJZTFMenuMethod()
        {
            BJZTFMenuMethod(currentURoom);
        }
        /// <summary>
        /// 合并帐单
        /// </summary>
        public void HBZDMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            var _context = MyDataContext.GetDataContext;

            var rzjl = _context.RZJL.FirstOrDefault(p => p.ROOM.ID == ucroom.Room.ID && p.RZBZ == "是");
            if (rzjl == null)
            {
                MessageBox.Show("该房间还没有客人入住,不能合并帐单!");
                return;
            }
            if (!string.IsNullOrEmpty(rzjl.F_ID))
            {
                MessageBox.Show("该房间客人已经存在联单，不用再合并帐单!");
                return;
            }
            //帐单合并只支持一层
            if (_context.RZJL.AsNoTracking().Count(p => p.F_ID == rzjl.ID) > 0)
            {
                MessageBox.Show("该房间客人已经作为父联单，不用再合并帐单!");
                return;
            }
            var frm = new fjFind("合并帐单");
            if (frm.ShowDialog() != DialogResult.OK)
                return;
            if (rzjl.ID == frm.SelectedRZJL.ID)
            {
                MessageBox.Show("不能和自己合并帐单");
                return;
            }
            if (!string.IsNullOrEmpty(frm.SelectedRZJL.F_ID))
            {
                MessageBox.Show("此房间已经属于某个联房中的子房间，操作失败");
                return;
            }
            rzjl.F_ID = frm.SelectedRZJL.ID;
            if (string.IsNullOrEmpty(frm.SelectedRZJL.LFH))
            {
                rzjl.LFH = _context.GetLfh();
                _context.RZJL.Find(frm.SelectedRZJL.ID).LFH = rzjl.LFH;
            }
            else
                rzjl.LFH = frm.SelectedRZJL.LFH;
           
            _context.SaveChanges();
            MessageBox.Show(rzjl.ROOM_ID + "成功合并帐单!");
            //写日志
            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.合并帐单;
            logInfo.RoomId = rzjl.ROOM_ID;
            logInfo.Rzid = rzjl.ID;
            logInfo.Message =  "原房号:" + ucroom.Room.ID + ",关联房号:" + frm.SelectedRZJL.ROOM_ID;
            logInfo.Save();
            #endregion
            RefreshRoomFromDataBase(ucroom);
            RefreshRoomFromDataBase(new List<string>() { frm.SelectedRZJL.ROOM_ID });
        }
        public void HBZDMenuMethod()
        {
            HBZDMenuMethod(currentURoom);
        }

        /// <summary>
        /// 拆分帐单
        /// </summary>
        public void CFZDMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            var _context = MyDataContext.GetDataContext;
            var rzjl = _context.RZJL.FirstOrDefault(p => p.ROOM.ID == ucroom.Room.ID && p.RZBZ == "是");
            if (rzjl == null)
            {
                MessageBox.Show("该房间还没有客人入住,不能拆分帐单!");
                return;
            }
            string fID = rzjl.F_ID;
            if (string.IsNullOrEmpty(fID))
            {
                MessageBox.Show("该房间客人没有联单，不用拆分帐单!");
                return;
            }
            if (MessageBox.Show("你确定对房间"+ rzjl.ROOM_ID+"拆分帐单？", hotel.Common.SysConsts.SysWarningCaption, MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;
            var Frzjl = _context.RZJL.Find(fID) ?? new RZJL();
            rzjl.F_ID = null; //""与null是不同的
            rzjl.LFH = null;
            _context.SaveChanges();
            MessageBox.Show(ucroom.Room.ID + "成功拆分帐单!");
            RefreshRoomFromDataBase(ucroom);
            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.拆分帐单;
            logInfo.RoomId = rzjl.ROOM_ID;
            logInfo.Rzid = rzjl.ID;
            logInfo.Message = "原房号:" + ucroom.Room.ID + ",关联房号: " + Frzjl.ROOM_ID +",父帐号: " + fID;
            logInfo.Save();
            #endregion
        }
        public void CFZDMenuMethod()
        {
            CFZDMenuMethod(currentURoom);
        }

        /// <summary>
        /// 房态修改
        /// </summary>
        public void FTXGMenuMethod(ucRoom ucroom)
        {
            if (!CheckucRoom(ucroom))
                return;
            var frm = new ftMaintain(ucroom.Room.ID);
            frm.ShowDialog();
            RefreshRoomFromDataBase(ucroom);
        }

        public void FTXGMenuMethod()
        {
            FTXGMenuMethod(currentURoom);
        }

        /// <summary>
        /// 删除客人
        /// </summary>
        /// <param name="ucroom"></param>
        public void SCKRMenuMethod(ucRoom ucroom)
        {
            using (var _context = MyDataContext.GetDataContext)
            {
                if (!UserLoginInfo.HaveRole(EnumRoole.Admin))
                {
                    MessageBox.Show("只有管理员才可以操作");
                    #region 日志纪录
                    var logInfo2 = new CcrzInfo();
                    logInfo2.Abstract = LogAbstractEnum.未知;
                    logInfo2.Type = LogTypeEnum.一般操作;
                    logInfo2.Message = "非管理员，试图删除客单";
                    logInfo2.Save();
                    #endregion
                    return;
                }

                if (!CheckucRoom(ucroom))
                    return;
                rzkrSelect selectFrm;
                var rzjls = _context.RZJL.Where(p => p.ROOM.ID == ucroom.Room.ID).ToList();
                RZJL rzjl = null;
                if (rzjls.Count == 0)
                {
                    MessageBox.Show("此房间没有客人入住或预订！");
                    return;
                }
                else if (rzjls.Count == 1)
                    rzjl = rzjls.First();
                else
                {
                    selectFrm = new rzkrSelect(rzjls);
                    if (selectFrm.ShowDialog() != DialogResult.OK)
                        return;
                    rzjl = selectFrm.SelectedRZJL;
                }
                //可冲调后再删除
                if (_context.XFJL.Where(p => p.RZJL_ID == rzjl.ID).Sum(p => p.JE) > 0)
                {
                    MessageBox.Show("此房间已经发生费用，不能删除!");
                    return;
                }
              
                string msg = "你是否确定删除帐号为:" + rzjl.ID + "房号为:" + ucroom.Room.ID +
                             "\n姓名:" + rzjl.RZRY.Max(p => p.KRXM) + ",押金:" + rzjl.YJ.ToString() + ",入住标志:" + rzjl.RZBZ
                        + "\n删除客单将同时删除该客人的账务信息,包括已经交班了的账务和已经夜审过了的房租和消费数据"
                        + "\n删除客单将可能引起报表数据的错误,确定继续执行吗?";
                if (MessageBox.Show(msg, hotel.Common.SysConsts.SysWarningCaption, MessageBoxButtons.OKCancel) != DialogResult.OK)
                    return;

                _context.RZJL_Delete(rzjl.ID, UserLoginInfo.FullName);

                MessageBox.Show("删除客单成功！");

                #region 日志纪录
                var logInfo = new CcrzInfo();
                logInfo.Abstract = LogAbstractEnum.删除客单;
                logInfo.RoomId = rzjl.ROOM_ID;
                logInfo.Rzid = rzjl.ID;
                logInfo.Message = "房号:" + rzjl.ROOM_ID + "，帐号:" + rzjl.ID + ",入住标志:" + rzjl.RZBZ 
                    + ",预定标志:" + rzjl.YDBZ + ",押金:=" + rzjl.YJ.ToString();
                logInfo.Save();
                #endregion
            }
            RefreshRoomFromDataBase(ucroom);            
        }
        //删除客单
        public void SCKRMenuMethod()
        {
            SCKRMenuMethod(currentURoom);
        }
    }
}
