﻿namespace hotel.Win
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.mi_ydjd = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_xyd = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_krdj = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_krxx = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_krhf = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_sckd = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_ftxg = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_syjz = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_zzkrlb = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_jzkrlb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmirlryb = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_ydkrlb = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_bjztf_hf = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiFrzxf = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_jbgl = new System.Windows.Forms.ToolStripMenuItem();
            this.tirbaob = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_wxjl = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_yjsh = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_khgl = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_xydwgl = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_xydwzw = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_hyklx = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_hyk = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_hysgl = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_cwbb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_skydb = new System.Windows.Forms.ToolStripMenuItem();
            this.tim_tdydb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_zzkrb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_jzryb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_xtgsrzb = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_krzfyebb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_yjmxbb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_zzryfzbb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_xfflhzb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_xfmxbb = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_xygxzfb = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_fjztbb = new System.Windows.Forms.ToolStripMenuItem();
            this.tim_wxfjb = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_zhcx = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_kscx = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_ysjl = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiccrz = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_xtcz = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_kfpz = new System.Windows.Forms.ToolStripMenuItem();
            this.micydm = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_xfxm = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.mi_xtyhxq = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_ccrz = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.cascadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrangeIconsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tim_bzml = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tmi_about = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.sbtnssft = new System.Windows.Forms.ToolStripButton();
            this.sbtnyddj = new System.Windows.Forms.ToolStripButton();
            this.sbtnrzdj = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sbtnbjya = new System.Windows.Forms.ToolStripButton();
            this.sbtnxfrd = new System.Windows.Forms.ToolStripButton();
            this.sbtnhfdj = new System.Windows.Forms.ToolStripButton();
            this.sbtnjztf = new System.Windows.Forms.ToolStripButton();
            this.sbtnFTXG = new System.Windows.Forms.ToolStripButton();
            this.sbtnjbgl = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.sbtnexit = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tmi_registe = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mi_ydjd,
            this.mi_syjz,
            this.mi_khgl,
            this.mi_cwbb,
            this.mi_zhcx,
            this.mi_xtcz,
            this.windowsMenu,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.MdiWindowListItem = this.windowsMenu;
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(873, 25);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // mi_ydjd
            // 
            this.mi_ydjd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator3,
            this.tmi_xyd,
            this.tmi_krdj,
            this.toolStripSeparator4,
            this.tmi_krxx,
            this.tmi_krhf,
            this.mi_sckd,
            this.tmi_ftxg,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
            this.mi_ydjd.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.mi_ydjd.Name = "mi_ydjd";
            this.mi_ydjd.Size = new System.Drawing.Size(84, 21);
            this.mi_ydjd.Text = "预订接待(&A)";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(121, 6);
            // 
            // tmi_xyd
            // 
            this.tmi_xyd.Image = global::hotel.Win.Properties.Resources.user;
            this.tmi_xyd.ImageTransparentColor = System.Drawing.Color.Black;
            this.tmi_xyd.Name = "tmi_xyd";
            this.tmi_xyd.Size = new System.Drawing.Size(124, 22);
            this.tmi_xyd.Text = "预订登记";
            this.tmi_xyd.Click += new System.EventHandler(this.tmi_xyd_Click);
            // 
            // tmi_krdj
            // 
            this.tmi_krdj.Name = "tmi_krdj";
            this.tmi_krdj.Size = new System.Drawing.Size(124, 22);
            this.tmi_krdj.Text = "入住登记";
            this.tmi_krdj.Click += new System.EventHandler(this.tmi_krdj_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(121, 6);
            // 
            // tmi_krxx
            // 
            this.tmi_krxx.ImageTransparentColor = System.Drawing.Color.Black;
            this.tmi_krxx.Name = "tmi_krxx";
            this.tmi_krxx.Size = new System.Drawing.Size(124, 22);
            this.tmi_krxx.Text = "客人信息";
            this.tmi_krxx.Click += new System.EventHandler(this.tmi_krxx_Click);
            // 
            // tmi_krhf
            // 
            this.tmi_krhf.ImageTransparentColor = System.Drawing.Color.Black;
            this.tmi_krhf.Name = "tmi_krhf";
            this.tmi_krhf.Size = new System.Drawing.Size(124, 22);
            this.tmi_krhf.Text = "换房登记";
            this.tmi_krhf.Click += new System.EventHandler(this.tmi_krhf_Click);
            // 
            // mi_sckd
            // 
            this.mi_sckd.Name = "mi_sckd";
            this.mi_sckd.Size = new System.Drawing.Size(124, 22);
            this.mi_sckd.Text = "删除客单";
            this.mi_sckd.Click += new System.EventHandler(this.mi_sckd_Click);
            // 
            // tmi_ftxg
            // 
            this.tmi_ftxg.Name = "tmi_ftxg";
            this.tmi_ftxg.Size = new System.Drawing.Size(124, 22);
            this.tmi_ftxg.Text = "房态修改";
            this.tmi_ftxg.Click += new System.EventHandler(this.tmi_ftxg_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(121, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.exitToolStripMenuItem.Text = "退出(&X)";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // mi_syjz
            // 
            this.mi_syjz.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mi_zzkrlb,
            this.mi_jzkrlb,
            this.tmirlryb,
            this.mi_ydkrlb,
            this.toolStripSeparator9,
            this.tmi_bjztf_hf,
            this.tmiFrzxf,
            this.tmi_jbgl,
            this.tirbaob,
            this.toolStripSeparator10,
            this.tmi_wxjl,
            this.toolStripSeparator12,
            this.tmi_yjsh});
            this.mi_syjz.Name = "mi_syjz";
            this.mi_syjz.Size = new System.Drawing.Size(84, 21);
            this.mi_syjz.Text = "前台营业(&B)";
            // 
            // mi_zzkrlb
            // 
            this.mi_zzkrlb.Name = "mi_zzkrlb";
            this.mi_zzkrlb.Size = new System.Drawing.Size(136, 22);
            this.mi_zzkrlb.Text = "在住房间";
            this.mi_zzkrlb.Click += new System.EventHandler(this.mi_zzkrlb_Click);
            // 
            // mi_jzkrlb
            // 
            this.mi_jzkrlb.Name = "mi_jzkrlb";
            this.mi_jzkrlb.Size = new System.Drawing.Size(136, 22);
            this.mi_jzkrlb.Text = "结账房间";
            this.mi_jzkrlb.Click += new System.EventHandler(this.mi_jzkrlb_Click);
            // 
            // tmirlryb
            // 
            this.tmirlryb.Name = "tmirlryb";
            this.tmirlryb.Size = new System.Drawing.Size(136, 22);
            this.tmirlryb.Text = "预离客人";
            this.tmirlryb.Click += new System.EventHandler(this.tmirlryb_Click);
            // 
            // mi_ydkrlb
            // 
            this.mi_ydkrlb.Name = "mi_ydkrlb";
            this.mi_ydkrlb.Size = new System.Drawing.Size(136, 22);
            this.mi_ydkrlb.Text = "预订房间";
            this.mi_ydkrlb.Click += new System.EventHandler(this.mi_ydkrlb_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(133, 6);
            // 
            // tmi_bjztf_hf
            // 
            this.tmi_bjztf_hf.Name = "tmi_bjztf_hf";
            this.tmi_bjztf_hf.Size = new System.Drawing.Size(136, 22);
            this.tmi_bjztf_hf.Text = "不结账账务";
            this.tmi_bjztf_hf.Click += new System.EventHandler(this.tmi_bjztf_hf_Click);
            // 
            // tmiFrzxf
            // 
            this.tmiFrzxf.Name = "tmiFrzxf";
            this.tmiFrzxf.Size = new System.Drawing.Size(136, 22);
            this.tmiFrzxf.Text = "非入住消费";
            this.tmiFrzxf.Click += new System.EventHandler(this.tmiFrzxf_Click);
            // 
            // tmi_jbgl
            // 
            this.tmi_jbgl.Name = "tmi_jbgl";
            this.tmi_jbgl.Size = new System.Drawing.Size(136, 22);
            this.tmi_jbgl.Text = "交班管理";
            this.tmi_jbgl.Click += new System.EventHandler(this.tmi_jbgl_Click);
            // 
            // tirbaob
            // 
            this.tirbaob.Name = "tirbaob";
            this.tirbaob.Size = new System.Drawing.Size(136, 22);
            this.tirbaob.Text = "营业交款表";
            this.tirbaob.Click += new System.EventHandler(this.tirbaob_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(133, 6);
            // 
            // tmi_wxjl
            // 
            this.tmi_wxjl.Name = "tmi_wxjl";
            this.tmi_wxjl.Size = new System.Drawing.Size(136, 22);
            this.tmi_wxjl.Text = "房间维修";
            this.tmi_wxjl.Click += new System.EventHandler(this.tmi_wxjl_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(133, 6);
            // 
            // tmi_yjsh
            // 
            this.tmi_yjsh.Name = "tmi_yjsh";
            this.tmi_yjsh.Size = new System.Drawing.Size(136, 22);
            this.tmi_yjsh.Text = "夜审执行";
            this.tmi_yjsh.Click += new System.EventHandler(this.tmi_yjsh_Click);
            // 
            // mi_khgl
            // 
            this.mi_khgl.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mi_xydwgl,
            this.tmi_xydwzw,
            this.toolStripSeparator14,
            this.tmi_hyklx,
            this.tmi_hyk,
            this.toolStripSeparator17,
            this.tmi_hysgl});
            this.mi_khgl.Name = "mi_khgl";
            this.mi_khgl.Size = new System.Drawing.Size(84, 21);
            this.mi_khgl.Text = "客户管理(&C)";
            // 
            // mi_xydwgl
            // 
            this.mi_xydwgl.Name = "mi_xydwgl";
            this.mi_xydwgl.Size = new System.Drawing.Size(148, 22);
            this.mi_xydwgl.Text = "协议单位管理";
            this.mi_xydwgl.Click += new System.EventHandler(this.mi_xydwgl_Click);
            // 
            // tmi_xydwzw
            // 
            this.tmi_xydwzw.Name = "tmi_xydwzw";
            this.tmi_xydwzw.Size = new System.Drawing.Size(148, 22);
            this.tmi_xydwzw.Text = "协议单位帐务";
            this.tmi_xydwzw.Click += new System.EventHandler(this.tmi_xydwzw_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(145, 6);
            // 
            // tmi_hyklx
            // 
            this.tmi_hyklx.Name = "tmi_hyklx";
            this.tmi_hyklx.Size = new System.Drawing.Size(148, 22);
            this.tmi_hyklx.Text = "会员卡类型";
            this.tmi_hyklx.Click += new System.EventHandler(this.tmi_hyklx_Click);
            // 
            // tmi_hyk
            // 
            this.tmi_hyk.Name = "tmi_hyk";
            this.tmi_hyk.Size = new System.Drawing.Size(148, 22);
            this.tmi_hyk.Text = "会员卡管理";
            this.tmi_hyk.Click += new System.EventHandler(this.tmi_hyk_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(145, 6);
            // 
            // tmi_hysgl
            // 
            this.tmi_hysgl.Name = "tmi_hysgl";
            this.tmi_hysgl.Size = new System.Drawing.Size(148, 22);
            this.tmi_hysgl.Text = "会议室管理";
            this.tmi_hysgl.Click += new System.EventHandler(this.tmi_hysgl_Click);
            // 
            // mi_cwbb
            // 
            this.mi_cwbb.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmi_skydb,
            this.tim_tdydb,
            this.tmi_zzkrb,
            this.tmi_jzryb,
            this.tmi_xtgsrzb,
            this.toolStripSeparator16,
            this.tmi_krzfyebb,
            this.tmi_yjmxbb,
            this.tmi_zzryfzbb,
            this.tmi_xfflhzb,
            this.tmi_xfmxbb,
            this.tmi_xygxzfb,
            this.toolStripSeparator15,
            this.tmi_fjztbb,
            this.tim_wxfjb});
            this.mi_cwbb.Name = "mi_cwbb";
            this.mi_cwbb.Size = new System.Drawing.Size(85, 21);
            this.mi_cwbb.Text = "财务报表(&D)";
            // 
            // tmi_skydb
            // 
            this.tmi_skydb.Name = "tmi_skydb";
            this.tmi_skydb.Size = new System.Drawing.Size(160, 22);
            this.tmi_skydb.Text = "散客预订表";
            this.tmi_skydb.Click += new System.EventHandler(this.tmi_skydb_Click);
            // 
            // tim_tdydb
            // 
            this.tim_tdydb.Name = "tim_tdydb";
            this.tim_tdydb.Size = new System.Drawing.Size(160, 22);
            this.tim_tdydb.Text = "团队预订表";
            this.tim_tdydb.Click += new System.EventHandler(this.tim_tdydb_Click);
            // 
            // tmi_zzkrb
            // 
            this.tmi_zzkrb.Name = "tmi_zzkrb";
            this.tmi_zzkrb.Size = new System.Drawing.Size(160, 22);
            this.tmi_zzkrb.Text = "今日入住人表";
            this.tmi_zzkrb.Click += new System.EventHandler(this.tmi_zzkrb_Click);
            // 
            // tmi_jzryb
            // 
            this.tmi_jzryb.Name = "tmi_jzryb";
            this.tmi_jzryb.Size = new System.Drawing.Size(160, 22);
            this.tmi_jzryb.Text = "结帐人员表";
            this.tmi_jzryb.Click += new System.EventHandler(this.tmi_jzryb_Click);
            // 
            // tmi_xtgsrzb
            // 
            this.tmi_xtgsrzb.Name = "tmi_xtgsrzb";
            this.tmi_xtgsrzb.Size = new System.Drawing.Size(160, 22);
            this.tmi_xtgsrzb.Text = "协议公司入住表";
            this.tmi_xtgsrzb.Click += new System.EventHandler(this.tmi_xtgsrzb_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(157, 6);
            // 
            // tmi_krzfyebb
            // 
            this.tmi_krzfyebb.Name = "tmi_krzfyebb";
            this.tmi_krzfyebb.Size = new System.Drawing.Size(160, 22);
            this.tmi_krzfyebb.Text = "客人帐户余额表";
            this.tmi_krzfyebb.Click += new System.EventHandler(this.tmi_krzfyebb_Click);
            // 
            // tmi_yjmxbb
            // 
            this.tmi_yjmxbb.Name = "tmi_yjmxbb";
            this.tmi_yjmxbb.Size = new System.Drawing.Size(160, 22);
            this.tmi_yjmxbb.Text = "押金明细报表";
            this.tmi_yjmxbb.Click += new System.EventHandler(this.tmi_yjmxbb_Click);
            // 
            // tmi_zzryfzbb
            // 
            this.tmi_zzryfzbb.Name = "tmi_zzryfzbb";
            this.tmi_zzryfzbb.Size = new System.Drawing.Size(160, 22);
            this.tmi_zzryfzbb.Text = "在住客人房租表";
            this.tmi_zzryfzbb.Click += new System.EventHandler(this.tmi_zzryfzbb_Click);
            // 
            // tmi_xfflhzb
            // 
            this.tmi_xfflhzb.Name = "tmi_xfflhzb";
            this.tmi_xfflhzb.Size = new System.Drawing.Size(160, 22);
            this.tmi_xfflhzb.Text = "消费分类汇总表";
            this.tmi_xfflhzb.Click += new System.EventHandler(this.tmi_xfflhzb_Click);
            // 
            // tmi_xfmxbb
            // 
            this.tmi_xfmxbb.Name = "tmi_xfmxbb";
            this.tmi_xfmxbb.Size = new System.Drawing.Size(160, 22);
            this.tmi_xfmxbb.Text = "消费项目明细表";
            this.tmi_xfmxbb.Click += new System.EventHandler(this.tmi_xfmxbb_Click);
            // 
            // tmi_xygxzfb
            // 
            this.tmi_xygxzfb.Name = "tmi_xygxzfb";
            this.tmi_xygxzfb.Size = new System.Drawing.Size(160, 22);
            this.tmi_xygxzfb.Text = "协议公司支付表";
            this.tmi_xygxzfb.Click += new System.EventHandler(this.tmi_xygxzfb_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(157, 6);
            // 
            // tmi_fjztbb
            // 
            this.tmi_fjztbb.Name = "tmi_fjztbb";
            this.tmi_fjztbb.Size = new System.Drawing.Size(160, 22);
            this.tmi_fjztbb.Text = "房间状态表";
            this.tmi_fjztbb.Click += new System.EventHandler(this.tmi_fjztbb_Click);
            // 
            // tim_wxfjb
            // 
            this.tim_wxfjb.Name = "tim_wxfjb";
            this.tim_wxfjb.Size = new System.Drawing.Size(160, 22);
            this.tim_wxfjb.Text = "维修房间表";
            this.tim_wxfjb.Click += new System.EventHandler(this.tim_wxfjb_Click);
            // 
            // mi_zhcx
            // 
            this.mi_zhcx.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmi_kscx,
            this.toolStripSeparator11,
            this.tmi_ysjl,
            this.tmiccrz});
            this.mi_zhcx.Name = "mi_zhcx";
            this.mi_zhcx.Size = new System.Drawing.Size(68, 21);
            this.mi_zhcx.Text = "综合查询";
            // 
            // tmi_kscx
            // 
            this.tmi_kscx.Name = "tmi_kscx";
            this.tmi_kscx.Size = new System.Drawing.Size(124, 22);
            this.tmi_kscx.Text = "客史查询";
            this.tmi_kscx.Click += new System.EventHandler(this.tmi_kscx_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(121, 6);
            // 
            // tmi_ysjl
            // 
            this.tmi_ysjl.Name = "tmi_ysjl";
            this.tmi_ysjl.Size = new System.Drawing.Size(124, 22);
            this.tmi_ysjl.Text = "夜审查询";
            this.tmi_ysjl.Click += new System.EventHandler(this.tmi_ysjl_Click);
            // 
            // tmiccrz
            // 
            this.tmiccrz.Name = "tmiccrz";
            this.tmiccrz.Size = new System.Drawing.Size(124, 22);
            this.tmiccrz.Text = "操作日志";
            this.tmiccrz.Click += new System.EventHandler(this.tmiccrz_Click);
            // 
            // mi_xtcz
            // 
            this.mi_xtcz.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mi_kfpz,
            this.micydm,
            this.mi_xfxm,
            this.toolStripSeparator18,
            this.mi_xtyhxq,
            this.tmi_ccrz,
            this.toolStripSeparator7,
            this.optionsToolStripMenuItem});
            this.mi_xtcz.Name = "mi_xtcz";
            this.mi_xtcz.Size = new System.Drawing.Size(83, 21);
            this.mi_xtcz.Text = "系统操作(&E)";
            // 
            // mi_kfpz
            // 
            this.mi_kfpz.ImageTransparentColor = System.Drawing.Color.Black;
            this.mi_kfpz.Name = "mi_kfpz";
            this.mi_kfpz.Size = new System.Drawing.Size(124, 22);
            this.mi_kfpz.Text = "客房配置";
            this.mi_kfpz.Click += new System.EventHandler(this.mi_kfpz_Click);
            // 
            // micydm
            // 
            this.micydm.ImageTransparentColor = System.Drawing.Color.Black;
            this.micydm.Name = "micydm";
            this.micydm.Size = new System.Drawing.Size(124, 22);
            this.micydm.Text = "常用代码";
            this.micydm.Click += new System.EventHandler(this.micydm_Click);
            // 
            // mi_xfxm
            // 
            this.mi_xfxm.Name = "mi_xfxm";
            this.mi_xfxm.Size = new System.Drawing.Size(124, 22);
            this.mi_xfxm.Text = "消费项目";
            this.mi_xfxm.Click += new System.EventHandler(this.mi_xfxm_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(121, 6);
            // 
            // mi_xtyhxq
            // 
            this.mi_xtyhxq.Name = "mi_xtyhxq";
            this.mi_xtyhxq.Size = new System.Drawing.Size(124, 22);
            this.mi_xtyhxq.Text = "用户权限";
            this.mi_xtyhxq.Click += new System.EventHandler(this.mi_xtyhxq_Click);
            // 
            // tmi_ccrz
            // 
            this.tmi_ccrz.Name = "tmi_ccrz";
            this.tmi_ccrz.Size = new System.Drawing.Size(124, 22);
            this.tmi_ccrz.Text = "系统日志";
            this.tmi_ccrz.Click += new System.EventHandler(this.tmi_ccrz_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(121, 6);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.optionsToolStripMenuItem.Text = "选项(&O)";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // windowsMenu
            // 
            this.windowsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewMenu,
            this.toolStripSeparator6,
            this.cascadeToolStripMenuItem,
            this.tileVerticalToolStripMenuItem,
            this.tileHorizontalToolStripMenuItem,
            this.closeAllToolStripMenuItem,
            this.arrangeIconsToolStripMenuItem});
            this.windowsMenu.Name = "windowsMenu";
            this.windowsMenu.Size = new System.Drawing.Size(64, 21);
            this.windowsMenu.Text = "窗口(&W)";
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBarToolStripMenuItem,
            this.statusBarToolStripMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(141, 22);
            this.viewMenu.Text = "视图(&V)";
            // 
            // toolBarToolStripMenuItem
            // 
            this.toolBarToolStripMenuItem.Checked = true;
            this.toolBarToolStripMenuItem.CheckOnClick = true;
            this.toolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolBarToolStripMenuItem.Name = "toolBarToolStripMenuItem";
            this.toolBarToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.toolBarToolStripMenuItem.Text = "工具栏(&T)";
            this.toolBarToolStripMenuItem.Click += new System.EventHandler(this.ToolBarToolStripMenuItem_Click);
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckOnClick = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.statusBarToolStripMenuItem.Text = "状态栏(&S)";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.StatusBarToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(138, 6);
            // 
            // cascadeToolStripMenuItem
            // 
            this.cascadeToolStripMenuItem.Name = "cascadeToolStripMenuItem";
            this.cascadeToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.cascadeToolStripMenuItem.Text = "层叠(&C)";
            this.cascadeToolStripMenuItem.Click += new System.EventHandler(this.CascadeToolStripMenuItem_Click);
            // 
            // tileVerticalToolStripMenuItem
            // 
            this.tileVerticalToolStripMenuItem.Name = "tileVerticalToolStripMenuItem";
            this.tileVerticalToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.tileVerticalToolStripMenuItem.Text = "垂直平铺(&V)";
            this.tileVerticalToolStripMenuItem.Click += new System.EventHandler(this.TileVerticalToolStripMenuItem_Click);
            // 
            // tileHorizontalToolStripMenuItem
            // 
            this.tileHorizontalToolStripMenuItem.Name = "tileHorizontalToolStripMenuItem";
            this.tileHorizontalToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.tileHorizontalToolStripMenuItem.Text = "水平平铺(&H)";
            this.tileHorizontalToolStripMenuItem.Click += new System.EventHandler(this.TileHorizontalToolStripMenuItem_Click);
            // 
            // closeAllToolStripMenuItem
            // 
            this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
            this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.closeAllToolStripMenuItem.Text = "全部关闭(&L)";
            this.closeAllToolStripMenuItem.Click += new System.EventHandler(this.CloseAllToolStripMenuItem_Click);
            // 
            // arrangeIconsToolStripMenuItem
            // 
            this.arrangeIconsToolStripMenuItem.Name = "arrangeIconsToolStripMenuItem";
            this.arrangeIconsToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.arrangeIconsToolStripMenuItem.Text = "排列图标(&A)";
            this.arrangeIconsToolStripMenuItem.Click += new System.EventHandler(this.ArrangeIconsToolStripMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tim_bzml,
            this.tmi_registe,
            this.toolStripSeparator8,
            this.tmi_about});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(61, 21);
            this.helpMenu.Text = "帮助(&H)";
            // 
            // tim_bzml
            // 
            this.tim_bzml.Name = "tim_bzml";
            this.tim_bzml.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.tim_bzml.Size = new System.Drawing.Size(166, 22);
            this.tim_bzml.Text = "目录(&C)";
            this.tim_bzml.Click += new System.EventHandler(this.contentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(163, 6);
            // 
            // tmi_about
            // 
            this.tmi_about.Name = "tmi_about";
            this.tmi_about.Size = new System.Drawing.Size(166, 22);
            this.tmi_about.Text = "关于(&A) ...";
            this.tmi_about.Click += new System.EventHandler(this.tmi_about_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.Font = new System.Drawing.Font("SimSun", 10F);
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbtnssft,
            this.sbtnyddj,
            this.sbtnrzdj,
            this.toolStripSeparator1,
            this.sbtnbjya,
            this.sbtnxfrd,
            this.sbtnhfdj,
            this.sbtnjztf,
            this.sbtnFTXG,
            this.sbtnjbgl,
            this.toolStripSeparator2,
            this.sbtnexit});
            this.toolStrip.Location = new System.Drawing.Point(0, 25);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(873, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "ToolStrip";
            // 
            // sbtnssft
            // 
            this.sbtnssft.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnssft.Name = "sbtnssft";
            this.sbtnssft.Size = new System.Drawing.Size(67, 22);
            this.sbtnssft.Text = "实时房态";
            this.sbtnssft.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnssft.Click += new System.EventHandler(this.sbtnssft_Click);
            // 
            // sbtnyddj
            // 
            this.sbtnyddj.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnyddj.Name = "sbtnyddj";
            this.sbtnyddj.Size = new System.Drawing.Size(67, 22);
            this.sbtnyddj.Text = "预订登记";
            this.sbtnyddj.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnyddj.Click += new System.EventHandler(this.tmi_xyd_Click);
            // 
            // sbtnrzdj
            // 
            this.sbtnrzdj.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnrzdj.Name = "sbtnrzdj";
            this.sbtnrzdj.Size = new System.Drawing.Size(67, 22);
            this.sbtnrzdj.Text = "入住登记";
            this.sbtnrzdj.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnrzdj.Click += new System.EventHandler(this.tmi_krdj_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // sbtnbjya
            // 
            this.sbtnbjya.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnbjya.Name = "sbtnbjya";
            this.sbtnbjya.Size = new System.Drawing.Size(67, 22);
            this.sbtnbjya.Text = "补交押金";
            this.sbtnbjya.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnbjya.Click += new System.EventHandler(this.sbtnbjya_Click);
            // 
            // sbtnxfrd
            // 
            this.sbtnxfrd.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnxfrd.Name = "sbtnxfrd";
            this.sbtnxfrd.Size = new System.Drawing.Size(67, 22);
            this.sbtnxfrd.Text = "消费入单";
            this.sbtnxfrd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnxfrd.Click += new System.EventHandler(this.sbtnxfrd_Click);
            // 
            // sbtnhfdj
            // 
            this.sbtnhfdj.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnhfdj.Name = "sbtnhfdj";
            this.sbtnhfdj.Size = new System.Drawing.Size(67, 22);
            this.sbtnhfdj.Text = "换房登记";
            this.sbtnhfdj.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnhfdj.Click += new System.EventHandler(this.tmi_krhf_Click);
            // 
            // sbtnjztf
            // 
            this.sbtnjztf.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnjztf.Name = "sbtnjztf";
            this.sbtnjztf.Size = new System.Drawing.Size(67, 22);
            this.sbtnjztf.Text = "结账退房";
            this.sbtnjztf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnjztf.Click += new System.EventHandler(this.sbtnjztf_Click);
            // 
            // sbtnFTXG
            // 
            this.sbtnFTXG.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnFTXG.Name = "sbtnFTXG";
            this.sbtnFTXG.Size = new System.Drawing.Size(67, 22);
            this.sbtnFTXG.Text = "房态修改";
            this.sbtnFTXG.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnFTXG.Click += new System.EventHandler(this.tmi_ftxg_Click);
            // 
            // sbtnjbgl
            // 
            this.sbtnjbgl.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnjbgl.Name = "sbtnjbgl";
            this.sbtnjbgl.Size = new System.Drawing.Size(67, 22);
            this.sbtnjbgl.Text = "交班管理";
            this.sbtnjbgl.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnjbgl.Click += new System.EventHandler(this.tmi_jbgl_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // sbtnexit
            // 
            this.sbtnexit.ImageTransparentColor = System.Drawing.Color.Black;
            this.sbtnexit.Name = "sbtnexit";
            this.sbtnexit.Size = new System.Drawing.Size(67, 22);
            this.sbtnexit.Text = "退出系统";
            this.sbtnexit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sbtnexit.Click += new System.EventHandler(this.sbtnexit_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(873, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(32, 17);
            this.toolStripStatusLabel.Text = "状态";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel1.Text = "111l1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(131, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(131, 17);
            this.toolStripStatusLabel3.Text = "toolStripStatusLabel3";
            // 
            // tmi_registe
            // 
            this.tmi_registe.Name = "tmi_registe";
            this.tmi_registe.Size = new System.Drawing.Size(166, 22);
            this.tmi_registe.Text = "注册";
            this.tmi_registe.Click += new System.EventHandler(this.tmi_registe_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 453);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "酒店管理系统";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem mi_sckd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem tmi_about;
        private System.Windows.Forms.ToolStripMenuItem tileHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mi_ydjd;
        private System.Windows.Forms.ToolStripMenuItem tmi_xyd;
        private System.Windows.Forms.ToolStripMenuItem tmi_krdj;
        private System.Windows.Forms.ToolStripMenuItem tmi_krxx;
        private System.Windows.Forms.ToolStripMenuItem tmi_krhf;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mi_xtcz;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowsMenu;
        private System.Windows.Forms.ToolStripMenuItem cascadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrangeIconsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem tim_bzml;
        private System.Windows.Forms.ToolStripButton sbtnssft;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem mi_syjz;
        private System.Windows.Forms.ToolStripMenuItem mi_khgl;
        private System.Windows.Forms.ToolStripMenuItem mi_cwbb;
        private System.Windows.Forms.ToolStripMenuItem mi_kfpz;
        private System.Windows.Forms.ToolStripMenuItem micydm;
        private System.Windows.Forms.ToolStripMenuItem mi_xtyhxq;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem toolBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem mi_xfxm;
        private System.Windows.Forms.ToolStripMenuItem mi_xydwgl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem tmi_ftxg;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem tmi_bjztf_hf;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem tmi_jbgl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem tmi_yjsh;
        private System.Windows.Forms.ToolStripButton sbtnyddj;
        private System.Windows.Forms.ToolStripButton sbtnrzdj;
        private System.Windows.Forms.ToolStripButton sbtnbjya;
        private System.Windows.Forms.ToolStripButton sbtnxfrd;
        private System.Windows.Forms.ToolStripButton sbtnhfdj;
        private System.Windows.Forms.ToolStripButton sbtnjztf;
        private System.Windows.Forms.ToolStripButton sbtnFTXG;
        private System.Windows.Forms.ToolStripButton sbtnjbgl;
        private System.Windows.Forms.ToolStripButton sbtnexit;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem tmi_hyklx;
        private System.Windows.Forms.ToolStripMenuItem tmi_hyk;
        private System.Windows.Forms.ToolStripMenuItem tmi_skydb;
        private System.Windows.Forms.ToolStripMenuItem tim_tdydb;
        private System.Windows.Forms.ToolStripMenuItem tmi_zzkrb;
        private System.Windows.Forms.ToolStripMenuItem tim_wxfjb;
        private System.Windows.Forms.ToolStripMenuItem tmi_jzryb;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem tmi_zzryfzbb;
        private System.Windows.Forms.ToolStripMenuItem tmi_xtgsrzb;
        private System.Windows.Forms.ToolStripMenuItem tmi_fjztbb;
        private System.Windows.Forms.ToolStripMenuItem tmi_xfflhzb;
        private System.Windows.Forms.ToolStripMenuItem tmi_xfmxbb;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem tmi_yjmxbb;
        private System.Windows.Forms.ToolStripMenuItem tmi_krzfyebb;
        private System.Windows.Forms.ToolStripMenuItem tmi_xydwzw;
        private System.Windows.Forms.ToolStripMenuItem tmi_xygxzfb;
        private System.Windows.Forms.ToolStripMenuItem tmi_ccrz;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem tmi_hysgl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem mi_ydkrlb;
        private System.Windows.Forms.ToolStripMenuItem mi_zzkrlb;
        private System.Windows.Forms.ToolStripMenuItem mi_jzkrlb;
        private System.Windows.Forms.ToolStripMenuItem tmirlryb;
        private System.Windows.Forms.ToolStripMenuItem tirbaob;
        private System.Windows.Forms.ToolStripMenuItem tmi_wxjl;
        private System.Windows.Forms.ToolStripMenuItem mi_zhcx;
        private System.Windows.Forms.ToolStripMenuItem tmi_kscx;
        private System.Windows.Forms.ToolStripMenuItem tmi_ysjl;
        private System.Windows.Forms.ToolStripMenuItem tmiccrz;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem tmiFrzxf;
        private System.Windows.Forms.ToolStripMenuItem tmi_registe;
    }
}



