﻿namespace hotel.Win.BaseData
{
    partial class xfflMaintain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtdm = new System.Windows.Forms.TextBox();
            this.chkfws = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtmc = new System.Windows.Forms.TextBox();
            this.chksfhz = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 6;
            this.label1.Text = "分类代码";
            // 
            // txtdm
            // 
            this.txtdm.Location = new System.Drawing.Point(111, 22);
            this.txtdm.Name = "txtdm";
            this.txtdm.Size = new System.Drawing.Size(220, 23);
            this.txtdm.TabIndex = 1;
            // 
            // chkfws
            // 
            this.chkfws.AutoSize = true;
            this.chkfws.Location = new System.Drawing.Point(42, 87);
            this.chkfws.Name = "chkfws";
            this.chkfws.Size = new System.Drawing.Size(124, 18);
            this.chkfws.TabIndex = 3;
            this.chkfws.Text = "是否需要服务生";
            this.chkfws.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 9;
            this.label2.Text = "分类名称";
            // 
            // txtmc
            // 
            this.txtmc.Location = new System.Drawing.Point(111, 51);
            this.txtmc.Name = "txtmc";
            this.txtmc.Size = new System.Drawing.Size(220, 23);
            this.txtmc.TabIndex = 2;
            // 
            // chksfhz
            // 
            this.chksfhz.AutoSize = true;
            this.chksfhz.Checked = true;
            this.chksfhz.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chksfhz.Location = new System.Drawing.Point(42, 117);
            this.chksfhz.Name = "chksfhz";
            this.chksfhz.Size = new System.Drawing.Size(194, 18);
            this.chksfhz.TabIndex = 4;
            this.chksfhz.Text = "是否汇总（在营业报表中）";
            this.chksfhz.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtdm);
            this.groupBox1.Controls.Add(this.txtmc);
            this.groupBox1.Controls.Add(this.chkfws);
            this.groupBox1.Controls.Add(this.chksfhz);
            this.groupBox1.Location = new System.Drawing.Point(6, -4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(456, 161);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // xfflMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(467, 248);
            this.Controls.Add(this.groupBox1);
            this.Name = "xfflMaintain";
            this.Text = "消费项目分类维护";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdm;
        private System.Windows.Forms.CheckBox chkfws;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtmc;
        private System.Windows.Forms.CheckBox chksfhz;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
