﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
namespace hotel.Win.BaseData
{
    /// <summary>
    /// 协议房价
    /// </summary>
    public partial class xyfjMaintain : hotel.MyUserControl.MaintainBaseForm
    {
        int Id;
        hotelEntities _context;
        XYFJ dataModel;
        XYDW xydwModel;
        public xyfjMaintain()
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = MyDataContext.GetDataContext;
            Id = 0;
            cobxfjfl.DataSource = RoomTypeDal.GetAll();
            cobxfjfl.DisplayMember = "JC";
        }

        /// <summary>
        /// 用于增加
        /// </summary>
        /// <param name="ld">楼栋名称</param>
        public xyfjMaintain(string DWID)
            : this()
        {
            base.IsEdit = false;
            base.DeleteButtonEnabled = false;
            base.StatusMessage = "数据处于增加状态";
            int dwid = Convert.ToInt32(DWID);
            xydwModel = _context.XYDW.First(p => p.ID == dwid);
            Add();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public xyfjMaintain(int id)
            : this()
        {
            BindData(id);
        }

        private void BindData(int id)
        {
            this.Id = id;
            dataModel = _context.XYFJ.First(p => p.ID == id);
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());
            txtfj.Value = dataModel.FJ;
            txtyj.Value = dataModel.YJ;
            txtbzsm.Text = dataModel.BZSM;            
            xydwModel = dataModel.XYDW;
            // cobxfjfl.SelectedIndex  = cobxzl.Items.IndexOf(dataModel.ROOM_TYPE);
            for (int i = 0; i < cobxfjfl.Items.Count; i++)
            {
                if ((cobxfjfl.Items[i] as ROOM_TYPE).ID == dataModel.ROOM_TYPE.ID)
                {
                    cobxfjfl.SelectedIndex = i;
                    break;
                }
            }
            cobxfjfl.Enabled = false;
            IsEdit = true;
            StatusMessage = "修改数据状态";

        }

        private bool Add()
        {
            dataModel = new XYFJ { ID = Id };
            dataModel.XYDW = xydwModel;
            cobxfjfl.Enabled = true;
            return true;
        }
        private bool Delete()
        {
            //int cnt = _context.ROOM.Where(p => p.ROOM_TYPE.ID == dataModel.ID).Count();
            //if ( cnt> 0)
            //{
            //    MessageBox.Show("此房间种类下已经设置了" + cnt.ToString() + "个房间，不能删除！");
            //    return false;
            //}
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "协议单位房价单位[" + dataModel.XYDW.DWMC + "],房类[" + dataModel.ROOM_TYPE_ID + "]";
            logInfo.Save();
            #endregion
            _context.XYFJ.Remove(dataModel);
            _context.SaveChanges();
           
            return true;
        }

        private bool CheckData()
        {
            if (!IsEdit)
            {
                string typeId = (cobxfjfl.SelectedValue as ROOM_TYPE).ID;
                int dwid = xydwModel.ID;
                if (_context.XYFJ.Where(p => p.ROOM_TYPE.ID == typeId && p.XYDW.ID == dwid).Count() > 0)
                {
                    MessageBox.Show("此" + cobxfjfl.Text + "房间分类的房价已经制定，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false;
           
            dataModel.BZSM = txtbzsm.Text;
            dataModel.FJ = txtfj.Value;
            dataModel.YJ = txtyj.Value;
            if (IsEdit)
            {
                dataModel.XGRQ = DateTime.Now;
                dataModel.XGRY = UserLoginInfo.FullName;
            }
            else
            {
                dataModel.CJRQ = DateTime.Now;
                dataModel.CJRY = UserLoginInfo.FullName;
                dataModel.XYDW = xydwModel;
                string typeId = (cobxfjfl.SelectedValue as ROOM_TYPE).ID;
                dataModel.ROOM_TYPE = _context.ROOM_TYPE.First(p => p.ID == typeId);
                _context.XYFJ.Add(dataModel);

            }
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = (IsEdit ? "修改" : "增加") +  "协议单位房价单位[" + dataModel.XYDW.DWMC + "],房类[" + dataModel.ROOM_TYPE_ID + "]";
            logInfo.Save();
            #endregion
            
            return true;
        }

        private void cobxfjfl_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbckj.Text = (cobxfjfl.SelectedValue as ROOM_TYPE).SKJG.ToString();
        }
    }
}
