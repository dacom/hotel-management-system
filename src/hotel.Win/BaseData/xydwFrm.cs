﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
using hotel.Win.Subscribe;


namespace hotel.Win.BaseData
{
    /// <summary>
    /// 协议单位管理
    /// </summary>
    public partial class xydwFrm : Form
    {
        private hotelEntities _context;
        public xydwFrm()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            InitDWData(true);
        }

        private void InitDWData(bool flag)
        {
            if (!flag)
                return;

            griddw.AutoGenerateColumns = false;
            griddw.DataSource = _context.XYDW.AsNoTracking().OrderBy(p => p.DWMC).ToList();
        }

        private void btndwbj_Click(object sender, EventArgs e)
        {
            xydwMaintain frm;
            string id = Common.ControlsUtility.GetActiveGridValue(griddw, "DWID");
            if (id == "")
                frm = new xydwMaintain(true);
            else
                frm = new xydwMaintain(Convert.ToInt32(id));
            frm.CallBack = InitDWData;
            frm.ShowDialog();
        }

        private void btnfjbj_Click(object sender, EventArgs e)
        {
            xyfjMaintain frm;
            string dwid = Common.ControlsUtility.GetActiveGridValue(griddw, "DWID");
            string id = Common.ControlsUtility.GetActiveGridValue(gridfj, "FJID");
            if (id == "")
                frm = new xyfjMaintain(dwid);
            else
                frm = new xyfjMaintain(Convert.ToInt32(id));
            frm.CallBack = InitXYFJData;
            frm.ShowDialog();
        }

        private void InitXYFJData(bool flag)
        {
            if (!flag)
                return;
            int flid = Convert.ToInt32(Common.ControlsUtility.GetActiveGridValue(griddw, "DWID"));
            gridfj.AutoGenerateColumns = false;
            var qry = from r in _context.XYFJ.AsNoTracking()
                      join t in _context.ROOM_TYPE.AsNoTracking() on r.ROOM_TYPE.ID equals t.ID
                      where r.XYDW.ID == flid
                      orderby r.ID
                      select new
                      {
                          r.BZSM,
                          r.CJRQ,
                          r.CJRY,
                          r.FJ,
                          r.ID,
                          r.XGRQ,
                          r.XGRY,
                          r.YJ,
                          FLJC = t.JC
                      };
            gridfj.DataSource = qry.ToList();
        }

        private void griddw_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            InitXYFJData(true);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btncopy_Click(object sender, EventArgs e)
        {
            string dwid = Common.ControlsUtility.GetActiveGridValue(griddw, "DWID");
            if (string.IsNullOrEmpty(dwid))
                return;
            var frm = new xydwFind();
            if (frm.ShowDialog() != DialogResult.OK)
                return;
            var ydwId = frm.SelectedXYDW.ID;
            _context.CopyXieYiFangJia(ydwId, Convert.ToInt32(dwid));
            InitXYFJData(true);
        }
    }
}
