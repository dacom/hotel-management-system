﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;

namespace hotel.Win.BaseData
{
    /// <summary>
    /// 消费项目配置
    /// </summary>
    public partial class xfxmFrm : Form
    {       
        public xfxmFrm()
        {
            InitializeComponent();
            
            InitLBData(true);
        }

        private void InitLBData(bool flag)
        {
            if (!flag)
                return;

            gridfl.AutoGenerateColumns = false;
            using (var _context = MyDataContext.GetDataContext)
            {
                gridfl.DataSource = _context.XFXMFL.AsNoTracking().OrderBy(p => p.FLMC).ToList();
            }
            gridfl_CellClick(gridfl, null);
        }

        private void btnzlbj_Click(object sender, EventArgs e)
        {
            xfflMaintain frm;
            string id = Common.ControlsUtility.GetActiveGridValue(gridfl, "FLID");
            if (id == "")
                frm = new xfflMaintain(true);
            else
                frm = new xfflMaintain(id);
            frm.CallBack = InitLBData;
            frm.ShowDialog();
        }

        private void btnxmbj_Click(object sender, EventArgs e)
        {
            xfxmMaintain frm;
            string flid = Common.ControlsUtility.GetActiveGridValue(gridfl, "FLID");
            string id = Common.ControlsUtility.GetActiveGridValue(gridxm, "XMID");
            if (id == "")
                frm = new xfxmMaintain(flid);
            else
                frm = new xfxmMaintain(Convert.ToInt32(id));
            frm.CallBack = InitXMData;
            frm.ShowDialog();
        }

        private void InitXMData(bool flag)
        {
            if (!flag)
                return;
            string flid = Common.ControlsUtility.GetActiveGridValue(gridfl, "FLID");
            if (string.IsNullOrEmpty(flid))
                return;
            gridxm.AutoGenerateColumns = false;
            using (var _context = MyDataContext.GetDataContext)
            {
                gridxm.DataSource = _context.XFXM.AsNoTracking().Where(p => p.XFXMFL.ID == flid).ToList();
            }
        }
        private void gridfl_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            InitXMData(true);
        }
    }
}