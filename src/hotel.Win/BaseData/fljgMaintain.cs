﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;
using System.Data.SqlClient;

namespace hotel.Win.BaseData
{
    public partial class fljgMaintain : hotel.MyUserControl.MaintainBaseForm
    {
        hotelEntities _context;
        RoomTypePrice dataModel;
        string RoomTypeId;
        public fljgMaintain()
        {
            InitializeComponent();
            cobxxymc.Items.Clear();
            cobxxymc.Items.AddRange(RoomStstusCnName.GetJeiDaiAvailables.ToArray());
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = MyDataContext.GetDataContext;
        }

        /// <summary>
        /// 用于增加
        /// </summary>
        public fljgMaintain(string roomTypeId)
            : this()
        {
            RoomTypeId = roomTypeId;
            base.IsEdit = false;
            base.DeleteButtonEnabled = false;
            base.StatusMessage = "数据处于增加状态";
            Add();
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public fljgMaintain(int id)
            : this()
        {
            BindData(id);
        }

        private void BindData(int id)
        {
            dataModel = _context.RoomTypePrice.Find(id);
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());
            cobxxymc.Text = dataModel.XYMC;
            txtxyjg.Value = dataModel.XYJG;
            cobxcsms.Text = dataModel.CSMS;
            txtcssc.Value = dataModel.CSSC.Value;
            chksfmr.Checked = dataModel.SFMR == "是";
            chksfys.Checked = dataModel.SFYS == "是";
            chksfxs.Checked = dataModel.SFXS == "是";
            chksfjf.Checked = dataModel.SFJF == "是";
            txtzdf_sc.Value = dataModel.ZDF_SC;
            txtzdf_js.Value = dataModel.ZDF_JS;
            gboxZdf.Enabled = dataModel.XYMC.Contains("钟点房");
            txtbzsm.Text = dataModel.BZSM;
            RoomTypeId = dataModel.ROOM_TYPE_ID;
            IsEdit = true;
            StatusMessage = "修改数据状态";
        }

        private bool Add()
        {
            gboxZdf.Enabled = false;
            dataModel = new RoomTypePrice();
            dataModel.ROOM_TYPE_ID = RoomTypeId;
            cobxxymc.Text = "门市价";
            txtxyjg.Value = dataModel.XYJG;
            cobxcsms.SelectedIndex = 0;
            txtcssc.Value = 15;
            chksfmr.Checked = false;
            chksfys.Checked = true;
            chksfxs.Checked = true;
            chksfjf.Checked = true;
            txtzdf_sc.Value = 0;
            txtzdf_js.Value = 0;
            txtbzsm.Text = "";
            return true;
        }
        private bool Delete()
        {
            /*int cnt = _context.RZJL.Where(p=>p.ROOM.ROOM_TYPE_ID== dataModel.ROOM_TYPE_ID && p.XYJG_MC== dataModel.XYMC).Count();
            if (cnt > 0)
            {
                MessageBox.Show("此房间已经发生了" + cnt.ToString() + "此入住，不能删除！");
                return false;
            }*/

            _context.RoomTypePrice.Remove(dataModel);
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.RoomId = null;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "删除房类协议名称[" + dataModel.XYMC + "],房类=" + dataModel.ROOM_TYPE_ID;
            logInfo.Save();
            #endregion
            return true;
        }

        private bool CheckData()
        {
            var info = ValidataHelper.StartVerify(cobxxymc.Text)
            .IsNotNullOrEmpty("房号不能为空输入为空")
            .Max(16, "房号长度不能大于16个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }

            if (!IsEdit)
            {
                if (_context.RoomTypePrice.AsNoTracking().Where(p => p.ROOM_TYPE_ID == dataModel.ROOM_TYPE_ID && p.XYMC == cobxxymc.Text).Count() > 0)
                {
                    MessageBox.Show("协议名称" + cobxxymc.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false;
            dataModel.BZSM = txtbzsm.Text;
            dataModel.XYMC = cobxxymc.Text;
            dataModel.XYJG = txtxyjg.Value;
            dataModel.CSMS = cobxcsms.Text;
            dataModel.CSSC = txtcssc.Value;
            dataModel.SFMR = chksfmr.Checked ? "是" : "否";
            dataModel.SFYS = chksfys.Checked ? "是" : "否";
            dataModel.SFXS = chksfxs.Checked ? "是" : "否";
            dataModel.SFJF = chksfjf.Checked ? "是" : "否";
            dataModel.ZDF_SC = txtzdf_sc.Value;
            dataModel.ZDF_JS = txtzdf_js.Value;
            if (IsEdit)
            {
                //dataModel.XGRQ = DateTime.Now;
                //dataModel.XGRY = UserLoginInfo.FullName;  

            }
            else
            {
                _context.RoomTypePrice.Add(dataModel);
            }
            //默认处理，只有一个
            if (dataModel.SFMR == "是")
            {
                var sqlstr = "update dbo.RoomTypePrice set SFMR='否' where ROOM_TYPE_ID =@ROOM_TYPE_ID";
                var ps = new SqlParameter("@ROOM_TYPE_ID", RoomTypeId);
                _context.Database.ExecuteSqlCommand(sqlstr, ps);
            }
           
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
            logInfo.RoomId = null;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = (IsEdit ? "修改" : "增加") + "房类价格[" + dataModel.ID + "],价格=" + dataModel.XYJG.ToString();
            logInfo.Save();
            #endregion
            _context.SaveChanges();
            return true;
        }

        private void cobxxymc_Leave(object sender, EventArgs e)
        {
            gboxZdf.Enabled = cobxxymc.Text.Contains("钟点房");
            if (gboxZdf.Enabled)
                cobxcsms.SelectedIndex = 1;
        }

        private void cobxxymc_SelectedIndexChanged(object sender, EventArgs e)
        {
            gboxZdf.Enabled = cobxxymc.Text.Contains("钟点房");
        }
    }
}
