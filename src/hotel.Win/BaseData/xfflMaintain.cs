﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;

namespace hotel.Win.BaseData
{
    public partial class xfflMaintain : hotel.MyUserControl.MaintainBaseForm
    {
        string Id;
        hotelEntities _context;
        XFXMFL dataModel;

        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="isAdd"></param>
        public xfflMaintain(bool isAdd)
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = MyDataContext.GetDataContext;
            Id = "0";
            if (isAdd)
            {
                base.IsEdit = false;
                base.DeleteButtonEnabled = false;
                base.StatusMessage = "数据处于增加状态";
                Add();
            }
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public xfflMaintain(string id)
            : this(false)
        {
            BindData(id);
        }

        private void BindData(string id)
        {
            this.Id = id;
            dataModel = _context.XFXMFL.First(p => p.ID == id);
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());
            txtdm.Text = dataModel.ID;
            txtdm.ReadOnly = true;
            txtmc.Text = dataModel.FLMC;
            chkfws.Checked = dataModel.SF_FWS == "是" ? true : false;
            chksfhz.Checked = dataModel.SFHZ == "是" ? true : false;
            IsEdit = true;
            StatusMessage = "修改数据状态";
        }

        private bool Add()
        {
            txtdm.ReadOnly = false;
            dataModel = new XFXMFL();
            return true;
        }
        private bool Delete()
        {
            if (dataModel.ID == "01")
            {
                MessageBox.Show("系统项目不能删除！");
                return false;
            }
            int cnt = _context.XFXM.Count(p => p.XFXMFL_ID == dataModel.ID);
            if (cnt > 0)
            {
                MessageBox.Show("此分类下已经设置了" + cnt.ToString() + "个消费项目，不能删除！");
                return false;
            }

            _context.XFXMFL.Remove(dataModel);
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "删除消费项目分类[" + dataModel.FLMC + "]";
            logInfo.Save();
            #endregion
            return true;
        }

        private bool CheckData()
        {
            var info = ValidataHelper.StartVerify(txtdm.Text)
            .IsNotNullOrEmpty("代码不能为空输入为空")
            .Max(10, "代码长度不能大于10个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            info = ValidataHelper.StartVerify(txtmc.Text)
           .IsNotNullOrEmpty("名称不能为空输入为空")
           .Max(50, "名称长度不能大于50个字符")
           .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }

            if (!IsEdit)
            {
                if (_context.XFXMFL.Where(p => p.ID == txtdm.Text).Count() > 0)
                {
                    MessageBox.Show("代码" + txtdm.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false;
            dataModel.FLMC = txtmc.Text;
            dataModel.SF_FWS = chkfws.Checked ? "是" : "否";
            dataModel.SFHZ = chksfhz.Checked ? "是" : "否";

            if (IsEdit)
            {
                //dataModel.XGRQ = DateTime.Now;
                //dataModel.XGRY = UserLoginInfo.FullName;                
            }
            else
            {
                dataModel.ID = txtdm.Text;
                //_context.Attach(dataModel);
                _context.XFXMFL.Add(dataModel);
            }
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = (IsEdit ? "修改" : "增加") + "消费分类[" + dataModel.FLMC + "]";
            logInfo.Save();
            #endregion
            return true;
        }
    }
}
