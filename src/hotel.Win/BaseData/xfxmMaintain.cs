﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;

namespace hotel.Win.BaseData
{
    public partial class xfxmMaintain : hotel.MyUserControl.MaintainBaseForm
    {
        int Id;
        hotelEntities _context;
        XFXM dataModel;
        XFXMFL flModel;

        public xfxmMaintain()
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = MyDataContext.GetDataContext;
            Id = 0;
        }

        /// <summary>
        /// 用于增加
        /// </summary>
        /// <param name="ld">楼栋名称</param>
        public xfxmMaintain(string FLID)
            : this()
        {
            base.IsEdit = false;
            base.DeleteButtonEnabled = false;
            base.StatusMessage = "数据处于增加状态";
            flModel = _context.XFXMFL.First(p => p.ID == FLID);
            Add();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public xfxmMaintain(int id)
            : this()
        {
            BindData(id);
        }

        private void BindData(int id)
        {
            this.Id = id;
            dataModel = _context.XFXM.First(p => p.ID == id);
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());
            txtdm.Text = dataModel.DM;
            txtmc.Text = dataModel.MC;
            txtywmc.Text = dataModel.YWMC;
            txtdj.Value = (decimal)dataModel.DJ;
            txtbzsm.Text = dataModel.BZSM;            
            flModel = dataModel.XFXMFL;
            txtmc.ReadOnly = dataModel.XGBZ == 1;
            txtywmc.ReadOnly = dataModel.XGBZ == 1;
            //base.DeleteButtonEnabled = dataModel.XGBZ == 0;
            //base.PostButtonEnabled = dataModel.XGBZ == 0;
            txtdm.ReadOnly = true;
            IsEdit = true;
            StatusMessage = "修改数据状态";
        }

        private bool Add()
        {
            dataModel = new XFXM();
            dataModel.XFXMFL = flModel;
            txtdm.ReadOnly = false;
            txtdm.Text = "";
            txtmc.ReadOnly = false;
            txtmc.Text = "";
            txtywmc.ReadOnly = false;
            txtywmc.Text = "";
            txtbzsm.Text = "";
            return true;
        }
        private bool Delete()
        {
            //int cnt = _context.ROOM.Where(p => p.ROOM_TYPE.ID == dataModel.ID).Count();
            //if ( cnt> 0)
            //{
            //    MessageBox.Show("此房间种类下已经设置了" + cnt.ToString() + "个房间，不能删除！");
            //    return false;
            //}            
            if (dataModel.XFXMFL_ID == "01" && dataModel.XGBZ ==1)
            {
                MessageBox.Show("系统消费项目不能删除！");
                return false;
            }
            
            _context.XFXM.Remove(dataModel);
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message ="删除消费项目,[" + dataModel.MC + "]";
            logInfo.Save();
            #endregion
            return true;
        }

        private bool CheckData()
        {
            var info = ValidataHelper.StartVerify(txtdm.Text)
            .IsNotNullOrEmpty("代码不能为空输入为空")
            .Max(6, "代码长度不能大于6个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            info = ValidataHelper.StartVerify(txtmc.Text)
           .IsNotNullOrEmpty("名称不能为空输入为空")
           .Max(30, "名称长度不能大于30个字符")
           .EndVerify();
            ValidataHelper.StartVerify(txtdj.Value).Min(0, "单价不能小于0").EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            
            if (!IsEdit)
            {
                if (_context.XFXM.Where(p => p.DM == txtdm.Text).Count() > 0)
                {
                    MessageBox.Show("代码" + txtdm.Text + "已经存在，请查证！");
                    return false;
                }
                if (_context.XFXM.Where(p => p.MC == txtmc.Text).Count() > 0)
                {
                    MessageBox.Show("名称" + txtmc.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false;
            dataModel.MC = txtmc.Text;
            dataModel.DM = txtdm.Text;
            dataModel.DJ = txtdj.Value;
            dataModel.YWMC = txtywmc.Text;
            dataModel.BZSM = txtbzsm.Text;

            if (IsEdit)
            {
                dataModel.XGRQ = DateTime.Now;
                dataModel.XGRY = UserLoginInfo.FullName;
            }
            else
            {
                dataModel.ID = Id;
                dataModel.XGBZ = 0;
                dataModel.CJRQ = DateTime.Now;
                dataModel.CJRY = UserLoginInfo.FullName;
                dataModel.KXF = "是";
                _context.XFXM.Add(dataModel);

            }
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = (IsEdit ? "修改" : "增加") + "消费项目[" + dataModel.MC + "]";
            logInfo.Save();
            #endregion
                
                
            return true;
        }

        private void txtdm_Leave(object sender, EventArgs e)
        {
            if (txtywmc.Text.Length == 0)
                txtywmc.Text = txtdm.Text;
        }
    }
}
