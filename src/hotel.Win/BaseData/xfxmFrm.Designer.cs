﻿namespace hotel.Win.BaseData
{
    partial class xfxmFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gridxm = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnxmbj = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gridfl = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnzlbj = new System.Windows.Forms.Button();
            this.FLID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridxm)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridfl)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gridxm);
            this.groupBox3.Controls.Add(this.panel2);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 175);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(799, 394);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "消费项目";
            // 
            // gridxm
            // 
            this.gridxm.AllowUserToAddRows = false;
            this.gridxm.AllowUserToDeleteRows = false;
            this.gridxm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridxm.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column10,
            this.Column14,
            this.Column9,
            this.Column11,
            this.Column15,
            this.Column13,
            this.Column7,
            this.Column16,
            this.Column1,
            this.XMID});
            this.gridxm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridxm.Location = new System.Drawing.Point(3, 47);
            this.gridxm.Name = "gridxm";
            this.gridxm.ReadOnly = true;
            this.gridxm.RowHeadersWidth = 25;
            this.gridxm.RowTemplate.Height = 23;
            this.gridxm.Size = new System.Drawing.Size(793, 344);
            this.gridxm.TabIndex = 2;
            this.gridxm.DoubleClick += new System.EventHandler(this.btnxmbj_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnxmbj);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(793, 28);
            this.panel2.TabIndex = 1;
            // 
            // btnxmbj
            // 
            this.btnxmbj.Location = new System.Drawing.Point(9, 3);
            this.btnxmbj.Name = "btnxmbj";
            this.btnxmbj.Size = new System.Drawing.Size(88, 23);
            this.btnxmbj.TabIndex = 5;
            this.btnxmbj.Text = "编辑项目";
            this.btnxmbj.UseVisualStyleBackColor = true;
            this.btnxmbj.Click += new System.EventHandler(this.btnxmbj_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gridfl);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(799, 175);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "消费项目分类";
            // 
            // gridfl
            // 
            this.gridfl.AllowUserToAddRows = false;
            this.gridfl.AllowUserToDeleteRows = false;
            this.gridfl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridfl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FLID,
            this.MC,
            this.Column4,
            this.Column5});
            this.gridfl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridfl.Location = new System.Drawing.Point(3, 47);
            this.gridfl.Name = "gridfl";
            this.gridfl.ReadOnly = true;
            this.gridfl.RowHeadersWidth = 25;
            this.gridfl.RowTemplate.Height = 23;
            this.gridfl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridfl.Size = new System.Drawing.Size(793, 125);
            this.gridfl.TabIndex = 2;
            this.gridfl.DoubleClick += new System.EventHandler(this.btnzlbj_Click);
            this.gridfl.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridfl_CellClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnzlbj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(793, 28);
            this.panel1.TabIndex = 0;
            // 
            // btnzlbj
            // 
            this.btnzlbj.Location = new System.Drawing.Point(9, 3);
            this.btnzlbj.Name = "btnzlbj";
            this.btnzlbj.Size = new System.Drawing.Size(88, 23);
            this.btnzlbj.TabIndex = 5;
            this.btnzlbj.Text = "编辑分类";
            this.btnzlbj.UseVisualStyleBackColor = true;
            this.btnzlbj.Click += new System.EventHandler(this.btnzlbj_Click);
            // 
            // FLID
            // 
            this.FLID.DataPropertyName = "ID";
            this.FLID.HeaderText = "分类代码";
            this.FLID.Name = "FLID";
            this.FLID.ReadOnly = true;
            // 
            // MC
            // 
            this.MC.DataPropertyName = "FLMC";
            this.MC.HeaderText = "分类名称";
            this.MC.Name = "MC";
            this.MC.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "SF_FWS";
            this.Column4.HeaderText = "是否服务生";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 110;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "SFHZ";
            this.Column5.HeaderText = "是否汇总";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "DM";
            this.Column8.HeaderText = "代码";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 60;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "MC";
            this.Column10.HeaderText = "名称";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "YWMC";
            this.Column14.HeaderText = "英文名称";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 90;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "DJ";
            this.Column9.HeaderText = "单价";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "XGBZ";
            this.Column11.HeaderText = "修改标志";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "BZSM";
            this.Column15.HeaderText = "备注说明";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 120;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "XGRY";
            this.Column13.HeaderText = "修改人员";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "XGRQ";
            this.Column7.HeaderText = "修改日期";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "CJRY";
            this.Column16.HeaderText = "创建人员";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "CJRQ";
            this.Column1.HeaderText = "创建日期";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // XMID
            // 
            this.XMID.DataPropertyName = "ID";
            this.XMID.HeaderText = "项目ID";
            this.XMID.Name = "XMID";
            this.XMID.ReadOnly = true;
            // 
            // xfxmFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 569);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "xfxmFrm";
            this.Text = "消费项目配置";
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridxm)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridfl)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView gridxm;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnxmbj;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView gridfl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnzlbj;
        private System.Windows.Forms.DataGridViewTextBoxColumn FLID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMID;
    }
}