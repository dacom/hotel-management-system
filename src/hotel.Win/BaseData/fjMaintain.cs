﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;

namespace hotel.Win.BaseData
{
    public partial class fjMaintain : hotel.MyUserControl.MaintainBaseForm
    {
        string Id;
        hotelEntities _context;
        ROOM dataModel;
        public fjMaintain()
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = MyDataContext.GetDataContext;
            cobxzl.DataSource = RoomTypeDal.GetAll();
            cobxzl.DisplayMember = "JC";
            Id = "0";
        }

        /// <summary>
        /// 用于增加
        /// </summary>
        /// <param name="ld">楼栋名称</param>
        public fjMaintain(string ld, bool isAdd)
            : this()
        {
            base.IsEdit = false;
            base.DeleteButtonEnabled = false;
            txtld.Text = ld;
            base.StatusMessage = "数据处于增加状态";
            Add();            
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public fjMaintain(string id)
            : this()
        {
            btncopy.Visible = true;
            BindData(id);
        }

        private void BindData(string id)
        {
            this.Id = id;
            dataModel = _context.ROOM.First(p => p.ID == id);
            //dataModel = _context.ROOM.Where(p => p.ID == id).FirstOrDefault();
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());
            txtfh.Text = dataModel.ID;
            txtlc.Value = dataModel.LC;
            txtld.Text = dataModel.LD;
            // cobxzl.SelectedIndex  = cobxzl.Items.IndexOf(dataModel.ROOM_TYPE);
            for (int i = 0; i < cobxzl.Items.Count; i++)
            {
                if ((cobxzl.Items[i] as ROOM_TYPE).ID == dataModel.ROOM_TYPE.ID)
                {
                    cobxzl.SelectedIndex = i;
                    break;
                } 
            }
           
            txtskfj.Value = (decimal)dataModel.SKFJ;
            chksffk.Checked = dataModel.SFKF == "是";
            txtfjms.Text = dataModel.FJMS;
            txtdhfj.Text = dataModel.DHFJ;
            txtdhwx.Text = dataModel.DHWX;
            txtbzsm.Text = dataModel.BZSM;
            txtcs.Value = (decimal)dataModel.CS;
            txtcx.Text = dataModel.CX;
            IsEdit = true;
            StatusMessage = "修改数据状态";
        }

        private bool Add()
        {
            if (hotel.Common.Utility.IsInt32(this.txtfh.Text))
                txtfh.Text = (Convert.ToInt32(txtfh.Text) + 1).ToString();
            dataModel = new ROOM { ID = Id, LC = 1 };
            btncopy.Visible = false;
            return true;
        }
        private bool Delete()
        {
            int cnt = _context.RZJL.Where(p => p.ROOM_ID == dataModel.ID).Count();
            if (cnt > 0)
            {
                MessageBox.Show("此房间已经发生了" + cnt.ToString() + "此入住，不能删除！");
                return false;
            }
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.RoomId = dataModel.ID;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "删除房间[" + dataModel.ID + "]"; ;
            logInfo.Save();
            #endregion
            _context.ROOM.Remove(dataModel);
            _context.SaveChanges();
            btncopy.Visible = false;
          
            return true;
        }

        private bool CheckData()
        {
            if (cobxzl.SelectedIndex < 0)
            {
                MessageBox.Show("请选择房间种类!");
                return false;
            }
            var info = ValidataHelper.StartVerify(txtfh.Text)
            .IsNotNullOrEmpty("房号不能为空输入为空")
            .Max(6, "房号长度不能大于6个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            info = ValidataHelper.StartVerify(txtld.Text)
           .IsNotNullOrEmpty("楼栋号不能为空输入为空")
           .Max(10, "楼栋号长度不能大于10个字符")
           .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            info = ValidataHelper.StartVerify(txtlc.Text)
           .IsNotNullOrEmpty("楼层号不能为空输入为空")
           .Max(8, "楼层号长度不能大于8个字符")
           .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
           
            if (!IsEdit)
            {
                if (_context.ROOM.AsNoTracking().Where(p => p.ID == txtfh.Text).Count() > 0)
                {
                    MessageBox.Show("房号" + txtfh.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false;
            dataModel.BZSM = txtbzsm.Text;
            dataModel.CS = (int)txtcs.Value;
            dataModel.CX = txtcx.Text;
            dataModel.DHFJ = txtdhfj.Text;
            dataModel.DHWX = txtdhwx.Text;
            dataModel.FJMS = txtfjms.Text;
            dataModel.LC = (int)txtlc.Value;
            dataModel.LD = txtld.Text;
            dataModel.SFKF = chksffk.Checked ? "是" : "否";
            dataModel.SKFJ = txtskfj.Value;
            string room_type_id= (cobxzl.SelectedValue as ROOM_TYPE).ID;
            ROOM_TYPE room_type = _context.ROOM_TYPE.Where(p => p.ID == room_type_id).FirstOrDefault();
            room_type.ROOM.Add(dataModel);
            if (IsEdit)
            {
                //dataModel.XGRQ = DateTime.Now;
                //dataModel.XGRY = UserLoginInfo.FullName;  
               
            }
            else
            {
                dataModel.ID = txtfh.Text;
                dataModel.ZT = "空净房";
                Id = txtfh.Text;
                _context.ROOM.Add(dataModel);
               
            }
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
            logInfo.RoomId = dataModel.ID;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = (IsEdit ? "修改" : "增加") + "房间[" + dataModel.ID + "]";
            logInfo.Save();
            #endregion
            _context.SaveChanges();
            btncopy.Visible = true;
            return true;
        }

        private void cobxzl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cobxzl.SelectedValue == null)
                return;
            var obj = cobxzl.SelectedValue as ROOM_TYPE;
            txtcs.Value = (decimal)obj.CS;
            txtskfj.Value = (decimal)obj.SKJG;
        }

        private void btncopy_Click(object sender, EventArgs e)
        {
            var frm = new fjMaintainCopy(Id);
            frm.ShowDialog();
        }
    }
}
