﻿namespace hotel.Win.BaseData
{
    partial class xtcydmMaintain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtdm = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtmc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkmr = new System.Windows.Forms.CheckBox();
            this.numxh = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.txtlb = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.numxh)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "代码：";
            // 
            // txtdm
            // 
            this.txtdm.Location = new System.Drawing.Point(113, 55);
            this.txtdm.Name = "txtdm";
            this.txtdm.Size = new System.Drawing.Size(219, 23);
            this.txtdm.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "名称：";
            // 
            // txtmc
            // 
            this.txtmc.Location = new System.Drawing.Point(113, 93);
            this.txtmc.Name = "txtmc";
            this.txtmc.Size = new System.Drawing.Size(219, 23);
            this.txtmc.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 14);
            this.label4.TabIndex = 4;
            this.label4.Text = "排序号：";
            // 
            // chkmr
            // 
            this.chkmr.AutoSize = true;
            this.chkmr.Location = new System.Drawing.Point(222, 130);
            this.chkmr.Name = "chkmr";
            this.chkmr.Size = new System.Drawing.Size(96, 18);
            this.chkmr.TabIndex = 4;
            this.chkmr.Text = "是否默认值";
            this.chkmr.UseVisualStyleBackColor = true;
            // 
            // numxh
            // 
            this.numxh.Location = new System.Drawing.Point(113, 129);
            this.numxh.Name = "numxh";
            this.numxh.Size = new System.Drawing.Size(71, 23);
            this.numxh.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 8;
            this.label3.Text = "类别名称：";
            // 
            // txtlb
            // 
            this.txtlb.Location = new System.Drawing.Point(113, 16);
            this.txtlb.Name = "txtlb";
            this.txtlb.ReadOnly = true;
            this.txtlb.Size = new System.Drawing.Size(219, 23);
            this.txtlb.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtdm);
            this.groupBox1.Controls.Add(this.numxh);
            this.groupBox1.Controls.Add(this.txtlb);
            this.groupBox1.Controls.Add(this.chkmr);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtmc);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(6, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(456, 172);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // xtcydmMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(468, 256);
            this.Controls.Add(this.groupBox1);
            this.Name = "xtcydmMaintain";
            this.Text = "常用代码数据维护";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.numxh)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtmc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkmr;
        private System.Windows.Forms.NumericUpDown numxh;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtlb;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
