﻿namespace hotel.Win.BaseData
{
    partial class xyfjMaintain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtfj = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.cobxfjfl = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtyj = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbckj = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtfj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtfj
            // 
            this.txtfj.Location = new System.Drawing.Point(116, 53);
            this.txtfj.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtfj.Name = "txtfj";
            this.txtfj.Size = new System.Drawing.Size(120, 23);
            this.txtfj.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 14);
            this.label4.TabIndex = 9;
            this.label4.Text = "房价：";
            // 
            // cobxfjfl
            // 
            this.cobxfjfl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxfjfl.FormattingEnabled = true;
            this.cobxfjfl.Location = new System.Drawing.Point(116, 16);
            this.cobxfjfl.Name = "cobxfjfl";
            this.cobxfjfl.Size = new System.Drawing.Size(141, 21);
            this.cobxfjfl.TabIndex = 0;
            this.cobxfjfl.SelectedIndexChanged += new System.EventHandler(this.cobxfjfl_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 12;
            this.label8.Text = "房间类型：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "佣金：";
            // 
            // txtyj
            // 
            this.txtyj.Location = new System.Drawing.Point(116, 92);
            this.txtyj.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtyj.Name = "txtyj";
            this.txtyj.Size = new System.Drawing.Size(120, 23);
            this.txtyj.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 9;
            this.label2.Text = "备注说明：";
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(116, 131);
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(264, 23);
            this.txtbzsm.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(251, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "参考价：";
            // 
            // lbckj
            // 
            this.lbckj.AutoSize = true;
            this.lbckj.Location = new System.Drawing.Point(306, 58);
            this.lbckj.Name = "lbckj";
            this.lbckj.Size = new System.Drawing.Size(35, 14);
            this.lbckj.TabIndex = 15;
            this.lbckj.Text = "0000";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lbckj);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtfj);
            this.groupBox1.Controls.Add(this.txtbzsm);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cobxfjfl);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtyj);
            this.groupBox1.Location = new System.Drawing.Point(5, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(451, 181);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // xyfjMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(461, 259);
            this.Controls.Add(this.groupBox1);
            this.Name = "xyfjMaintain";
            this.Text = "协议单位房价";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.txtfj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown txtfj;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cobxfjfl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtyj;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbckj;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
