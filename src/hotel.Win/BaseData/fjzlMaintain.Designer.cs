﻿namespace hotel.Win.BaseData
{
    partial class fjzlMaintain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtdm = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtmc = new System.Windows.Forms.TextBox();
            this.txtjc = new System.Windows.Forms.TextBox();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.txtgpjg = new System.Windows.Forms.NumericUpDown();
            this.txtskjg = new System.Windows.Forms.NumericUpDown();
            this.txtxh = new System.Windows.Forms.NumericUpDown();
            this.txtcs = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtyj = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.txtgpjg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtskjg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtxh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcs)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "代码：";
            // 
            // txtdm
            // 
            this.txtdm.Location = new System.Drawing.Point(113, 23);
            this.txtdm.Name = "txtdm";
            this.txtdm.Size = new System.Drawing.Size(227, 23);
            this.txtdm.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "名称：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "简称：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 2;
            this.label4.Text = "挂牌价格：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(230, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 2;
            this.label5.Text = "散客价格：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(244, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 14);
            this.label6.TabIndex = 2;
            this.label6.Text = "排序号：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 206);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 14);
            this.label7.TabIndex = 2;
            this.label7.Text = "备注说明：";
            // 
            // txtmc
            // 
            this.txtmc.Location = new System.Drawing.Point(113, 55);
            this.txtmc.Name = "txtmc";
            this.txtmc.Size = new System.Drawing.Size(227, 23);
            this.txtmc.TabIndex = 1;
            // 
            // txtjc
            // 
            this.txtjc.Location = new System.Drawing.Point(113, 87);
            this.txtjc.Name = "txtjc";
            this.txtjc.Size = new System.Drawing.Size(227, 23);
            this.txtjc.TabIndex = 2;
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(113, 206);
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(296, 23);
            this.txtbzsm.TabIndex = 7;
            // 
            // txtgpjg
            // 
            this.txtgpjg.DecimalPlaces = 2;
            this.txtgpjg.Location = new System.Drawing.Point(113, 119);
            this.txtgpjg.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtgpjg.Name = "txtgpjg";
            this.txtgpjg.Size = new System.Drawing.Size(100, 23);
            this.txtgpjg.TabIndex = 3;
            // 
            // txtskjg
            // 
            this.txtskjg.DecimalPlaces = 2;
            this.txtskjg.Location = new System.Drawing.Point(309, 122);
            this.txtskjg.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtskjg.Name = "txtskjg";
            this.txtskjg.Size = new System.Drawing.Size(100, 23);
            this.txtskjg.TabIndex = 4;
            // 
            // txtxh
            // 
            this.txtxh.Location = new System.Drawing.Point(309, 174);
            this.txtxh.Name = "txtxh";
            this.txtxh.Size = new System.Drawing.Size(100, 23);
            this.txtxh.TabIndex = 6;
            // 
            // txtcs
            // 
            this.txtcs.Location = new System.Drawing.Point(113, 174);
            this.txtcs.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtcs.Name = "txtcs";
            this.txtcs.Size = new System.Drawing.Size(100, 23);
            this.txtcs.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(58, 174);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 14);
            this.label10.TabIndex = 11;
            this.label10.Text = "床数：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtcs);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtbzsm);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtjc);
            this.groupBox1.Controls.Add(this.txtgpjg);
            this.groupBox1.Controls.Add(this.txtmc);
            this.groupBox1.Controls.Add(this.txtyj);
            this.groupBox1.Controls.Add(this.txtskjg);
            this.groupBox1.Controls.Add(this.txtdm);
            this.groupBox1.Controls.Add(this.txtxh);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(458, 239);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(34, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 2;
            this.label8.Text = "每日押金：";
            // 
            // txtyj
            // 
            this.txtyj.DecimalPlaces = 2;
            this.txtyj.Location = new System.Drawing.Point(113, 145);
            this.txtyj.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtyj.Name = "txtyj";
            this.txtyj.Size = new System.Drawing.Size(100, 23);
            this.txtyj.TabIndex = 4;
            // 
            // fjzlMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(458, 314);
            this.Controls.Add(this.groupBox1);
            this.Name = "fjzlMaintain";
            this.Text = "房间种类数据维护";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.txtgpjg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtskjg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtxh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcs)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtmc;
        private System.Windows.Forms.TextBox txtjc;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.NumericUpDown txtgpjg;
        private System.Windows.Forms.NumericUpDown txtskjg;
        private System.Windows.Forms.NumericUpDown txtxh;
        private System.Windows.Forms.NumericUpDown txtcs;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown txtyj;
        private System.Windows.Forms.Label label8;
    }
}
