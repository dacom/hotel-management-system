﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
namespace hotel.Win.BaseData
{
    public partial class xtcydmFrm : Form
    {
        EnumXtcydmLB currLBDM;
        public xtcydmFrm()
        {
            InitializeComponent();
            btnbj.Enabled = false;           
            BindLB();
        }

        private void BindLB()
        {
            lstLB.Items.Clear();
            foreach (var item in new xtcydmLB())
            {
                lstLB.Items.Add(item.Value);
            }
        }

        private void lstLB_Click(object sender, EventArgs e)
        {
            currLBDM = new xtcydmLB().FirstOrDefault(p => p.Value == lstLB.SelectedItem.ToString()).Key;
            BindGrid(true);
            btnbj.Enabled = true;
        }
        private void BindGrid(bool isChange)
        {
            if (!isChange)
                return;
            gridXM.AutoGenerateColumns = false;
            gridXM.DataSource = XTCYDMDal.GetAllByLBDM(currLBDM.ToString());
        }
        private void btnbj_Click(object sender, EventArgs e)
        {            
            string id = Common.ControlsUtility.GetActiveGridValue(gridXM, "ID");
            xtcydmMaintain frm;
            if (id == "")
                frm = new xtcydmMaintain(currLBDM);
            else
                frm = new xtcydmMaintain(Convert.ToInt32(id));
            frm.CallBack = BindGrid;
            frm.ShowDialog();            
        }
    }
}
