﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
namespace hotel.Win.BaseData
{
    /// <summary>
    /// 客房配置
    /// </summary>
    public partial class kypzFrm : Form
    {
        private hotelEntities _context;
        public kypzFrm()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            InitData(true);
        }

        private void InitData(bool flag)
        {
            if (!flag)
                return;
            cobxld.DataSource = XTCYDMDal.GetAllByLBDM(EnumXtcydmLB.LDH.ToString());
            cobxld.DisplayMember = "XMMC";
            if (cobxld.SelectedValue !=null)
                cobxld_SelectedIndexChanged(cobxld, null);
            gridzl.AutoGenerateColumns = false;
            gridfj.AutoGenerateColumns = false;
            gridzl.DataSource = RoomTypeDal.GetAll();
        }

        private void btnldbj_Click(object sender, EventArgs e)
        {
            xtcydmMaintain frm;
            if (cobxld.SelectedIndex < 0)
                frm = new xtcydmMaintain(EnumXtcydmLB.LDH);
            else
                frm = new xtcydmMaintain((cobxld.SelectedValue as XTCYDM).ID);
            frm.CallBack = InitData;
            frm.ShowDialog();
        }

         private void btnzlbj_Click(object sender, EventArgs e)
        {
            fjzlMaintain frm;
            if (gridzl.SelectedRows.Count == 0)
                frm = new fjzlMaintain(true);
            else
                frm = new fjzlMaintain(gridzl.SelectedRows[0].Cells["ID"].Value.ToString());
            frm.CallBack = InitData;
            frm.ShowDialog();
        }

        private void BindFJXX(string fjzlId)
        {
            if (cobxld.SelectedValue == null)
            {
                MessageBox.Show("必须先维护好楼栋号！");
                return;
            }
            string ld = (cobxld.SelectedValue as XTCYDM).XMMC;            
            var qry = from r in _context.ROOM.AsNoTracking()
                      join t in _context.ROOM_TYPE.AsNoTracking() on r.ROOM_TYPE.ID equals t.ID
                      where t.ID == fjzlId && r.LD == ld
                      orderby r.ID
                      select new
                      {
                          r.BZSM,
                          r.CS,
                          r.CX,
                          r.DHFJ,
                          r.DHWX,
                          r.FJMS,
                          r.HHBZ,
                          r.ID,
                          r.LC,
                          r.LD,
                          r.SFKF,
                          r.SKFJ,
                          r.ZT,
                          t.JC
                      };
            gridfj.AutoGenerateColumns = false;
            gridfj.DataSource = qry.ToList();
            lbfjs.Text = qry.Count().ToString();
        }
        /// <summary>
        /// 房类价格
        /// </summary>
        /// <param name="fjzlId"></param>
        private void BindFLJG(string fjzlId)
        {
            if (cobxld.SelectedValue == null)
            {
                MessageBox.Show("必须先维护好楼栋号！");
                return;
            }
         
            var qry = from r in _context.RoomTypePrice.AsNoTracking()
                      where r.ROOM_TYPE_ID == fjzlId
                      orderby r.XYMC
                      select r;
            gridFljg.AutoGenerateColumns = false;
            gridFljg.DataSource = qry.ToList();
        }
         private void BindFJXXCalback(bool flag)
        {
            if (flag)
                BindFJXX(gridzl.SelectedRows[0].Cells["ID"].Value.ToString());
        }
        private void btnfjbj_Click(object sender, EventArgs e)
        {
            string id = Common.ControlsUtility.GetActiveGridValue(gridfj, "ColumnFJID");
            fjMaintain frm;
            if (id=="")
            {
                if (cobxld.SelectedIndex < 0)
                {
                    MessageBox.Show("增加房间时，请选择楼栋号！");
                }
                frm = new fjMaintain(cobxld.Text,true);
            }
            else
                frm = new fjMaintain(id);
            frm.CallBack = BindFJXXCalback;
            frm.ShowDialog();
            if (gridzl.CurrentRow != null)
                gridzl_CellClick(gridzl,null);
        }
       

        private void gridzl_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (mainTab.SelectedIndex == 0)
                BindFJXX(gridzl.CurrentRow.Cells["ID"].Value.ToString());
            else
                BindFLJG(gridzl.CurrentRow.Cells["ID"].Value.ToString());
        }

        private void cobxld_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cobxld.SelectedValue == null)
                return;
            string ld = (cobxld.SelectedValue as XTCYDM).XMMC;
            var qry = from r in _context.ROOM.AsNoTracking()
                      join t in _context.ROOM_TYPE.AsNoTracking() on r.ROOM_TYPE.ID equals t.ID
                      where r.LD == ld
                      orderby r.ID
                      select new
                      {
                          r.BZSM,
                          r.CS,
                          r.CX,
                          r.DHFJ,
                          r.DHWX,
                          r.FJMS,
                          r.HHBZ,
                          r.ID,
                          r.LC,
                          r.LD,
                          r.SFKF,
                          r.SKFJ,
                          //r.YDBZ,
                          r.ZT,
                          t.JC
                      };
            gridfj.AutoGenerateColumns = false;
            var datas = qry.ToList();
            gridfj.DataSource =datas ;
            lbfjs.Text = datas.Count().ToString();
            cobxlc.DataSource = qry.GroupBy(p => p.LC).Select(p => p.Key).ToList();
        }

        private void cobxlc_SelectedIndexChanged(object sender, EventArgs e)
        {
            var lc = Convert.ToInt32(cobxlc.Text);
            var qry = from r in _context.ROOM
                      join t in _context.ROOM_TYPE on r.ROOM_TYPE.ID equals t.ID
                      where r.LC == lc
                      orderby r.ID
                      select new
                      {
                          r.BZSM,
                          r.CS,
                          r.CX,
                          r.DHFJ,
                          r.DHWX,
                          r.FJMS,
                          r.HHBZ,
                          r.ID,
                          r.LC,
                          r.LD,
                          r.SFKF,
                          r.SKFJ,
                          //r.YDBZ,
                          r.ZT,
                          t.JC
                      };
            gridfj.AutoGenerateColumns = false;
            var datas = qry.ToList();
            gridfj.DataSource = datas;
            lbfjs.Text = datas.Count().ToString();
        }

        private void mainTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mainTab.SelectedIndex==0)
                BindFJXX(gridzl.CurrentRow.Cells["ID"].Value.ToString());
            else
                BindFLJG(gridzl.CurrentRow.Cells["ID"].Value.ToString());
        }

        private void btnjg_Click(object sender, EventArgs e)
        {
            string str = Common.ControlsUtility.GetActiveGridValue(gridFljg, "ColumnID");
          
            fljgMaintain frm;
            if (str == "")
            {
                var fjzlid = gridzl.CurrentRow.Cells["ID"].Value.ToString();
                if (string.IsNullOrEmpty(fjzlid))
                {
                    MessageBox.Show("增加房类价格时，请选择一个房类！");
                }
                frm = new fljgMaintain(fjzlid);
            }
            else
                frm = new fljgMaintain(Convert.ToInt32(str));
            frm.CallBack = BindFJXXCalback;
            frm.ShowDialog();
            if (gridzl.CurrentRow != null)
                gridzl_CellClick(gridzl, null);
        }       
    }
}
