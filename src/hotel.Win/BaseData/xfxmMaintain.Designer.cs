﻿namespace hotel.Win.BaseData
{
    partial class xfxmMaintain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtdm = new System.Windows.Forms.TextBox();
            this.txtmc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtywmc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdj = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtdj)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "项目代码：";
            // 
            // txtdm
            // 
            this.txtdm.Location = new System.Drawing.Point(116, 15);
            this.txtdm.Name = "txtdm";
            this.txtdm.Size = new System.Drawing.Size(220, 23);
            this.txtdm.TabIndex = 1;
            this.txtdm.Leave += new System.EventHandler(this.txtdm_Leave);
            // 
            // txtmc
            // 
            this.txtmc.Location = new System.Drawing.Point(116, 48);
            this.txtmc.Name = "txtmc";
            this.txtmc.Size = new System.Drawing.Size(220, 23);
            this.txtmc.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 9;
            this.label2.Text = "项目名称：";
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(116, 147);
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(220, 23);
            this.txtbzsm.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 9;
            this.label3.Text = "备注说明：";
            // 
            // txtywmc
            // 
            this.txtywmc.Location = new System.Drawing.Point(116, 81);
            this.txtywmc.Name = "txtywmc";
            this.txtywmc.Size = new System.Drawing.Size(220, 23);
            this.txtywmc.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 9;
            this.label4.Text = "英文名称：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(61, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 9;
            this.label5.Text = "单价：";
            // 
            // txtdj
            // 
            this.txtdj.DecimalPlaces = 2;
            this.txtdj.Location = new System.Drawing.Point(116, 114);
            this.txtdj.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtdj.Name = "txtdj";
            this.txtdj.Size = new System.Drawing.Size(120, 23);
            this.txtdj.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtdj);
            this.groupBox1.Controls.Add(this.txtdm);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtmc);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtbzsm);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtywmc);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(6, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(448, 189);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // xfxmMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(462, 266);
            this.Controls.Add(this.groupBox1);
            this.Name = "xfxmMaintain";
            this.Text = "消费项目维护";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.txtdj)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdm;
        private System.Windows.Forms.TextBox txtmc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtywmc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown txtdj;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
