﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;
namespace hotel.Win.BaseData
{
    public partial class fjzlMaintain : hotel.MyUserControl.MaintainBaseForm
    {       
        string Id;
        hotelEntities _context;
        ROOM_TYPE dataModel;

        /// <summary>
        /// 用于增加
        /// </summary>
        public fjzlMaintain(bool isAdd)
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = new hotelEntities();
            Id = "0";
            if (isAdd)
            {
                base.IsEdit = false;
                base.DeleteButtonEnabled = false;
                base.StatusMessage = "数据处于增加状态";
                Add();                
            }
        }       
       
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public fjzlMaintain(string id)
            : this(false)
        {
            BindData(id);
        }

        private void BindData(string id)
        {
            this.Id = id;
            dataModel = _context.ROOM_TYPE.First(p => p.ID == id);
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());            
            txtdm.Text = dataModel.ID;
            txtmc.Text = dataModel.MC;
            txtjc.Text = dataModel.JC;
            txtgpjg.Value = (decimal)dataModel.GPJG;
            txtskjg.Value = (decimal)dataModel.SKJG;
            txtxh.Value = (decimal)dataModel.XH;
            txtcs.Value = (decimal)dataModel.CS;
            txtyj.Value = dataModel.YJ;
            txtbzsm.Text = dataModel.BZSM;
            IsEdit = true;
            StatusMessage = "修改数据状态";

        }

        private bool Add()
        {
            var maxxh =  _context.ROOM_TYPE.Count();
            txtxh.Value = maxxh == 0 ? 1 : (decimal)maxxh + 1;
            dataModel = new ROOM_TYPE();
            txtyj.Value = 100;
            return true;
        }
        private bool Delete()
        {
            int cnt = _context.ROOM.Where(p => p.ROOM_TYPE.ID == dataModel.ID).Count();
            if ( cnt> 0)
            {
                MessageBox.Show("此房间种类下已经设置了" + cnt.ToString() + "个房间，不能删除！");
                return false;
            }
            cnt = _context.RoomTypePrice.Where(p => p.ROOM_TYPE.ID == dataModel.ID).Count();
            if (cnt > 0)
            {
                MessageBox.Show("此房间种类下已经设置了" + cnt.ToString() + "个房类价格，不能删除！");
                return false;
            }

            _context.ROOM_TYPE.Remove(dataModel);
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "房间种类[" + dataModel.MC + "]" ;
            logInfo.Save();
            #endregion
            return true;
        }

        private bool CheckData()
        {
            var info = ValidataHelper.StartVerify(txtdm.Text)
            .IsNotNullOrEmpty("代码不能为空输入为空")
            .Max(10, "代码长度不能大于10个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
             info = ValidataHelper.StartVerify(txtmc.Text)
            .IsNotNullOrEmpty("名称不能为空输入为空")
            .Max(32, "名称长度不能大于32个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            info = ValidataHelper.StartVerify(txtjc.Text)
           .IsNotNullOrEmpty("简称不能为空输入为空")
           .Max(20, "简称长度不能大于20个字符")
           .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            info = ValidataHelper.StartVerify(txtyj.Value)
           .Min(0, "每日押金不能小于0")
           .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            if (!IsEdit)
            {
                if (_context.ROOM_TYPE.Where(p => p.ID == txtdm.Text).Count() > 0)
                {
                    MessageBox.Show("代码" + txtdm.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false ;
            dataModel.MC = txtmc.Text;
            dataModel.JC = txtjc.Text;
            dataModel.GPJG = txtgpjg.Value;
            dataModel.SKJG = txtskjg.Value;            
            dataModel.XH = (int)txtxh.Value;
            dataModel.BZSM = txtbzsm.Text;
            dataModel.CS = (int)txtcs.Value;
            dataModel.YJ = txtyj.Value;
            if (IsEdit)
            {                
                //dataModel.XGRQ = DateTime.Now;
                //dataModel.XGRY = UserLoginInfo.FullName;                
            }
            else
            {
                dataModel.ID = txtdm.Text;
                //_context.Attach(dataModel);
                _context.ROOM_TYPE.Add(dataModel);
                #region 日志纪录
                CcrzInfo logInfo = null;
                logInfo = new CcrzInfo();
                logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
                logInfo.Type = LogTypeEnum.一般操作;
                logInfo.Message = (IsEdit ? "修改" : "增加") + "房间种类[" + dataModel.MC + "]";
                logInfo.Save();
                #endregion
            }
            _context.SaveChanges();
           
            return true;
        }
    }
}
