﻿namespace hotel.Win.BaseData
{
    partial class xtcydmFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstLB = new System.Windows.Forms.ListBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnbj = new System.Windows.Forms.Button();
            this.gridXM = new System.Windows.Forms.DataGridView();
            this.XMDM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridXM)).BeginInit();
            this.SuspendLayout();
            // 
            // lstLB
            // 
            this.lstLB.Dock = System.Windows.Forms.DockStyle.Left;
            this.lstLB.FormattingEnabled = true;
            this.lstLB.Location = new System.Drawing.Point(0, 0);
            this.lstLB.Name = "lstLB";
            this.lstLB.Size = new System.Drawing.Size(240, 569);
            this.lstLB.TabIndex = 0;
            this.lstLB.Click += new System.EventHandler(this.lstLB_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(240, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 569);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnbj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(243, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(518, 33);
            this.panel1.TabIndex = 4;
            // 
            // btnbj
            // 
            this.btnbj.Location = new System.Drawing.Point(15, 4);
            this.btnbj.Name = "btnbj";
            this.btnbj.Size = new System.Drawing.Size(75, 23);
            this.btnbj.TabIndex = 0;
            this.btnbj.Text = "编辑";
            this.btnbj.UseVisualStyleBackColor = true;
            this.btnbj.Click += new System.EventHandler(this.btnbj_Click);
            // 
            // gridXM
            // 
            this.gridXM.AllowUserToAddRows = false;
            this.gridXM.AllowUserToDeleteRows = false;
            this.gridXM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridXM.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.XMDM,
            this.MC,
            this.Column3,
            this.Column4,
            this.ID});
            this.gridXM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridXM.Location = new System.Drawing.Point(243, 33);
            this.gridXM.Name = "gridXM";
            this.gridXM.ReadOnly = true;
            this.gridXM.RowHeadersWidth = 25;
            this.gridXM.RowTemplate.Height = 23;
            this.gridXM.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridXM.Size = new System.Drawing.Size(518, 536);
            this.gridXM.TabIndex = 5;
            this.gridXM.DoubleClick += new System.EventHandler(this.btnbj_Click);
            // 
            // XMDM
            // 
            this.XMDM.DataPropertyName = "XMDM";
            this.XMDM.HeaderText = "代码";
            this.XMDM.Name = "XMDM";
            this.XMDM.ReadOnly = true;
            // 
            // MC
            // 
            this.MC.DataPropertyName = "XMMC";
            this.MC.HeaderText = "名称";
            this.MC.Name = "MC";
            this.MC.ReadOnly = true;
            this.MC.Width = 200;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "MRBZ";
            this.Column3.HeaderText = "默认标志";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "XH";
            this.Column4.HeaderText = "排序码";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 90;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // xtcydmFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 569);
            this.Controls.Add(this.gridXM);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.lstLB);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "xtcydmFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "系统常用代码管理";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridXM)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstLB;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView gridXM;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMDM;
        private System.Windows.Forms.DataGridViewTextBoxColumn MC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.Button btnbj;
    }
}