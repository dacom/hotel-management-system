﻿namespace hotel.Win.BaseData
{
    partial class xydwMaintain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtxydm = new System.Windows.Forms.TextBox();
            this.txtdwmc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtlxr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtdh = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtgzje = new System.Windows.Forms.NumericUpDown();
            this.chkgzf = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtdz = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cobxzt = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cobxxyfl = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtgzje)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 6;
            this.label1.Text = "协议代码：";
            // 
            // txtxydm
            // 
            this.txtxydm.Location = new System.Drawing.Point(122, 16);
            this.txtxydm.Name = "txtxydm";
            this.txtxydm.Size = new System.Drawing.Size(140, 23);
            this.txtxydm.TabIndex = 0;
            // 
            // txtdwmc
            // 
            this.txtdwmc.Location = new System.Drawing.Point(122, 46);
            this.txtdwmc.Name = "txtdwmc";
            this.txtdwmc.Size = new System.Drawing.Size(291, 23);
            this.txtdwmc.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "单位名称：";
            // 
            // txtlxr
            // 
            this.txtlxr.Location = new System.Drawing.Point(122, 106);
            this.txtlxr.Name = "txtlxr";
            this.txtlxr.Size = new System.Drawing.Size(78, 23);
            this.txtlxr.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "联系人：";
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(122, 196);
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(291, 23);
            this.txtbzsm.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 6;
            this.label4.Text = "挂账金额：";
            // 
            // txtdh
            // 
            this.txtdh.Location = new System.Drawing.Point(122, 136);
            this.txtdh.Name = "txtdh";
            this.txtdh.Size = new System.Drawing.Size(291, 23);
            this.txtdh.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(67, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 6;
            this.label5.Text = "电话：";
            // 
            // txtgzje
            // 
            this.txtgzje.Location = new System.Drawing.Point(122, 76);
            this.txtgzje.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.txtgzje.Name = "txtgzje";
            this.txtgzje.Size = new System.Drawing.Size(140, 23);
            this.txtgzje.TabIndex = 3;
            // 
            // chkgzf
            // 
            this.chkgzf.AutoSize = true;
            this.chkgzf.Location = new System.Drawing.Point(268, 79);
            this.chkgzf.Name = "chkgzf";
            this.chkgzf.Size = new System.Drawing.Size(82, 18);
            this.chkgzf.TabIndex = 4;
            this.chkgzf.Text = "能否挂账";
            this.chkgzf.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(67, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 14);
            this.label6.TabIndex = 6;
            this.label6.Text = "地址：";
            // 
            // txtdz
            // 
            this.txtdz.Location = new System.Drawing.Point(122, 166);
            this.txtdz.Name = "txtdz";
            this.txtdz.Size = new System.Drawing.Size(291, 23);
            this.txtdz.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(269, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 14);
            this.label7.TabIndex = 6;
            this.label7.Text = "状态：";
            // 
            // cobxzt
            // 
            this.cobxzt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxzt.FormattingEnabled = true;
            this.cobxzt.Items.AddRange(new object[] {
            "在用",
            "停用"});
            this.cobxzt.Location = new System.Drawing.Point(324, 18);
            this.cobxzt.Name = "cobxzt";
            this.cobxzt.Size = new System.Drawing.Size(87, 21);
            this.cobxzt.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(206, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 6;
            this.label8.Text = "协议分类：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 200);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 6;
            this.label9.Text = "备注说明：";
            // 
            // cobxxyfl
            // 
            this.cobxxyfl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxxyfl.FormattingEnabled = true;
            this.cobxxyfl.Location = new System.Drawing.Point(272, 108);
            this.cobxxyfl.Name = "cobxxyfl";
            this.cobxxyfl.Size = new System.Drawing.Size(141, 21);
            this.cobxxyfl.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cobxxyfl);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.chkgzf);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cobxzt);
            this.groupBox1.Controls.Add(this.txtbzsm);
            this.groupBox1.Controls.Add(this.txtxydm);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtgzje);
            this.groupBox1.Controls.Add(this.txtlxr);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtdwmc);
            this.groupBox1.Controls.Add(this.txtdz);
            this.groupBox1.Controls.Add(this.txtdh);
            this.groupBox1.Location = new System.Drawing.Point(5, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(456, 240);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // xydwMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(465, 321);
            this.Controls.Add(this.groupBox1);
            this.Name = "xydwMaintain";
            this.Text = "协议单位维护";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.txtgzje)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtxydm;
        private System.Windows.Forms.TextBox txtdwmc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtlxr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtdh;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown txtgzje;
        private System.Windows.Forms.CheckBox chkgzf;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtdz;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cobxzt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cobxxyfl;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
