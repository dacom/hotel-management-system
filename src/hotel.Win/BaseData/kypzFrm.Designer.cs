﻿namespace hotel.Win.BaseData
{
    partial class kypzFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnldbj = new System.Windows.Forms.Button();
            this.cobxld = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gridzl = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnYJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnzlbj = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.FJID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cobxlc = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbfjs = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnfjdel = new System.Windows.Forms.Button();
            this.btnfjbj = new System.Windows.Forms.Button();
            this.mainTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gridfj = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridFljg = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnjg = new System.Windows.Forms.Button();
            this.ColumnFJID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridzl)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.mainTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridFljg)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnldbj);
            this.groupBox1.Controls.Add(this.cobxld);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(878, 46);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "楼栋";
            // 
            // btnldbj
            // 
            this.btnldbj.Location = new System.Drawing.Point(217, 19);
            this.btnldbj.Name = "btnldbj";
            this.btnldbj.Size = new System.Drawing.Size(82, 23);
            this.btnldbj.TabIndex = 4;
            this.btnldbj.Text = "编辑楼栋";
            this.btnldbj.UseVisualStyleBackColor = true;
            this.btnldbj.Click += new System.EventHandler(this.btnldbj_Click);
            // 
            // cobxld
            // 
            this.cobxld.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxld.FormattingEnabled = true;
            this.cobxld.Location = new System.Drawing.Point(69, 19);
            this.cobxld.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cobxld.Name = "cobxld";
            this.cobxld.Size = new System.Drawing.Size(141, 21);
            this.cobxld.TabIndex = 3;
            this.cobxld.SelectedIndexChanged += new System.EventHandler(this.cobxld_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "楼栋号：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gridzl);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 46);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(878, 175);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "房间种类";
            // 
            // gridzl
            // 
            this.gridzl.AllowUserToAddRows = false;
            this.gridzl.AllowUserToDeleteRows = false;
            this.gridzl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridzl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.MC,
            this.Column1,
            this.Column6,
            this.Column2,
            this.Column3,
            this.ColumnYJ,
            this.Column4,
            this.Column5});
            this.gridzl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridzl.Location = new System.Drawing.Point(3, 47);
            this.gridzl.Name = "gridzl";
            this.gridzl.ReadOnly = true;
            this.gridzl.RowHeadersWidth = 25;
            this.gridzl.RowTemplate.Height = 23;
            this.gridzl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridzl.Size = new System.Drawing.Size(872, 125);
            this.gridzl.TabIndex = 2;
            this.gridzl.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridzl_CellClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "代码";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // MC
            // 
            this.MC.DataPropertyName = "MC";
            this.MC.HeaderText = "名称";
            this.MC.Name = "MC";
            this.MC.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "JC";
            this.Column1.HeaderText = "简称";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "CS";
            this.Column6.HeaderText = "床数";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 60;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "GPJG";
            this.Column2.HeaderText = "挂牌价格";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "SKJG";
            this.Column3.HeaderText = "散客价格";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // ColumnYJ
            // 
            this.ColumnYJ.DataPropertyName = "YJ";
            this.ColumnYJ.HeaderText = "每日押金";
            this.ColumnYJ.Name = "ColumnYJ";
            this.ColumnYJ.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "XH";
            this.Column4.HeaderText = "排序码";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 90;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "BZSM";
            this.Column5.HeaderText = "备注说明";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnzlbj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(872, 28);
            this.panel1.TabIndex = 0;
            // 
            // btnzlbj
            // 
            this.btnzlbj.Location = new System.Drawing.Point(9, 3);
            this.btnzlbj.Name = "btnzlbj";
            this.btnzlbj.Size = new System.Drawing.Size(118, 23);
            this.btnzlbj.TabIndex = 5;
            this.btnzlbj.Text = "编辑房间种类";
            this.btnzlbj.UseVisualStyleBackColor = true;
            this.btnzlbj.Click += new System.EventHandler(this.btnzlbj_Click);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 221);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(878, 3);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // FJID
            // 
            this.FJID.DataPropertyName = "ID";
            this.FJID.HeaderText = "房间号";
            this.FJID.Name = "FJID";
            this.FJID.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "LC";
            this.Column8.HeaderText = "楼层";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 60;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "JC";
            this.Column10.HeaderText = "房屋类型";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "CX";
            this.Column14.HeaderText = "朝向";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 60;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "SKFJ";
            this.Column9.HeaderText = "散客房价";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "FJMS";
            this.Column11.HeaderText = "房间描述";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "CS";
            this.Column15.HeaderText = "床数";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 60;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "DHFJ";
            this.Column12.HeaderText = "电话分机";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "DHWX";
            this.Column13.HeaderText = "电话外线";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "LD";
            this.Column7.HeaderText = "楼栋";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "BZSM";
            this.Column16.HeaderText = "备注说明";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cobxlc);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lbfjs);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnfjdel);
            this.panel2.Controls.Add(this.btnfjbj);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(864, 28);
            this.panel2.TabIndex = 1;
            // 
            // cobxlc
            // 
            this.cobxlc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxlc.FormattingEnabled = true;
            this.cobxlc.Location = new System.Drawing.Point(155, 4);
            this.cobxlc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cobxlc.Name = "cobxlc";
            this.cobxlc.Size = new System.Drawing.Size(66, 21);
            this.cobxlc.TabIndex = 10;
            this.cobxlc.SelectedIndexChanged += new System.EventHandler(this.cobxlc_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(112, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 14);
            this.label3.TabIndex = 9;
            this.label3.Text = "楼层";
            // 
            // lbfjs
            // 
            this.lbfjs.AutoSize = true;
            this.lbfjs.Location = new System.Drawing.Point(295, 6);
            this.lbfjs.Name = "lbfjs";
            this.lbfjs.Size = new System.Drawing.Size(14, 14);
            this.lbfjs.TabIndex = 8;
            this.lbfjs.Text = "0";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(228, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 14);
            this.label2.TabIndex = 7;
            this.label2.Text = "房间总数";
            // 
            // btnfjdel
            // 
            this.btnfjdel.Location = new System.Drawing.Point(349, 3);
            this.btnfjdel.Name = "btnfjdel";
            this.btnfjdel.Size = new System.Drawing.Size(78, 23);
            this.btnfjdel.TabIndex = 6;
            this.btnfjdel.Text = "删除选定";
            this.btnfjdel.UseVisualStyleBackColor = true;
            this.btnfjdel.Visible = false;
            // 
            // btnfjbj
            // 
            this.btnfjbj.Location = new System.Drawing.Point(9, 3);
            this.btnfjbj.Name = "btnfjbj";
            this.btnfjbj.Size = new System.Drawing.Size(88, 23);
            this.btnfjbj.TabIndex = 5;
            this.btnfjbj.Text = "编辑房间";
            this.btnfjbj.UseVisualStyleBackColor = true;
            this.btnfjbj.Click += new System.EventHandler(this.btnfjbj_Click);
            // 
            // mainTab
            // 
            this.mainTab.Controls.Add(this.tabPage1);
            this.mainTab.Controls.Add(this.tabPage2);
            this.mainTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTab.Location = new System.Drawing.Point(0, 224);
            this.mainTab.Name = "mainTab";
            this.mainTab.SelectedIndex = 0;
            this.mainTab.Size = new System.Drawing.Size(878, 266);
            this.mainTab.TabIndex = 3;
            this.mainTab.SelectedIndexChanged += new System.EventHandler(this.mainTab_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gridfj);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(870, 239);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "房间配置";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gridfj
            // 
            this.gridfj.AllowUserToAddRows = false;
            this.gridfj.AllowUserToDeleteRows = false;
            this.gridfj.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridfj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnFJID,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22});
            this.gridfj.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridfj.Location = new System.Drawing.Point(3, 31);
            this.gridfj.Name = "gridfj";
            this.gridfj.ReadOnly = true;
            this.gridfj.RowHeadersWidth = 25;
            this.gridfj.RowTemplate.Height = 23;
            this.gridfj.Size = new System.Drawing.Size(864, 205);
            this.gridfj.TabIndex = 3;
            this.gridfj.DoubleClick += new System.EventHandler(this.btnfjbj_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridFljg);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(870, 239);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "房类价格";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridFljg
            // 
            this.gridFljg.AllowUserToAddRows = false;
            this.gridFljg.AllowUserToDeleteRows = false;
            this.gridFljg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridFljg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.ColumnID});
            this.gridFljg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridFljg.Location = new System.Drawing.Point(3, 31);
            this.gridFljg.Name = "gridFljg";
            this.gridFljg.ReadOnly = true;
            this.gridFljg.RowHeadersWidth = 25;
            this.gridFljg.RowTemplate.Height = 23;
            this.gridFljg.Size = new System.Drawing.Size(864, 205);
            this.gridFljg.TabIndex = 4;
            this.gridFljg.DoubleClick += new System.EventHandler(this.btnjg_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "XYMC";
            this.dataGridViewTextBoxColumn1.HeaderText = "协议名称";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "XYJG";
            this.dataGridViewTextBoxColumn2.HeaderText = "协议价格";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "SFMR";
            this.dataGridViewTextBoxColumn3.HeaderText = "是否默认";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 60;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "SFJF";
            this.dataGridViewTextBoxColumn4.HeaderText = "是否积分";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 60;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "SFYS";
            this.dataGridViewTextBoxColumn5.HeaderText = "是否夜审";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 60;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "SFXS";
            this.dataGridViewTextBoxColumn6.HeaderText = "是否显示";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 60;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "ZDF_SC";
            this.dataGridViewTextBoxColumn7.HeaderText = "钟点房时长";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 110;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ZDF_JS";
            this.dataGridViewTextBoxColumn8.HeaderText = "钟点房超时加收";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 130;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "CSMS";
            this.dataGridViewTextBoxColumn9.HeaderText = "超时模式";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "CSSC";
            this.dataGridViewTextBoxColumn10.HeaderText = "超时时长";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "BZSM";
            this.dataGridViewTextBoxColumn11.HeaderText = "备注说明";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // ColumnID
            // 
            this.ColumnID.DataPropertyName = "ID";
            this.ColumnID.HeaderText = "ID";
            this.ColumnID.Name = "ColumnID";
            this.ColumnID.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnjg);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(864, 28);
            this.panel3.TabIndex = 3;
            // 
            // btnjg
            // 
            this.btnjg.Location = new System.Drawing.Point(9, 3);
            this.btnjg.Name = "btnjg";
            this.btnjg.Size = new System.Drawing.Size(88, 23);
            this.btnjg.TabIndex = 5;
            this.btnjg.Text = "编辑价格";
            this.btnjg.UseVisualStyleBackColor = true;
            this.btnjg.Click += new System.EventHandler(this.btnjg_Click);
            // 
            // ColumnFJID
            // 
            this.ColumnFJID.DataPropertyName = "ID";
            this.ColumnFJID.HeaderText = "房间号";
            this.ColumnFJID.Name = "ColumnFJID";
            this.ColumnFJID.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "LC";
            this.dataGridViewTextBoxColumn13.HeaderText = "楼层";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 60;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "JC";
            this.dataGridViewTextBoxColumn14.HeaderText = "房屋类型";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "CX";
            this.dataGridViewTextBoxColumn15.HeaderText = "朝向";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 60;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "SKFJ";
            this.dataGridViewTextBoxColumn16.HeaderText = "散客房价";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "FJMS";
            this.dataGridViewTextBoxColumn17.HeaderText = "房间描述";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "CS";
            this.dataGridViewTextBoxColumn18.HeaderText = "床数";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 60;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "DHFJ";
            this.dataGridViewTextBoxColumn19.HeaderText = "电话分机";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "DHWX";
            this.dataGridViewTextBoxColumn20.HeaderText = "电话外线";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "LD";
            this.dataGridViewTextBoxColumn21.HeaderText = "楼栋";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "BZSM";
            this.dataGridViewTextBoxColumn22.HeaderText = "备注说明";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // kypzFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 490);
            this.Controls.Add(this.mainTab);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "kypzFrm";
            this.Text = "客房配置";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridzl)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.mainTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridFljg)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cobxld;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnldbj;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnzlbj;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnfjbj;
        private System.Windows.Forms.Button btnfjdel;
        private System.Windows.Forms.DataGridView gridFljggridfj;
        private System.Windows.Forms.DataGridView gridzl;
        private System.Windows.Forms.DataGridViewTextBoxColumn FJID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.Label lbfjs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cobxlc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnYJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.TabControl mainTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView gridFljg;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnjg;
        private System.Windows.Forms.DataGridView gridfj;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFJID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;

    }
}