﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;

namespace hotel.Win.BaseData
{
    /// <summary>
    /// 协议单位维护
    /// </summary>
    public partial class xydwMaintain : hotel.MyUserControl.MaintainBaseForm
    {
        int Id;
        hotelEntities _context;
        XYDW dataModel;
        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="isAdd"></param>
        public xydwMaintain(bool isAdd)
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = MyDataContext.GetDataContext;
            Id = 0;
            cobxxyfl.DataSource = XTCYDMDal.GetAllByLBDM(EnumXtcydmLB.XYDWFL.ToString());
            cobxxyfl.DisplayMember = "XMMC";
            cobxxyfl.ValueMember = "XMDM";
            if (isAdd)
            {
                base.IsEdit = false;
                base.DeleteButtonEnabled = false;
                base.StatusMessage = "数据处于增加状态";
                Add();
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public xydwMaintain(int id)
            : this(false)
        {
            BindData(id);
        }

        private void BindData(int id)
        {
            this.Id = id;
            dataModel = _context.XYDW.First(p => p.ID == id);
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());
            txtxydm.Text = dataModel.XYDM;
            txtdwmc.Text = dataModel.DWMC;
            chkgzf.Checked = dataModel.KFGZ == "是" ? true : false;
            cobxzt.Text = dataModel.ZT;
            txtgzje.Value = dataModel.GZJE;
            txtlxr.Text = dataModel.LXR;
            txtdz.Text = dataModel.DZ;
            txtdh.Text = dataModel.DH;
            cobxxyfl.Text = dataModel.XYFL;
            txtbzsm.Text = dataModel.BZSM;
            IsEdit = true;
            StatusMessage = "修改数据状态";
        }

        private bool Add()
        {
            txtxydm.Text = "";
            cobxzt.SelectedIndex = 0;
            dataModel = new XYDW { ID = Id, KFGZ = "是", GZJE = 0, ZT = "在用" };
            return true;
        }

        private bool Delete()
        {
            int cnt = dataModel.XYFJ.Count();
            if (cnt > 0)
            {
                MessageBox.Show("此分类下已经设置了" + cnt.ToString() + "个协议房价，不能删除！");
                return false;
            }
            _context.XYDW.Remove(dataModel);
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "协议单位[" + dataModel.DWMC + "]";
            logInfo.Save();
            #endregion
            return true;
        }

        private bool CheckData()
        {
            var info = ValidataHelper.StartVerify(txtxydm.Text)
            .IsNotNullOrEmpty("代码不能为空输入为空")
            .Max(16, "代码长度不能大于16个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            info = ValidataHelper.StartVerify(txtdwmc.Text)
           .IsNotNullOrEmpty("单位名称不能为空输入为空")
           .Max(60, "单位名称长度不能大于60个字符")
           .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }

            if (!IsEdit)
            {
                if (_context.XYDW.Where(p => p.XYDM == txtxydm.Text).Count() > 0)
                {
                    MessageBox.Show("协议代码" + txtxydm.Text + "已经存在，请查证！");
                    return false;
                }
                if (_context.XYDW.Where(p => p.DWMC == txtdwmc.Text).Count() > 0)
                {
                    MessageBox.Show("单位名称" + txtdwmc.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false;
            dataModel.BZSM = txtbzsm.Text;
            dataModel.DH = txtdh.Text;
            dataModel.DWMC = txtdwmc.Text;
            dataModel.XYDM = txtxydm.Text;
            dataModel.DZ = txtdz.Text;
            dataModel.GZJE = txtgzje.Value;
            dataModel.KFGZ = chkgzf.Checked ? "是" : "否";
            dataModel.LXR = txtlxr.Text;
            dataModel.XYFL = cobxxyfl.Text;
            dataModel.ZT = cobxzt.Text;

            if (IsEdit)
            {
                dataModel.XGRQ = DateTime.Now;
                dataModel.XGRY = UserLoginInfo.FullName;
            }
            else
            {
                dataModel.CJRQ = DateTime.Now;
                dataModel.CJRY = UserLoginInfo.FullName;
                _context.XYDW.Add(dataModel);
            }
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = (IsEdit ? "修改" : "增加") + "协议单位[" + dataModel.DWMC + "]";
            logInfo.Save();
            #endregion
            return true;
        }
    }
}
