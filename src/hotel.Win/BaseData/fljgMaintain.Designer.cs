﻿namespace hotel.Win.BaseData
{
    partial class fljgMaintain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label11 = new System.Windows.Forms.Label();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.chksfmr = new System.Windows.Forms.CheckBox();
            this.txtxyjg = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chksfjf = new System.Windows.Forms.CheckBox();
            this.chksfys = new System.Windows.Forms.CheckBox();
            this.chksfxs = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtzdf_sc = new System.Windows.Forms.NumericUpDown();
            this.gboxZdf = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtzdf_js = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cobxxymc = new System.Windows.Forms.ComboBox();
            this.cobxcsms = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtcssc = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtxyjg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtzdf_sc)).BeginInit();
            this.gboxZdf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtzdf_js)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcssc)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(-1, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 14);
            this.label11.TabIndex = 19;
            this.label11.Text = "房间协议名称：";
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(109, 227);
            this.txtbzsm.Multiline = true;
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(449, 36);
            this.txtbzsm.TabIndex = 20;
            // 
            // chksfmr
            // 
            this.chksfmr.AutoSize = true;
            this.chksfmr.Checked = true;
            this.chksfmr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chksfmr.Location = new System.Drawing.Point(114, 121);
            this.chksfmr.Name = "chksfmr";
            this.chksfmr.Size = new System.Drawing.Size(82, 18);
            this.chksfmr.TabIndex = 18;
            this.chksfmr.Text = "是否默认";
            this.chksfmr.UseVisualStyleBackColor = true;
            // 
            // txtxyjg
            // 
            this.txtxyjg.DecimalPlaces = 2;
            this.txtxyjg.Location = new System.Drawing.Point(109, 46);
            this.txtxyjg.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtxyjg.Name = "txtxyjg";
            this.txtxyjg.Size = new System.Drawing.Size(125, 23);
            this.txtxyjg.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 16;
            this.label9.Text = "备注说明：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "超时模式：";
            // 
            // chksfjf
            // 
            this.chksfjf.AutoSize = true;
            this.chksfjf.Checked = true;
            this.chksfjf.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chksfjf.Location = new System.Drawing.Point(202, 121);
            this.chksfjf.Name = "chksfjf";
            this.chksfjf.Size = new System.Drawing.Size(82, 18);
            this.chksfjf.TabIndex = 18;
            this.chksfjf.Text = "是否积分";
            this.chksfjf.UseVisualStyleBackColor = true;
            // 
            // chksfys
            // 
            this.chksfys.AutoSize = true;
            this.chksfys.Checked = true;
            this.chksfys.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chksfys.Location = new System.Drawing.Point(289, 121);
            this.chksfys.Name = "chksfys";
            this.chksfys.Size = new System.Drawing.Size(82, 18);
            this.chksfys.TabIndex = 21;
            this.chksfys.Text = "是否夜审";
            this.chksfys.UseVisualStyleBackColor = true;
            // 
            // chksfxs
            // 
            this.chksfxs.AutoSize = true;
            this.chksfxs.Checked = true;
            this.chksfxs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chksfxs.Location = new System.Drawing.Point(377, 121);
            this.chksfxs.Name = "chksfxs";
            this.chksfxs.Size = new System.Drawing.Size(110, 18);
            this.chksfxs.TabIndex = 21;
            this.chksfxs.Text = "是否前台显示";
            this.chksfxs.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "钟点房时长：";
            // 
            // txtzdf_sc
            // 
            this.txtzdf_sc.DecimalPlaces = 2;
            this.txtzdf_sc.Location = new System.Drawing.Point(117, 26);
            this.txtzdf_sc.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtzdf_sc.Name = "txtzdf_sc";
            this.txtzdf_sc.Size = new System.Drawing.Size(82, 23);
            this.txtzdf_sc.TabIndex = 14;
            // 
            // gboxZdf
            // 
            this.gboxZdf.Controls.Add(this.label4);
            this.gboxZdf.Controls.Add(this.txtzdf_js);
            this.gboxZdf.Controls.Add(this.label2);
            this.gboxZdf.Controls.Add(this.label3);
            this.gboxZdf.Controls.Add(this.txtzdf_sc);
            this.gboxZdf.Controls.Add(this.label1);
            this.gboxZdf.Location = new System.Drawing.Point(29, 155);
            this.gboxZdf.Name = "gboxZdf";
            this.gboxZdf.Size = new System.Drawing.Size(529, 66);
            this.gboxZdf.TabIndex = 22;
            this.gboxZdf.TabStop = false;
            this.gboxZdf.Text = "钟点房设置";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(483, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 14);
            this.label4.TabIndex = 18;
            this.label4.Text = "(元)";
            // 
            // txtzdf_js
            // 
            this.txtzdf_js.Location = new System.Drawing.Point(398, 24);
            this.txtzdf_js.Maximum = new decimal(new int[] {
            9999990,
            0,
            0,
            0});
            this.txtzdf_js.Name = "txtzdf_js";
            this.txtzdf_js.Size = new System.Drawing.Size(82, 23);
            this.txtzdf_js.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(202, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 18;
            this.label2.Text = "(小时)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(273, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "超时每小时加收：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 14);
            this.label6.TabIndex = 17;
            this.label6.Text = "协议价格：";
            // 
            // cobxxymc
            // 
            this.cobxxymc.FormattingEnabled = true;
            this.cobxxymc.Location = new System.Drawing.Point(109, 14);
            this.cobxxymc.Name = "cobxxymc";
            this.cobxxymc.Size = new System.Drawing.Size(153, 21);
            this.cobxxymc.TabIndex = 23;
            this.cobxxymc.SelectedIndexChanged += new System.EventHandler(this.cobxxymc_SelectedIndexChanged);
            this.cobxxymc.Leave += new System.EventHandler(this.cobxxymc_Leave);
            // 
            // cobxcsms
            // 
            this.cobxcsms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxcsms.FormattingEnabled = true;
            this.cobxcsms.Items.AddRange(new object[] {
            "半日租",
            "小时"});
            this.cobxcsms.Location = new System.Drawing.Point(109, 78);
            this.cobxcsms.Name = "cobxcsms";
            this.cobxcsms.Size = new System.Drawing.Size(153, 21);
            this.cobxcsms.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(280, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 14);
            this.label7.TabIndex = 17;
            this.label7.Text = "超时时长：";
            // 
            // txtcssc
            // 
            this.txtcssc.Location = new System.Drawing.Point(363, 78);
            this.txtcssc.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.txtcssc.Name = "txtcssc";
            this.txtcssc.Size = new System.Drawing.Size(96, 23);
            this.txtcssc.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(465, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 25;
            this.label8.Text = "(分钟)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(280, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(293, 12);
            this.label10.TabIndex = 26;
            this.label10.Text = "名称中包括【钟点房】三个字时，可以使用钟点房配置";
            // 
            // fljgMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(595, 345);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cobxcsms);
            this.Controls.Add(this.cobxxymc);
            this.Controls.Add(this.gboxZdf);
            this.Controls.Add(this.chksfxs);
            this.Controls.Add(this.chksfys);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtbzsm);
            this.Controls.Add(this.chksfjf);
            this.Controls.Add(this.chksfmr);
            this.Controls.Add(this.txtcssc);
            this.Controls.Add(this.txtxyjg);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Name = "fljgMaintain";
            this.Text = "房类价格维护";
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.txtxyjg, 0);
            this.Controls.SetChildIndex(this.txtcssc, 0);
            this.Controls.SetChildIndex(this.chksfmr, 0);
            this.Controls.SetChildIndex(this.chksfjf, 0);
            this.Controls.SetChildIndex(this.txtbzsm, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.chksfys, 0);
            this.Controls.SetChildIndex(this.chksfxs, 0);
            this.Controls.SetChildIndex(this.gboxZdf, 0);
            this.Controls.SetChildIndex(this.cobxxymc, 0);
            this.Controls.SetChildIndex(this.cobxcsms, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            ((System.ComponentModel.ISupportInitialize)(this.txtxyjg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtzdf_sc)).EndInit();
            this.gboxZdf.ResumeLayout(false);
            this.gboxZdf.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtzdf_js)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcssc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.CheckBox chksfmr;
        private System.Windows.Forms.NumericUpDown txtxyjg;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chksfjf;
        private System.Windows.Forms.CheckBox chksfys;
        private System.Windows.Forms.CheckBox chksfxs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtzdf_sc;
        private System.Windows.Forms.GroupBox gboxZdf;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown txtzdf_js;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cobxxymc;
        private System.Windows.Forms.ComboBox cobxcsms;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown txtcssc;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
    }
}
