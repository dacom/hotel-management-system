﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;
namespace hotel.Win.BaseData
{
    public partial class xtcydmMaintain : hotel.MyUserControl.MaintainBaseForm
    {
        xtcydmLB diclb = new xtcydmLB();
        string LBDM;
        int Id;
        hotelEntities _context;
        XTCYDM dataModel;

        private xtcydmMaintain()
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = new hotelEntities();
            Id = 0;
        }
        /// <summary>
        /// 用于增加
        /// </summary>
        /// <param name="lbdm"></param>
        public xtcydmMaintain(EnumXtcydmLB lbdm)
            : this()
        {            
                base.IsEdit = false;
                base.DeleteButtonEnabled = false;
                LBDM = lbdm.ToString();
                txtlb.Text = diclb[lbdm];
                Add();                
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public xtcydmMaintain(int id)
            : this()
        {
            BindData(id);
        }

        private void BindData(int id)
        {
            this.Id = id;
            dataModel = _context.XTCYDM.First(p => p.ID == id);
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());
            LBDM = dataModel.LBDM;
            txtlb.Text = dataModel.LBMC;
            txtdm.Text = dataModel.XMDM;
            txtmc.Text = dataModel.XMMC;
            numxh.Value =(decimal)dataModel.XH;
            chkmr.Checked = dataModel.MRBZ == "是";
            IsEdit = true;
            StatusMessage = "修改数据状态";

        }

        private bool Add()
        {
            var maxxh = (from q in _context.XTCYDM
                         where q.LBDM == LBDM
                         select q.XH).Count();
            numxh.Value = maxxh == 0 ? 1 : (decimal)maxxh + 1;
            dataModel = new XTCYDM { ID = Id };
            chkmr.Checked = false;
            txtdm.Text = "";
            txtmc.Text = "";
            return true;
        }
        private bool Delete()
        {
            if (_context.ROOM.Where(p => p.LD == dataModel.XMMC).Count() > 0)
            {
                MessageBox.Show("此楼栋号下已经设置了房间，不能删除！");
                return false;
            }
             
            _context.XTCYDM.Remove(dataModel);
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "删除系统常用代码,[" + dataModel.XMMC + "]";
            logInfo.Save();
            #endregion
            return true;
        }

        private bool CheckData()
        {
            var info = ValidataHelper.StartVerify(txtdm.Text)
            .IsNotNullOrEmpty("项目代码不能为空输入为空")
            .Max(16, "项目代码长度不能大于16个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
             info = ValidataHelper.StartVerify(txtmc.Text)
            .IsNotNullOrEmpty("项目名称不能为空输入为空")
            .Max(16, "项目名称长度不能大于64个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            if (!IsEdit)
            {
                if (_context.XTCYDM.Where(p => p.LBDM == LBDM && p.XMDM == txtdm.Text).Count() > 0)
                {
                    MessageBox.Show("项目代码" + txtdm.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false ;
            dataModel.XMMC = txtmc.Text;
            dataModel.XMDM = txtdm.Text;
            dataModel.XH = (int)numxh.Value;
            dataModel.MRBZ = "否"; //后边特殊处理
            if (IsEdit)
            {                
                dataModel.XGRQ = DateTime.Now;
                dataModel.XGRY = UserLoginInfo.FullName;                
            }
            else
            {
                dataModel.CJRQ = DateTime.Now;
                dataModel.CJRY = UserLoginInfo.FullName;
                dataModel.LBDM = LBDM.ToString();
                dataModel.LBMC = txtlb.Text;
                //_context.Attach(dataModel);
                _context.XTCYDM.Add(dataModel);
                #region 日志纪录
                CcrzInfo logInfo = null;
                logInfo = new CcrzInfo();
                logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
                logInfo.Type = LogTypeEnum.一般操作;
                logInfo.Message = (IsEdit ? "修改" : "增加") + "常用代码[" + dataModel.XMMC + "]";
                logInfo.Save();
                #endregion
            }
            _context.SaveChanges();
            //默认值只能有一个
            if (chkmr.Checked)
            {
                foreach (var item in _context.XTCYDM.Where(p=>p.LBDM == LBDM))
                {
                    if (item.XMDM == txtdm.Text)
                        item.MRBZ = "是";
                    else
                        item.MRBZ = "否";
                }
                _context.SaveChanges();
            }
           
            return true;
        }
    }

    /// <summary>
    /// 系统常用代码类别
    /// </summary>
    public sealed class xtcydmLB:Dictionary<EnumXtcydmLB,string> 
    {
        public xtcydmLB()
        {
            Add(EnumXtcydmLB.LDH, "楼栋号");
            Add(EnumXtcydmLB.ZFFS, "支付方式");
            Add(EnumXtcydmLB.ZJLX, "证件类型");
            Add(EnumXtcydmLB.XYDWFL, "协议单位分类");
        }        
    }

}
