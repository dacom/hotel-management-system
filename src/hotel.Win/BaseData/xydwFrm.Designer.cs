﻿namespace hotel.Win.BaseData
{
    partial class xydwFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gridfj = new System.Windows.Forms.DataGridView();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FJID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnfjbj = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.griddw = new System.Windows.Forms.DataGridView();
            this.XYDM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DWID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btndwbj = new System.Windows.Forms.Button();
            this.btncopy = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.griddw)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gridfj);
            this.groupBox3.Controls.Add(this.panel2);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 326);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(813, 223);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "协议房间";
            // 
            // gridfj
            // 
            this.gridfj.AllowUserToAddRows = false;
            this.gridfj.AllowUserToDeleteRows = false;
            this.gridfj.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridfj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column10,
            this.Column14,
            this.Column9,
            this.Column13,
            this.Column7,
            this.Column16,
            this.Column1,
            this.FJID});
            this.gridfj.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridfj.Location = new System.Drawing.Point(3, 47);
            this.gridfj.Name = "gridfj";
            this.gridfj.ReadOnly = true;
            this.gridfj.RowHeadersWidth = 25;
            this.gridfj.RowTemplate.Height = 23;
            this.gridfj.Size = new System.Drawing.Size(807, 173);
            this.gridfj.TabIndex = 2;
            this.gridfj.DoubleClick += new System.EventHandler(this.btnfjbj_Click);
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "FLJC";
            this.Column8.HeaderText = "房类";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 60;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "FJ";
            this.Column10.HeaderText = "房价";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "YJ";
            this.Column14.HeaderText = "佣金";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 90;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "BZSM";
            this.Column9.HeaderText = "备注说明";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "XGRY";
            this.Column13.HeaderText = "修改人员";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "XGRQ";
            this.Column7.HeaderText = "修改日期";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "CJRY";
            this.Column16.HeaderText = "创建人员";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "CJRQ";
            this.Column1.HeaderText = "创建日期";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // FJID
            // 
            this.FJID.DataPropertyName = "ID";
            this.FJID.HeaderText = "ID";
            this.FJID.Name = "FJID";
            this.FJID.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btncopy);
            this.panel2.Controls.Add(this.btnfjbj);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(807, 28);
            this.panel2.TabIndex = 1;
            // 
            // btnfjbj
            // 
            this.btnfjbj.Location = new System.Drawing.Point(9, 3);
            this.btnfjbj.Name = "btnfjbj";
            this.btnfjbj.Size = new System.Drawing.Size(88, 23);
            this.btnfjbj.TabIndex = 5;
            this.btnfjbj.Text = "编辑房价";
            this.btnfjbj.UseVisualStyleBackColor = true;
            this.btnfjbj.Click += new System.EventHandler(this.btnfjbj_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.griddw);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(813, 326);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "协议单位";
            // 
            // griddw
            // 
            this.griddw.AllowUserToAddRows = false;
            this.griddw.AllowUserToDeleteRows = false;
            this.griddw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.griddw.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.XYDM,
            this.Column4,
            this.Column5,
            this.Column2,
            this.Column3,
            this.Column6,
            this.Column12,
            this.Column17,
            this.Column18,
            this.Column19,
            this.DWID});
            this.griddw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griddw.Location = new System.Drawing.Point(3, 47);
            this.griddw.Name = "griddw";
            this.griddw.ReadOnly = true;
            this.griddw.RowHeadersWidth = 25;
            this.griddw.RowTemplate.Height = 23;
            this.griddw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.griddw.Size = new System.Drawing.Size(807, 276);
            this.griddw.TabIndex = 2;
            this.griddw.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.griddw_CellClick);
            this.griddw.DoubleClick += new System.EventHandler(this.btndwbj_Click);
            // 
            // XYDM
            // 
            this.XYDM.DataPropertyName = "XYDM";
            this.XYDM.HeaderText = "协议代码";
            this.XYDM.Name = "XYDM";
            this.XYDM.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "DWMC";
            this.Column4.HeaderText = "单位名称";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 110;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "LXR";
            this.Column5.HeaderText = "联系人";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "DH";
            this.Column2.HeaderText = "电话";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "DZ";
            this.Column3.HeaderText = "地址";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "KFGZ";
            this.Column6.HeaderText = "可否挂账";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "GZJE";
            this.Column12.HeaderText = "挂账金额";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "ZT";
            this.Column17.HeaderText = "状态";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "XYFL";
            this.Column18.HeaderText = "协议分类";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "BZSM";
            this.Column19.HeaderText = "备注说明";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // DWID
            // 
            this.DWID.DataPropertyName = "ID";
            this.DWID.HeaderText = "协议ID";
            this.DWID.Name = "DWID";
            this.DWID.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btndwbj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(807, 28);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(710, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "关  闭";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btndwbj
            // 
            this.btndwbj.Location = new System.Drawing.Point(9, 3);
            this.btndwbj.Name = "btndwbj";
            this.btndwbj.Size = new System.Drawing.Size(88, 23);
            this.btndwbj.TabIndex = 5;
            this.btndwbj.Text = "编辑单位";
            this.btndwbj.UseVisualStyleBackColor = true;
            this.btndwbj.Click += new System.EventHandler(this.btndwbj_Click);
            // 
            // btncopy
            // 
            this.btncopy.Location = new System.Drawing.Point(120, 3);
            this.btncopy.Name = "btncopy";
            this.btncopy.Size = new System.Drawing.Size(149, 23);
            this.btncopy.TabIndex = 6;
            this.btncopy.Text = "复制其他单位...";
            this.btncopy.UseVisualStyleBackColor = true;
            this.btncopy.Click += new System.EventHandler(this.btncopy_Click);
            // 
            // xydwFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 549);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "xydwFrm";
            this.Text = "协议单位管理";
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridfj)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.griddw)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView gridfj;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnfjbj;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView griddw;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btndwbj;
        private System.Windows.Forms.DataGridViewTextBoxColumn XYDM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn DWID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn FJID;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btncopy;
    }
}