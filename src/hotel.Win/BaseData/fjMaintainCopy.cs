﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;
using hotel.MyUserControl;
namespace hotel.Win.BaseData
{
    public partial class fjMaintainCopy : Form
    {
        private ROOM sourceRoom;
        hotelEntities _context;
        public fjMaintainCopy(string sourceRoomId)
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            sourceRoom = _context.ROOM.FirstOrDefault(p => p.ID == sourceRoomId);
            if (sourceRoom == null)
            {
                btnok.Enabled = false;
                txtsl.Enabled = false;
                return;
            }
            lbysroom.Text = sourceRoomId;
            txtsl_ValueChanged(txtsl, null);
        }

        private void txtsl_ValueChanged(object sender, EventArgs e)
        {
            if (lbysroom.Text.Length < 3)
            {
                MessageBox.Show("原始房间号不能小于3");
                btnok.Enabled = false;
                return;
            }
            string str = lbysroom.Text.Substring(lbysroom.Text.Length - 2, 2);
            if (!hotel.Common.Utility.IsInt32(str))
            {
                MessageBox.Show("原始房间号的后两位不是有效的数字");
                btnok.Enabled = false;
                return;
            }
            int sxh = Convert.ToInt32(str);
            string roomprefix = lbysroom.Text.Substring(0, lbysroom.Text.Length - 2);
            lbnextroom.Text = roomprefix + (sxh + 1).ToString().PadLeft(2, '0');
            lblastroom.Text = roomprefix + (sxh + (int)txtsl.Value).ToString().PadLeft(2, '0');
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("你确定要复制房间？", hotel.Common.SysConsts.SysWarningCaption, MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;
            string str = lbysroom.Text.Substring(lbysroom.Text.Length - 2, 2);
            int sxh = Convert.ToInt32(str);
            string roomprefix = lbysroom.Text.Substring(0, lbysroom.Text.Length - 2);
            lbnextroom.Text = (sxh + 1).ToString();
            int count = 0;           
            ROOM_TYPE room_type = sourceRoom.ROOM_TYPE;
            for (int i = 1; i <= (int)txtsl.Value; i++)
            {
                string Id = roomprefix + (sxh + i).ToString().PadLeft(2, '0');
                if (_context.ROOM.Count(p => p.ID == Id) > 0)
                    continue;

                var dataModel = new ROOM { ID = Id, LC = 1 };
                dataModel.BZSM = sourceRoom.BZSM;
                dataModel.CS = sourceRoom.CS;
                dataModel.CX = sourceRoom.CX;
                dataModel.DHFJ = sourceRoom.DHFJ;
                dataModel.DHWX = sourceRoom.DHWX;
                dataModel.FJMS = sourceRoom.FJMS;
                dataModel.LC = sourceRoom.LC;
                dataModel.LD = sourceRoom.LD;
                dataModel.SFKF = sourceRoom.SFKF;
                dataModel.SKFJ = sourceRoom.SKFJ;
                dataModel.ZT = RoomStstusCnName.KongJing;
                dataModel.ROOM_TYPE = room_type;
                _context.ROOM.Add(dataModel);
                count++;
            }
            _context.SaveChanges();
            MessageBox.Show("房间复制完毕，共创建" + count.ToString() + "个房间！");
            
            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.增加数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "同楼层房间复制";
            logInfo.Save();
            #endregion
            this.DialogResult = DialogResult.OK;
        }

        private void btncancle_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
