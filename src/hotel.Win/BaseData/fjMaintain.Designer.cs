﻿namespace hotel.Win.BaseData
{
    partial class fjMaintain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtfh = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtld = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cobxzl = new System.Windows.Forms.ComboBox();
            this.txtskfj = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.chksffk = new System.Windows.Forms.CheckBox();
            this.txtfjms = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtdhfj = new System.Windows.Forms.TextBox();
            this.txtdhwx = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtcs = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.txtcx = new System.Windows.Forms.TextBox();
            this.btncopy = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtlc = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.txtskfj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcs)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtlc)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 6;
            this.label1.Text = "房号：";
            // 
            // txtfh
            // 
            this.txtfh.Location = new System.Drawing.Point(98, 20);
            this.txtfh.Name = "txtfh";
            this.txtfh.Size = new System.Drawing.Size(77, 23);
            this.txtfh.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(249, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "楼层号：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(249, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "楼栋号：";
            // 
            // txtld
            // 
            this.txtld.Location = new System.Drawing.Point(317, 59);
            this.txtld.Name = "txtld";
            this.txtld.ReadOnly = true;
            this.txtld.Size = new System.Drawing.Size(125, 23);
            this.txtld.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 6;
            this.label4.Text = "房屋种类：";
            // 
            // cobxzl
            // 
            this.cobxzl.FormattingEnabled = true;
            this.cobxzl.Location = new System.Drawing.Point(98, 59);
            this.cobxzl.Name = "cobxzl";
            this.cobxzl.Size = new System.Drawing.Size(125, 21);
            this.cobxzl.TabIndex = 3;
            this.cobxzl.SelectedIndexChanged += new System.EventHandler(this.cobxzl_SelectedIndexChanged);
            // 
            // txtskfj
            // 
            this.txtskfj.DecimalPlaces = 2;
            this.txtskfj.Location = new System.Drawing.Point(98, 96);
            this.txtskfj.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtskfj.Name = "txtskfj";
            this.txtskfj.Size = new System.Drawing.Size(125, 23);
            this.txtskfj.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 9;
            this.label5.Text = "散客房价：";
            // 
            // chksffk
            // 
            this.chksffk.AutoSize = true;
            this.chksffk.Checked = true;
            this.chksffk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chksffk.Location = new System.Drawing.Point(317, 141);
            this.chksffk.Name = "chksffk";
            this.chksffk.Size = new System.Drawing.Size(82, 18);
            this.chksffk.TabIndex = 9;
            this.chksffk.Text = "是否客房";
            this.chksffk.UseVisualStyleBackColor = true;
            // 
            // txtfjms
            // 
            this.txtfjms.Location = new System.Drawing.Point(98, 208);
            this.txtfjms.Multiline = true;
            this.txtfjms.Name = "txtfjms";
            this.txtfjms.Size = new System.Drawing.Size(344, 36);
            this.txtfjms.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 219);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "房间描述：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 14);
            this.label7.TabIndex = 9;
            this.label7.Text = "电话分机：";
            // 
            // txtdhfj
            // 
            this.txtdhfj.Location = new System.Drawing.Point(98, 174);
            this.txtdhfj.Name = "txtdhfj";
            this.txtdhfj.Size = new System.Drawing.Size(125, 23);
            this.txtdhfj.TabIndex = 10;
            // 
            // txtdhwx
            // 
            this.txtdhwx.Location = new System.Drawing.Point(317, 174);
            this.txtdhwx.Name = "txtdhwx";
            this.txtdhwx.Size = new System.Drawing.Size(125, 23);
            this.txtdhwx.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(235, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 9;
            this.label8.Text = "电话外线：";
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(98, 259);
            this.txtbzsm.Multiline = true;
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(344, 36);
            this.txtbzsm.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 258);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 9;
            this.label9.Text = "备注说明：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(263, 102);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 14);
            this.label10.TabIndex = 9;
            this.label10.Text = "床数：";
            // 
            // txtcs
            // 
            this.txtcs.Location = new System.Drawing.Point(317, 96);
            this.txtcs.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtcs.Name = "txtcs";
            this.txtcs.Size = new System.Drawing.Size(125, 23);
            this.txtcs.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 141);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 12;
            this.label11.Text = "房价朝向：";
            // 
            // txtcx
            // 
            this.txtcx.Location = new System.Drawing.Point(98, 135);
            this.txtcx.Name = "txtcx";
            this.txtcx.Size = new System.Drawing.Size(125, 23);
            this.txtcx.TabIndex = 8;
            // 
            // btncopy
            // 
            this.btncopy.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btncopy.Location = new System.Drawing.Point(181, 21);
            this.btncopy.Name = "btncopy";
            this.btncopy.Size = new System.Drawing.Size(61, 23);
            this.btncopy.TabIndex = 14;
            this.btncopy.Text = "复制...";
            this.btncopy.UseVisualStyleBackColor = true;
            this.btncopy.Click += new System.EventHandler(this.btncopy_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtlc);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btncopy);
            this.groupBox1.Controls.Add(this.txtfjms);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtdhfj);
            this.groupBox1.Controls.Add(this.txtcx);
            this.groupBox1.Controls.Add(this.txtbzsm);
            this.groupBox1.Controls.Add(this.chksffk);
            this.groupBox1.Controls.Add(this.txtdhwx);
            this.groupBox1.Controls.Add(this.txtcs);
            this.groupBox1.Controls.Add(this.txtskfj);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtld);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtfh);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cobxzl);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 303);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // txtlc
            // 
            this.txtlc.Location = new System.Drawing.Point(317, 21);
            this.txtlc.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.txtlc.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.txtlc.Name = "txtlc";
            this.txtlc.Size = new System.Drawing.Size(125, 23);
            this.txtlc.TabIndex = 15;
            // 
            // fjMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(467, 378);
            this.Controls.Add(this.groupBox1);
            this.Name = "fjMaintain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "房间数据维护";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.txtskfj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcs)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtlc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtfh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtld;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cobxzl;
        private System.Windows.Forms.NumericUpDown txtskfj;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chksffk;
        private System.Windows.Forms.TextBox txtfjms;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtdhfj;
        private System.Windows.Forms.TextBox txtdhwx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown txtcs;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtcx;
        private System.Windows.Forms.Button btncopy;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown txtlc;
    }
}
