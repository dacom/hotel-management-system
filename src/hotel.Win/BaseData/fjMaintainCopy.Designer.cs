﻿namespace hotel.Win.BaseData
{
    partial class fjMaintainCopy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.txtsl = new System.Windows.Forms.NumericUpDown();
            this.lbysroom = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbnextroom = new System.Windows.Forms.Label();
            this.btncancle = new System.Windows.Forms.Button();
            this.lblastroom = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtsl)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "原始房间号：";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(70, 178);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(68, 28);
            this.btnok.TabIndex = 1;
            this.btnok.Text = "确定";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // txtsl
            // 
            this.txtsl.Location = new System.Drawing.Point(139, 49);
            this.txtsl.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.txtsl.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtsl.Name = "txtsl";
            this.txtsl.Size = new System.Drawing.Size(67, 23);
            this.txtsl.TabIndex = 2;
            this.txtsl.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.txtsl.ValueChanged += new System.EventHandler(this.txtsl_ValueChanged);
            // 
            // lbysroom
            // 
            this.lbysroom.AutoSize = true;
            this.lbysroom.Location = new System.Drawing.Point(139, 20);
            this.lbysroom.Name = "lbysroom";
            this.lbysroom.Size = new System.Drawing.Size(35, 14);
            this.lbysroom.TabIndex = 3;
            this.lbysroom.Text = "8888";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "复制房间数量：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "下一房间示例：";
            // 
            // lbnextroom
            // 
            this.lbnextroom.AutoSize = true;
            this.lbnextroom.Location = new System.Drawing.Point(139, 87);
            this.lbnextroom.Name = "lbnextroom";
            this.lbnextroom.Size = new System.Drawing.Size(35, 14);
            this.lbnextroom.TabIndex = 5;
            this.lbnextroom.Text = "8889";
            // 
            // btncancle
            // 
            this.btncancle.Location = new System.Drawing.Point(151, 178);
            this.btncancle.Name = "btncancle";
            this.btncancle.Size = new System.Drawing.Size(68, 28);
            this.btncancle.TabIndex = 6;
            this.btncancle.Text = "取消";
            this.btncancle.UseVisualStyleBackColor = true;
            this.btncancle.Click += new System.EventHandler(this.btncancle_Click);
            // 
            // lblastroom
            // 
            this.lblastroom.AutoSize = true;
            this.lblastroom.Location = new System.Drawing.Point(139, 116);
            this.lblastroom.Name = "lblastroom";
            this.lblastroom.Size = new System.Drawing.Size(35, 14);
            this.lblastroom.TabIndex = 8;
            this.lblastroom.Text = "8889";
            this.lblastroom.Click += new System.EventHandler(this.txtsl_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 14);
            this.label6.TabIndex = 7;
            this.label6.Text = "最后房间示例：";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(26, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(263, 19);
            this.label4.TabIndex = 9;
            this.label4.Text = "提示：只适合相同楼层，相同的房间会忽略";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtsl);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btncancle);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbysroom);
            this.groupBox1.Controls.Add(this.lblastroom);
            this.groupBox1.Controls.Add(this.btnok);
            this.groupBox1.Controls.Add(this.lbnextroom);
            this.groupBox1.Location = new System.Drawing.Point(4, -4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(299, 214);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // fjMaintainCopy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 217);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "fjMaintainCopy";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "房间复制";
            ((System.ComponentModel.ISupportInitialize)(this.txtsl)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.NumericUpDown txtsl;
        private System.Windows.Forms.Label lbysroom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbnextroom;
        private System.Windows.Forms.Button btncancle;
        private System.Windows.Forms.Label lblastroom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}