﻿namespace hotel.Win
{
    partial class MainControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainControl));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanelFT = new System.Windows.Forms.FlowLayoutPanel();
            this.lbftall = new System.Windows.Forms.Label();
            this.lbftall_cnt = new System.Windows.Forms.Label();
            this.lbkjf = new System.Windows.Forms.Label();
            this.lbkjf_cnt = new System.Windows.Forms.Label();
            this.lbydf = new System.Windows.Forms.Label();
            this.lbydf_cnt = new System.Windows.Forms.Label();
            this.lbzf = new System.Windows.Forms.Label();
            this.lbzf_cnt = new System.Windows.Forms.Label();
            this.lbkzf = new System.Windows.Forms.Label();
            this.lbkzf_cnt = new System.Windows.Forms.Label();
            this.lbwxf = new System.Windows.Forms.Label();
            this.lbwxf_cnt = new System.Windows.Forms.Label();
            this.lbskf = new System.Windows.Forms.Label();
            this.lbtdf = new System.Windows.Forms.Label();
            this.lbzdf = new System.Windows.Forms.Label();
            this.lbbmf = new System.Windows.Forms.Label();
            this.lbldf = new System.Windows.Forms.Label();
            this.lbcbf = new System.Windows.Forms.Label();
            this.lbzyf = new System.Windows.Forms.Label();
            this.lbmff = new System.Windows.Forms.Label();
            this.lbqlf = new System.Windows.Forms.Label();
            this.lbsdf = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbrzl = new System.Windows.Forms.Label();
            this.rbtnyd = new System.Windows.Forms.RadioButton();
            this.rbtnkf = new System.Windows.Forms.RadioButton();
            this.rbtnqt = new System.Windows.Forms.RadioButton();
            this.cobxzl = new System.Windows.Forms.ComboBox();
            this.txtfjh = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuRoom = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tmikryd = new System.Windows.Forms.ToolStripMenuItem();
            this.tmikrtj = new System.Windows.Forms.ToolStripMenuItem();
            this.tmi_krxx = new System.Windows.Forms.ToolStripMenuItem();
            this.tmikrhf = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tmizhika = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiCkfk = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tmixtyj = new System.Windows.Forms.ToolStripMenuItem();
            this.tmixfrd = new System.Windows.Forms.ToolStripMenuItem();
            this.tmijztf = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tmihbzt = new System.Windows.Forms.ToolStripMenuItem();
            this.tmicfzd = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tmifjxg = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiccrz = new System.Windows.Forms.ToolStripMenuItem();
            this.groupProgress = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lbmsg = new System.Windows.Forms.Label();
            this.plnTootip = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbtooltipfjid = new System.Windows.Forms.Label();
            this.btntooltipClose = new System.Windows.Forms.Button();
            this.gboxToolTip_kr = new System.Windows.Forms.GroupBox();
            this.lbtoolTip_ye = new System.Windows.Forms.Label();
            this.lbtoolTip_xf = new System.Windows.Forms.Label();
            this.lbtoolTip_zf = new System.Windows.Forms.Label();
            this.lbtoolTip_ldrq = new System.Windows.Forms.Label();
            this.lbtoolTip_ddrq = new System.Windows.Forms.Label();
            this.lbtoolTip_rs = new System.Windows.Forms.Label();
            this.lbtoolTip_xm = new System.Windows.Forms.Label();
            this.gboxToolTip_fj = new System.Windows.Forms.GroupBox();
            this.lbtoolTip_ms = new System.Windows.Forms.Label();
            this.lbtoolTip_fl = new System.Windows.Forms.Label();
            this.lbtoolTip_cs = new System.Windows.Forms.Label();
            this.lbtoolTip_fz = new System.Windows.Forms.Label();
            this.lbtoolTip_cx = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnKeycard = new System.Windows.Forms.Button();
            this.btnrefresh = new System.Windows.Forms.Button();
            this.btnfzf = new System.Windows.Forms.Button();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.chksjft = new System.Windows.Forms.CheckBox();
            this.chkpop = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.contextMenuStripFzf = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tmi_xfrd = new System.Windows.Forms.ToolStripMenuItem();
            this.tmp_hysgl = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelFT.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.contextMenuRoom.SuspendLayout();
            this.groupProgress.SuspendLayout();
            this.plnTootip.SuspendLayout();
            this.panel4.SuspendLayout();
            this.gboxToolTip_kr.SuspendLayout();
            this.gboxToolTip_fj.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.contextMenuStripFzf.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(833, 646);
            this.tabMain.TabIndex = 4;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.flowLayoutPanelFT);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(833, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(150, 682);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // flowLayoutPanelFT
            // 
            this.flowLayoutPanelFT.Controls.Add(this.lbftall);
            this.flowLayoutPanelFT.Controls.Add(this.lbftall_cnt);
            this.flowLayoutPanelFT.Controls.Add(this.lbkjf);
            this.flowLayoutPanelFT.Controls.Add(this.lbkjf_cnt);
            this.flowLayoutPanelFT.Controls.Add(this.lbydf);
            this.flowLayoutPanelFT.Controls.Add(this.lbydf_cnt);
            this.flowLayoutPanelFT.Controls.Add(this.lbzf);
            this.flowLayoutPanelFT.Controls.Add(this.lbzf_cnt);
            this.flowLayoutPanelFT.Controls.Add(this.lbkzf);
            this.flowLayoutPanelFT.Controls.Add(this.lbkzf_cnt);
            this.flowLayoutPanelFT.Controls.Add(this.lbwxf);
            this.flowLayoutPanelFT.Controls.Add(this.lbwxf_cnt);
            this.flowLayoutPanelFT.Controls.Add(this.lbskf);
            this.flowLayoutPanelFT.Controls.Add(this.lbtdf);
            this.flowLayoutPanelFT.Controls.Add(this.lbzdf);
            this.flowLayoutPanelFT.Controls.Add(this.lbbmf);
            this.flowLayoutPanelFT.Controls.Add(this.lbldf);
            this.flowLayoutPanelFT.Controls.Add(this.lbcbf);
            this.flowLayoutPanelFT.Controls.Add(this.lbzyf);
            this.flowLayoutPanelFT.Controls.Add(this.lbmff);
            this.flowLayoutPanelFT.Controls.Add(this.lbqlf);
            this.flowLayoutPanelFT.Controls.Add(this.lbsdf);
            this.flowLayoutPanelFT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelFT.Location = new System.Drawing.Point(0, 147);
            this.flowLayoutPanelFT.Name = "flowLayoutPanelFT";
            this.flowLayoutPanelFT.Size = new System.Drawing.Size(150, 535);
            this.flowLayoutPanelFT.TabIndex = 16;
            // 
            // lbftall
            // 
            this.lbftall.BackColor = System.Drawing.Color.Aqua;
            this.lbftall.ImageIndex = 0;
            this.lbftall.Location = new System.Drawing.Point(3, 3);
            this.lbftall.Margin = new System.Windows.Forms.Padding(3);
            this.lbftall.Name = "lbftall";
            this.lbftall.Size = new System.Drawing.Size(80, 25);
            this.lbftall.TabIndex = 15;
            this.lbftall.Text = "全部";
            this.lbftall.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbftall.Click += new System.EventHandler(this.lbftall_Click);
            // 
            // lbftall_cnt
            // 
            this.lbftall_cnt.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbftall_cnt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbftall_cnt.ImageIndex = 0;
            this.lbftall_cnt.Location = new System.Drawing.Point(89, 3);
            this.lbftall_cnt.Margin = new System.Windows.Forms.Padding(3);
            this.lbftall_cnt.Name = "lbftall_cnt";
            this.lbftall_cnt.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.lbftall_cnt.Size = new System.Drawing.Size(49, 25);
            this.lbftall_cnt.TabIndex = 15;
            this.lbftall_cnt.Text = "8888间";
            this.lbftall_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbkjf
            // 
            this.lbkjf.BackColor = System.Drawing.Color.LightYellow;
            this.lbkjf.ImageIndex = 0;
            this.lbkjf.Location = new System.Drawing.Point(3, 34);
            this.lbkjf.Margin = new System.Windows.Forms.Padding(3);
            this.lbkjf.Name = "lbkjf";
            this.lbkjf.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.lbkjf.Size = new System.Drawing.Size(80, 25);
            this.lbkjf.TabIndex = 14;
            this.lbkjf.Tag = "空净房";
            this.lbkjf.Text = "空净房";
            this.lbkjf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbkjf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbkjf_cnt
            // 
            this.lbkjf_cnt.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbkjf_cnt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbkjf_cnt.ImageIndex = 0;
            this.lbkjf_cnt.Location = new System.Drawing.Point(89, 34);
            this.lbkjf_cnt.Margin = new System.Windows.Forms.Padding(3);
            this.lbkjf_cnt.Name = "lbkjf_cnt";
            this.lbkjf_cnt.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.lbkjf_cnt.Size = new System.Drawing.Size(49, 25);
            this.lbkjf_cnt.TabIndex = 16;
            this.lbkjf_cnt.Text = "8888间";
            this.lbkjf_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbydf
            // 
            this.lbydf.BackColor = System.Drawing.Color.CadetBlue;
            this.lbydf.ImageIndex = 0;
            this.lbydf.Location = new System.Drawing.Point(3, 65);
            this.lbydf.Margin = new System.Windows.Forms.Padding(3);
            this.lbydf.Name = "lbydf";
            this.lbydf.Size = new System.Drawing.Size(80, 25);
            this.lbydf.TabIndex = 14;
            this.lbydf.Text = "预订房";
            this.lbydf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbydf.Click += new System.EventHandler(this.lbydf_Click);
            // 
            // lbydf_cnt
            // 
            this.lbydf_cnt.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbydf_cnt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbydf_cnt.ImageIndex = 0;
            this.lbydf_cnt.Location = new System.Drawing.Point(89, 65);
            this.lbydf_cnt.Margin = new System.Windows.Forms.Padding(3);
            this.lbydf_cnt.Name = "lbydf_cnt";
            this.lbydf_cnt.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.lbydf_cnt.Size = new System.Drawing.Size(49, 25);
            this.lbydf_cnt.TabIndex = 16;
            this.lbydf_cnt.Text = "8888间";
            this.lbydf_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbzf
            // 
            this.lbzf.BackColor = System.Drawing.Color.LightPink;
            this.lbzf.ImageIndex = 0;
            this.lbzf.Location = new System.Drawing.Point(3, 96);
            this.lbzf.Margin = new System.Windows.Forms.Padding(3);
            this.lbzf.Name = "lbzf";
            this.lbzf.Size = new System.Drawing.Size(80, 25);
            this.lbzf.TabIndex = 14;
            this.lbzf.Tag = "散客房";
            this.lbzf.Text = "住  房";
            this.lbzf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbzf.Click += new System.EventHandler(this.lbzf_Click);
            // 
            // lbzf_cnt
            // 
            this.lbzf_cnt.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbzf_cnt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbzf_cnt.ImageIndex = 0;
            this.lbzf_cnt.Location = new System.Drawing.Point(89, 96);
            this.lbzf_cnt.Margin = new System.Windows.Forms.Padding(3);
            this.lbzf_cnt.Name = "lbzf_cnt";
            this.lbzf_cnt.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.lbzf_cnt.Size = new System.Drawing.Size(49, 25);
            this.lbzf_cnt.TabIndex = 16;
            this.lbzf_cnt.Text = "8888间";
            this.lbzf_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbkzf
            // 
            this.lbkzf.BackColor = System.Drawing.Color.MediumOrchid;
            this.lbkzf.ImageIndex = 0;
            this.lbkzf.Location = new System.Drawing.Point(3, 127);
            this.lbkzf.Margin = new System.Windows.Forms.Padding(3);
            this.lbkzf.Name = "lbkzf";
            this.lbkzf.Size = new System.Drawing.Size(80, 25);
            this.lbkzf.TabIndex = 14;
            this.lbkzf.Tag = "空脏房";
            this.lbkzf.Text = "空脏房";
            this.lbkzf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbkzf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbkzf_cnt
            // 
            this.lbkzf_cnt.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbkzf_cnt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbkzf_cnt.ImageIndex = 0;
            this.lbkzf_cnt.Location = new System.Drawing.Point(89, 127);
            this.lbkzf_cnt.Margin = new System.Windows.Forms.Padding(3);
            this.lbkzf_cnt.Name = "lbkzf_cnt";
            this.lbkzf_cnt.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.lbkzf_cnt.Size = new System.Drawing.Size(49, 25);
            this.lbkzf_cnt.TabIndex = 16;
            this.lbkzf_cnt.Text = "8888间";
            this.lbkzf_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbwxf
            // 
            this.lbwxf.BackColor = System.Drawing.Color.DarkGray;
            this.lbwxf.ImageIndex = 0;
            this.lbwxf.Location = new System.Drawing.Point(3, 158);
            this.lbwxf.Margin = new System.Windows.Forms.Padding(3);
            this.lbwxf.Name = "lbwxf";
            this.lbwxf.Size = new System.Drawing.Size(80, 25);
            this.lbwxf.TabIndex = 14;
            this.lbwxf.Tag = "维修房";
            this.lbwxf.Text = "维修房";
            this.lbwxf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbwxf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbwxf_cnt
            // 
            this.lbwxf_cnt.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbwxf_cnt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbwxf_cnt.ImageIndex = 0;
            this.lbwxf_cnt.Location = new System.Drawing.Point(89, 158);
            this.lbwxf_cnt.Margin = new System.Windows.Forms.Padding(3);
            this.lbwxf_cnt.Name = "lbwxf_cnt";
            this.lbwxf_cnt.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.lbwxf_cnt.Size = new System.Drawing.Size(49, 25);
            this.lbwxf_cnt.TabIndex = 16;
            this.lbwxf_cnt.Text = "8888间";
            this.lbwxf_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbskf
            // 
            this.lbskf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbskf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbskf.ImageIndex = 0;
            this.lbskf.Location = new System.Drawing.Point(3, 186);
            this.lbskf.Name = "lbskf";
            this.lbskf.Padding = new System.Windows.Forms.Padding(5);
            this.lbskf.Size = new System.Drawing.Size(144, 25);
            this.lbskf.TabIndex = 14;
            this.lbskf.Tag = "散客房";
            this.lbskf.Text = "散客房 8/88";
            this.lbskf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbskf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbtdf
            // 
            this.lbtdf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbtdf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbtdf.ImageIndex = 0;
            this.lbtdf.Location = new System.Drawing.Point(3, 211);
            this.lbtdf.Name = "lbtdf";
            this.lbtdf.Padding = new System.Windows.Forms.Padding(5);
            this.lbtdf.Size = new System.Drawing.Size(144, 25);
            this.lbtdf.TabIndex = 14;
            this.lbtdf.Tag = "团队房";
            this.lbtdf.Text = "团队房 8/88";
            this.lbtdf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbtdf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbzdf
            // 
            this.lbzdf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbzdf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbzdf.ImageIndex = 0;
            this.lbzdf.Location = new System.Drawing.Point(3, 236);
            this.lbzdf.Name = "lbzdf";
            this.lbzdf.Padding = new System.Windows.Forms.Padding(5);
            this.lbzdf.Size = new System.Drawing.Size(144, 25);
            this.lbzdf.TabIndex = 14;
            this.lbzdf.Tag = "钟点房";
            this.lbzdf.Text = "钟点房 8/88";
            this.lbzdf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbzdf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbbmf
            // 
            this.lbbmf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbbmf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbbmf.ImageIndex = 0;
            this.lbbmf.Location = new System.Drawing.Point(3, 261);
            this.lbbmf.Name = "lbbmf";
            this.lbbmf.Padding = new System.Windows.Forms.Padding(5);
            this.lbbmf.Size = new System.Drawing.Size(144, 25);
            this.lbbmf.TabIndex = 14;
            this.lbbmf.Tag = "保密房";
            this.lbbmf.Text = "保密房 8/88";
            this.lbbmf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbbmf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbldf
            // 
            this.lbldf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbldf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbldf.ImageIndex = 0;
            this.lbldf.Location = new System.Drawing.Point(3, 286);
            this.lbldf.Name = "lbldf";
            this.lbldf.Padding = new System.Windows.Forms.Padding(5);
            this.lbldf.Size = new System.Drawing.Size(144, 25);
            this.lbldf.TabIndex = 14;
            this.lbldf.Tag = "离店房";
            this.lbldf.Text = "离店房 8/88";
            this.lbldf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbldf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbcbf
            // 
            this.lbcbf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbcbf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbcbf.ImageIndex = 0;
            this.lbcbf.Location = new System.Drawing.Point(3, 311);
            this.lbcbf.Name = "lbcbf";
            this.lbcbf.Padding = new System.Windows.Forms.Padding(5);
            this.lbcbf.Size = new System.Drawing.Size(144, 25);
            this.lbcbf.TabIndex = 14;
            this.lbcbf.Tag = "长包房";
            this.lbcbf.Text = "长包房 8/88";
            this.lbcbf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbcbf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbzyf
            // 
            this.lbzyf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbzyf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbzyf.ImageIndex = 0;
            this.lbzyf.Location = new System.Drawing.Point(3, 336);
            this.lbzyf.Name = "lbzyf";
            this.lbzyf.Padding = new System.Windows.Forms.Padding(5);
            this.lbzyf.Size = new System.Drawing.Size(144, 25);
            this.lbzyf.TabIndex = 17;
            this.lbzyf.Tag = "自用房";
            this.lbzyf.Text = "自用房 8/88";
            this.lbzyf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbzyf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbmff
            // 
            this.lbmff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbmff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbmff.ImageIndex = 0;
            this.lbmff.Location = new System.Drawing.Point(3, 361);
            this.lbmff.Name = "lbmff";
            this.lbmff.Padding = new System.Windows.Forms.Padding(5);
            this.lbmff.Size = new System.Drawing.Size(144, 25);
            this.lbmff.TabIndex = 14;
            this.lbmff.Tag = "免费房";
            this.lbmff.Text = "免费房 8/88";
            this.lbmff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbmff.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbqlf
            // 
            this.lbqlf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbqlf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbqlf.ImageIndex = 0;
            this.lbqlf.Location = new System.Drawing.Point(3, 386);
            this.lbqlf.Name = "lbqlf";
            this.lbqlf.Padding = new System.Windows.Forms.Padding(5);
            this.lbqlf.Size = new System.Drawing.Size(144, 25);
            this.lbqlf.TabIndex = 14;
            this.lbqlf.Tag = "清理房";
            this.lbqlf.Text = "清理房 8/88";
            this.lbqlf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbqlf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // lbsdf
            // 
            this.lbsdf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbsdf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbsdf.ImageIndex = 0;
            this.lbsdf.Location = new System.Drawing.Point(3, 411);
            this.lbsdf.Name = "lbsdf";
            this.lbsdf.Padding = new System.Windows.Forms.Padding(5);
            this.lbsdf.Size = new System.Drawing.Size(147, 25);
            this.lbsdf.TabIndex = 14;
            this.lbsdf.Tag = "锁定房";
            this.lbsdf.Text = "锁定房 8/88";
            this.lbsdf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbsdf.Click += new System.EventHandler(this.lbkjf_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox12);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lbrzl);
            this.panel1.Controls.Add(this.rbtnyd);
            this.panel1.Controls.Add(this.rbtnkf);
            this.panel1.Controls.Add(this.rbtnqt);
            this.panel1.Controls.Add(this.cobxzl);
            this.panel1.Controls.Add(this.txtfjh);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 16);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(150, 131);
            this.panel1.TabIndex = 15;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(5, 71);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(50, 50);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 20;
            this.pictureBox12.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("SimSun", 11F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.ImageIndex = 0;
            this.label2.Location = new System.Drawing.Point(64, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.label2.Size = new System.Drawing.Size(55, 21);
            this.label2.TabIndex = 18;
            this.label2.Text = "入住率";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbrzl
            // 
            this.lbrzl.AutoSize = true;
            this.lbrzl.Font = new System.Drawing.Font("SimSun", 14F, System.Drawing.FontStyle.Bold);
            this.lbrzl.ForeColor = System.Drawing.Color.Red;
            this.lbrzl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbrzl.ImageIndex = 0;
            this.lbrzl.Location = new System.Drawing.Point(63, 96);
            this.lbrzl.Margin = new System.Windows.Forms.Padding(3);
            this.lbrzl.Name = "lbrzl";
            this.lbrzl.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.lbrzl.Size = new System.Drawing.Size(42, 25);
            this.lbrzl.TabIndex = 18;
            this.lbrzl.Text = "88%";
            this.lbrzl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rbtnyd
            // 
            this.rbtnyd.AutoSize = true;
            this.rbtnyd.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rbtnyd.Location = new System.Drawing.Point(90, 6);
            this.rbtnyd.Margin = new System.Windows.Forms.Padding(0);
            this.rbtnyd.Name = "rbtnyd";
            this.rbtnyd.Size = new System.Drawing.Size(47, 16);
            this.rbtnyd.TabIndex = 17;
            this.rbtnyd.Text = "预订";
            this.rbtnyd.UseVisualStyleBackColor = true;
            // 
            // rbtnkf
            // 
            this.rbtnkf.AutoSize = true;
            this.rbtnkf.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rbtnkf.Location = new System.Drawing.Point(46, 6);
            this.rbtnkf.Margin = new System.Windows.Forms.Padding(0);
            this.rbtnkf.Name = "rbtnkf";
            this.rbtnkf.Size = new System.Drawing.Size(47, 16);
            this.rbtnkf.TabIndex = 16;
            this.rbtnkf.Text = "客房";
            this.rbtnkf.UseVisualStyleBackColor = true;
            // 
            // rbtnqt
            // 
            this.rbtnqt.AutoSize = true;
            this.rbtnqt.Checked = true;
            this.rbtnqt.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rbtnqt.Location = new System.Drawing.Point(3, 6);
            this.rbtnqt.Margin = new System.Windows.Forms.Padding(0);
            this.rbtnqt.Name = "rbtnqt";
            this.rbtnqt.Size = new System.Drawing.Size(47, 16);
            this.rbtnqt.TabIndex = 15;
            this.rbtnqt.TabStop = true;
            this.rbtnqt.Text = "前台";
            this.rbtnqt.UseVisualStyleBackColor = true;
            // 
            // cobxzl
            // 
            this.cobxzl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxzl.FormattingEnabled = true;
            this.cobxzl.Location = new System.Drawing.Point(5, 47);
            this.cobxzl.Name = "cobxzl";
            this.cobxzl.Size = new System.Drawing.Size(127, 21);
            this.cobxzl.TabIndex = 14;
            this.cobxzl.SelectedIndexChanged += new System.EventHandler(this.cobxzl_SelectedIndexChanged);
            // 
            // txtfjh
            // 
            this.txtfjh.Location = new System.Drawing.Point(67, 23);
            this.txtfjh.Name = "txtfjh";
            this.txtfjh.ReadOnly = true;
            this.txtfjh.Size = new System.Drawing.Size(65, 23);
            this.txtfjh.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "当前房间";
            // 
            // contextMenuRoom
            // 
            this.contextMenuRoom.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.contextMenuRoom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmikryd,
            this.tmikrtj,
            this.tmi_krxx,
            this.tmikrhf,
            this.toolStripSeparator4,
            this.tmizhika,
            this.tmiCkfk,
            this.toolStripSeparator1,
            this.tmixtyj,
            this.tmixfrd,
            this.tmijztf,
            this.toolStripSeparator3,
            this.tmihbzt,
            this.tmicfzd,
            this.toolStripSeparator2,
            this.tmifjxg,
            this.tmiccrz});
            this.contextMenuRoom.Name = "contextMenuRoom";
            this.contextMenuRoom.Size = new System.Drawing.Size(153, 362);
            this.contextMenuRoom.Opened += new System.EventHandler(this.contextMenuRoom_Opened);
            // 
            // tmikryd
            // 
            this.tmikryd.Name = "tmikryd";
            this.tmikryd.Size = new System.Drawing.Size(152, 24);
            this.tmikryd.Text = "新预订";
            this.tmikryd.Click += new System.EventHandler(this.tmikryd_Click);
            // 
            // tmikrtj
            // 
            this.tmikrtj.Name = "tmikrtj";
            this.tmikrtj.Size = new System.Drawing.Size(152, 24);
            this.tmikrtj.Text = "客人登记";
            this.tmikrtj.Click += new System.EventHandler(this.tmikrtj_Click);
            // 
            // tmi_krxx
            // 
            this.tmi_krxx.Name = "tmi_krxx";
            this.tmi_krxx.Size = new System.Drawing.Size(152, 24);
            this.tmi_krxx.Text = "客人信息";
            this.tmi_krxx.Click += new System.EventHandler(this.tmi_krxx_Click);
            // 
            // tmikrhf
            // 
            this.tmikrhf.Name = "tmikrhf";
            this.tmikrhf.Size = new System.Drawing.Size(152, 24);
            this.tmikrhf.Text = "客人换房";
            this.tmikrhf.Click += new System.EventHandler(this.tmikrhf_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(149, 6);
            // 
            // tmizhika
            // 
            this.tmizhika.Name = "tmizhika";
            this.tmizhika.Size = new System.Drawing.Size(152, 24);
            this.tmizhika.Text = "制  卡";
            this.tmizhika.Click += new System.EventHandler(this.tmizhika_Click);
            // 
            // tmiCkfk
            // 
            this.tmiCkfk.Name = "tmiCkfk";
            this.tmiCkfk.Size = new System.Drawing.Size(152, 24);
            this.tmiCkfk.Text = "查看房卡";
            this.tmiCkfk.Click += new System.EventHandler(this.tmiCkfk_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // tmixtyj
            // 
            this.tmixtyj.Name = "tmixtyj";
            this.tmixtyj.Size = new System.Drawing.Size(152, 24);
            this.tmixtyj.Text = "续订押金";
            this.tmixtyj.Click += new System.EventHandler(this.tmixtyj_Click);
            // 
            // tmixfrd
            // 
            this.tmixfrd.Name = "tmixfrd";
            this.tmixfrd.Size = new System.Drawing.Size(152, 24);
            this.tmixfrd.Text = "消费入单";
            this.tmixfrd.Click += new System.EventHandler(this.tmixfrd_Click);
            // 
            // tmijztf
            // 
            this.tmijztf.Image = global::hotel.Win.Properties.Resources.DollarSign;
            this.tmijztf.Name = "tmijztf";
            this.tmijztf.Size = new System.Drawing.Size(152, 24);
            this.tmijztf.Text = "结帐退房";
            this.tmijztf.Click += new System.EventHandler(this.tmijztf_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(149, 6);
            // 
            // tmihbzt
            // 
            this.tmihbzt.Name = "tmihbzt";
            this.tmihbzt.Size = new System.Drawing.Size(152, 24);
            this.tmihbzt.Text = "合并帐单";
            this.tmihbzt.Click += new System.EventHandler(this.tmihbzt_Click);
            // 
            // tmicfzd
            // 
            this.tmicfzd.Name = "tmicfzd";
            this.tmicfzd.Size = new System.Drawing.Size(152, 24);
            this.tmicfzd.Text = "拆分帐单";
            this.tmicfzd.Click += new System.EventHandler(this.tmicfzd_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
            // 
            // tmifjxg
            // 
            this.tmifjxg.Name = "tmifjxg";
            this.tmifjxg.Size = new System.Drawing.Size(152, 24);
            this.tmifjxg.Text = "房态修改";
            this.tmifjxg.Click += new System.EventHandler(this.tmifjxg_Click);
            // 
            // tmiccrz
            // 
            this.tmiccrz.Name = "tmiccrz";
            this.tmiccrz.Size = new System.Drawing.Size(152, 24);
            this.tmiccrz.Text = "操作日志";
            this.tmiccrz.Click += new System.EventHandler(this.tmiccrz_Click);
            // 
            // groupProgress
            // 
            this.groupProgress.BackColor = System.Drawing.Color.White;
            this.groupProgress.Controls.Add(this.progressBar1);
            this.groupProgress.Controls.Add(this.lbmsg);
            this.groupProgress.Location = new System.Drawing.Point(309, 317);
            this.groupProgress.Name = "groupProgress";
            this.groupProgress.Size = new System.Drawing.Size(386, 122);
            this.groupProgress.TabIndex = 19;
            this.groupProgress.TabStop = false;
            this.groupProgress.Text = "正在创建房间请稍候";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 60);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(361, 25);
            this.progressBar1.TabIndex = 1;
            // 
            // lbmsg
            // 
            this.lbmsg.AutoSize = true;
            this.lbmsg.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbmsg.ForeColor = System.Drawing.Color.Red;
            this.lbmsg.Location = new System.Drawing.Point(30, 23);
            this.lbmsg.Name = "lbmsg";
            this.lbmsg.Size = new System.Drawing.Size(152, 16);
            this.lbmsg.TabIndex = 0;
            this.lbmsg.Text = "正在创建房间：8888";
            // 
            // plnTootip
            // 
            this.plnTootip.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.plnTootip.Controls.Add(this.panel4);
            this.plnTootip.Controls.Add(this.gboxToolTip_kr);
            this.plnTootip.Controls.Add(this.gboxToolTip_fj);
            this.plnTootip.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.plnTootip.Location = new System.Drawing.Point(220, 22);
            this.plnTootip.Name = "plnTootip";
            this.plnTootip.Size = new System.Drawing.Size(220, 128);
            this.plnTootip.TabIndex = 2;
            this.plnTootip.Visible = false;
            this.plnTootip.Click += new System.EventHandler(this.btntooltipClose_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Khaki;
            this.panel4.Controls.Add(this.lbtooltipfjid);
            this.panel4.Controls.Add(this.btntooltipClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(220, 26);
            this.panel4.TabIndex = 13;
            // 
            // lbtooltipfjid
            // 
            this.lbtooltipfjid.AutoSize = true;
            this.lbtooltipfjid.ForeColor = System.Drawing.Color.Red;
            this.lbtooltipfjid.Location = new System.Drawing.Point(3, 1);
            this.lbtooltipfjid.Name = "lbtooltipfjid";
            this.lbtooltipfjid.Size = new System.Drawing.Size(29, 12);
            this.lbtooltipfjid.TabIndex = 2;
            this.lbtooltipfjid.Text = "8888";
            // 
            // btntooltipClose
            // 
            this.btntooltipClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btntooltipClose.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btntooltipClose.Location = new System.Drawing.Point(176, 2);
            this.btntooltipClose.Name = "btntooltipClose";
            this.btntooltipClose.Size = new System.Drawing.Size(42, 23);
            this.btntooltipClose.TabIndex = 0;
            this.btntooltipClose.Text = "关闭";
            this.btntooltipClose.UseVisualStyleBackColor = true;
            this.btntooltipClose.Click += new System.EventHandler(this.btntooltipClose_Click);
            // 
            // gboxToolTip_kr
            // 
            this.gboxToolTip_kr.Controls.Add(this.lbtoolTip_ye);
            this.gboxToolTip_kr.Controls.Add(this.lbtoolTip_xf);
            this.gboxToolTip_kr.Controls.Add(this.lbtoolTip_zf);
            this.gboxToolTip_kr.Controls.Add(this.lbtoolTip_ldrq);
            this.gboxToolTip_kr.Controls.Add(this.lbtoolTip_ddrq);
            this.gboxToolTip_kr.Controls.Add(this.lbtoolTip_rs);
            this.gboxToolTip_kr.Controls.Add(this.lbtoolTip_xm);
            this.gboxToolTip_kr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gboxToolTip_kr.Location = new System.Drawing.Point(0, -72);
            this.gboxToolTip_kr.Name = "gboxToolTip_kr";
            this.gboxToolTip_kr.Size = new System.Drawing.Size(220, 100);
            this.gboxToolTip_kr.TabIndex = 12;
            this.gboxToolTip_kr.TabStop = false;
            this.gboxToolTip_kr.Text = "客人信息";
            // 
            // lbtoolTip_ye
            // 
            this.lbtoolTip_ye.AutoSize = true;
            this.lbtoolTip_ye.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbtoolTip_ye.Location = new System.Drawing.Point(152, 78);
            this.lbtoolTip_ye.Name = "lbtoolTip_ye";
            this.lbtoolTip_ye.Size = new System.Drawing.Size(65, 12);
            this.lbtoolTip_ye.TabIndex = 14;
            this.lbtoolTip_ye.Text = "余额：8888";
            // 
            // lbtoolTip_xf
            // 
            this.lbtoolTip_xf.AutoSize = true;
            this.lbtoolTip_xf.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbtoolTip_xf.Location = new System.Drawing.Point(83, 78);
            this.lbtoolTip_xf.Name = "lbtoolTip_xf";
            this.lbtoolTip_xf.Size = new System.Drawing.Size(65, 12);
            this.lbtoolTip_xf.TabIndex = 13;
            this.lbtoolTip_xf.Text = "消费：8888";
            // 
            // lbtoolTip_zf
            // 
            this.lbtoolTip_zf.AutoSize = true;
            this.lbtoolTip_zf.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbtoolTip_zf.Location = new System.Drawing.Point(8, 78);
            this.lbtoolTip_zf.Name = "lbtoolTip_zf";
            this.lbtoolTip_zf.Size = new System.Drawing.Size(71, 12);
            this.lbtoolTip_zf.TabIndex = 12;
            this.lbtoolTip_zf.Text = "支付：88888";
            // 
            // lbtoolTip_ldrq
            // 
            this.lbtoolTip_ldrq.AutoSize = true;
            this.lbtoolTip_ldrq.Location = new System.Drawing.Point(8, 55);
            this.lbtoolTip_ldrq.Name = "lbtoolTip_ldrq";
            this.lbtoolTip_ldrq.Size = new System.Drawing.Size(65, 12);
            this.lbtoolTip_ldrq.TabIndex = 11;
            this.lbtoolTip_ldrq.Text = "离店日期：";
            // 
            // lbtoolTip_ddrq
            // 
            this.lbtoolTip_ddrq.AutoSize = true;
            this.lbtoolTip_ddrq.Location = new System.Drawing.Point(8, 34);
            this.lbtoolTip_ddrq.Name = "lbtoolTip_ddrq";
            this.lbtoolTip_ddrq.Size = new System.Drawing.Size(125, 12);
            this.lbtoolTip_ddrq.TabIndex = 10;
            this.lbtoolTip_ddrq.Text = "抵店日期：2010-06-16";
            // 
            // lbtoolTip_rs
            // 
            this.lbtoolTip_rs.AutoSize = true;
            this.lbtoolTip_rs.Location = new System.Drawing.Point(138, 16);
            this.lbtoolTip_rs.Name = "lbtoolTip_rs";
            this.lbtoolTip_rs.Size = new System.Drawing.Size(41, 12);
            this.lbtoolTip_rs.TabIndex = 9;
            this.lbtoolTip_rs.Text = "人数：";
            // 
            // lbtoolTip_xm
            // 
            this.lbtoolTip_xm.Location = new System.Drawing.Point(8, 16);
            this.lbtoolTip_xm.Name = "lbtoolTip_xm";
            this.lbtoolTip_xm.Size = new System.Drawing.Size(125, 12);
            this.lbtoolTip_xm.TabIndex = 8;
            this.lbtoolTip_xm.Text = "姓名：";
            // 
            // gboxToolTip_fj
            // 
            this.gboxToolTip_fj.Controls.Add(this.lbtoolTip_ms);
            this.gboxToolTip_fj.Controls.Add(this.lbtoolTip_fl);
            this.gboxToolTip_fj.Controls.Add(this.lbtoolTip_cs);
            this.gboxToolTip_fj.Controls.Add(this.lbtoolTip_fz);
            this.gboxToolTip_fj.Controls.Add(this.lbtoolTip_cx);
            this.gboxToolTip_fj.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gboxToolTip_fj.Location = new System.Drawing.Point(0, 28);
            this.gboxToolTip_fj.Name = "gboxToolTip_fj";
            this.gboxToolTip_fj.Size = new System.Drawing.Size(220, 100);
            this.gboxToolTip_fj.TabIndex = 5;
            this.gboxToolTip_fj.TabStop = false;
            this.gboxToolTip_fj.Text = "房间描述";
            // 
            // lbtoolTip_ms
            // 
            this.lbtoolTip_ms.Location = new System.Drawing.Point(13, 52);
            this.lbtoolTip_ms.Name = "lbtoolTip_ms";
            this.lbtoolTip_ms.Size = new System.Drawing.Size(201, 45);
            this.lbtoolTip_ms.TabIndex = 8;
            this.lbtoolTip_ms.Text = "描述：";
            // 
            // lbtoolTip_fl
            // 
            this.lbtoolTip_fl.AutoSize = true;
            this.lbtoolTip_fl.Location = new System.Drawing.Point(13, 17);
            this.lbtoolTip_fl.Name = "lbtoolTip_fl";
            this.lbtoolTip_fl.Size = new System.Drawing.Size(41, 12);
            this.lbtoolTip_fl.TabIndex = 9;
            this.lbtoolTip_fl.Text = "房类：";
            // 
            // lbtoolTip_cs
            // 
            this.lbtoolTip_cs.AutoSize = true;
            this.lbtoolTip_cs.Location = new System.Drawing.Point(143, 37);
            this.lbtoolTip_cs.Name = "lbtoolTip_cs";
            this.lbtoolTip_cs.Size = new System.Drawing.Size(41, 12);
            this.lbtoolTip_cs.TabIndex = 7;
            this.lbtoolTip_cs.Text = "床数：";
            // 
            // lbtoolTip_fz
            // 
            this.lbtoolTip_fz.AutoSize = true;
            this.lbtoolTip_fz.Location = new System.Drawing.Point(143, 17);
            this.lbtoolTip_fz.Name = "lbtoolTip_fz";
            this.lbtoolTip_fz.Size = new System.Drawing.Size(41, 12);
            this.lbtoolTip_fz.TabIndex = 5;
            this.lbtoolTip_fz.Text = "房租：";
            // 
            // lbtoolTip_cx
            // 
            this.lbtoolTip_cx.AutoSize = true;
            this.lbtoolTip_cx.Location = new System.Drawing.Point(13, 36);
            this.lbtoolTip_cx.Name = "lbtoolTip_cx";
            this.lbtoolTip_cx.Size = new System.Drawing.Size(41, 12);
            this.lbtoolTip_cx.TabIndex = 6;
            this.lbtoolTip_cx.Text = "朝向：";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnKeycard);
            this.panel3.Controls.Add(this.btnrefresh);
            this.panel3.Controls.Add(this.btnfzf);
            this.panel3.Controls.Add(this.progressBar2);
            this.panel3.Controls.Add(this.chksjft);
            this.panel3.Controls.Add(this.chkpop);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 646);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(833, 36);
            this.panel3.TabIndex = 20;
            // 
            // btnKeycard
            // 
            this.btnKeycard.Location = new System.Drawing.Point(239, 6);
            this.btnKeycard.Name = "btnKeycard";
            this.btnKeycard.Size = new System.Drawing.Size(95, 25);
            this.btnKeycard.TabIndex = 25;
            this.btnKeycard.Text = "读房卡信息";
            this.btnKeycard.UseVisualStyleBackColor = true;
            this.btnKeycard.Click += new System.EventHandler(this.btnKeycard_Click);
            // 
            // btnrefresh
            // 
            this.btnrefresh.Location = new System.Drawing.Point(138, 6);
            this.btnrefresh.Name = "btnrefresh";
            this.btnrefresh.Size = new System.Drawing.Size(95, 25);
            this.btnrefresh.TabIndex = 25;
            this.btnrefresh.Text = "刷新房态";
            this.btnrefresh.UseVisualStyleBackColor = true;
            this.btnrefresh.Click += new System.EventHandler(this.btnrefresh_Click);
            // 
            // btnfzf
            // 
            this.btnfzf.Location = new System.Drawing.Point(12, 5);
            this.btnfzf.Name = "btnfzf";
            this.btnfzf.Size = new System.Drawing.Size(108, 25);
            this.btnfzf.TabIndex = 24;
            this.btnfzf.Text = "非住房消费↓";
            this.btnfzf.UseVisualStyleBackColor = true;
            this.btnfzf.Click += new System.EventHandler(this.btnfzf_Click);
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(584, 23);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(100, 10);
            this.progressBar2.TabIndex = 2;
            this.progressBar2.Visible = false;
            // 
            // chksjft
            // 
            this.chksjft.AutoSize = true;
            this.chksjft.Checked = true;
            this.chksjft.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chksjft.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chksjft.Location = new System.Drawing.Point(585, 5);
            this.chksjft.Name = "chksjft";
            this.chksjft.Size = new System.Drawing.Size(96, 16);
            this.chksjft.TabIndex = 1;
            this.chksjft.Text = "定时刷新房态";
            this.chksjft.UseVisualStyleBackColor = true;
            this.chksjft.CheckedChanged += new System.EventHandler(this.chksjft_CheckedChanged);
            // 
            // chkpop
            // 
            this.chkpop.AutoSize = true;
            this.chkpop.Checked = true;
            this.chkpop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkpop.Font = new System.Drawing.Font("SimSun", 9F);
            this.chkpop.Location = new System.Drawing.Point(731, 5);
            this.chkpop.Name = "chkpop";
            this.chkpop.Size = new System.Drawing.Size(96, 16);
            this.chkpop.TabIndex = 0;
            this.chkpop.Text = "提示房间信息";
            this.chkpop.UseVisualStyleBackColor = true;
            this.chkpop.CheckedChanged += new System.EventHandler(this.chkpop_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabMain);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(833, 682);
            this.panel2.TabIndex = 22;
            // 
            // contextMenuStripFzf
            // 
            this.contextMenuStripFzf.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmi_xfrd,
            this.tmp_hysgl});
            this.contextMenuStripFzf.Name = "contextMenuStrip1";
            this.contextMenuStripFzf.Size = new System.Drawing.Size(137, 48);
            // 
            // tmi_xfrd
            // 
            this.tmi_xfrd.Name = "tmi_xfrd";
            this.tmi_xfrd.Size = new System.Drawing.Size(136, 22);
            this.tmi_xfrd.Text = "消费入单";
            this.tmi_xfrd.Click += new System.EventHandler(this.tmi_xfrd_Click);
            // 
            // tmp_hysgl
            // 
            this.tmp_hysgl.Name = "tmp_hysgl";
            this.tmp_hysgl.Size = new System.Drawing.Size(136, 22);
            this.tmp_hysgl.Text = "会议室管理";
            this.tmp_hysgl.Click += new System.EventHandler(this.tmp_hysgl_Click);
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 682);
            this.ControlBox = false;
            this.Controls.Add(this.plnTootip);
            this.Controls.Add(this.groupProgress);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainControl";
            this.ShowIcon = false;
            this.Text = "实时房态";
            this.Activated += new System.EventHandler(this.MainControl_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainControl_FormClosing);
            this.Load += new System.EventHandler(this.MainControl_Load);
            this.groupBox1.ResumeLayout(false);
            this.flowLayoutPanelFT.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.contextMenuRoom.ResumeLayout(false);
            this.groupProgress.ResumeLayout(false);
            this.groupProgress.PerformLayout();
            this.plnTootip.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.gboxToolTip_kr.ResumeLayout(false);
            this.gboxToolTip_kr.PerformLayout();
            this.gboxToolTip_fj.ResumeLayout(false);
            this.gboxToolTip_fj.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.contextMenuStripFzf.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelFT;
        private System.Windows.Forms.Label lbydf;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cobxzl;
        private System.Windows.Forms.TextBox txtfjh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtnyd;
        private System.Windows.Forms.RadioButton rbtnkf;
        private System.Windows.Forms.RadioButton rbtnqt;
        private System.Windows.Forms.ContextMenuStrip contextMenuRoom;
        private System.Windows.Forms.ToolStripMenuItem tmikrtj;
        private System.Windows.Forms.ToolStripMenuItem tmikryd;
        private System.Windows.Forms.ToolStripMenuItem tmikrhf;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tmixtyj;
        private System.Windows.Forms.ToolStripMenuItem tmixfrd;
        private System.Windows.Forms.ToolStripMenuItem tmijztf;
        private System.Windows.Forms.ToolStripMenuItem tmihbzt;
        private System.Windows.Forms.ToolStripMenuItem tmicfzd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tmifjxg;
        private System.Windows.Forms.ToolStripMenuItem tmi_krxx;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Label lbkjf;
        private System.Windows.Forms.Label lbskf;
        private System.Windows.Forms.Label lbtdf;
        private System.Windows.Forms.Label lbzdf;
        private System.Windows.Forms.Label lbcbf;
        private System.Windows.Forms.Label lbmff;
        private System.Windows.Forms.Label lbbmf;
        private System.Windows.Forms.Label lbldf;
        private System.Windows.Forms.Label lbqlf;
        private System.Windows.Forms.Label lbkzf;
        private System.Windows.Forms.Label lbwxf;
        private System.Windows.Forms.Label lbsdf;
        private System.Windows.Forms.Label lbftall;
        private System.Windows.Forms.Label lbftall_cnt;
        private System.Windows.Forms.Label lbkjf_cnt;
        private System.Windows.Forms.Label lbydf_cnt;
        private System.Windows.Forms.Label lbzf;
        private System.Windows.Forms.Label lbzf_cnt;
        private System.Windows.Forms.Label lbkzf_cnt;
        private System.Windows.Forms.Label lbwxf_cnt;
        private System.Windows.Forms.Label lbzyf;
        private System.Windows.Forms.GroupBox groupProgress;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lbmsg;
        private System.Windows.Forms.Panel plnTootip;
        private System.Windows.Forms.Button btntooltipClose;
        private System.Windows.Forms.Label lbtooltipfjid;
        private System.Windows.Forms.GroupBox gboxToolTip_fj;
        private System.Windows.Forms.Label lbtoolTip_fl;
        private System.Windows.Forms.Label lbtoolTip_ms;
        private System.Windows.Forms.Label lbtoolTip_cs;
        private System.Windows.Forms.Label lbtoolTip_fz;
        private System.Windows.Forms.Label lbtoolTip_cx;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbrzl;
        private System.Windows.Forms.GroupBox gboxToolTip_kr;
        private System.Windows.Forms.Label lbtoolTip_ye;
        private System.Windows.Forms.Label lbtoolTip_xf;
        private System.Windows.Forms.Label lbtoolTip_zf;
        private System.Windows.Forms.Label lbtoolTip_ldrq;
        private System.Windows.Forms.Label lbtoolTip_ddrq;
        private System.Windows.Forms.Label lbtoolTip_rs;
        private System.Windows.Forms.Label lbtoolTip_xm;
        private System.Windows.Forms.CheckBox chkpop;
        private System.Windows.Forms.CheckBox chksjft;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnfzf;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripFzf;
        private System.Windows.Forms.ToolStripMenuItem tmi_xfrd;
        private System.Windows.Forms.ToolStripMenuItem tmp_hysgl;
        private System.Windows.Forms.Button btnrefresh;
        private System.Windows.Forms.Button btnKeycard;
        private System.Windows.Forms.ToolStripMenuItem tmizhika;
        private System.Windows.Forms.ToolStripMenuItem tmiccrz;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem tmiCkfk;
    }
}