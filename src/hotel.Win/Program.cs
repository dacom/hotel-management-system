﻿#define TRIAL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using hotel.Win.Common;
using hotel.DAL;
using System.Threading;
using hotel.Common;
namespace hotel.Win
{
    static class Program
    {
        private static Log4NetLog _log = new Log4NetLog();
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {     
             bool runone;
             System.Threading.Mutex run = new System.Threading.Mutex(true, "dacongQQ2546677", out runone);
             if (!runone)
             {
                 System.Windows.Forms.Application.DoEvents();  
                 MessageBox.Show("已经运行了一个酒店系统，不能同时运行。");
                 return;
             }
            #region 异常处理
            #if !DEBUG
            // Add the event handler for handling UI thread exceptions to the event.     
            Application.ThreadException += new ThreadExceptionEventHandler(UIThreadException);
            // Set the unhandled exception mode to force all Windows Forms errors to go through     
            // our handler.     处理未捕获的异常
            //Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            // Add the event handler for handling non-UI thread exceptions to the event.      
            AppDomain.CurrentDomain.UnhandledException +=new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);   
            #endif
            #endregion
           
            Application.EnableVisualStyles();            
            Application.SetCompatibleTextRenderingDefault(false);
            using (hotelEntities _context = MyDataContext.GetDataContext)
            {
                try
                {
                    _context.Database.Exists();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    MessageBox.Show("请检查数据库是已否经打开\n" + ex.Message);
                    return;
                }
            }
              
           
            //测试用
            //Application.Run(new Form1());
            //return;
            if (!Grant.IsGrant)
            {
                if (!Grant.isTrial)
                {
                    Application.Run(new RegisterForm());
                    return;   
                }
            }
            if (Convert.ToDateTime(XTCSModel.GetXTCS.YYRQ) > DateTime.Now)
            {
                MessageBox.Show("操作系统的时间不能小于酒店系统的营业时间，" + XTCSModel.GetXTCS.YYRQ);
                return;
            }
#if DEBUG
            UserLoginInfo.UserId = "admin";
            UserLoginInfo.UserName = "管理员"; 
            var _context2 = MyDataContext.GetDataContext;
            UserLoginInfo.Roles = new List<string>();
            UserLoginInfo.Roles.AddRange(_context2.YHJS.Where(p => p.YHID == UserLoginInfo.UserId).Select(p => p.JSID));
            Application.Run(new MainForm());//MainForm Form1
#else
              LoginForm lg = new LoginForm(Properties.Settings.Default.UserId);
            if (lg.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new MainForm());//MainForm
                run.ReleaseMutex();
            }

#endif

            #region SplashScreen
            //SplashScreen.ShowSplashScreen();
            //Application.DoEvents();
            //SplashScreen.SetStatus("Loading module 1");
            //System.Threading.Thread.Sleep(500);
            //SplashScreen.SetStatus("Loading module 2");
            //System.Threading.Thread.Sleep(300);
            //SplashScreen.SetStatus("Loading module 3");
            //System.Threading.Thread.Sleep(900);
            //SplashScreen.SetStatus("Loading module 4");
            //System.Threading.Thread.Sleep(100);
            //SplashScreen.SetStatus("Loading module 5");
            //System.Threading.Thread.Sleep(400);
            //SplashScreen.SetStatus("Loading module 6");
            //System.Threading.Thread.Sleep(50);
            //SplashScreen.SetStatus("Loading module 7");
            //System.Threading.Thread.Sleep(240);
            //SplashScreen.SetStatus("Loading module 8");
            //System.Threading.Thread.Sleep(900);
            //SplashScreen.SetStatus("Loading module 9");
            //System.Threading.Thread.Sleep(240);
            //SplashScreen.SetStatus("Loading module 10");
            //System.Threading.Thread.Sleep(90);
            //SplashScreen.SetStatus("Loading module 11");
            //System.Threading.Thread.Sleep(1000);
            //SplashScreen.SetStatus("Loading module 12");
            //System.Threading.Thread.Sleep(100);
            //SplashScreen.SetStatus("Loading module 13");
            //System.Threading.Thread.Sleep(500);
            //SplashScreen.SetStatus("Loading module 14", false);
            //System.Threading.Thread.Sleep(1000);
            //SplashScreen.SetStatus("Loading module 14a", false);
            //System.Threading.Thread.Sleep(1000);
            //SplashScreen.SetStatus("Loading module 14b", false);
            //System.Threading.Thread.Sleep(1000);
            //SplashScreen.SetStatus("Loading module 14c", false);
            //System.Threading.Thread.Sleep(1000);
            //SplashScreen.SetStatus("Loading module 15");
            //System.Threading.Thread.Sleep(20);
            //SplashScreen.SetStatus("Loading module 16");
            //System.Threading.Thread.Sleep(450);
            //SplashScreen.SetStatus("Loading module 17");
            //System.Threading.Thread.Sleep(240);
            //SplashScreen.SetStatus("Loading module 18");
            //System.Threading.Thread.Sleep(90);

            #endregion


        }

        private static void UIThreadException(object sender, ThreadExceptionEventArgs t)
        {
            _log.Error("UIThreadException", t.Exception);
#if DEBUG
            string errorMsg = "An application error occurred. Please contact the adminstrator " +
            "with the following information:\n\n";
            errorMsg += t.Exception.Message + "\n\nStack Trace:\n" + t.Exception.StackTrace;
            MessageBox.Show(errorMsg);
#else
            MessageBox.Show("出现未处理的UI异常，如下：\n" + t.Exception.Message);
#endif

        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            _log.Error("CurrentDomain_UnhandledException", ex);

#if DEBUG
            string errorMsg = "An application error occurred. Please contact the adminstrator " +
            "with the following information:\n\n";
            errorMsg += ex.Message + "\n\nStack Trace:\n" + ex.StackTrace;
            MessageBox.Show(errorMsg);
#else
            MessageBox.Show("出现未处理的线程异常异常，如下：\n" + ex.Message);
#endif


        } 
    }
}
