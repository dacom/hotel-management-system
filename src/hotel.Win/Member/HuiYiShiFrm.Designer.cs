﻿namespace hotel.Win.Member
{
    partial class HuiYiShiFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.txtts = new System.Windows.Forms.NumericUpDown();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbtnrz = new System.Windows.Forms.RadioButton();
            this.rbthyd = new System.Windows.Forms.RadioButton();
            this.btnclose = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtfy = new System.Windows.Forms.NumericUpDown();
            this.txtyj = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txthyrs = new System.Windows.Forms.NumericUpDown();
            this.txtlxdh = new System.Windows.Forms.TextBox();
            this.txtlxr = new System.Windows.Forms.TextBox();
            this.txthymc = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpkssj = new System.Windows.Forms.DateTimePicker();
            this.dtprq = new System.Windows.Forms.DateTimePicker();
            this.btnedithys = new System.Windows.Forms.Button();
            this.cobxhys = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbjssj = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gboxrz = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnqzrz = new System.Windows.Forms.Button();
            this.txtfy2 = new System.Windows.Forms.NumericUpDown();
            this.txtyj2 = new System.Windows.Forms.NumericUpDown();
            this.lbzl = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.maingrid = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnqxyd = new System.Windows.Forms.Button();
            this.btnqdrz = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnyd = new System.Windows.Forms.Button();
            this.btnquery = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.mainTab = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtts)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txthyrs)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.gboxrz.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfy2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.mainTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(95, 286);
            this.txtbzsm.Multiline = true;
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(305, 48);
            this.txtbzsm.TabIndex = 4;
            // 
            // txtts
            // 
            this.txtts.Location = new System.Drawing.Point(95, 78);
            this.txtts.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtts.Name = "txtts";
            this.txtts.Size = new System.Drawing.Size(48, 23);
            this.txtts.TabIndex = 3;
            this.txtts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtts.ValueChanged += new System.EventHandler(this.txtts_ValueChanged);
            this.txtts.Leave += new System.EventHandler(this.txtts_ValueChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.btnclose);
            this.tabPage1.Controls.Add(this.btnok);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(745, 489);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "新增会议室使用";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbtnrz);
            this.groupBox3.Controls.Add(this.rbthyd);
            this.groupBox3.Location = new System.Drawing.Point(25, 22);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(174, 55);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "操作选择";
            // 
            // rbtnrz
            // 
            this.rbtnrz.AutoSize = true;
            this.rbtnrz.Location = new System.Drawing.Point(80, 22);
            this.rbtnrz.Name = "rbtnrz";
            this.rbtnrz.Size = new System.Drawing.Size(53, 18);
            this.rbtnrz.TabIndex = 1;
            this.rbtnrz.TabStop = true;
            this.rbtnrz.Text = "入帐";
            this.rbtnrz.UseVisualStyleBackColor = true;
            this.rbtnrz.CheckedChanged += new System.EventHandler(this.rbtnrz_CheckedChanged);
            // 
            // rbthyd
            // 
            this.rbthyd.AutoSize = true;
            this.rbthyd.Location = new System.Drawing.Point(18, 22);
            this.rbthyd.Name = "rbthyd";
            this.rbthyd.Size = new System.Drawing.Size(53, 18);
            this.rbthyd.TabIndex = 0;
            this.rbthyd.TabStop = true;
            this.rbthyd.Text = "预订";
            this.rbthyd.UseVisualStyleBackColor = true;
            this.rbthyd.CheckedChanged += new System.EventHandler(this.rbthyd_CheckedChanged);
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(217, 448);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(84, 32);
            this.btnclose.TabIndex = 4;
            this.btnclose.Text = "关 闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(127, 448);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(84, 32);
            this.btnok.TabIndex = 4;
            this.btnok.Text = "保存";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtfy);
            this.groupBox2.Controls.Add(this.txtyj);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.dtpkssj);
            this.groupBox2.Controls.Add(this.dtprq);
            this.groupBox2.Controls.Add(this.btnedithys);
            this.groupBox2.Controls.Add(this.cobxhys);
            this.groupBox2.Controls.Add(this.txtbzsm);
            this.groupBox2.Controls.Add(this.txtts);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.lbjssj);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(25, 82);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(486, 348);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "使用情况";
            // 
            // txtfy
            // 
            this.txtfy.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtfy.Location = new System.Drawing.Point(249, 107);
            this.txtfy.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtfy.Name = "txtfy";
            this.txtfy.Size = new System.Drawing.Size(93, 23);
            this.txtfy.TabIndex = 11;
            // 
            // txtyj
            // 
            this.txtyj.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtyj.Location = new System.Drawing.Point(95, 107);
            this.txtyj.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtyj.Name = "txtyj";
            this.txtyj.Size = new System.Drawing.Size(93, 23);
            this.txtyj.TabIndex = 11;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txthyrs);
            this.groupBox4.Controls.Add(this.txtlxdh);
            this.groupBox4.Controls.Add(this.txtlxr);
            this.groupBox4.Controls.Add(this.txthymc);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(25, 138);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(375, 140);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "会议信息";
            // 
            // txthyrs
            // 
            this.txthyrs.Location = new System.Drawing.Point(101, 103);
            this.txthyrs.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txthyrs.Name = "txthyrs";
            this.txthyrs.Size = new System.Drawing.Size(48, 23);
            this.txthyrs.TabIndex = 6;
            this.txthyrs.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txtlxdh
            // 
            this.txtlxdh.Location = new System.Drawing.Point(101, 74);
            this.txtlxdh.Name = "txtlxdh";
            this.txtlxdh.Size = new System.Drawing.Size(260, 23);
            this.txtlxdh.TabIndex = 5;
            // 
            // txtlxr
            // 
            this.txtlxr.Location = new System.Drawing.Point(101, 45);
            this.txtlxr.Name = "txtlxr";
            this.txtlxr.Size = new System.Drawing.Size(260, 23);
            this.txtlxr.TabIndex = 5;
            // 
            // txthymc
            // 
            this.txthymc.Location = new System.Drawing.Point(101, 16);
            this.txthymc.Name = "txthymc";
            this.txthymc.Size = new System.Drawing.Size(260, 23);
            this.txthymc.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 106);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 14);
            this.label13.TabIndex = 4;
            this.label13.Text = "会议人数：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 14);
            this.label10.TabIndex = 4;
            this.label10.Text = "联系电话：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 14);
            this.label9.TabIndex = 3;
            this.label9.Text = "联系人：";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 2;
            this.label8.Text = "会议名称：";
            // 
            // dtpkssj
            // 
            this.dtpkssj.CustomFormat = "HH:mm";
            this.dtpkssj.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpkssj.Location = new System.Drawing.Point(231, 48);
            this.dtpkssj.Name = "dtpkssj";
            this.dtpkssj.Size = new System.Drawing.Size(93, 23);
            this.dtpkssj.TabIndex = 9;
            this.dtpkssj.ValueChanged += new System.EventHandler(this.txtts_ValueChanged);
            // 
            // dtprq
            // 
            this.dtprq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtprq.Location = new System.Drawing.Point(95, 49);
            this.dtprq.Name = "dtprq";
            this.dtprq.Size = new System.Drawing.Size(130, 23);
            this.dtprq.TabIndex = 7;
            this.dtprq.Value = new System.DateTime(2010, 12, 30, 0, 0, 0, 0);
            this.dtprq.ValueChanged += new System.EventHandler(this.txtts_ValueChanged);
            // 
            // btnedithys
            // 
            this.btnedithys.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnedithys.Location = new System.Drawing.Point(259, 19);
            this.btnedithys.Name = "btnedithys";
            this.btnedithys.Size = new System.Drawing.Size(75, 23);
            this.btnedithys.TabIndex = 6;
            this.btnedithys.Text = "维护...";
            this.btnedithys.UseVisualStyleBackColor = true;
            this.btnedithys.Click += new System.EventHandler(this.btnedithys_Click);
            // 
            // cobxhys
            // 
            this.cobxhys.FormattingEnabled = true;
            this.cobxhys.Location = new System.Drawing.Point(95, 20);
            this.cobxhys.Name = "cobxhys";
            this.cobxhys.Size = new System.Drawing.Size(158, 21);
            this.cobxhys.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 289);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 1;
            this.label11.Text = "备注说明：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(34, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 14);
            this.label7.TabIndex = 1;
            this.label7.Text = "小时：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(166, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "结束时间：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(194, 109);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 14);
            this.label15.TabIndex = 1;
            this.label15.Text = "费用：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(34, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 14);
            this.label14.TabIndex = 1;
            this.label14.Text = "押金：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(6, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 14);
            this.label6.TabIndex = 1;
            this.label6.Text = "开始时间：";
            // 
            // lbjssj
            // 
            this.lbjssj.AutoSize = true;
            this.lbjssj.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbjssj.Location = new System.Drawing.Point(249, 80);
            this.lbjssj.Name = "lbjssj";
            this.lbjssj.Size = new System.Drawing.Size(142, 14);
            this.lbjssj.TabIndex = 1;
            this.lbjssj.Text = "2010-12-30 22：54";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "会议室：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gboxrz);
            this.tabPage2.Controls.Add(this.maingrid);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(745, 489);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "查询会议室使用记录";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gboxrz
            // 
            this.gboxrz.Controls.Add(this.button2);
            this.gboxrz.Controls.Add(this.btnqzrz);
            this.gboxrz.Controls.Add(this.txtfy2);
            this.gboxrz.Controls.Add(this.txtyj2);
            this.gboxrz.Controls.Add(this.lbzl);
            this.gboxrz.Controls.Add(this.label16);
            this.gboxrz.Controls.Add(this.label5);
            this.gboxrz.Controls.Add(this.label12);
            this.gboxrz.Location = new System.Drawing.Point(41, 105);
            this.gboxrz.Name = "gboxrz";
            this.gboxrz.Size = new System.Drawing.Size(228, 154);
            this.gboxrz.TabIndex = 5;
            this.gboxrz.TabStop = false;
            this.gboxrz.Text = "确认入帐";
            this.gboxrz.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(157, 115);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 33);
            this.button2.TabIndex = 17;
            this.button2.Tag = "0";
            this.button2.Text = "关闭";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnqzrz
            // 
            this.btnqzrz.Location = new System.Drawing.Point(68, 115);
            this.btnqzrz.Name = "btnqzrz";
            this.btnqzrz.Size = new System.Drawing.Size(72, 33);
            this.btnqzrz.TabIndex = 16;
            this.btnqzrz.Tag = "0";
            this.btnqzrz.Text = "确定";
            this.btnqzrz.UseVisualStyleBackColor = true;
            this.btnqzrz.Click += new System.EventHandler(this.btnqzrz_Click);
            // 
            // txtfy2
            // 
            this.txtfy2.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtfy2.Location = new System.Drawing.Point(73, 61);
            this.txtfy2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtfy2.Name = "txtfy2";
            this.txtfy2.Size = new System.Drawing.Size(93, 23);
            this.txtfy2.TabIndex = 14;
            this.txtfy2.ValueChanged += new System.EventHandler(this.txtfy2_ValueChanged);
            this.txtfy2.Click += new System.EventHandler(this.txtfy2_ValueChanged);
            // 
            // txtyj2
            // 
            this.txtyj2.Enabled = false;
            this.txtyj2.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtyj2.Location = new System.Drawing.Point(73, 24);
            this.txtyj2.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtyj2.Name = "txtyj2";
            this.txtyj2.ReadOnly = true;
            this.txtyj2.Size = new System.Drawing.Size(93, 23);
            this.txtyj2.TabIndex = 15;
            // 
            // lbzl
            // 
            this.lbzl.AutoSize = true;
            this.lbzl.ForeColor = System.Drawing.Color.Red;
            this.lbzl.Location = new System.Drawing.Point(73, 91);
            this.lbzl.Name = "lbzl";
            this.lbzl.Size = new System.Drawing.Size(21, 14);
            this.lbzl.TabIndex = 12;
            this.lbzl.Text = "88";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(18, 91);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 14);
            this.label16.TabIndex = 12;
            this.label16.Text = "找零：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(18, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 12;
            this.label5.Text = "费用：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(18, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 14);
            this.label12.TabIndex = 13;
            this.label12.Text = "押金：";
            // 
            // maingrid
            // 
            this.maingrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.maingrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maingrid.Location = new System.Drawing.Point(3, 105);
            this.maingrid.Name = "maingrid";
            this.maingrid.ReadOnly = true;
            this.maingrid.RowHeadersWidth = 25;
            this.maingrid.RowTemplate.Height = 23;
            this.maingrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.maingrid.Size = new System.Drawing.Size(739, 381);
            this.maingrid.TabIndex = 4;
            this.maingrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.maingrid_CellClick);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnqxyd);
            this.groupBox5.Controls.Add(this.btnqdrz);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(3, 59);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(739, 46);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "操作";
            // 
            // btnqxyd
            // 
            this.btnqxyd.Location = new System.Drawing.Point(137, 17);
            this.btnqxyd.Name = "btnqxyd";
            this.btnqxyd.Size = new System.Drawing.Size(91, 23);
            this.btnqxyd.TabIndex = 10;
            this.btnqxyd.Tag = "0";
            this.btnqxyd.Text = "取消预订";
            this.btnqxyd.UseVisualStyleBackColor = true;
            this.btnqxyd.Click += new System.EventHandler(this.btnqxyd_Click);
            // 
            // btnqdrz
            // 
            this.btnqdrz.Location = new System.Drawing.Point(38, 17);
            this.btnqdrz.Name = "btnqdrz";
            this.btnqdrz.Size = new System.Drawing.Size(91, 23);
            this.btnqdrz.TabIndex = 10;
            this.btnqdrz.Tag = "0";
            this.btnqdrz.Text = "确定入帐";
            this.btnqdrz.UseVisualStyleBackColor = true;
            this.btnqdrz.Click += new System.EventHandler(this.btnqdrz_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnyd);
            this.groupBox1.Controls.Add(this.btnquery);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(739, 56);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询条件";
            // 
            // btnyd
            // 
            this.btnyd.Location = new System.Drawing.Point(491, 22);
            this.btnyd.Name = "btnyd";
            this.btnyd.Size = new System.Drawing.Size(91, 23);
            this.btnyd.TabIndex = 9;
            this.btnyd.Tag = "0";
            this.btnyd.Text = "所有预订";
            this.btnyd.UseVisualStyleBackColor = true;
            this.btnyd.Click += new System.EventHandler(this.btnyd_Click);
            // 
            // btnquery
            // 
            this.btnquery.Location = new System.Drawing.Point(394, 22);
            this.btnquery.Name = "btnquery";
            this.btnquery.Size = new System.Drawing.Size(91, 23);
            this.btnquery.TabIndex = 9;
            this.btnquery.Tag = "0";
            this.btnquery.Text = "开始查询";
            this.btnquery.UseVisualStyleBackColor = true;
            this.btnquery.Click += new System.EventHandler(this.btnquery_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(258, 22);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(130, 23);
            this.dateTimePicker2.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(231, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 14);
            this.label2.TabIndex = 7;
            this.label2.Text = "到";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(98, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 23);
            this.dateTimePicker1.TabIndex = 6;
            this.dateTimePicker1.Value = new System.DateTime(2010, 12, 30, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "使用日期从";
            // 
            // mainTab
            // 
            this.mainTab.Controls.Add(this.tabPage1);
            this.mainTab.Controls.Add(this.tabPage2);
            this.mainTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTab.Location = new System.Drawing.Point(0, 0);
            this.mainTab.Name = "mainTab";
            this.mainTab.SelectedIndex = 0;
            this.mainTab.Size = new System.Drawing.Size(753, 516);
            this.mainTab.TabIndex = 1;
            this.mainTab.SelectedIndexChanged += new System.EventHandler(this.mainTab_SelectedIndexChanged);
            // 
            // HuiYiShiFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 516);
            this.Controls.Add(this.mainTab);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "HuiYiShiFrm";
            this.ShowInTaskbar = false;
            this.Text = "会议室管理";
            ((System.ComponentModel.ISupportInitialize)(this.txtts)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txthyrs)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.gboxrz.ResumeLayout(false);
            this.gboxrz.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtfy2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtyj2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.mainTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.NumericUpDown txtts;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbtnrz;
        private System.Windows.Forms.RadioButton rbthyd;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnquery;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl mainTab;
        private System.Windows.Forms.Button btnedithys;
        private System.Windows.Forms.ComboBox cobxhys;
        private System.Windows.Forms.DateTimePicker dtprq;
        private System.Windows.Forms.DateTimePicker dtpkssj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbjssj;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown txthyrs;
        private System.Windows.Forms.TextBox txtlxdh;
        private System.Windows.Forms.TextBox txtlxr;
        private System.Windows.Forms.TextBox txthymc;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown txtfy;
        private System.Windows.Forms.NumericUpDown txtyj;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnyd;
        private System.Windows.Forms.DataGridView maingrid;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnqxyd;
        private System.Windows.Forms.Button btnqdrz;
        private System.Windows.Forms.GroupBox gboxrz;
        private System.Windows.Forms.NumericUpDown txtfy2;
        private System.Windows.Forms.NumericUpDown txtyj2;
        private System.Windows.Forms.Label lbzl;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnqzrz;
    }
}