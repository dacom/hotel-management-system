﻿namespace hotel.Win.Member
{
    partial class HuiYuanMaintain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtkh = new System.Windows.Forms.TextBox();
            this.txtxm = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cobxxb = new System.Windows.Forms.ComboBox();
            this.txtdh = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdz = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cobxklx = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cobxzt = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpksrq = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpjsrq = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpcsrq = new System.Windows.Forms.DateTimePicker();
            this.cobxzjlx = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtzjhm = new System.Windows.Forms.TextBox();
            this.txtgzdw = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtjf = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtljcz = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtxfcs = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtljxf = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 6;
            this.label1.Text = "卡号:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(200, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "姓名:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(426, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "会员照片:";
            // 
            // txtkh
            // 
            this.txtkh.Location = new System.Drawing.Point(92, 17);
            this.txtkh.Name = "txtkh";
            this.txtkh.Size = new System.Drawing.Size(98, 23);
            this.txtkh.TabIndex = 0;
            // 
            // txtxm
            // 
            this.txtxm.Location = new System.Drawing.Point(248, 16);
            this.txtxm.Name = "txtxm";
            this.txtxm.Size = new System.Drawing.Size(63, 23);
            this.txtxm.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(317, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 14);
            this.label4.TabIndex = 6;
            this.label4.Text = "性别:";
            // 
            // cobxxb
            // 
            this.cobxxb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxxb.FormattingEnabled = true;
            this.cobxxb.Items.AddRange(new object[] {
            "男",
            "女"});
            this.cobxxb.Location = new System.Drawing.Point(362, 17);
            this.cobxxb.Name = "cobxxb";
            this.cobxxb.Size = new System.Drawing.Size(51, 21);
            this.cobxxb.TabIndex = 2;
            // 
            // txtdh
            // 
            this.txtdh.Location = new System.Drawing.Point(248, 47);
            this.txtdh.Name = "txtdh";
            this.txtdh.Size = new System.Drawing.Size(165, 23);
            this.txtdh.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(200, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 14);
            this.label5.TabIndex = 6;
            this.label5.Text = "电话:";
            // 
            // txtdz
            // 
            this.txtdz.Location = new System.Drawing.Point(92, 77);
            this.txtdz.Name = "txtdz";
            this.txtdz.Size = new System.Drawing.Size(321, 23);
            this.txtdz.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 6;
            this.label6.Text = "地址:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 14);
            this.label7.TabIndex = 6;
            this.label7.Text = "卡类型:";
            // 
            // cobxklx
            // 
            this.cobxklx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxklx.FormattingEnabled = true;
            this.cobxklx.Items.AddRange(new object[] {
            "男",
            "女"});
            this.cobxklx.Location = new System.Drawing.Point(92, 107);
            this.cobxklx.Name = "cobxklx";
            this.cobxklx.Size = new System.Drawing.Size(82, 21);
            this.cobxklx.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(424, 46);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(119, 158);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(203, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 14);
            this.label8.TabIndex = 6;
            this.label8.Text = "状态:";
            // 
            // cobxzt
            // 
            this.cobxzt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxzt.FormattingEnabled = true;
            this.cobxzt.Items.AddRange(new object[] {
            "有效",
            "挂失",
            "无效"});
            this.cobxzt.Location = new System.Drawing.Point(248, 105);
            this.cobxzt.Name = "cobxzt";
            this.cobxzt.Size = new System.Drawing.Size(89, 21);
            this.cobxzt.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 139);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 14);
            this.label9.TabIndex = 6;
            this.label9.Text = "开始日期:";
            // 
            // dtpksrq
            // 
            this.dtpksrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpksrq.Location = new System.Drawing.Point(92, 135);
            this.dtpksrq.Name = "dtpksrq";
            this.dtpksrq.Size = new System.Drawing.Size(109, 23);
            this.dtpksrq.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(204, 139);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 14);
            this.label10.TabIndex = 6;
            this.label10.Text = "终止日期:";
            // 
            // dtpjsrq
            // 
            this.dtpjsrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpjsrq.Location = new System.Drawing.Point(281, 135);
            this.dtpjsrq.Name = "dtpjsrq";
            this.dtpjsrq.Size = new System.Drawing.Size(109, 23);
            this.dtpjsrq.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 14);
            this.label11.TabIndex = 6;
            this.label11.Text = "出生日期:";
            // 
            // dtpcsrq
            // 
            this.dtpcsrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpcsrq.Location = new System.Drawing.Point(92, 47);
            this.dtpcsrq.Name = "dtpcsrq";
            this.dtpcsrq.Size = new System.Drawing.Size(109, 23);
            this.dtpcsrq.TabIndex = 3;
            // 
            // cobxzjlx
            // 
            this.cobxzjlx.FormattingEnabled = true;
            this.cobxzjlx.Location = new System.Drawing.Point(92, 165);
            this.cobxzjlx.Name = "cobxzjlx";
            this.cobxzjlx.Size = new System.Drawing.Size(130, 21);
            this.cobxzjlx.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 197);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 14);
            this.label12.TabIndex = 12;
            this.label12.Text = "证件号码:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 14);
            this.label13.TabIndex = 11;
            this.label13.Text = "证件类型:";
            // 
            // txtzjhm
            // 
            this.txtzjhm.Location = new System.Drawing.Point(92, 193);
            this.txtzjhm.Name = "txtzjhm";
            this.txtzjhm.Size = new System.Drawing.Size(321, 23);
            this.txtzjhm.TabIndex = 9;
            // 
            // txtgzdw
            // 
            this.txtgzdw.Location = new System.Drawing.Point(92, 223);
            this.txtgzdw.Name = "txtgzdw";
            this.txtgzdw.Size = new System.Drawing.Size(321, 23);
            this.txtgzdw.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 226);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 14);
            this.label14.TabIndex = 12;
            this.label14.Text = "工作单位:";
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(92, 253);
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(321, 23);
            this.txtbzsm.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 255);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 14);
            this.label15.TabIndex = 12;
            this.label15.Text = "备注说明:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtjf);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtljcz);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtxfcs);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtljxf);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Location = new System.Drawing.Point(5, 280);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(408, 80);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "消费信息";
            // 
            // txtjf
            // 
            this.txtjf.Location = new System.Drawing.Point(276, 45);
            this.txtjf.Name = "txtjf";
            this.txtjf.ReadOnly = true;
            this.txtjf.Size = new System.Drawing.Size(98, 23);
            this.txtjf.TabIndex = 93;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(228, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 14);
            this.label19.TabIndex = 7;
            this.label19.Text = "积分:";
            // 
            // txtljcz
            // 
            this.txtljcz.Location = new System.Drawing.Point(276, 16);
            this.txtljcz.Name = "txtljcz";
            this.txtljcz.ReadOnly = true;
            this.txtljcz.Size = new System.Drawing.Size(98, 23);
            this.txtljcz.TabIndex = 91;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(200, 19);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 14);
            this.label18.TabIndex = 7;
            this.label18.Text = "累计充值:";
            // 
            // txtxfcs
            // 
            this.txtxfcs.Location = new System.Drawing.Point(86, 45);
            this.txtxfcs.Name = "txtxfcs";
            this.txtxfcs.ReadOnly = true;
            this.txtxfcs.Size = new System.Drawing.Size(98, 23);
            this.txtxfcs.TabIndex = 92;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 14);
            this.label17.TabIndex = 7;
            this.label17.Text = "消费次数:";
            // 
            // txtljxf
            // 
            this.txtljxf.Location = new System.Drawing.Point(86, 16);
            this.txtljxf.Name = "txtljxf";
            this.txtljxf.ReadOnly = true;
            this.txtljxf.Size = new System.Drawing.Size(98, 23);
            this.txtljxf.TabIndex = 90;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 14);
            this.label16.TabIndex = 7;
            this.label16.Text = "累计消费:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cobxzjlx);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtbzsm);
            this.groupBox2.Controls.Add(this.txtxm);
            this.groupBox2.Controls.Add(this.txtgzdw);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtzjhm);
            this.groupBox2.Controls.Add(this.txtdh);
            this.groupBox2.Controls.Add(this.dtpjsrq);
            this.groupBox2.Controls.Add(this.txtdz);
            this.groupBox2.Controls.Add(this.dtpcsrq);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.dtpksrq);
            this.groupBox2.Controls.Add(this.txtkh);
            this.groupBox2.Controls.Add(this.cobxklx);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cobxzt);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cobxxb);
            this.groupBox2.Location = new System.Drawing.Point(4, -3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(563, 367);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            // 
            // HuiYuanMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(573, 445);
            this.Controls.Add(this.groupBox2);
            this.Name = "HuiYuanMaintain";
            this.Text = "会员卡维护";
            this.Controls.SetChildIndex(this.groupBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtkh;
        private System.Windows.Forms.TextBox txtxm;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cobxxb;
        private System.Windows.Forms.TextBox txtdh;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtdz;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cobxklx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cobxzt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpksrq;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtpjsrq;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpcsrq;
        private System.Windows.Forms.ComboBox cobxzjlx;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtzjhm;
        private System.Windows.Forms.TextBox txtgzdw;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtjf;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtljcz;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtxfcs;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtljxf;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}
