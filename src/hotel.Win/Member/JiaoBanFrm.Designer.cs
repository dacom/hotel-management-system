﻿namespace hotel.Win.Member
{
    partial class JiaoBanFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbsbyj = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbsbye = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btndyjkb = new System.Windows.Forms.Button();
            this.btnclose = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lbfzfxf = new System.Windows.Forms.Label();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.txtxf = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtsj = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.lbtfsdd = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbjjry = new System.Windows.Forms.Label();
            this.lbjjje = new System.Windows.Forms.Label();
            this.lbzmyj = new System.Windows.Forms.Label();
            this.lbtfs = new System.Windows.Forms.Label();
            this.lbkfs = new System.Windows.Forms.Label();
            this.lbydqxj = new System.Windows.Forms.Label();
            this.lbqdgz = new System.Windows.Forms.Label();
            this.lbbbsr = new System.Windows.Forms.Label();
            this.txtdbsj = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.maingrid = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnbdy = new System.Windows.Forms.Button();
            this.btnquery = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.mainTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtxf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsj)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTab
            // 
            this.mainTab.Controls.Add(this.tabPage1);
            this.mainTab.Controls.Add(this.tabPage2);
            this.mainTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTab.Location = new System.Drawing.Point(0, 0);
            this.mainTab.Name = "mainTab";
            this.mainTab.SelectedIndex = 0;
            this.mainTab.Size = new System.Drawing.Size(801, 517);
            this.mainTab.TabIndex = 0;
            this.mainTab.SelectedIndexChanged += new System.EventHandler(this.mainTab_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.btndyjkb);
            this.tabPage1.Controls.Add(this.btnclose);
            this.tabPage1.Controls.Add(this.btnok);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.txtdbsj);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(793, 490);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "新增交班";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbsbyj);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.lbsbye);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(20, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(454, 51);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "上个班交接情况";
            // 
            // lbsbyj
            // 
            this.lbsbyj.AutoSize = true;
            this.lbsbyj.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbsbyj.Location = new System.Drawing.Point(237, 19);
            this.lbsbyj.Name = "lbsbyj";
            this.lbsbyj.Size = new System.Drawing.Size(21, 14);
            this.lbsbyj.TabIndex = 5;
            this.lbsbyj.Text = "88";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(155, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 14);
            this.label15.TabIndex = 4;
            this.label15.Text = "账面押金：";
            // 
            // lbsbye
            // 
            this.lbsbye.AutoSize = true;
            this.lbsbye.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbsbye.Location = new System.Drawing.Point(92, 19);
            this.lbsbye.Name = "lbsbye";
            this.lbsbye.Size = new System.Drawing.Size(23, 14);
            this.lbsbye.TabIndex = 3;
            this.lbsbye.Text = "88";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 2;
            this.label4.Text = "上班余额：";
            // 
            // btndyjkb
            // 
            this.btndyjkb.Location = new System.Drawing.Point(328, 400);
            this.btndyjkb.Name = "btndyjkb";
            this.btndyjkb.Size = new System.Drawing.Size(92, 32);
            this.btndyjkb.TabIndex = 5;
            this.btndyjkb.Text = "打印交款表";
            this.btndyjkb.UseVisualStyleBackColor = true;
            this.btndyjkb.Click += new System.EventHandler(this.btndyjkb_Click);
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(220, 400);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(84, 32);
            this.btnclose.TabIndex = 4;
            this.btnclose.Text = "关 闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(130, 400);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(84, 32);
            this.btnok.TabIndex = 4;
            this.btnok.Text = "确定交班";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.lbfzfxf);
            this.groupBox2.Controls.Add(this.txtbzsm);
            this.groupBox2.Controls.Add(this.txtxf);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtsj);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.lbtfsdd);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.lbjjry);
            this.groupBox2.Controls.Add(this.lbjjje);
            this.groupBox2.Controls.Add(this.lbzmyj);
            this.groupBox2.Controls.Add(this.lbtfs);
            this.groupBox2.Controls.Add(this.lbkfs);
            this.groupBox2.Controls.Add(this.lbydqxj);
            this.groupBox2.Controls.Add(this.lbqdgz);
            this.groupBox2.Controls.Add(this.lbbbsr);
            this.groupBox2.Location = new System.Drawing.Point(20, 97);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(454, 286);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "当班营业情况";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(34, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 14);
            this.label12.TabIndex = 5;
            this.label12.Text = "非住房收入：";
            // 
            // lbfzfxf
            // 
            this.lbfzfxf.AutoSize = true;
            this.lbfzfxf.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbfzfxf.Location = new System.Drawing.Point(125, 48);
            this.lbfzfxf.Name = "lbfzfxf";
            this.lbfzfxf.Size = new System.Drawing.Size(23, 14);
            this.lbfzfxf.TabIndex = 4;
            this.lbfzfxf.Text = "88";
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(94, 232);
            this.txtbzsm.Multiline = true;
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(276, 48);
            this.txtbzsm.TabIndex = 3;
            // 
            // txtxf
            // 
            this.txtxf.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtxf.Location = new System.Drawing.Point(112, 108);
            this.txtxf.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtxf.Name = "txtxf";
            this.txtxf.Size = new System.Drawing.Size(155, 23);
            this.txtxf.TabIndex = 1;
            this.txtxf.ValueChanged += new System.EventHandler(this.txtsj_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(29, 232);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 1;
            this.label11.Text = "备注说明：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 200);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 14);
            this.label10.TabIndex = 1;
            this.label10.Text = "交接人员：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 174);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 1;
            this.label9.Text = "交接金额：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(32, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 1;
            this.label8.Text = "账面押金：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(18, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 14);
            this.label7.TabIndex = 1;
            this.label7.Text = "下放营业款：";
            // 
            // txtsj
            // 
            this.txtsj.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtsj.Location = new System.Drawing.Point(112, 78);
            this.txtsj.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtsj.Name = "txtsj";
            this.txtsj.Size = new System.Drawing.Size(155, 23);
            this.txtsj.TabIndex = 0;
            this.txtsj.ValueChanged += new System.EventHandler(this.txtsj_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(18, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 14);
            this.label6.TabIndex = 1;
            this.label6.Text = "上交营业款：";
            // 
            // lbtfsdd
            // 
            this.lbtfsdd.AutoSize = true;
            this.lbtfsdd.Location = new System.Drawing.Point(217, 174);
            this.lbtfsdd.Name = "lbtfsdd";
            this.lbtfsdd.Size = new System.Drawing.Size(63, 14);
            this.lbtfsdd.TabIndex = 1;
            this.lbtfsdd.Text = "退房数：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(217, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 14);
            this.label13.TabIndex = 1;
            this.label13.Text = "开房数：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(197, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 14);
            this.label14.TabIndex = 1;
            this.label14.Text = "预定取消：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(197, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 14);
            this.label17.TabIndex = 1;
            this.label17.Text = "单位挂帐：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 14);
            this.label5.TabIndex = 1;
            this.label5.Text = "本班客房收入：";
            // 
            // lbjjry
            // 
            this.lbjjry.AutoSize = true;
            this.lbjjry.Location = new System.Drawing.Point(109, 200);
            this.lbjjry.Name = "lbjjry";
            this.lbjjry.Size = new System.Drawing.Size(21, 14);
            this.lbjjry.TabIndex = 1;
            this.lbjjry.Text = "88";
            // 
            // lbjjje
            // 
            this.lbjjje.AutoSize = true;
            this.lbjjje.Font = new System.Drawing.Font("SimSun", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbjjje.ForeColor = System.Drawing.Color.Red;
            this.lbjjje.Location = new System.Drawing.Point(105, 174);
            this.lbjjje.Name = "lbjjje";
            this.lbjjje.Size = new System.Drawing.Size(31, 19);
            this.lbjjje.TabIndex = 1;
            this.lbjjje.Text = "88";
            // 
            // lbzmyj
            // 
            this.lbzmyj.AutoSize = true;
            this.lbzmyj.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbzmyj.Location = new System.Drawing.Point(109, 146);
            this.lbzmyj.Name = "lbzmyj";
            this.lbzmyj.Size = new System.Drawing.Size(26, 16);
            this.lbzmyj.TabIndex = 1;
            this.lbzmyj.Text = "88";
            // 
            // lbtfs
            // 
            this.lbtfs.AutoSize = true;
            this.lbtfs.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbtfs.Location = new System.Drawing.Point(280, 174);
            this.lbtfs.Name = "lbtfs";
            this.lbtfs.Size = new System.Drawing.Size(23, 14);
            this.lbtfs.TabIndex = 1;
            this.lbtfs.Text = "88";
            // 
            // lbkfs
            // 
            this.lbkfs.AutoSize = true;
            this.lbkfs.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbkfs.Location = new System.Drawing.Point(280, 149);
            this.lbkfs.Name = "lbkfs";
            this.lbkfs.Size = new System.Drawing.Size(23, 14);
            this.lbkfs.TabIndex = 1;
            this.lbkfs.Text = "88";
            // 
            // lbydqxj
            // 
            this.lbydqxj.AutoSize = true;
            this.lbydqxj.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbydqxj.Location = new System.Drawing.Point(280, 23);
            this.lbydqxj.Name = "lbydqxj";
            this.lbydqxj.Size = new System.Drawing.Size(23, 14);
            this.lbydqxj.TabIndex = 1;
            this.lbydqxj.Text = "88";
            // 
            // lbqdgz
            // 
            this.lbqdgz.AutoSize = true;
            this.lbqdgz.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbqdgz.Location = new System.Drawing.Point(280, 48);
            this.lbqdgz.Name = "lbqdgz";
            this.lbqdgz.Size = new System.Drawing.Size(23, 14);
            this.lbqdgz.TabIndex = 1;
            this.lbqdgz.Text = "88";
            // 
            // lbbbsr
            // 
            this.lbbbsr.AutoSize = true;
            this.lbbbsr.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbbbsr.Location = new System.Drawing.Point(125, 23);
            this.lbbbsr.Name = "lbbbsr";
            this.lbbbsr.Size = new System.Drawing.Size(23, 14);
            this.lbbbsr.TabIndex = 1;
            this.lbbbsr.Text = "88";
            // 
            // txtdbsj
            // 
            this.txtdbsj.Location = new System.Drawing.Point(99, 68);
            this.txtdbsj.Name = "txtdbsj";
            this.txtdbsj.ReadOnly = true;
            this.txtdbsj.Size = new System.Drawing.Size(375, 23);
            this.txtdbsj.TabIndex = 1;
            this.txtdbsj.Text = "2010-0707  2010-0707 ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "当班时间：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.maingrid);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(793, 490);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "交班查询";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // maingrid
            // 
            this.maingrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.maingrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maingrid.Location = new System.Drawing.Point(3, 60);
            this.maingrid.Name = "maingrid";
            this.maingrid.ReadOnly = true;
            this.maingrid.RowHeadersWidth = 25;
            this.maingrid.RowTemplate.Height = 23;
            this.maingrid.Size = new System.Drawing.Size(787, 427);
            this.maingrid.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnbdy);
            this.groupBox1.Controls.Add(this.btnquery);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(787, 57);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询条件";
            // 
            // btnbdy
            // 
            this.btnbdy.Location = new System.Drawing.Point(500, 15);
            this.btnbdy.Name = "btnbdy";
            this.btnbdy.Size = new System.Drawing.Size(196, 36);
            this.btnbdy.TabIndex = 10;
            this.btnbdy.Tag = "0";
            this.btnbdy.Text = "重新打印最后一次交款表";
            this.btnbdy.UseVisualStyleBackColor = true;
            this.btnbdy.Click += new System.EventHandler(this.btnbdy_Click);
            // 
            // btnquery
            // 
            this.btnquery.Location = new System.Drawing.Point(403, 16);
            this.btnquery.Name = "btnquery";
            this.btnquery.Size = new System.Drawing.Size(91, 35);
            this.btnquery.TabIndex = 9;
            this.btnquery.Tag = "0";
            this.btnquery.Text = "开始查询";
            this.btnquery.UseVisualStyleBackColor = true;
            this.btnquery.Click += new System.EventHandler(this.btnquery_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(258, 22);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(130, 23);
            this.dateTimePicker2.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(231, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 14);
            this.label2.TabIndex = 7;
            this.label2.Text = "到";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(98, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 23);
            this.dateTimePicker1.TabIndex = 6;
            this.dateTimePicker1.Value = new System.DateTime(2010, 12, 30, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "交班日期从";
            // 
            // JiaoBanFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 517);
            this.Controls.Add(this.mainTab);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "JiaoBanFrm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "交班管理";
            this.mainTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtxf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsj)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl mainTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView maingrid;
        private System.Windows.Forms.Button btnquery;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtdbsj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown txtxf;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown txtsj;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbjjry;
        private System.Windows.Forms.Label lbjjje;
        private System.Windows.Forms.Label lbzmyj;
        private System.Windows.Forms.Label lbbbsr;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbqdgz;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbtfsdd;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbtfs;
        private System.Windows.Forms.Label lbkfs;
        private System.Windows.Forms.Button btndyjkb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbydqxj;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbfzfxf;
        private System.Windows.Forms.Button btnbdy;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbsbye;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbsbyj;
        private System.Windows.Forms.Label label15;
    }
}