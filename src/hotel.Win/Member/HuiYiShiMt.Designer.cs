﻿namespace hotel.Win.Member
{
    partial class HuiYiShiMt
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtmc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtzws = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cobxlx = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtxsf = new System.Windows.Forms.NumericUpDown();
            this.txtwz = new System.Windows.Forms.TextBox();
            this.txtms = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtzws)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtxsf)).BeginInit();
            this.SuspendLayout();
            // 
            // txtmc
            // 
            this.txtmc.Location = new System.Drawing.Point(107, 22);
            this.txtmc.Name = "txtmc";
            this.txtmc.Size = new System.Drawing.Size(188, 23);
            this.txtmc.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "名称:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 14);
            this.label2.TabIndex = 8;
            this.label2.Text = "坐位数:";
            // 
            // txtzws
            // 
            this.txtzws.Location = new System.Drawing.Point(107, 51);
            this.txtzws.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtzws.Name = "txtzws";
            this.txtzws.Size = new System.Drawing.Size(120, 23);
            this.txtzws.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 14);
            this.label3.TabIndex = 8;
            this.label3.Text = "描述:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "类型:";
            // 
            // cobxlx
            // 
            this.cobxlx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxlx.FormattingEnabled = true;
            this.cobxlx.Items.AddRange(new object[] {
            "大型",
            "中型",
            "小型"});
            this.cobxlx.Location = new System.Drawing.Point(106, 110);
            this.cobxlx.Name = "cobxlx";
            this.cobxlx.Size = new System.Drawing.Size(121, 21);
            this.cobxlx.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 14);
            this.label5.TabIndex = 8;
            this.label5.Text = "每小时费用:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(59, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 8;
            this.label6.Text = "位置:";
            // 
            // txtxsf
            // 
            this.txtxsf.Location = new System.Drawing.Point(107, 80);
            this.txtxsf.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtxsf.Name = "txtxsf";
            this.txtxsf.Size = new System.Drawing.Size(120, 23);
            this.txtxsf.TabIndex = 2;
            // 
            // txtwz
            // 
            this.txtwz.Location = new System.Drawing.Point(106, 137);
            this.txtwz.Name = "txtwz";
            this.txtwz.Size = new System.Drawing.Size(188, 23);
            this.txtwz.TabIndex = 4;
            // 
            // txtms
            // 
            this.txtms.Location = new System.Drawing.Point(106, 170);
            this.txtms.Multiline = true;
            this.txtms.Name = "txtms";
            this.txtms.Size = new System.Drawing.Size(188, 58);
            this.txtms.TabIndex = 5;
            // 
            // HuiYiShiMt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(428, 333);
            this.Controls.Add(this.txtmc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtwz);
            this.Controls.Add(this.txtzws);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cobxlx);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtms);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtxsf);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Name = "HuiYiShiMt";
            this.Text = "会议室基本信息";
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtxsf, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.txtms, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.cobxlx, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtzws, 0);
            this.Controls.SetChildIndex(this.txtwz, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.txtmc, 0);
            ((System.ComponentModel.ISupportInitialize)(this.txtzws)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtxsf)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtmc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown txtzws;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cobxlx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown txtxsf;
        private System.Windows.Forms.TextBox txtwz;
        private System.Windows.Forms.TextBox txtms;
    }
}
