﻿namespace hotel.Win.Member
{
    partial class HuiYuanTypeFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btndel = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtjf = new System.Windows.Forms.NumericUpDown();
            this.txtzk = new System.Windows.Forms.NumericUpDown();
            this.txtbzsm = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtlx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.maingrid = new System.Windows.Forms.DataGridView();
            this.HYK_LX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnclose = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtjf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtzk)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnclose);
            this.groupBox1.Controls.Add(this.btndel);
            this.groupBox1.Controls.Add(this.btnsave);
            this.groupBox1.Controls.Add(this.btnadd);
            this.groupBox1.Controls.Add(this.txtjf);
            this.groupBox1.Controls.Add(this.txtzk);
            this.groupBox1.Controls.Add(this.txtbzsm);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtlx);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(779, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "会员卡类型维护";
            // 
            // btndel
            // 
            this.btndel.Location = new System.Drawing.Point(537, 56);
            this.btndel.Name = "btndel";
            this.btndel.Size = new System.Drawing.Size(75, 23);
            this.btndel.TabIndex = 6;
            this.btndel.Text = "删除";
            this.btndel.UseVisualStyleBackColor = true;
            this.btndel.Click += new System.EventHandler(this.btndel_Click);
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(456, 56);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 23);
            this.btnsave.TabIndex = 5;
            this.btnsave.Text = "保存";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnadd
            // 
            this.btnadd.Location = new System.Drawing.Point(375, 56);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(75, 23);
            this.btnadd.TabIndex = 4;
            this.btnadd.Text = "增加";
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // txtjf
            // 
            this.txtjf.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtjf.Location = new System.Drawing.Point(374, 21);
            this.txtjf.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtjf.Name = "txtjf";
            this.txtjf.Size = new System.Drawing.Size(70, 23);
            this.txtjf.TabIndex = 2;
            // 
            // txtzk
            // 
            this.txtzk.DecimalPlaces = 2;
            this.txtzk.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.txtzk.Location = new System.Drawing.Point(241, 20);
            this.txtzk.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtzk.Name = "txtzk";
            this.txtzk.Size = new System.Drawing.Size(56, 23);
            this.txtzk.TabIndex = 2;
            // 
            // txtbzsm
            // 
            this.txtbzsm.Location = new System.Drawing.Point(82, 55);
            this.txtbzsm.Name = "txtbzsm";
            this.txtbzsm.Size = new System.Drawing.Size(217, 23);
            this.txtbzsm.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "备注说明";
            // 
            // txtlx
            // 
            this.txtlx.Location = new System.Drawing.Point(82, 20);
            this.txtlx.Name = "txtlx";
            this.txtlx.Size = new System.Drawing.Size(112, 23);
            this.txtlx.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(305, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "积分总数";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(206, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "折扣";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "类型";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.maingrid);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 94);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(779, 409);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "会员卡类型";
            // 
            // maingrid
            // 
            this.maingrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.maingrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HYK_LX,
            this.Column1,
            this.Column2,
            this.Column3});
            this.maingrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maingrid.Location = new System.Drawing.Point(3, 19);
            this.maingrid.Name = "maingrid";
            this.maingrid.ReadOnly = true;
            this.maingrid.RowHeadersWidth = 25;
            this.maingrid.RowTemplate.Height = 23;
            this.maingrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.maingrid.Size = new System.Drawing.Size(773, 387);
            this.maingrid.TabIndex = 2;
            this.maingrid.SelectionChanged += new System.EventHandler(this.maingrid_SelectionChanged);
            // 
            // HYK_LX
            // 
            this.HYK_LX.DataPropertyName = "HYK_LX";
            this.HYK_LX.HeaderText = "类型";
            this.HYK_LX.Name = "HYK_LX";
            this.HYK_LX.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ZK";
            this.Column1.FillWeight = 80F;
            this.Column1.HeaderText = "折扣";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "JFZS";
            this.Column2.HeaderText = "积分总数";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "BZSM";
            this.Column3.HeaderText = "备注说明";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(618, 55);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(88, 23);
            this.btnclose.TabIndex = 8;
            this.btnclose.Text = "关  闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // HuiYuanTypeFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 503);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "HuiYuanTypeFrm";
            this.Text = "会员卡类型";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtjf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtzk)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView maingrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn HYK_LX;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.TextBox txtlx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbzsm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown txtjf;
        private System.Windows.Forms.NumericUpDown txtzk;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btndel;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnclose;
    }
}