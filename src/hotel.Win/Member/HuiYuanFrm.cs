﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using hotel.Win.BaseData;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;

namespace hotel.Win.Member
{
    /// <summary>
    /// 会员卡管理
    /// </summary>
    public partial class HuiYuanFrm : Form
    {
        public HuiYuanFrm()
        {
            InitializeComponent();
            cobxlx.DisplayMember = "HYK_LX";
            cobxlx.DataSource = MyDataContext.GetDataContext.HYKLX.AsNoTracking().OrderBy(p => p.HYK_LX).ToList();           
            if (cobxlx.Items.Count > 0)
                cobxlx.SelectedIndex = 0;
            ControlsUtility.ResetColumnFromXML(maingrid);
        }
        private void BindData(string lx)
        {            
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = MyDataContext.GetDataContext.HYK.AsNoTracking().Where(p=>p.HYKLX.HYK_LX == lx).ToList();
            statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
        }
        private void cobxlx_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData(cobxlx.Text);
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            string id = Common.ControlsUtility.GetActiveGridValue(maingrid, "KH");
            HuiYuanMaintain frm;
            if (id == "")
            {                
                frm = new HuiYuanMaintain(true);
            }
            else
                frm = new HuiYuanMaintain(id);
            frm.CallBack = (p) => { cobxlx_SelectedIndexChanged(null, null); };
            frm.ShowDialog();           
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
