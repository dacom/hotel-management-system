﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;
namespace hotel.Win.Member
{
    public partial class HuiYuanTypeFrm : Form
    {
        bool IsNew;
        hotelEntities _context;
        HYKLX model;
        public HuiYuanTypeFrm()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            IsNew = false;
            BindData();
        }

        private void BindData()
        {
            btnsave.Enabled = false;
            btndel.Enabled = false;
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = _context.HYKLX.AsNoTracking().OrderBy(p => p.HYK_LX).ToList();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            btnsave.Enabled = true;
            btndel.Enabled = false;
            IsNew = true;
            txtlx.Text = "";
            txtlx.ReadOnly = false;
            txtzk.Value = 1;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = (!IsNew ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = (!IsNew ? "修改" : "增加") + "会员卡类型[" + txtlx.Text + "]";
            logInfo.Save();
            #endregion
            
            if (IsNew)
            {
                if (txtlx.Text.Length == 0)
                    return;
                if (_context.HYKLX.Count(p => p.HYK_LX == txtlx.Text) > 0)
                {
                    MessageBox.Show(txtlx.Text + "类型已经存在！");
                    return;
                }
                model = new HYKLX { HYK_LX = txtlx.Text};
                _context.HYKLX.Add(model);
            }
            model.BZSM = txtbzsm.Text;
            model.JFZS = (int)txtjf.Value;
            model.ZK = txtzk.Value;
            _context.SaveChanges();
            btndel.Enabled = true;
            IsNew = false;
            BindData();
            MessageBox.Show("保存成功！");
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            if (_context.HYK.Count(p => p.HYKLX.HYK_LX == model.HYK_LX) > 0)
            {
                MessageBox.Show("已经有会员卡使用此类型，不能删除！");
                return;
            }
            _context.HYKLX.Remove(model);
            _context.SaveChanges();
            model = null;
            btnsave.Enabled = false;
            btndel.Enabled = false;
            BindData();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "会员卡[" + model.HYK_LX + "]";
            logInfo.Save();
            #endregion
        }

        private void maingrid_SelectionChanged(object sender, EventArgs e)
        {
            if (maingrid.CurrentRow == null)
                return;
            var d = maingrid.CurrentRow.DataBoundItem as HYKLX;
            model = _context.HYKLX.Find(d.HYK_LX);
            if (model == null)
                return;
            txtlx.ReadOnly = true;
            txtlx.Text = model.HYK_LX;
            txtzk.Value = (decimal)model.ZK;
            txtjf.Value = (decimal)model.JFZS;
            txtbzsm.Text = model.BZSM;
            btnsave.Enabled = true;
            btndel.Enabled = true;
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
