﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;

namespace hotel.Win.Member
{
    public partial class HuiYuanMaintain : hotel.MyUserControl.MaintainBaseForm
    {
        string Id;
        hotelEntities _context;
        HYK dataModel;
       /// <summary>
        /// 用于增加
        /// </summary>
        public HuiYuanMaintain(bool isAdd)
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = new hotelEntities();
            Id = "0";
            cobxklx.DataSource = _context.HYKLX.AsNoTracking().OrderBy(p => p.HYK_LX).ToList();
            cobxklx.DisplayMember = "HYK_LX";
            cobxzjlx.DataSource = XTCYDMDal.GetAllByLBDM(EnumXtcydmLB.ZJLX.ToString());
            cobxzjlx.DisplayMember = "XMMC";
            cobxzjlx.DisplayMember = "XMMC";
            if (isAdd)
            {
                base.IsEdit = false;
                base.DeleteButtonEnabled = false;
                base.StatusMessage = "数据处于增加状态";
                Add();                
            }
        }       
       
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public HuiYuanMaintain(string id)
            : this(false)
        {
            BindData(id);
        }

        private void BindData(string KH)
        {
            this.Id = KH;
            dataModel = _context.HYK.FirstOrDefault(p => p.KH == KH);
            if (dataModel == null)
                throw new Exception("没有找到数据KH=" + KH.ToString());
            txtkh.Text = dataModel.KH;
            txtxm.Text = dataModel.XM;
            cobxxb.Text = dataModel.XB;
            dtpcsrq.Value = (DateTime)dataModel.CSRQ;
            txtdh.Text = dataModel.DH;
            txtdz.Text = dataModel.DZ;
            cobxklx.Text = dataModel.HYKLX.HYK_LX;
            cobxzt.Text = dataModel.ZT;
            dtpksrq.Value = (DateTime)dataModel.KSRQ;
            dtpjsrq.Value = (DateTime)dataModel.ZZRQ;
            cobxzjlx.Text = dataModel.ZJLX;
            txtzjhm.Text = dataModel.ZJHM;
            txtgzdw.Text = dataModel.GZDW;
            txtbzsm.Text = dataModel.BZSM;

            //消费
            txtljxf.Text = dataModel.LJXF.ToString();
            txtxfcs.Text = dataModel.XFCS.ToString();
            txtjf.Text = dataModel.JF.ToString();
            txtljcz.Text = dataModel.LJCZ.ToString();
            IsEdit = true;
            StatusMessage = "修改数据状态";

        }

        private bool Add()
        {
            txtkh.Text ="K" + _context.GetNextValue("HYKH").First().ToString().PadLeft(5,'0');
            dataModel = new HYK { KH = txtkh.Text };
            cobxxb.SelectedIndex = 0;
            cobxklx.SelectedIndex = 0;
            cobxzjlx.SelectedIndex = 0;
            cobxzt.SelectedIndex = 0;
            dtpcsrq.Value = DateTime.Now.AddYears(-30);
            dtpksrq.Value = DateTime.Now.Date;
            dtpjsrq.Value = DateTime.Now.AddDays(90);
            return true;
        }
        private bool Delete()
        {
            _context.HYK.Remove(dataModel);
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "删除会员[" + dataModel.XM + "]";
            logInfo.Save();
            #endregion
            return true;
        }

        private bool CheckData()
        {
            var info = ValidataHelper.StartVerify(txtkh.Text)
            .IsNotNullOrEmpty("卡号不能为空输入为空")
            .Max(18, "卡号长度不能大于18个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
             info = ValidataHelper.StartVerify(txtxm.Text)
            .IsNotNullOrEmpty("姓名不能为空输入为空")
            .Max(12, "姓名长度不能大于12个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }            
            if (!IsEdit)
            {
                if (_context.HYK.Where(p => p.KH == txtkh.Text).Count() > 0)
                {
                    MessageBox.Show("卡号" + txtkh.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false ;
            dataModel.BZSM = txtbzsm.Text;
            dataModel.CJRQ = DateTime.Now;
            dataModel.CJRY = UserLoginInfo.FullName;
            dataModel.CSRQ = dtpcsrq.Value;
            dataModel.DH = txtdh.Text;
            dataModel.DZ = txtdz.Text;
            dataModel.GZDW = txtgzdw.Text;
            dataModel.KSRQ = dtpksrq.Value;
            dataModel.XB = cobxxb.Text;
            dataModel.XM = txtxm.Text;
            dataModel.ZJHM = txtzjhm.Text;
            dataModel.ZJLX = cobxzjlx.Text;
            dataModel.ZT = cobxzt.Text;
            dataModel.ZZRQ = dtpjsrq.Value;
            dataModel.HYKLX = _context.HYKLX.FirstOrDefault(p => p.HYK_LX == cobxklx.Text);
            
            if (IsEdit)
            {                
                dataModel.XGRQ = DateTime.Now;
                dataModel.XGRY = UserLoginInfo.FullName;
            }
            else
            {
                dataModel.KH = txtkh.Text;
                dataModel.XFCS = 0;
                dataModel.MM = "";
                dataModel.LJCZ = 0;
                dataModel.JF = 0;
                dataModel.LJXF = 0;
                _context.HYK.Add(dataModel);
            }
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = (IsEdit ? "修改" : "增加") + "会员[" + dataModel.XM + "]";
            logInfo.Save();
            #endregion
            _context.SaveChanges();


            return true;
        }
    }
}
