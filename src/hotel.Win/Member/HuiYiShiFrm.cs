﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;


namespace hotel.Win.Member
{
    /// <summary>
    /// 会议室使用记录
    /// </summary>
    public partial class HuiYiShiFrm : Form
    {
        private hotelEntities _context;
        Action RefreshData;
        public HuiYiShiFrm()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            btnyd.Tag = "0";
            ControlsUtility.ResetColumnFromXML(maingrid);
            dateTimePicker1.Value = DateTime.Now.AddDays(-7).Date;
            dateTimePicker2.Value = DateTime.Now.Date;
            dtprq.Value = DateTime.Now.Date;
            dtpkssj.Value = DateTime.Now;
            txtts.Value = 2M;
            InitData(true);
            btnqxyd.Enabled = false;
            btnqdrz.Enabled = false;
        }

        private void InitData(bool flag)
        {
            if (!flag)
                return;
            cobxhys.DataSource = _context.HYS.AsNoTracking().OrderBy(p => p.MC).ToList();
            cobxhys.DisplayMember = "MC";
        }

        private void btnedithys_Click(object sender, EventArgs e)
        {
            HuiYiShiMt frm;
            if (cobxhys.SelectedItem == null)
                frm = new HuiYiShiMt(true);
            else
                frm = new HuiYiShiMt((cobxhys.SelectedItem as HYS).ID);
            frm.CallBack = InitData;
            frm.ShowDialog();
        }

        private void btnquery_Click(object sender, EventArgs e)
        {
            btnqxyd.Enabled = false;
            btnqdrz.Enabled = false;
            var rq2 = dateTimePicker2.Value.AddHours(23.9);
            var qry = from q in _context.HYS_SYJL
                      where q.KSSJ >= dateTimePicker1.Value && q.KSSJ <= rq2
                      orderby q.CJRQ descending
                      select new
                                   {
                                       q.ID,
                                       HYS_MC = q.HYS.MC,
                                       q.BZSM,
                                       q.CJRQ,
                                       q.CJRY,
                                       q.HYMC,
                                       q.HYRS,
                                       q.JE,
                                       q.JSSJ,
                                       q.KSSJ,
                                       q.LXDH,
                                       q.LXR,
                                       q.XGRQ,
                                       q.XGRY,
                                       q.YJ,
                                       q.ZT
                                   };
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            RefreshData = () => { btnquery_Click(null, null); };
        }

        private void btnyd_Click(object sender, EventArgs e)
        {
            btnqxyd.Enabled = false;
            btnqdrz.Enabled = false;
            btnyd.Tag = "1";
            var qry = from q in _context.HYS_SYJL
                      where q.ZT == "预订"
                      orderby q.CJRQ descending
                      select new
                                   {
                                       q.ID,
                                       HYS_MC = q.HYS.MC,
                                       q.BZSM,
                                       q.CJRQ,
                                       q.CJRY,
                                       q.HYMC,
                                       q.HYRS,
                                       q.JE,
                                       q.JSSJ,
                                       q.KSSJ,
                                       q.LXDH,
                                       q.LXR,
                                       q.XGRQ,
                                       q.XGRY,
                                       q.YJ,
                                       q.ZT
                                   };
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            RefreshData = () => { btnyd_Click(null, null); };
        }

        private void mainTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mainTab.SelectedIndex == 1 && btnyd.Tag.ToString() == "0")
                btnyd_Click(null, null);
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            string zt = "";
            if (rbthyd.Checked)
                zt = "预订";
            else if (rbtnrz.Checked)
                zt = "入帐";
            else
            {
                MessageBox.Show("请正确选择预订或入账");
                return;
            }
            if (cobxhys.SelectedItem == null)
            {
                MessageBox.Show("请正确选择会议室!");
                return;
            }
            int hysid = (cobxhys.SelectedValue as HYS).ID;
            var hys = _context.HYS.First(p => p.ID == hysid);
            var dataModel = new HYS_SYJL();
            dataModel.BZSM = txtbzsm.Text;
            dataModel.CJRQ = DateTime.Now;
            dataModel.CJRY = UserLoginInfo.FullName;
            dataModel.HYMC = txthymc.Text;
            dataModel.HYRS = (int)txthyrs.Value;
            dataModel.HYS = hys;
            dataModel.JE = (int)txtfy.Value;
            dataModel.KSSJ = Convert.ToDateTime(dtprq.Value.Date.ToString("yyyy-MM-dd") + " " + dtpkssj.Value.ToShortTimeString());
            dataModel.JSSJ = Convert.ToDateTime(lbjssj.Text);
            dataModel.LXDH = txtlxdh.Text;
            dataModel.LXR = txtlxr.Text;
            dataModel.YJ = (int)txtyj.Value;
            dataModel.ZT = zt;
            if (dataModel.JSSJ < dataModel.KSSJ)
            {
                MessageBox.Show("结束日期不小于开始日期");
                return;
            }
            if (zt == "预订")
            {
                if (_context.HYS_SYJL.Count(p => p.ZT == zt && dataModel.KSSJ >= p.KSSJ && dataModel.KSSJ <= p.JSSJ) > 0)
                {
                    MessageBox.Show("此开始时间，已经有人预订此会议室，请查证！");
                    return;
                }
                if (_context.HYS_SYJL.Count(p => p.ZT == zt && dataModel.JSSJ >= p.KSSJ && dataModel.JSSJ <= p.JSSJ) > 0)
                {
                    MessageBox.Show("此结束时间，已经有人预订此会议室，请查证！");
                    return;
                }
                if (_context.HYS_SYJL.Count(p => p.ZT == zt && p.KSSJ >= dataModel.KSSJ && p.KSSJ <= dataModel.JSSJ) > 0)
                {
                    MessageBox.Show("此开始时间，已经有人预订此会议室，请查证！");
                    return;
                }
                if (_context.HYS_SYJL.Count(p => p.ZT == zt && p.JSSJ >= dataModel.KSSJ && p.JSSJ <= dataModel.JSSJ) > 0)
                {
                    MessageBox.Show("此结束时间，已经有人预订此会议室，请查证！");
                    return;
                }
            }
            _context.HYS_SYJL.Add(dataModel);
            //消费记录
            if (zt == "入帐")
                InsertXFJL_ARC(txtfy.Value);
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.增加数据;
            logInfo.Type = LogTypeEnum.涉及金额;
            logInfo.Message = "增加会议室[" + zt + "],押金=" + dataModel.YJ.ToString() + ",费用=" +  dataModel.JE.ToString() ;
            logInfo.Save();
            #endregion
            _context.SaveChanges();
            MessageBox.Show("增加" + zt +"成功，请查证!");
        }

        /// <summary>
        /// 消费记录
        /// </summary>
        private void InsertXFJL_ARC(decimal xfje)
        {
            var xfjlModel = new XFJL { RZJL_ID = SysConsts.Room9999RZID, ROOM_ID = SysConsts.Room9999 };
            xfjlModel.CJRQ = DateTime.Now;
            xfjlModel.CJRY = UserLoginInfo.FullName;
            xfjlModel.DJ = xfje;
            xfjlModel.JE = xfje;
            xfjlModel.SL = 1;            
            xfjlModel.XFXM = _context.XFXM.First(p => p.DM == "HYS");//系统固定= HYS
            xfjlModel.XFXM_MC = xfjlModel.XFXM.MC;
            xfjlModel.XFBZ = "消";
            xfjlModel.YJ = 0;
            xfjlModel.ZFFS = "人民币";
            xfjlModel.BZSM = "会议室直接消费";
            _context.XFJL.Add(xfjlModel);
        }
        private void rbthyd_CheckedChanged(object sender, EventArgs e)
        {
            txtfy.Value = 0;
            txtfy.Enabled = false;
            txtyj.Enabled = true;
        }

        private void rbtnrz_CheckedChanged(object sender, EventArgs e)
        {
            txtyj.Value = 0;
            txtyj.Enabled = false;
            txtfy.Enabled = true;
        }

        private void txtts_ValueChanged(object sender, EventArgs e)
        {
            var d = Convert.ToDateTime(dtprq.Value.Date.ToString("yyyy-MM-dd") + " " + dtpkssj.Value.ToShortTimeString()).AddHours((double)txtts.Value);
            lbjssj.Text = d.ToString();
        }

        private void btnqdrz_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(ControlsUtility.GetActiveGridValue(maingrid, "ID"));
            var selmodel = _context.HYS_SYJL.First(p => p.ID == id);
            if (selmodel.ZT != "预订")
            {
                MessageBox.Show("不具备条件，必须为预订状态");
                return;
            }
            txtyj2.Value = (decimal)selmodel.YJ;
            txtfy2.Value = 0M;
            lbzl.Text = txtyj2.Value.ToString();
            Point formPoint = this.PointToClient(Control.MousePosition);
            formPoint.X = formPoint.X + 5;
            formPoint.Y = formPoint.Y + 5;
            gboxrz.Location = formPoint;
            gboxrz.Visible = true;
        }

        private void btnqxyd_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(ControlsUtility.GetActiveGridValue(maingrid, "ID"));
            var selmodel = _context.HYS_SYJL.First(p => p.ID == id);
            if (selmodel.ZT != "预订")
            {
                MessageBox.Show("不具备条件，必须为预订状态");
                return;
            }
            selmodel.ZT = "取消";
            _context.SaveChanges();
            MessageBox.Show("取消成功！");
            RefreshData();
        }

        private void maingrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnqxyd.Enabled = true;
            btnqdrz.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            gboxrz.Visible = false;
        }

        private void btnqzrz_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(ControlsUtility.GetActiveGridValue(maingrid, "ID"));
            var selmodel = _context.HYS_SYJL.First(p => p.ID == id);
            selmodel.ZT = "完成";
            selmodel.JE = txtfy2.Value;
            InsertXFJL_ARC(txtfy2.Value);
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.消费入单;
            logInfo.Type = LogTypeEnum.涉及金额;
            logInfo.Message = "会议室消费入账,押金=" + txtyj2.Value.ToString() + ",费用=" + txtfy2.Value.ToString();
            logInfo.Save();
            #endregion
            MessageBox.Show("入帐成功");
            gboxrz.Visible = false;
            RefreshData();
        }

        private void txtfy2_ValueChanged(object sender, EventArgs e)
        {
            lbzl.Text = (txtfy2.Value - txtfy2.Value).ToString();
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
