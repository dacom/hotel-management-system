﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;

namespace hotel.Win.Member
{
    /// <summary>
    /// 会议室基本信息维护
    /// </summary>
    public partial class HuiYiShiMt : hotel.MyUserControl.MaintainBaseForm
    {
        int Id;
        hotelEntities _context;
        HYS dataModel;

        /// <summary>
        /// 用于增加
        /// </summary>
        public HuiYiShiMt(bool isAdd)
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;
            _context = new hotelEntities();
            Id = 0;
            if (isAdd)
            {
                base.IsEdit = false;
                base.DeleteButtonEnabled = false;
                base.StatusMessage = "数据处于增加状态";
                Add();                
            }
        }       
       
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public HuiYiShiMt(int id)
            : this(false)
        {
            BindData(id);
        }

        private void BindData(int id)
        {
            this.Id = id;
            dataModel = _context.HYS.First(p => p.ID == id);
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());                        
            txtmc.Text = dataModel.MC;
            txtzws.Value = (int)dataModel.ZWS;
            txtxsf.Value = (int)dataModel.XSF;           
            cobxlx.Text = dataModel.LX;
            txtwz.Text = dataModel.WZ;
            txtms.Text = dataModel.MS;
            IsEdit = true;
            StatusMessage = "修改数据状态";

        }

        private bool Add()
        {
            dataModel = new HYS { ID = Id };
            return true;
        }
        private bool Delete()
        {
            int cnt = _context.HYS_SYJL.Where(p => p.HYS.ID == dataModel.ID).Count();
            if ( cnt> 0)
            {
                MessageBox.Show("此会议室已经被使用" + cnt.ToString() + "个次，不能删除！");
                return false;
            }

            _context.HYS.Remove(dataModel);
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "删除会议室,[" + dataModel.MC + "]";
            logInfo.Save();
            #endregion
            return true;
        }

        private bool CheckData()
        {
            var info = ValidataHelper.StartVerify(txtmc.Text)
            .IsNotNullOrEmpty("代码不能为空输入为空")
            .Max(28, "代码长度不能大于28个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
             info = ValidataHelper.StartVerify(txtmc.Text)
            .IsNotNullOrEmpty("名称不能为空输入为空")
            .Max(32, "名称长度不能大于32个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }           
            if (!IsEdit)
            {
                if (_context.HYS.Where(p => p.MC == txtmc.Text).Count() > 0)
                {
                    MessageBox.Show("名称" + txtmc.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false ;
            dataModel.MC = txtmc.Text;
            dataModel.LX = cobxlx.Text;
            dataModel.MS = txtms.Text;
            dataModel.WZ = txtwz.Text;            
            dataModel.XSF = (int)txtxsf.Value;
            dataModel.ZWS = (int)txtzws.Value;            
            if (IsEdit)
            {                
                //dataModel.XGRQ = DateTime.Now;
                //dataModel.XGRY = UserLoginInfo.FullName;                
            }
            else
            {
                _context.HYS.Add(dataModel);
                
            }
            _context.SaveChanges();
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = (IsEdit ? LogAbstractEnum.修改数据 : LogAbstractEnum.增加数据);
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = (IsEdit ? "修改" : "增加") + "会议室[" + dataModel.MC + "]";
            logInfo.Save();
            #endregion
            return true;
        }
    }
}
