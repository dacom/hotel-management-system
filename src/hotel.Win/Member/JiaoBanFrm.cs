﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Report;
namespace hotel.Win.Member
{   
    /// <summary>
    /// 交班管理
    /// </summary>
    public partial class JiaoBanFrm : Form
    {
        hotelEntities _context;
        DateTime sbsj;//上个班日期
        int jbid = 0;
        public JiaoBanFrm()
        {
            InitializeComponent();
            btndyjkb.Enabled = false;
            _context = MyDataContext.GetDataContext;
            ControlsUtility.ResetColumnFromXML(maingrid);
            dateTimePicker1.Value = DateTime.Now.Date;
            dateTimePicker2.Value = DateTime.Now.Date;
            sbsj = DateTime.Now;
            mainTab.SelectedIndex = 0;
            BindData();            
        }
         
        private void BindGridData()
        {            
            var rq2 = dateTimePicker2.Value.AddHours(23.9);
            var qry = from ys in _context.JBJL.AsNoTracking()
                      where ys.CJRQ >= dateTimePicker1.Value && ys.CJRQ <= rq2
                      orderby ys.CJRQ descending
                      select ys;
            btnquery.Tag = "1";
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            btnbdy.Enabled = maingrid.Rows.Count > 0;
        }

        private void btnquery_Click(object sender, EventArgs e)
        {
            BindGridData();
        }

        /// <summary>
        /// 绑定当班数据
        /// </summary>
        private void BindData()
        {
            int cnt = _context.JBJL.Count();                 
            if (cnt==0)
            {
                sbsj = Convert.ToDateTime(XTCSModel.GetXTCS.YYRQ);
                txtdbsj.Text = sbsj.ToString("yyyy-MM-dd HH:mm") + " 至 " + DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                lbsbye.Text = "0";
                lbsbyj.Text = "0";
            }
            else
            {
                var id = _context.JBJL.Max(p => p.ID);    
                var dmModel = _context.JBJL.FirstOrDefault(p => p.ID == id);
                sbsj = (DateTime)(dmModel.XB_JSSJ);
                txtdbsj.Text = sbsj.ToString("yyyy-MM-dd HH:mm") + " 至 " + DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                lbsbye.Text = (dmModel.JJJE - dmModel.ZMYJ).ToString();
                lbsbyj.Text = dmModel.ZMYJ.ToString();
            }
            //decimal? je = _context.XFJL.Where(p => p.CJRQ >= sbrq).Sum(p => p.YJ);
            //decimal yjsr = je == null ? 0 : (decimal)je;//押金收入

            decimal? je = _context.XFJL_ARC.Where(p => p.RZJL_ARC.JZRQ >= sbsj && p.RZJL_ARC.JZBZ == "是").Sum(p => p.JE);
            decimal jzsr = je == null ? 0 : (decimal)je;//结帐收入
            lbbbsr.Text = jzsr.ToString();
            //签单挂账
            je = _context.XYDW_GZ.Where(p => p.GZRQ >= sbsj).Sum(p => p.GZJE);
            lbqdgz.Text = (je == null ? 0 : (decimal)je).ToString();
            //预定取消金
            je = _context.UV_RZJL_XFJL_ARC.Where(p => p.JZRQ >= sbsj && p.JZBZ=="消").Sum(p => p.YJ);
            lbydqxj.Text = (je == null ? 0 : (decimal)je).ToString();
            //非住房消费
            je = _context.XFJL_ARC.Where(p => p.CJRQ >= sbsj && p.RZJL_ID == SysConsts.Room9999RZID).Sum(p => p.JE);
            lbfzfxf.Text = (je == null ? 0 : (decimal)je).ToString();
            //账面押金，考虑不结账退房的情况
            je = _context.UV_RZJL_YJ.Sum(p => p.YJ);
            lbzmyj.Text = (je == null ? 0 : (decimal)je).ToString();
            txtsj.Value = 0;
            txtxf.Value = 0;
            lbjjry.Text = UserLoginInfo.UserName;
            txtsj_ValueChanged(this, null);

            je = _context.RZJL.Count(p => p.CJRQ >= sbsj && p.RZBZ == "是");
            lbkfs.Text = (je == null ? 0 : (decimal)je).ToString();
            je = _context.RZJL_ARC.Count(p => p.JZRQ >= sbsj && p.JZBZ == "是");
            lbtfs.Text = (je == null ? 0 : (decimal)je).ToString();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("交班现金：" + lbjjje.Text + "\n你确定交班吗？", SysConsts.SysInformationCaption, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            var model = new JBJL();
            model.BBSR = Convert.ToDecimal(lbbbsr.Text);
            model.BZSM = txtbzsm.Text;
            model.CJRQ = DateTime.Now;
            model.GH = UserLoginInfo.UserId;
            model.JJJE = Convert.ToDecimal(lbjjje.Text);
            model.KFS = Convert.ToInt32(lbkfs.Text);
            model.TFS = Convert.ToInt32(lbtfs.Text);            
            model.SBJY = Convert.ToDecimal(lbsbye.Text);
            model.SJ_YYK = txtsj.Value;
            model.SB_KSSJ = sbsj;
            model.XB_JSSJ = DateTime.Now;
            model.XF_YYK = txtxf.Value;
            model.XM = UserLoginInfo.UserName;
            model.ZMYJ = Convert.ToDecimal(lbzmyj.Text);
            model.GZJE = Convert.ToDecimal(lbqdgz.Text);
            _context.JBJL.Add(model);
            _context.SaveChanges();
            jbid = model.ID;
            MessageBox.Show("交班成功，请打印交款表,打印后退出系统");
            btnok.Enabled = false;
            btndyjkb.Enabled = true;
            btnclose.Text = "退出系统";
            #region 日志纪录
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.员工交班;
            logInfo.Type = LogTypeEnum.涉及金额;
            logInfo.Message = "员工交班交接金额" + lbjjje.Text + ",上班余额=" + lbsbye.Text + ",本班收入=" + lbbbsr.Text +
                "，签单挂账=" + lbqdgz.Text + ",上交=" +txtsj.Value.ToString() + ",下发=" + txtxf.Value.ToString() +
                ",账目押金=" + lbzmyj.Text ;
            logInfo.Save();
            #endregion
            
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            if (jbid > 0)
                Application.Exit();
        }

        private void mainTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (btnquery.Tag.ToString() =="0")
                BindGridData();
        }

        private void txtsj_ValueChanged(object sender, EventArgs e)
        {
            decimal je = Convert.ToDecimal(lbsbye.Text) + Convert.ToDecimal(lbbbsr.Text) - txtsj.Value + txtxf.Value + Convert.ToDecimal(lbzmyj.Text) + Convert.ToDecimal(lbfzfxf.Text);
            lbjjje.Text = je.ToString();
        }

        private void btndyjkb_Click(object sender, EventArgs e)
        {
            if (jbid == 0)
                return;
            //营业日报,交款表
            ReportEntry.ShowYingYeRiBaoReport(jbid);
        }

        private void btnbdy_Click(object sender, EventArgs e)
        {            
            MessageBox.Show("此功能只用于重新打印最后一次交款表");
            var id = _context.JBJL.Max(p => p.ID);            
            ReportEntry.ShowYingYeRiBaoReport(id);
        }
    }
}
