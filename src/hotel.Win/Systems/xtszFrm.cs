﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
using hotel.MyUserControl;
using System.IO;
using System.Data.SqlClient;
namespace hotel.Win.Systems
{
    /// <summary>
    /// 系统参数设置
    /// </summary>
    public partial class xtszFrm : Form
    {
        hotelEntities _context;
        public xtszFrm()
        {
            InitializeComponent();
            UcRoomSet.GetUcRoomSet = null;
            XTCSModel.GetXTCS = null;
            _context = MyDataContext.GetDataContext;            
            tabMain.SelectedIndex = 0;
            BindData();
            txtyyrq.ReadOnly = !(UserLoginInfo.HaveRole(EnumRoole.Admin) || UserLoginInfo.HaveRole(EnumRoole.YeShen));
        }

        private void BindData()
        {
            XTCSModel xtcs = XTCSModel.GetXTCS;
            txtjdmc.Text = xtcs.JDMC;
            txtjddh.Text = xtcs.JDDH;
            chkdyyyd.Checked = xtcs.SFDY_YYD;
            txtyyrq.Text = xtcs.YYRQ;
            chkys.Checked = xtcs.SF_ZDYS;
            txtysip.Text = xtcs.ZDYS_IP;
            txtyssj.Text = xtcs.ZDYS_SJ;
            txtcs1.Text = xtcs.JBTFZ_SJ;
            txtcs2.Text = xtcs.JYTFZ_SJ;
            chkbf.Checked = xtcs.SF_ZDBF;
            txtbfip.Text = xtcs.ZDBF_IP;
            txtbflj.Text = xtcs.BFWJLJ;
            cobxbfgz.Text = xtcs.ZDBF_GZ;
        }
        /// <summary>
        /// 绑定房态设置
        /// </summary>
        private void BindRoomSet()
        {
            UcRoomSet ucRoomSet = UcRoomSet.GetUcRoomSet;
            txtkd.Value = ucRoomSet.Width;
            txtgd.Value = ucRoomSet.Height;
            txtzt.Value = (decimal)ucRoomSet.FontSize;
            chkjc.Checked = ucRoomSet.FontBold;
            txtsxpl.Value =(decimal)ucRoomSet.Frequency;
            //预览控件
            var room = _context.ROOM.FirstOrDefault();
            if (room == null)
                return;
            ucRoom ucr = new ucRoom(room);
            ucr.Location = new Point(30, 30);
            //ucr.Width = ucRoomSet.Width;
            //ucr.Height = ucRoomSet.Height;
            //var myFont = new Font("宋体", (float)ucRoomSet.FontSize,ucRoomSet.FontBold?System.Drawing.FontStyle.Bold:System.Drawing.FontStyle.Regular);          
            //ucr.LabelRoomFont = myFont;
            gBoxRoom.Controls.Add(ucr);
            txtkd_ValueChanged(txtkd, null);

        }
        public xtszFrm(int selectedIndex)
        {
            InitializeComponent();
            tabMain.SelectedIndex = selectedIndex;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            switch (tabMain.SelectedIndex)
            {
                case 0:
                    if (!SaveXTCS())
                        return;
                    break;
                case 1:
                    if (!SaveRoomSet())
                        return;
                    break;
                default:
                    return;
            }

            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.修改数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "修改系统参数";
            logInfo.Save();
            #endregion
            if (MessageBox.Show("保存成功，是否关闭参数设置?\n【注意】所有设置在下次启动时才会生效", SysConsts.SysInformationCaption, MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;
            this.DialogResult = DialogResult.OK;
        }
        /// <summary>
        /// 保存系统参数--常规页
        /// </summary>
        private bool SaveXTCS()
        {
            var info = ValidataHelper.StartVerify(txtjdmc.Text)
           .IsNotNullOrEmpty("酒店名称不能为空")
           .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }

            var qry = _context.XTCS.Where(p => p.FL == "常规");
            foreach (var item in qry)
            {
                item.XGRQ = DateTime.Now;
                item.XGRY = UserLoginInfo.FullName;
                switch (item.CSDM)
                {
                    case "BFWJLJ":
                        item.CSZ = txtbflj.Text;
                        break;
                    case "SF_ZDYS":
                        item.CSZ = chkys.Checked ? "1" : "0";
                        break;
                    case "SFDY_YYD":
                        item.CSZ = chkdyyyd.Checked ? "1" : "0";
                        break;
                    case "ZDYS_IP":
                        item.CSZ = txtysip.Text;
                        break;
                    case "ZDYS_SJ":
                        item.CSZ = txtyssj.Text;
                        break;
                    case "JDMC":
                        item.CSZ = txtjdmc.Text;
                        break;
                    case "JDDH":
                        item.CSZ = txtjddh.Text;
                        break;
                    case "JBTFZ_SJ":
                        item.CSZ = txtcs1.Text;
                        break;
                    case "JYTFZ_SJ":
                        item.CSZ = txtcs2.Text;
                        break;
                    case "SF_ZDBF":
                        item.CSZ = chkbf.Checked ? "1" : "0";
                        break;
                    case "ZDBF_IP":
                        item.CSZ = txtbfip.Text;
                        break;
                    case "ZDBF_GZ":
                        item.CSZ = cobxbfgz.Text;
                        break;
                    case "YYRQ":
                        if (hotel.Common.Utility.IsDate(txtyyrq.Text))
                            item.CSZ = txtyyrq.Text;
                        else
                            item.CSZ = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        break;
                    default:
                        break;
                }
            }
            _context.SaveChanges();
            return true;
        }

        /// 保存系统参数--房态图
        /// </summary>
        private bool SaveRoomSet()
        {
            var qry = _context.XTCS.Where(p => p.FL == "房态图");
            foreach (var item in qry)
            {
                item.XGRQ = DateTime.Now;
                item.XGRY = UserLoginInfo.FullName;
                switch (item.CSDM)
                {
                    case "ROOM_WIDTH":
                        item.CSZ = txtkd.Value.ToString();
                        break;
                    case "ROOM_HEIGHT":
                        item.CSZ = txtgd.Value.ToString();
                        break;
                    case "ROOM_FONT_SIZE":
                        item.CSZ = txtzt.Value.ToString();
                        break;
                    case "ROOM_FONT_BLOD":
                        item.CSZ = chkjc.Checked ? "1" : "0";
                        break;
                    case "ROOM_Status_Frequency":
                        item.CSZ = txtsxpl.Value.ToString();
                        break;
                    default:
                        break;
                }
            }
            _context.SaveChanges();
            return true;
        }
        private void btnbj_Click(object sender, EventArgs e)
        {
            txtysip.Text = Computer.Instance.IpAddress; //System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString();
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabMain.SelectedIndex == 0 && txtjdmc.Text.Length == 0)
                BindData();
            else if (tabMain.SelectedIndex == 1 && gBoxRoom.Controls.Count == 0)
                BindRoomSet();
        }

        private void txtkd_ValueChanged(object sender, EventArgs e)
        {
            if (gBoxRoom.Controls.Count == 0)
                return;
            var ucroom = gBoxRoom.Controls[0] as ucRoom;
            ucroom.Width = (int)txtkd.Value;
            ucroom.Height = (int)txtgd.Value;
            Font myFont = new Font("宋体", (float)txtzt.Value ,chkjc.Checked ? System.Drawing.FontStyle.Bold : System.Drawing.FontStyle.Regular);
            ucroom.LabelRoomFont = myFont;
            ucroom.Refresh();
        }

        private void btnroomdefault_Click(object sender, EventArgs e)
        {
            txtkd.Value = 54;
            txtgd.Value = 58;
            txtzt.Value = 12;
            txtsxpl.Value = 60;
            chkjc.Checked = true;
        }

        private void btnbf_Click(object sender, EventArgs e)
        {
            btnbf.Enabled = false;
            var _context = MyDataContext.GetDataContext;

            string path = txtbflj.Text;
            path += "hotel" + DateTime.Now.ToString("yyyyMMddHHmm") + ".bak";
            try
            {
                _context.BackUpHotel(path);
                MessageBox.Show("备份成功");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    MessageBox.Show(ex.InnerException.Message);
                else
                    MessageBox.Show(ex.Message);
                return;
            }
           
            btnbf.Enabled = true;
        }

        private void txtbflj_Leave(object sender, EventArgs e)
        {
            if (txtbflj.Text.Length ==0)
                return;
            if (txtbflj.Text[txtbflj.Text.Length-1] !='\\')
                txtbflj.Text +="\\";
        }
    }

    #region UcRoomSet

    /// <summary>
    /// 房间控件的设置
    /// </summary>
    public class UcRoomSet
    {
        static UcRoomSet _xtcs;
        public static UcRoomSet GetUcRoomSet
        {
            get
            {
                if (_xtcs == null)
                {
                    _xtcs = new UcRoomSet();
                }
                return _xtcs;
            }
            set { _xtcs = null; }
        }
        public UcRoomSet()
        {
            hotelEntities _context = MyDataContext.GetDataContext;
            var qry = _context.XTCS.Where(p => p.FL == "房态图");
            foreach (var item in qry)
            {
                #region 所有属性赋值
                switch (item.CSDM)
                {
                    case "ROOM_WIDTH":
                        Width = Convert.ToInt32(item.CSZ);
                        break;
                    case "ROOM_HEIGHT":
                        Height = Convert.ToInt32(item.CSZ);
                        break;
                    case "ROOM_FONT_SIZE":
                        FontSize = Convert.ToDouble(item.CSZ);
                        break;
                    case "ROOM_FONT_BLOD":
                        FontBold = item.CSZ == "1";
                        break;
                    case "ROOM_Status_Frequency":
                        Frequency = Convert.ToDouble(item.CSZ);
                        break;
                }
                #endregion
            }
        }
        /// <summary>
        /// Width
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// Height
        /// </summary>
        public int Height { get; set; }
        public double FontSize { get; set; }
        public bool FontBold { get; set; }
        public double Frequency { get; set; }

    }


    #endregion
}
