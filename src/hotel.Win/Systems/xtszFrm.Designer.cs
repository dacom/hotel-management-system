﻿namespace hotel.Win.Systems
{
    partial class xtszFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtbflj = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnbf = new System.Windows.Forms.Button();
            this.cobxbfgz = new System.Windows.Forms.ComboBox();
            this.chkbf = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtbfip = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtcs2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtcs1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkdyyyd = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnbj = new System.Windows.Forms.Button();
            this.txtyssj = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtysip = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkys = new System.Windows.Forms.CheckBox();
            this.txtjddh = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtyyrq = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtjdmc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtsxpl = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnroomdefault = new System.Windows.Forms.Button();
            this.gBoxRoom = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkjc = new System.Windows.Forms.CheckBox();
            this.txtzt = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.txtgd = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.txtkd = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btncancel = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.tabMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtsxpl)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtzt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkd)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPage1);
            this.tabMain.Controls.Add(this.tabPage2);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(561, 469);
            this.tabMain.TabIndex = 0;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.chkdyyyd);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.txtjddh);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtyyrq);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtjdmc);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(553, 442);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "常规";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtbflj);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.btnbf);
            this.groupBox3.Controls.Add(this.cobxbfgz);
            this.groupBox3.Controls.Add(this.chkbf);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtbfip);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Location = new System.Drawing.Point(15, 291);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(523, 110);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "备份规则";
            // 
            // txtbflj
            // 
            this.txtbflj.Location = new System.Drawing.Point(368, 40);
            this.txtbflj.Name = "txtbflj";
            this.txtbflj.Size = new System.Drawing.Size(149, 23);
            this.txtbflj.TabIndex = 13;
            this.txtbflj.Text = "c:\\";
            this.txtbflj.Leave += new System.EventHandler(this.txtbflj_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(278, 43);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 14);
            this.label12.TabIndex = 14;
            this.label12.Text = "服务器路径:";
            // 
            // btnbf
            // 
            this.btnbf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbf.Location = new System.Drawing.Point(368, 69);
            this.btnbf.Name = "btnbf";
            this.btnbf.Size = new System.Drawing.Size(116, 26);
            this.btnbf.TabIndex = 13;
            this.btnbf.Text = "现在备份";
            this.btnbf.UseVisualStyleBackColor = true;
            this.btnbf.Click += new System.EventHandler(this.btnbf_Click);
            // 
            // cobxbfgz
            // 
            this.cobxbfgz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxbfgz.FormattingEnabled = true;
            this.cobxbfgz.Items.AddRange(new object[] {
            "每次夜审之后"});
            this.cobxbfgz.Location = new System.Drawing.Point(151, 74);
            this.cobxbfgz.Name = "cobxbfgz";
            this.cobxbfgz.Size = new System.Drawing.Size(121, 21);
            this.cobxbfgz.TabIndex = 13;
            // 
            // chkbf
            // 
            this.chkbf.AutoSize = true;
            this.chkbf.Location = new System.Drawing.Point(31, 22);
            this.chkbf.Name = "chkbf";
            this.chkbf.Size = new System.Drawing.Size(96, 16);
            this.chkbf.TabIndex = 11;
            this.chkbf.Text = "是否自动备份";
            this.chkbf.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 14);
            this.label10.TabIndex = 9;
            this.label10.Text = "自动备份规则：";
            // 
            // txtbfip
            // 
            this.txtbfip.Location = new System.Drawing.Point(151, 40);
            this.txtbfip.Name = "txtbfip";
            this.txtbfip.Size = new System.Drawing.Size(121, 23);
            this.txtbfip.TabIndex = 12;
            this.txtbfip.Text = "255.255.255.255";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 14);
            this.label11.TabIndex = 7;
            this.label11.Text = "自动备份电脑IP：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtcs2);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtcs1);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(15, 199);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(523, 88);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "房租规则";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(206, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 14);
            this.label6.TabIndex = 14;
            this.label6.Text = "加收全天房租";
            // 
            // txtcs2
            // 
            this.txtcs2.Location = new System.Drawing.Point(125, 57);
            this.txtcs2.Name = "txtcs2";
            this.txtcs2.Size = new System.Drawing.Size(66, 23);
            this.txtcs2.TabIndex = 10;
            this.txtcs2.Text = "18:00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 14);
            this.label9.TabIndex = 12;
            this.label9.Text = "退房时间超过";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(206, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 14);
            this.label8.TabIndex = 11;
            this.label8.Text = "加收半天房租";
            // 
            // txtcs1
            // 
            this.txtcs1.Location = new System.Drawing.Point(125, 22);
            this.txtcs1.Name = "txtcs1";
            this.txtcs1.Size = new System.Drawing.Size(66, 23);
            this.txtcs1.TabIndex = 9;
            this.txtcs1.Text = "12:00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 14);
            this.label7.TabIndex = 7;
            this.label7.Text = "退房时间超过";
            // 
            // chkdyyyd
            // 
            this.chkdyyyd.AutoSize = true;
            this.chkdyyyd.Location = new System.Drawing.Point(361, 59);
            this.chkdyyyd.Name = "chkdyyyd";
            this.chkdyyyd.Size = new System.Drawing.Size(124, 18);
            this.chkdyyyd.TabIndex = 4;
            this.chkdyyyd.Text = "是否打印押金单";
            this.chkdyyyd.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnbj);
            this.groupBox1.Controls.Add(this.txtyssj);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtysip);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.chkys);
            this.groupBox1.Location = new System.Drawing.Point(15, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(523, 111);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "夜审";
            // 
            // btnbj
            // 
            this.btnbj.Location = new System.Drawing.Point(335, 45);
            this.btnbj.Name = "btnbj";
            this.btnbj.Size = new System.Drawing.Size(66, 23);
            this.btnbj.TabIndex = 7;
            this.btnbj.Text = "本机IP";
            this.btnbj.UseVisualStyleBackColor = true;
            this.btnbj.Click += new System.EventHandler(this.btnbj_Click);
            // 
            // txtyssj
            // 
            this.txtyssj.Location = new System.Drawing.Point(151, 75);
            this.txtyssj.Name = "txtyssj";
            this.txtyssj.Size = new System.Drawing.Size(177, 23);
            this.txtyssj.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 14);
            this.label5.TabIndex = 9;
            this.label5.Text = "自动夜审时间：";
            // 
            // txtysip
            // 
            this.txtysip.Location = new System.Drawing.Point(151, 46);
            this.txtysip.Name = "txtysip";
            this.txtysip.Size = new System.Drawing.Size(177, 23);
            this.txtysip.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 14);
            this.label4.TabIndex = 7;
            this.label4.Text = "自动夜审电脑IP：";
            // 
            // chkys
            // 
            this.chkys.AutoSize = true;
            this.chkys.Location = new System.Drawing.Point(31, 22);
            this.chkys.Name = "chkys";
            this.chkys.Size = new System.Drawing.Size(120, 16);
            this.chkys.TabIndex = 5;
            this.chkys.Text = "是否自动夜审提醒";
            this.chkys.UseVisualStyleBackColor = true;
            // 
            // txtjddh
            // 
            this.txtjddh.Location = new System.Drawing.Point(361, 15);
            this.txtjddh.Name = "txtjddh";
            this.txtjddh.Size = new System.Drawing.Size(177, 23);
            this.txtjddh.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(278, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "酒店电话：";
            // 
            // txtyyrq
            // 
            this.txtyyrq.Location = new System.Drawing.Point(95, 56);
            this.txtyyrq.Name = "txtyyrq";
            this.txtyyrq.ReadOnly = true;
            this.txtyyrq.Size = new System.Drawing.Size(177, 23);
            this.txtyyrq.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "营业日期：";
            // 
            // txtjdmc
            // 
            this.txtjdmc.Location = new System.Drawing.Point(95, 15);
            this.txtjdmc.Name = "txtjdmc";
            this.txtjdmc.Size = new System.Drawing.Size(177, 23);
            this.txtjdmc.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "酒店名称：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.btnroomdefault);
            this.tabPage2.Controls.Add(this.gBoxRoom);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(553, 442);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "房态图设置";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtsxpl);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Location = new System.Drawing.Point(38, 292);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(274, 57);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "实时房态设置";
            // 
            // txtsxpl
            // 
            this.txtsxpl.Location = new System.Drawing.Point(82, 22);
            this.txtsxpl.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtsxpl.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtsxpl.Name = "txtsxpl";
            this.txtsxpl.Size = new System.Drawing.Size(67, 23);
            this.txtsxpl.TabIndex = 5;
            this.txtsxpl.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(155, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 14);
            this.label17.TabIndex = 4;
            this.label17.Text = "秒钟";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 14);
            this.label16.TabIndex = 4;
            this.label16.Text = "刷新频率:";
            // 
            // btnroomdefault
            // 
            this.btnroomdefault.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnroomdefault.Location = new System.Drawing.Point(311, 89);
            this.btnroomdefault.Name = "btnroomdefault";
            this.btnroomdefault.Size = new System.Drawing.Size(75, 23);
            this.btnroomdefault.TabIndex = 20;
            this.btnroomdefault.Text = "读取默认";
            this.btnroomdefault.UseVisualStyleBackColor = true;
            this.btnroomdefault.Click += new System.EventHandler(this.btnroomdefault_Click);
            // 
            // gBoxRoom
            // 
            this.gBoxRoom.Location = new System.Drawing.Point(38, 137);
            this.gBoxRoom.Margin = new System.Windows.Forms.Padding(0);
            this.gBoxRoom.Name = "gBoxRoom";
            this.gBoxRoom.Padding = new System.Windows.Forms.Padding(0);
            this.gBoxRoom.Size = new System.Drawing.Size(274, 141);
            this.gBoxRoom.TabIndex = 10;
            this.gBoxRoom.TabStop = false;
            this.gBoxRoom.Text = "房间预览";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkjc);
            this.groupBox4.Controls.Add(this.txtzt);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.txtgd);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txtkd);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Location = new System.Drawing.Point(38, 15);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(274, 100);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "大小";
            // 
            // chkjc
            // 
            this.chkjc.AutoSize = true;
            this.chkjc.Checked = true;
            this.chkjc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkjc.Location = new System.Drawing.Point(192, 56);
            this.chkjc.Name = "chkjc";
            this.chkjc.Size = new System.Drawing.Size(48, 16);
            this.chkjc.TabIndex = 4;
            this.chkjc.Text = "加粗";
            this.chkjc.UseVisualStyleBackColor = true;
            this.chkjc.CheckedChanged += new System.EventHandler(this.txtkd_ValueChanged);
            // 
            // txtzt
            // 
            this.txtzt.Location = new System.Drawing.Point(54, 55);
            this.txtzt.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.txtzt.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.txtzt.Name = "txtzt";
            this.txtzt.Size = new System.Drawing.Size(67, 23);
            this.txtzt.TabIndex = 3;
            this.txtzt.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.txtzt.ValueChanged += new System.EventHandler(this.txtkd_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 14);
            this.label15.TabIndex = 2;
            this.label15.Text = "字体:";
            // 
            // txtgd
            // 
            this.txtgd.Location = new System.Drawing.Point(192, 17);
            this.txtgd.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.txtgd.Minimum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.txtgd.Name = "txtgd";
            this.txtgd.Size = new System.Drawing.Size(67, 23);
            this.txtgd.TabIndex = 2;
            this.txtgd.Value = new decimal(new int[] {
            58,
            0,
            0,
            0});
            this.txtgd.ValueChanged += new System.EventHandler(this.txtkd_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(144, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 0;
            this.label14.Text = "高度:";
            // 
            // txtkd
            // 
            this.txtkd.Location = new System.Drawing.Point(54, 17);
            this.txtkd.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.txtkd.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.txtkd.Name = "txtkd";
            this.txtkd.Size = new System.Drawing.Size(67, 23);
            this.txtkd.TabIndex = 1;
            this.txtkd.Value = new decimal(new int[] {
            54,
            0,
            0,
            0});
            this.txtkd.ValueChanged += new System.EventHandler(this.txtkd_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "宽度:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btncancel);
            this.panel1.Controls.Add(this.btnok);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 433);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(561, 36);
            this.panel1.TabIndex = 1;
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(401, 5);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(68, 28);
            this.btncancel.TabIndex = 21;
            this.btncancel.Text = "取消";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(315, 5);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(80, 28);
            this.btnok.TabIndex = 20;
            this.btnok.Text = "保存";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // xtszFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 469);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabMain);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "xtszFrm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "系统设置";
            this.tabMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtsxpl)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtzt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkd)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtjddh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtjdmc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtyyrq;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkdyyyd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtyssj;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtysip;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkys;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtcs2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtcs1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkbf;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtbfip;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cobxbfgz;
        private System.Windows.Forms.Button btnbf;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.TextBox txtbflj;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnbj;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown txtgd;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown txtkd;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkjc;
        private System.Windows.Forms.NumericUpDown txtzt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox gBoxRoom;
        private System.Windows.Forms.Button btnroomdefault;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown txtsxpl;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
    }
}