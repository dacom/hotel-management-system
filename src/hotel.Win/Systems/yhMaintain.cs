﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using hotel.DAL;
using hotel.Win.Common;
using hotel.Common;
using hotel.MyUserControl;
namespace hotel.Win.Systems
{
    public partial class yhMaintain : MaintainBaseForm
    {
        string Id;
        hotelEntities _context;
        XTYH dataModel;

        /// <summary>
        /// 用于增加
        /// </summary>
        public yhMaintain(bool isAdd)
        {
            InitializeComponent();
            base.OnAdd = this.Add;
            base.OnDelete = this.Delete;
            base.OnPost = this.Post;            
            _context = MyDataContext.GetDataContext;
            Id = "0";
            if (isAdd)
            {
                base.IsEdit = false;
                base.DeleteButtonEnabled = false;
                base.StatusMessage = "数据处于增加状态";
                Add();
            }
        }
        
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        public yhMaintain(string id)
            : this(false)
        {
            BindData(id);
        }

        private void BindData(string id)
        {
            this.Id = id;
            dataModel = _context.XTYH.First(p => p.ID == id);
            if (dataModel == null)
                throw new Exception("没有找到数据id=" + id.ToString());
            txtid.Text = dataModel.ID;
            txtid.ReadOnly = true;
            txtxm.Text = dataModel.XM;
            txtmm.Text = dataModel.MM;
            txtbm.Text = dataModel.BM;
            txtbz.Text = dataModel.BZSM;
            cobxzt.Text = dataModel.ZT;
            IsEdit = true;
            StatusMessage = "修改数据状态";

        }

        private bool Add()
        {
            txtid.ReadOnly = false;
            cobxzt.SelectedIndex = 0;
            txtxm.Text = "";
            txtid.Text = "";
            txtmm.Text = "";
            txtbz.Text = "";
            dataModel = new XTYH { ID = Id };
            return true;
        }
        private bool Delete()
        {
            if (dataModel.ID.Equals(SysConsts.SystemAdminId, StringComparison.InvariantCultureIgnoreCase))
            {
                MessageBox.Show("系统管理员不能删除！");
                return false;
            }
            _context.PROC_XTYH_Delete(dataModel.ID);
            _context.SaveChanges();
            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.删除数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "删除用户ID:" + dataModel.ID + ",姓名="+ dataModel.XM;
            logInfo.Save();
            #endregion
            return true;
        }

        private bool CheckData()
        {
            var info = ValidataHelper.StartVerify(txtid.Text)
            .IsNotNullOrEmpty("用户ID不能为空输入为空")
            .Max(12, "用户ID长度不能大于12个字符")
            .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            info = ValidataHelper.StartVerify(txtxm.Text)
           .IsNotNullOrEmpty("姓名不能为空输入为空")
           .Max(10, "姓名长度不能大于10个字符")
           .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            info = ValidataHelper.StartVerify(txtmm.Text)
           .IsNotNullOrEmpty("密码不能为空输入为空")
           .Min(4, "密码长度最小4个字符")
           .Max(64, "密码长度不能大于64个字符")
           .EndVerify();
            if (!info.Status)
            {
                MessageBox.Show(info.Message);
                return false;
            }
            if (!IsEdit)
            {
                if (_context.XTYH.Where(p => p.ID == txtid.Text).Count() > 0)
                {
                    MessageBox.Show("用户ID" + txtid.Text + "已经存在，请查证！");
                    return false;
                }
            }
            return true;
        }
        private bool Post()
        {
            if (!CheckData())
                return false;
            dataModel.BM = txtbm.Text;
            dataModel.BZSM = txtbz.Text;           
            dataModel.MM = txtmm.Text;
            dataModel.XM = txtxm.Text;
            dataModel.ZT = cobxzt.Text;
            if (IsEdit)
            {
                //dataModel.XGRQ = DateTime.Now;
                //dataModel.XGRY = UserLoginInfo.FullName;                
            }
            else
            {
                dataModel.ID = txtid.Text;
                _context.XTYH.Add(dataModel);

            }
            _context.SaveChanges();

            #region 日志纪录
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.增加数据;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "增加用户ID:" + dataModel.ID + ",姓名=" + dataModel.XM;
            logInfo.Save();
            #endregion
            return true;
        }

        private void yhMaintain_FormClosed(object sender, FormClosedEventArgs e)
        {
            _context.Dispose();
        }
    }
}
