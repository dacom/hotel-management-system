﻿namespace hotel.Win.Systems
{
    partial class yhMaintain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtid = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtxm = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtmm = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbz = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cobxzt = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 6;
            this.label1.Text = "用户ID：";
            // 
            // txtid
            // 
            this.txtid.Location = new System.Drawing.Point(113, 18);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(209, 23);
            this.txtid.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "姓名：";
            // 
            // txtxm
            // 
            this.txtxm.Location = new System.Drawing.Point(113, 59);
            this.txtxm.Name = "txtxm";
            this.txtxm.Size = new System.Drawing.Size(100, 23);
            this.txtxm.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "密码：";
            // 
            // txtmm
            // 
            this.txtmm.Location = new System.Drawing.Point(113, 100);
            this.txtmm.Name = "txtmm";
            this.txtmm.Size = new System.Drawing.Size(209, 23);
            this.txtmm.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 14);
            this.label4.TabIndex = 6;
            this.label4.Text = "部门：";
            // 
            // txtbm
            // 
            this.txtbm.Location = new System.Drawing.Point(113, 141);
            this.txtbm.Name = "txtbm";
            this.txtbm.Size = new System.Drawing.Size(209, 23);
            this.txtbm.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 6;
            this.label5.Text = "备注：";
            // 
            // txtbz
            // 
            this.txtbz.Location = new System.Drawing.Point(113, 182);
            this.txtbz.Name = "txtbz";
            this.txtbz.Size = new System.Drawing.Size(209, 23);
            this.txtbz.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(219, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 14);
            this.label6.TabIndex = 6;
            this.label6.Text = "状态：";
            // 
            // cobxzt
            // 
            this.cobxzt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxzt.FormattingEnabled = true;
            this.cobxzt.Items.AddRange(new object[] {
            "在用",
            "停用"});
            this.cobxzt.Location = new System.Drawing.Point(263, 60);
            this.cobxzt.Name = "cobxzt";
            this.cobxzt.Size = new System.Drawing.Size(59, 21);
            this.cobxzt.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cobxzt);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtbm);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtbz);
            this.groupBox1.Controls.Add(this.txtid);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtmm);
            this.groupBox1.Controls.Add(this.txtxm);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(5, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(447, 236);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // yhMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.ClientSize = new System.Drawing.Size(456, 314);
            this.Controls.Add(this.groupBox1);
            this.Name = "yhMaintain";
            this.Text = "系统用户维护";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.yhMaintain_FormClosed);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtxm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtmm;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbz;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cobxzt;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
