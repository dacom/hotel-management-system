﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using hotel.Win.Common;
using hotel.DAL;
using hotel.Win.BaseData;
using hotel.Win.Subscribe;
using hotel.Common;
using hotel.MyUserControl;
using hotel.Win.Systems;
using hotel.Win.Member;
using hotel.Win.Query;
namespace hotel.Win
{
    public partial class MainControl : Form
    {
        //hotelEntities _context;
        ucRoom currentURoom;//当前操作的ucRoom空间
        System.Timers.Timer RoomStatusTimer;//实时更新房态timer    
        ReadKeycardFrm keycardFrm;//读房卡信息窗口
        public MainControl()
        {
            InitializeComponent();
            
            SetRoomLabelStatusColor();
        }

        private void SetRoomLabelStatusColor()
        {
            lbftall.BackColor = RoomStatusColor.All;
            lbftall.ForeColor = RoomStatusColor.ForeColor;
            lbkjf.BackColor = RoomStatusColor.KongJinFang;
            lbkjf.ForeColor = RoomStatusColor.ForeColor;
            lbydf.BackColor = RoomStatusColor.YuDingFang;
            lbydf.ForeColor = RoomStatusColor.ForeColor;
            lbzf.BackColor = RoomStatusColor.ZhuFang;
            lbzf.ForeColor = RoomStatusColor.ForeColor;
            lbkzf.BackColor = RoomStatusColor.KongZangFang;
            lbkzf.ForeColor = RoomStatusColor.ForeColor;
            lbwxf.BackColor = RoomStatusColor.WeiXiuFang;
            lbwxf.ForeColor = RoomStatusColor.ForeColor;
            var imglist = RoomStatusImage.GetImageList;
            lbskf.ImageList = imglist;
            lbskf.ImageIndex = (int)RoomStstusEnum.SanKe;
            lbtdf.ImageList = imglist;
            lbtdf.ImageIndex = (int)RoomStstusEnum.TuanDui;
            lbzdf.ImageList = imglist;
            lbzdf.ImageIndex = (int)RoomStstusEnum.ZhongDian;
            lbcbf.ImageList = imglist;
            lbcbf.ImageIndex = (int)RoomStstusEnum.ChangBao;
            lbzyf.ImageList = imglist;
            lbzyf.ImageIndex = (int)RoomStstusEnum.ZiYong;
            lbmff.ImageList = imglist;
            lbmff.ImageIndex = (int)RoomStstusEnum.MianFei;
            //lbvipf.ImageList = imglist;
            //lbvipf.ImageIndex = (int)RoomStstusEnum.Vip;
            lbbmf.ImageList = imglist;
            lbbmf.ImageIndex = (int)RoomStstusEnum.BaoMi;
            lbldf.ImageList = imglist;
            lbldf.ImageIndex = (int)RoomStstusEnum.LiDian;
            lbqlf.ImageList = imglist;
            lbqlf.ImageIndex = (int)RoomStstusEnum.Qingli;
            lbsdf.ImageList = imglist;
            lbsdf.ImageIndex = (int)RoomStstusEnum.SuoDing;
        }
        /// <summary>
        /// 菜单授权
        /// </summary>
        private void MenuGrant()
        {
            var _context = MyDataContext.GetDataContext;
            var cdqxs = _context.CDQX.Where(p => p.XTYH.ID == UserLoginInfo.UserId && p.TC_CDBM != null);

            foreach (var item in cdqxs)
            {
                if (item.SFKY == 1)
                    continue;
                var mi = contextMenuRoom.Items.Find(item.TC_CDBM,false);
                if (mi != null && mi.Length > 0)
                    mi.First().Visible = false;
            }
            _context.Dispose();
        }
        private void MainControl_Load(object sender, EventArgs e)
        {
            MenuGrant();
            this.WindowState = FormWindowState.Maximized;
            //建立实时房态主页面
            CreateLDtabPage();

            this.cobxzl.SelectedIndexChanged -= new System.EventHandler(this.cobxzl_SelectedIndexChanged);
            cobxzl.DataSource = RoomTypeDal.GetAll2();
            cobxzl.DisplayMember = "MC";
            cobxzl.ValueMember = "ID";

            cobxzl.SelectedIndex = 0;
            this.cobxzl.SelectedIndexChanged += new System.EventHandler(this.cobxzl_SelectedIndexChanged);
            //建立实时更新房态定时器及执行事件
            CreateUpdateRoomStatusTimer();
        }

        /// <summary>
        /// 建立楼栋tabPage
        /// </summary>
        private void CreateLDtabPage()
        {
            tabMain.Controls.Clear();
            foreach (var item in XTCYDMDal.GetAllByLBDM(EnumXtcydmLB.LDH.ToString()))
            {
                var tabPage1 = new System.Windows.Forms.TabPage();
                //tabPage1.AutoScroll = true;
                this.tabMain.Controls.Add(tabPage1);
                tabPage1.Name = "tabPage" + item.ID.ToString();
                tabPage1.Padding = new System.Windows.Forms.Padding(3);
                tabPage1.Text = item.XMMC;
                tabPage1.UseVisualStyleBackColor = true;
                var tlpHouse1 = new System.Windows.Forms.TableLayoutPanel();
                //tlpHouse1.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;//效率很差
                tlpHouse1.AutoScroll = true;
                tlpHouse1.AutoSize = true;
                tlpHouse1.Dock = DockStyle.Top;
                tabPage1.Controls.Add(tlpHouse1);
                tlpHouse1.Dock = System.Windows.Forms.DockStyle.Fill;
            }
            if (tabMain.TabPages.Count > 0)
            {
                tabMain.SelectedIndex = 0;
                tabMain_SelectedIndexChanged(tabMain, null);
            }
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabMain.SelectedIndex < 0)
                return;
            var tlpHouse1 = tabMain.SelectedTab.Controls[0] as TableLayoutPanel;
            //tlpHouse1.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            string ld = tabMain.SelectedTab.Text;
            List<ROOM> houses;
            using (var _context = MyDataContext.GetDataContext)
            {
                var qry = from p in _context.ROOM.AsNoTracking().Include("ROOM_TYPE")
                          where p.LD == ld
                              select p;
                houses = qry.ToList();  
            }
            //CreateBuilding(tlpHouse1, houses);
            CreateRoomStateThread(tlpHouse1, houses);
            // ComputerRoomStatusLable();
        }




        #region 处理右边房态

        ///// <summary>
        ///// 建立房屋状态
        ///// </summary>
        //private void CreateRoomStatus(IQueryable<ROOM> houses)
        //{
        //    flowLayoutPanelFT.Controls.Clear();
        //    var qry = _context.ROOM_STATUS.OrderBy(p => p.XH).ToList();
        //    foreach (var item in qry)
        //    {
        //        string ztmc = item.MC + " " + houses.Where(p => p.ZT == item.MC).Count().ToString() + "间";
        //        flowLayoutPanelFT.Controls.Add(GetRoomStatusLabel(ztmc, (int)item.TPSY));
        //    }

        //}

        //private Label GetRoomStatusLabel(string text, int imageIndex)
        //{
        //    var label = new System.Windows.Forms.Label();
        //    label.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        //    label.ImageIndex = imageIndex >= imageListFT.Images.Count ? 0 : imageIndex;
        //    label.ImageList = this.imageListFT;
        //    label.Size = new System.Drawing.Size(123, 32);
        //    label.Text = text;
        //    label.Tag = text;
        //    label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        //    label.Click += new System.EventHandler(this.labelFT_Click);
        //    return label;
        //}
        #endregion

        private void cobxzl_SelectedIndexChanged(object sender, EventArgs e)
        {
            var tlpHouse1 = tabMain.SelectedTab.Controls[0] as TableLayoutPanel;
            if (cobxzl.Text == SysConsts.AllItem)
            {
                SetAllRoomVisble(true);
                return;
            }
            tlpHouse1.Visible = false;
            tlpHouse1.SuspendLayout();
            //SetAllRoomVisble(false);
            string fjzlid = cobxzl.SelectedValue.ToString();
            using (var _context = MyDataContext.GetDataContext)
            {
                var qry = _context.ROOM.Where(p => p.ROOM_TYPE.ID == fjzlid).Select(p => p.ID).ToList();
                foreach (var item in qry)
                {
                    if (AllRoom.ContainsKey(item) && AllRoom[item].Visible == false)
                        AllRoom[item].Visible = true;
                }
                foreach (var item in AllRoom.Keys.Except(qry))
                {
                    AllRoom[item].Visible = false;
                }
                tlpHouse1.ResumeLayout();
                tlpHouse1.Visible = true;
            }
        }

        private void contextMenuRoom_Opened(object sender, EventArgs e)
        {
            currentURoom = contextMenuRoom.SourceControl as ucRoom;
            if (!currentURoom.Selected)
            {
                 
                foreach (var item in AllRoom)
                {
                    if (item.Value.Selected)
                    {
                        item.Value.Selected = false;
                        item.Value.Refresh();
                    }
                }
                currentURoom.Selected = true;
                currentURoom.Refresh();
            }
            plnTootip.Visible = false;
            //tmiccrz.Visible = currentURoom.IsRuZhu;
        }

        #region 客房右键菜单
        /// <summary>
        /// 新预订
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmikryd_Click(object sender, EventArgs e)
        {
            NewYDMenuMethod(currentURoom);
        }

        /// <summary>
        /// 客人登记
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmikrtj_Click(object sender, EventArgs e)
        {
            KRTJMenuMethod(currentURoom);
        }

        /// <summary>
        /// 客人信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmi_krxx_Click(object sender, EventArgs e)
        {
            KRXXMenuMethod(currentURoom);
        }

        /// <summary>
        /// 客人换房
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmikrhf_Click(object sender, EventArgs e)
        {
            KRHFMenuMethod(currentURoom);
        }
        //制卡
        private void tmizhika_Click(object sender, EventArgs e)
        {
            ZhiKaFMenuMethod(currentURoom);
        }
        //查看房卡
         private void tmiCkfk_Click(object sender, EventArgs e)
        {
            FangKaViewMenuMethod(currentURoom);
        }

        /// 消费入单
        private void tmixfrd_Click(object sender, EventArgs e)
        {
            XFRDMenuMethod();
        }

        /// <summary>
        /// 押金处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmixtyj_Click(object sender, EventArgs e)
        {
            YJCLMenuMethod();
        }



        /// <summary>
        /// 结帐退房
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmijztf_Click(object sender, EventArgs e)
        {
            JZTFMenuMethod();
        }
        /// <summary>
        /// 不结帐退房
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tim_bjztf_Click(object sender, EventArgs e)
        {
            
        }
        /// <summary>
        /// 合并帐单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmihbzt_Click(object sender, EventArgs e)
        {
            HBZDMenuMethod();
        }


        /// <summary>
        /// 拆分帐单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmicfzd_Click(object sender, EventArgs e)
        {
            CFZDMenuMethod();
        }


        /// <summary>
        /// 房态修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmifjxg_Click(object sender, EventArgs e)
        {
            FTXGMenuMethod();
        }

        #endregion

        /// <summary>
        /// 刷新单个房间--从数据库
        /// </summary>
        /// <param name="ucroom"></param>
        private void RefreshRoomFromDataBase(ucRoom ucroom)
        {
            try
            {
                //_context.Refresh(System.Data.Objects.RefreshMode.StoreWins, ucroom.Room);
               // _context.Refresh(System.Data.Objects.RefreshMode.StoreWins, ucroom.Room.RZJL);
                using (var _context = MyDataContext.GetDataContext)
                {
                    var rm = _context.ROOM.Find(ucroom.Room.ID);
                    ucroom.Room.RZBZ = rm.RZBZ;
                    ucroom.Room.ZT = rm.ZT;
                    if (rm.RZBZ == "是")
                    {
                        var rzjl = _context.RZJL.AsNoTracking().FirstOrDefault(p => p.ROOM_ID == rm.ID && p.RZBZ == "是");
                        if (rzjl != null)
                        {
                            ucroom.LFH = rzjl.LFH;
                        }
                    }
                }
                ucroom.RefreshRoom();
                ComputerRoomStatusLable();
            }
            catch (Exception ex)
            {
                var log = Log.Instance;
                log.Abstract = LogAbstractEnum.未知;
                log.Message = "RefreshRoomFromDataBase(ucRoom ucroom):" + ex.Message;
                log.Error();
            }
          
        }
        /// <summary>
        /// 刷新多个房间--从数据库
        /// </summary>
        /// <param name="ucroom"></param>
        public void RefreshRoomFromDataBase(List<string> roomIds)
        {
            try
            {
                if (roomIds == null)
                    return;
                using (var _context = MyDataContext.GetDataContext)
                {
                    foreach (var id in roomIds)
                    {
                        var rm = _context.ROOM.Find(id);
                        AllRoom[id].Room.RZBZ = rm.RZBZ;
                        AllRoom[id].Room.ZT = rm.ZT;
                        if (rm.RZBZ == "是")
                        {
                            var rzjl = _context.RZJL.AsNoTracking().FirstOrDefault(p => p.ROOM_ID == rm.ID && p.RZBZ == "是");
                            if (rzjl != null)
                            {
                                AllRoom[id].LFH = rzjl.LFH;
                            }
                        }
                        AllRoom[id].RefreshRoom();
                    }
                }
                ComputerRoomStatusLable();
            }
            catch (Exception ex)
            {
                var log = Log.Instance;
                log.Abstract = LogAbstractEnum.未知;
                log.Message = "RefreshRoomFromDataBase(List<string> roomIds):" + ex.Message;
                log.Error();
            }
        }
        public void RefreshRoomFromDataBase(string roomId)
        {
            RefreshRoomFromDataBase(new List<string> { roomId });
        }
        /// <summary>
        /// 设置所有房间的可见性
        /// </summary>
        /// <param name="visible"></param>
        private void SetAllRoomVisble(bool visible)
        {
            var tlpHouse1 = tabMain.SelectedTab.Controls[0] as TableLayoutPanel;
            tlpHouse1.SuspendLayout();
            foreach (var item in AllRoom)
            {
                item.Value.Visible = visible;
            }
            tlpHouse1.ResumeLayout(false);
        }


        private string GetRoomStatusLable(string ztmc)
        {
            return ztmc + " " + AllRoom.Count(p => p.Value.Visible == true).ToString() + "/" + AllRoom.Count.ToString();
        }

        /// <summary>
        /// 重新计算房间状态标签
        /// </summary>
        private void ComputerRoomStatusLable()
        {
            string zs = AllRoom.Count.ToString();
            lbftall_cnt.Text = zs.ToString() + "间";
            var _context = MyDataContext.GetDataContext;
            int cnt = _context.RZJL.Where(p => p.YDBZ == "是").Select(p => p.ROOM.ID).Distinct().Count();
            lbydf_cnt.Text = cnt.ToString() + "间";

            var roomLst = _context.ROOM.ToList();
            cnt = roomLst.Count(p => p.RZBZ == "是");
            lbzf_cnt.Text = cnt.ToString() + "间";

            lbrzl.Text =  (Math.Round(Convert.ToDouble(cnt) / Convert.ToDouble(AllRoom.Count) * 100, 2)).ToString() + "%";

            cnt = roomLst.Count(p => p.ZT == "空净房");
            lbkjf_cnt.Text = cnt.ToString() + "间";

            cnt = roomLst.Count(p => p.ZT == "维修房");
            lbwxf_cnt.Text = cnt.ToString() + "间";

            cnt = roomLst.Count(p => p.ZT == "空脏房");
            lbkzf_cnt.Text = cnt.ToString() + "间";

            cnt = roomLst.Count(p => p.ZT == "散客房");
            lbskf.Text = "散客房 " + cnt.ToString() + " 间";

            cnt = roomLst.Count(p => p.ZT == "团队房");
            lbtdf.Text = "团队房 " + cnt.ToString() + " 间";

            cnt = roomLst.Count(p => p.ZT == "钟点房");
            lbzdf.Text = "钟点房 " + cnt.ToString() + " 间";

            cnt = roomLst.Count(p => p.ZT == "长包房");
            lbcbf.Text = "长包房 " + cnt.ToString() + " 间";

            cnt = roomLst.Count(p => p.ZT == "免费房");
            lbmff.Text = "免费房 " + cnt.ToString() + " 间";

            //cnt = roomLst.Count(p => p.ZT == "VIP房");
            //lbvipf.Text = "VIP房 " + cnt.ToString() + " 间";

            cnt = roomLst.Count(p => p.ZT == "自用房");
            lbzyf.Text = "自用房 " + cnt.ToString() + " 间";

            cnt = roomLst.Count(p => p.ZT == "保密房");
            lbbmf.Text = "保密房 " + cnt.ToString() + " 间";

            cnt = roomLst.Count(p => p.ZT == "离店房");
            lbldf.Text = "离店房 " + cnt.ToString() + " 间";

            cnt = roomLst.Count(p => p.ZT == "清理房");
            lbqlf.Text = "清理房 " + cnt.ToString() + " 间";

            //cnt = roomLst.Count(p => p.ZT == "毛病房");
            //lbmbf.Text = "毛病房 " + cnt.ToString() + " 间";

            cnt = roomLst.Count(p => p.ZT == "锁定房");
            lbsdf.Text = "锁定房 " + cnt.ToString() + " 间";
            _context.Dispose();
        }
        #region 右侧房态事件
        private void lbydf_Click(object sender, EventArgs e)
        {
            plnTootip.Visible = false;
            var tlpHouse1 = tabMain.SelectedTab.Controls[0] as TableLayoutPanel;
            tlpHouse1.Visible = false;
            tlpHouse1.SuspendLayout();
            var _context = MyDataContext.GetDataContext;
            var qry = _context.RZJL.Where(p => p.YDBZ == "是").Select(p => p.ROOM.ID).ToList();
            foreach (var item in qry)
            {
                if (AllRoom.ContainsKey(item) && AllRoom[item].Visible == false)
                    AllRoom[item].Visible = true;
            }
            foreach (var item in AllRoom.Keys.Except(qry))
            {
                AllRoom[item].Visible = false;
            }
            tlpHouse1.ResumeLayout();
            tlpHouse1.Visible = true;
            _context.Dispose();
        }

        /// <summary>
        /// 房态共用事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbkjf_Click(object sender, EventArgs e)
        {
            plnTootip.Visible = false;
            var tlpHouse1 = tabMain.SelectedTab.Controls[0] as TableLayoutPanel;
            tlpHouse1.Visible = false;
            tlpHouse1.SuspendLayout();
            string zt = (sender as Control).Tag.ToString();
            var _context = MyDataContext.GetDataContext;
            var qry = _context.ROOM.Where(p => p.ZT == zt).Select(p => p.ID).ToList();
            foreach (var item in qry)
            {
                if (AllRoom.ContainsKey(item) && AllRoom[item].Visible == false)
                    AllRoom[item].Visible = true;
            }
            foreach (var item in AllRoom.Keys.Except(qry))
            {
                AllRoom[item].Visible = false;
            }
            tlpHouse1.ResumeLayout();
            tlpHouse1.Visible = true;
            _context.Dispose();
        }

        /// <summary>
        /// 全部房间
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbftall_Click(object sender, EventArgs e)
        {
            plnTootip.Visible = false;
            SetAllRoomVisble(true);
        }
        /// <summary>
        /// 在住房
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbzf_Click(object sender, EventArgs e)
        {
            plnTootip.Visible = false;
            var tlpHouse1 = tabMain.SelectedTab.Controls[0] as TableLayoutPanel;
            tlpHouse1.SuspendLayout();
            tlpHouse1.Visible = false;
            var _context = MyDataContext.GetDataContext;
            var qry = _context.ROOM.Where(p => p.RZBZ == "是").Select(p => p.ID).ToList();
            foreach (var item in qry)
            {
                if (AllRoom.ContainsKey(item) && AllRoom[item].Visible == false)
                    AllRoom[item].Visible = true;
            }
            foreach (var item in AllRoom.Keys.Except(qry))
            {
                AllRoom[item].Visible = false;
            }
            tlpHouse1.ResumeLayout();
            tlpHouse1.Visible = true;
            _context.Dispose();
        }

        #endregion

        private void btntooltipClose_Click(object sender, EventArgs e)
        {
            plnTootip.Visible = false;
        }

        private void MainControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            var tlpHouse1 = tabMain.SelectedTab.Controls[0] as TableLayoutPanel;
            tlpHouse1.Visible = false;
        }

        private void chkpop_CheckedChanged(object sender, EventArgs e)
        {
            if (plnTootip.Visible == true)
                plnTootip.Visible = false;
        }

        private void chksjft_CheckedChanged(object sender, EventArgs e)
        {
            if (chksjft.Checked)
            {
                RoomStatusTimer.Enabled = true;
                RoomStatusTimer.AutoReset = true;
            }
            else
            {
                RoomStatusTimer.Enabled = false;
                RoomStatusTimer.AutoReset = false;
            }
        }

        #region 定时刷新房态

        /// <summary>
        /// 建立更新房态Timer
        /// </summary>
        private void CreateUpdateRoomStatusTimer()
        {
            //定时器
            if (RoomStatusTimer == null)
                RoomStatusTimer = new System.Timers.Timer(UcRoomSet.GetUcRoomSet.Frequency * 1000);//60 * 1000
            RoomStatusTimer.Elapsed += new System.Timers.ElapsedEventHandler(ExecuteUpdateRoomStatus);//到达时间的时候执行事件； 
            RoomStatusTimer.AutoReset = true;//设置是执行一次（false）还是一直执行(true)； 
            RoomStatusTimer.Enabled = true;           
        }
        /// <summary>
        /// 执行更新房态
        /// </summary>
        private void ExecuteUpdateRoomStatus(object source, System.Timers.ElapsedEventArgs e)
        {
            //RoomStatusTimer.Enabled = false;
            //myWorker.RunWorkerAsync();
            Thread myThread = new Thread(new ThreadStart(this.CreateThreadUpdateRoomStatus));
            myThread.IsBackground = true;
            myThread.Start();
        }        

        #endregion

        private void MainControl_Activated(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        #region 非住房菜单

        private void btnfzf_Click(object sender, EventArgs e)
        {
            Point formPoint = Control.MousePosition;
            contextMenuStripFzf.Show(formPoint);
        }       

        private void tmi_xfrd_Click(object sender, EventArgs e)
        {
            var rzzh = "";
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjl = _context.RZJL.FirstOrDefault(p => p.ID == SysConsts.Room9999RZID);
                if (rzjl == null)
                {
                    _context.InitRoom9999();
                    rzjl = _context.RZJL.FirstOrDefault(p => p.ID == SysConsts.Room9999RZID);
                }
                if (rzjl == null)
                {
                    MessageBox.Show("没有找到99999房间的入住记录!");
                    return;
                }
                rzzh = rzjl.ID;
            }
            var frm = new krfyMaintain(rzzh);
            frm.ShowDialog();
        }

        private void tmp_hysgl_Click(object sender, EventArgs e)
        {
            var frm = new HuiYiShiFrm();
            frm.ShowDialog();
        }

        #endregion

        private void btnrefresh_Click(object sender, EventArgs e)
        {
            using (var l_context = MyDataContext.GetDataContext)
            {
                var listroom = (from q in l_context.ROOM.AsNoTracking()
                                select new
                                {
                                    q.ID,
                                    q.ZT,
                                    q.RZBZ
                                }).ToList();
                var l_room = listroom.First();
                foreach (var item in AllRoom)
                {
                    l_room = listroom.First(p => p.ID == item.Key);
                    item.Value.Room.ZT = l_room.ZT;
                    item.Value.Room.RZBZ = l_room.RZBZ;
                    if (l_room.RZBZ == "是")
                    {
                        var rzjl = l_context.RZJL.AsNoTracking().FirstOrDefault(p => p.ROOM_ID == l_room.ID && p.RZBZ == "是");
                        if (rzjl != null)
                        {
                            item.Value.LFH = rzjl.LFH;
                        }
                        
                    }
                       
                    item.Value.RefreshRoom();                    
                }
                //钟点房到时提醒
                var dt = DateTime.Now.AddMinutes(5);
                var qry = (l_context.RZJL.AsNoTracking().Where(p => p.KRLB == RoomStstusCnName.ZhongDian && p.LDRQ <= dt)).ToList();
                string msg = "";
                if (qry.Count > 0)
                {
                    msg = "有下列钟点房5分钟后到期：\r\n";
                    foreach (var item in qry.ToList())
                    {
                        msg += "房间号：" + item.ROOM.ID + "\r\n";
                    }
                    // MessageBox.Show(msg);
                    var frm = new NoModalDialog("钟点房到期提示", msg);
                    frm.Show();
                }
            }                           
        }
        //读房卡信息
        private void btnKeycard_Click(object sender, EventArgs e)
        {          
            if (keycardFrm == null)
                keycardFrm = new ReadKeycardFrm();
            keycardFrm.ShowCardInfo();
        }
        
        //操作日志对话框
        CaoZuorzDialog czFrm;
        private void tmiccrz_Click(object sender, EventArgs e)
        {
            if (czFrm == null)
                czFrm = new CaoZuorzDialog();
            if (!CheckucRoom(currentURoom))
                return;
            List<RZJL> rzjls;
            using (var _context = MyDataContext.GetDataContext)
            {
                rzjls = _context.RZJL.Where(p => p.ROOM_ID == currentURoom.Room.ID && p.RZBZ == "是").ToList();
            }
            if (rzjls.Count > 0)
                czFrm.ShowByRzjl(rzjls.First().ID);
            else
                czFrm.ShowByRoom(currentURoom.Room.ID);
        }

        private void initMenu()
        {
            var _context = new hotel.DAL.hotelEntities();
            int zxh =1;
            string f_cdbm= "contextMenuRoom";
            var pmodel = new CDQX { YHID = SysConsts.SystemAdminId };
            pmodel.CDMC = "房态管理弹出菜单";
            pmodel.SFKY = 1;
            pmodel.ZXH = zxh;
            pmodel.Z_CDBM = f_cdbm;
            _context.CDQX.Add(pmodel);
            foreach (ToolStripItem item in contextMenuRoom.Items)
            {
                if (!(item is ToolStripMenuItem))
                    continue;
                var model = new CDQX { YHID = SysConsts.SystemAdminId };
                model.CDMC = item.Text;
                model.F_CDBM = f_cdbm;
                model.SFKY = 1;
                model.Z_CDBM = item.Name + "_No";
                model.TC_CDBM = item.Name;
                model.ZXH = zxh;
                _context.CDQX.Add(model);
                zxh++;
            }
            _context.SaveChanges();
        }
    }
}
