﻿namespace hotel.Win.Query
{
    partial class FeiRuZhuXiaoFei
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnquery = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.maingrid = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XFSL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnBZSM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XFID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ucQueryToolBar1 = new hotel.Win.Query.ucQueryToolBar();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(131, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 463);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(807, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "ff";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.btnquery);
            this.groupBox2.Controls.Add(this.dateTimePicker2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.dateTimePicker1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(807, 48);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "查询条件";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(664, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(137, 12);
            this.label10.TabIndex = 27;
            this.label10.Text = "按住Ctrl键可以多选打印";
            // 
            // btnquery
            // 
            this.btnquery.Location = new System.Drawing.Point(415, 9);
            this.btnquery.Name = "btnquery";
            this.btnquery.Size = new System.Drawing.Size(94, 32);
            this.btnquery.TabIndex = 9;
            this.btnquery.Text = "开始查询";
            this.btnquery.UseVisualStyleBackColor = true;
            this.btnquery.Click += new System.EventHandler(this.btnquery_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(263, 12);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(121, 23);
            this.dateTimePicker2.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 7;
            this.label1.Text = "入账日期从";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 14);
            this.label2.TabIndex = 7;
            this.label2.Text = "到";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(103, 12);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(121, 23);
            this.dateTimePicker1.TabIndex = 6;
            this.dateTimePicker1.Value = new System.DateTime(2010, 12, 30, 0, 0, 0, 0);
            // 
            // maingrid
            // 
            this.maingrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.maingrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column1,
            this.XFSL,
            this.Column6,
            this.Column2,
            this.Column3,
            this.ColumnBZSM,
            this.XFID});
            this.maingrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maingrid.Location = new System.Drawing.Point(0, 48);
            this.maingrid.Name = "maingrid";
            this.maingrid.RowHeadersWidth = 20;
            this.maingrid.RowTemplate.Height = 23;
            this.maingrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.maingrid.Size = new System.Drawing.Size(807, 415);
            this.maingrid.TabIndex = 12;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "XFXM_MC";
            this.Column4.HeaderText = "项目名称";
            this.Column4.Name = "Column4";
            this.Column4.Width = 90;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "JE";
            this.Column1.HeaderText = "金额";
            this.Column1.Name = "Column1";
            this.Column1.Width = 70;
            // 
            // XFSL
            // 
            this.XFSL.DataPropertyName = "SL";
            this.XFSL.HeaderText = "数量";
            this.XFSL.Name = "XFSL";
            this.XFSL.Width = 60;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "DJ";
            this.Column6.HeaderText = "单价";
            this.Column6.Name = "Column6";
            this.Column6.Width = 60;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "CJRQ";
            this.Column2.HeaderText = "入帐日期";
            this.Column2.Name = "Column2";
            this.Column2.Width = 120;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "CJRY";
            this.Column3.HeaderText = "入帐人";
            this.Column3.Name = "Column3";
            // 
            // ColumnBZSM
            // 
            this.ColumnBZSM.DataPropertyName = "BZSM";
            this.ColumnBZSM.HeaderText = "备注说明";
            this.ColumnBZSM.Name = "ColumnBZSM";
            // 
            // XFID
            // 
            this.XFID.DataPropertyName = "ID";
            this.XFID.HeaderText = "ID";
            this.XFID.Name = "XFID";
            this.XFID.Visible = false;
            // 
            // ucQueryToolBar1
            // 
            this.ucQueryToolBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.ucQueryToolBar1.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucQueryToolBar1.Location = new System.Drawing.Point(807, 0);
            this.ucQueryToolBar1.Name = "ucQueryToolBar1";
            this.ucQueryToolBar1.Size = new System.Drawing.Size(81, 485);
            this.ucQueryToolBar1.TabIndex = 12;
            this.ucQueryToolBar1.VisibleClose = true;
            this.ucQueryToolBar1.VisibleCombine = false;
            this.ucQueryToolBar1.VisibleExpExcel = true;
            this.ucQueryToolBar1.VisibleFind = false;
            this.ucQueryToolBar1.VisibleRefresh = true;
            // 
            // FeiRuZhuXiaoFei
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 485);
            this.Controls.Add(this.maingrid);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ucQueryToolBar1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FeiRuZhuXiaoFei";
            this.Text = "非入住消费账务";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private ucQueryToolBar ucQueryToolBar1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnquery;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DataGridView maingrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn XFSL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnBZSM;
        private System.Windows.Forms.DataGridViewTextBoxColumn XFID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
    }
}