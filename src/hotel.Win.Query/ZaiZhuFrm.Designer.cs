﻿namespace hotel.Win.Query
{
    partial class ZaiZhuFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtxm = new System.Windows.Forms.TextBox();
            this.btnquery = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.maingrid = new System.Windows.Forms.DataGridView();
            this.ucQueryToolBar1 = new hotel.Win.Query.ucQueryToolBar();
            this.chbxQf = new System.Windows.Forms.CheckBox();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(131, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 426);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(737, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "ff";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chbxQf);
            this.groupBox2.Controls.Add(this.txtxm);
            this.groupBox2.Controls.Add(this.btnquery);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(653, 44);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "查询条件";
            // 
            // txtxm
            // 
            this.txtxm.Location = new System.Drawing.Point(72, 15);
            this.txtxm.Name = "txtxm";
            this.txtxm.Size = new System.Drawing.Size(135, 23);
            this.txtxm.TabIndex = 10;
            this.txtxm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtxm_KeyDown);
            // 
            // btnquery
            // 
            this.btnquery.Location = new System.Drawing.Point(213, 15);
            this.btnquery.Name = "btnquery";
            this.btnquery.Size = new System.Drawing.Size(106, 25);
            this.btnquery.TabIndex = 9;
            this.btnquery.Text = "开始查询";
            this.btnquery.UseVisualStyleBackColor = true;
            this.btnquery.Click += new System.EventHandler(this.btnquery_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "姓名：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.maingrid);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(653, 382);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "在住客人信息";
            // 
            // maingrid
            // 
            this.maingrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.maingrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maingrid.Location = new System.Drawing.Point(3, 19);
            this.maingrid.Name = "maingrid";
            this.maingrid.ReadOnly = true;
            this.maingrid.RowHeadersWidth = 25;
            this.maingrid.RowTemplate.Height = 23;
            this.maingrid.Size = new System.Drawing.Size(647, 360);
            this.maingrid.TabIndex = 1;
            this.maingrid.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.maingrid_DataBindingComplete);
            // 
            // ucQueryToolBar1
            // 
            this.ucQueryToolBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.ucQueryToolBar1.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucQueryToolBar1.Location = new System.Drawing.Point(653, 0);
            this.ucQueryToolBar1.Name = "ucQueryToolBar1";
            this.ucQueryToolBar1.Size = new System.Drawing.Size(84, 426);
            this.ucQueryToolBar1.TabIndex = 7;
            this.ucQueryToolBar1.VisibleClose = true;
            this.ucQueryToolBar1.VisibleCombine = false;
            this.ucQueryToolBar1.VisibleExpExcel = true;
            this.ucQueryToolBar1.VisibleFind = false;
            this.ucQueryToolBar1.VisibleRefresh = true;
            // 
            // chbxQf
            // 
            this.chbxQf.AutoSize = true;
            this.chbxQf.Location = new System.Drawing.Point(336, 20);
            this.chbxQf.Name = "chbxQf";
            this.chbxQf.Size = new System.Drawing.Size(82, 18);
            this.chbxQf.TabIndex = 11;
            this.chbxQf.Text = "欠费客人";
            this.chbxQf.UseVisualStyleBackColor = true;
            this.chbxQf.CheckedChanged += new System.EventHandler(this.chbxQf_CheckedChanged);
            // 
            // ZaiZhuFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 448);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.ucQueryToolBar1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ZaiZhuFrm";
            this.ShowInTaskbar = false;
            this.Text = "在住客人信息";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maingrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private ucQueryToolBar ucQueryToolBar1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtxm;
        private System.Windows.Forms.Button btnquery;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView maingrid;
        private System.Windows.Forms.CheckBox chbxQf;
    }
}