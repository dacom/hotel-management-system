﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.DAL;
using hotel.Common;
using hotel.Win.Common;
using System.Data.Common;

namespace hotel.Win.Query
{
    /// <summary>
    /// 消费入帐[补救]
    /// </summary>
    public partial class XiaoFeiRuZhangMt : Form
    {
        hotelEntities _context;
        RZJL_ARC rzjlModel;
        public XiaoFeiRuZhangMt(string rzzh)
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            cobxfl.DataSource = _context.XFXMFL.AsNoTracking().OrderBy(p => p.FLMC).ToList();
            cobxfl.DisplayMember = "FLMC";
            cobxfl.ValueMember = "ID";
            rzjlModel = _context.RZJL_ARC.First(p => p.ID == rzzh);
            txtxfsl.Value = 1;
            gboxfyjl.Text = "房间号[" + rzjlModel.ROOM_ID + "]费用清单";

            gridxm.DataSource = _context.XFXM.AsNoTracking().Where(p => p.KXF == "是").OrderBy(p => p.MC).ToList();
            if (gridxm.Rows.Count > 0)
                gridxm_CellClick(gridxm, null);
            BindGrid();
        }

        private void BindGrid()
        {
            var qry = from q in _context.XFJL_ARC.AsNoTracking()
                                  where q.RZJL_ARC.ID == rzjlModel.ID && q.XFXM_MC != "押金"
                                  orderby q.CJRQ
                                  select new
                                  {
                                      q.CJRQ,
                                      q.CJRY,
                                      q.JE,
                                      q.SL,
                                      q.DJ,
                                      q.XFXM_MC,
                                      q.BZSM,
                                      q.ID
                                  };
            gridmain.DataSource = qry.ToList();
        }

        private void gridxm_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string dj = Common.ControlsUtility.GetActiveGridValue(gridxm, "XMDJ");
            txtdj.Value = Convert.ToDecimal(dj);
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            string XMID = Common.ControlsUtility.GetActiveGridValue(gridxm, "XMID");
            if (XMID == "")
                return;
            if (txtdj.Value == 0)
            {
                MessageBox.Show("必须输入单价，才能正常增加！");
                return;
            }

            int xfid = Convert.ToInt32(XMID);
            var xfxmModel = _context.XFXM.First(p => p.ID == xfid);
            var d =_context.GetNextValue("XFJL_ARC");
            int id = -d.Cast<int>().First(); //用负数
            var xfjlModel = _context.XFJL_ARC.Create();
            xfjlModel.ID = id;
            xfjlModel.RZJL_ID = rzjlModel.ID;
            xfjlModel.ROOM_ID = rzjlModel.ROOM_ID;
            xfjlModel.CJRQ = DateTime.Now;
            xfjlModel.CJRY = UserLoginInfo.FullName;
            xfjlModel.DJ = txtdj.Value;
            xfjlModel.SL = (int)txtxfsl.Value;
            xfjlModel.JE = xfjlModel.DJ * xfjlModel.SL;
            xfjlModel.RZJL_ARC = rzjlModel;
            xfjlModel.XFXM_ID = xfxmModel.ID;
            xfjlModel.XFXM_MC = xfxmModel.MC;
            xfjlModel.XFBZ = "消";
            xfjlModel.ZFFS = "人民币";
            xfjlModel.YJ = 0;
            xfjlModel.BZSM = "结账后补账";
            _context.XFJL_ARC.Add(xfjlModel);
            _context.SaveChanges();
            BindGrid();
            //写日志
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.消费入单;
            logInfo.RoomId = rzjlModel.ROOM_ID;
            logInfo.Rzid = rzjlModel.ID;
            logInfo.Message = "补救费消费，消费项目:" + xfxmModel.MC + ",金额:" + xfjlModel.JE;
            logInfo.Save();
        }

        private void cobxfl_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = cobxfl.SelectedValue.ToString();
            gridxm.AutoGenerateColumns = false;
            gridxm.DataSource = _context.XFXM.AsNoTracking().Where(p => p.XFXMFL.ID == id && p.KXF == "是").OrderBy(p => p.MC).ToList();
        }

        private void txtdm_TextChanged(object sender, EventArgs e)
        {
            //string esql = "select a.DM,a.MC,a.DJ,a.YWMC,a.ID from XFXM as a where a.KXF ='是' and  a.YWMC like @ywmc order by mc";

            //ObjectParameter op = new ObjectParameter("ywmc", "%" + txtdm.Text.Trim() + "%");
            //ObjectQuery<DbDataRecord> query = _context.CreateQuery<DbDataRecord>(esql, op);
            var ywmc = txtdm.Text.Trim();
            gridxm.DataSource = _context.XFXM.AsNoTracking().Where(p => p.YWMC.Contains(ywmc)).ToList();
            if (gridxm.Rows.Count > 0)
                gridxm_CellClick(gridxm, null);
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private XFJL_ARC GetSelectXFJL()
        {
            string XFID = Common.ControlsUtility.GetActiveGridValue(gridmain, "XFID");
            if (XFID == "")
            {
                MessageBox.Show("请选择一个消费记录!");
                return null;
            }
            int id = Convert.ToInt32(XFID);
            return _context.XFJL_ARC.FirstOrDefault(p => p.ID == id);
        }
        private void btnctxz_Click(object sender, EventArgs e)
        {
            var xfjlModel = GetSelectXFJL();
            if (xfjlModel == null)
                return;
            if (xfjlModel.JE.Value <= 0)
            {
                MessageBox.Show("选中的消费项目小于0，操作失败");
                return;
            }

            if (MessageBox.Show("消费金额:" + xfjlModel.JE.ToString() + "\n你确定冲调吗？", "费用冲调", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                return;
            var newModel = new XFJL_ARC();
            var d = _context.GetNextValue("XFJL_ARC");
            int id = -d.Cast<int>().First(); //用负数
            newModel.ID = id;
            newModel.CJRQ = DateTime.Now;
            newModel.CJRY = UserLoginInfo.FullName;
            newModel.DJ = 0;
            newModel.JE = -xfjlModel.JE; //负值
            newModel.SL = 1;
            newModel.ROOM_ID = xfjlModel.ROOM_ID;
            newModel.RZJL_ID = xfjlModel.RZJL_ID;
            newModel.XFXM_ID =xfjlModel.XFXM_ID;
            newModel.XFXM_MC =xfjlModel.XFXM_MC;
            newModel.YJ = 0;
            newModel.ZFFS = xfjlModel.ZFFS;
            newModel.XFBZ = "冲";
            newModel.BZSM = "消费冲调";
            _context.XFJL_ARC.Add(newModel);
            _context.SaveChanges();
            BindGrid();
            #region 日志
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.消费退单;
            logInfo.RoomId = rzjlModel.ROOM_ID;
            logInfo.Rzid = rzjlModel.ID;
            logInfo.Message = "结账后，冲调消费，金额:" + xfjlModel.YJ.ToString();
            logInfo.Save();
            #endregion
        }
    }
}
