﻿namespace hotel.Win.Query
{
    partial class ucQueryToolBar
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.mainToolBar = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnFind = new System.Windows.Forms.ToolStripButton();
            this.tsbtnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnexp = new System.Windows.Forms.ToolStripButton();
            this.tsbtnCombine = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnClose = new System.Windows.Forms.ToolStripButton();
            this.mainToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainToolBar
            // 
            this.mainToolBar.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.mainToolBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.mainToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.tsbtnFind,
            this.tsbtnRefresh,
            this.toolStripSeparator1,
            this.tsbtnexp,
            this.tsbtnCombine,
            this.toolStripSeparator3,
            this.tsbtnClose});
            this.mainToolBar.Location = new System.Drawing.Point(0, 0);
            this.mainToolBar.Name = "mainToolBar";
            this.mainToolBar.Size = new System.Drawing.Size(82, 231);
            this.mainToolBar.TabIndex = 3;
            this.mainToolBar.Text = "toolStrip1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(79, 6);
            // 
            // tsbtnFind
            // 
            this.tsbtnFind.Image = global::hotel.Win.Query.Properties.Resources.Find;
            this.tsbtnFind.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbtnFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnFind.Name = "tsbtnFind";
            this.tsbtnFind.Size = new System.Drawing.Size(79, 21);
            this.tsbtnFind.Text = "查找  ";
            this.tsbtnFind.Click += new System.EventHandler(this.tsbtnFind_Click);
            // 
            // tsbtnRefresh
            // 
            this.tsbtnRefresh.Image = global::hotel.Win.Query.Properties.Resources.arrow_refresh;
            this.tsbtnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbtnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnRefresh.Name = "tsbtnRefresh";
            this.tsbtnRefresh.Size = new System.Drawing.Size(79, 21);
            this.tsbtnRefresh.Text = "刷新数据";
            this.tsbtnRefresh.Click += new System.EventHandler(this.tsbtnRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(79, 6);
            // 
            // tsbtnexp
            // 
            this.tsbtnexp.Image = global::hotel.Win.Query.Properties.Resources.excel1;
            this.tsbtnexp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbtnexp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnexp.Name = "tsbtnexp";
            this.tsbtnexp.Size = new System.Drawing.Size(79, 21);
            this.tsbtnexp.Text = "导出Excel";
            this.tsbtnexp.Click += new System.EventHandler(this.tsbtnexp_Click);
            // 
            // tsbtnCombine
            // 
            this.tsbtnCombine.Image = global::hotel.Win.Query.Properties.Resources.filter;
            this.tsbtnCombine.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbtnCombine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCombine.Name = "tsbtnCombine";
            this.tsbtnCombine.Size = new System.Drawing.Size(79, 21);
            this.tsbtnCombine.Text = "组合查询";
            this.tsbtnCombine.Click += new System.EventHandler(this.tsbtnCombine_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(79, 6);
            // 
            // tsbtnClose
            // 
            this.tsbtnClose.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.tsbtnClose.Image = global::hotel.Win.Query.Properties.Resources.control_power_blue;
            this.tsbtnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnClose.Name = "tsbtnClose";
            this.tsbtnClose.Size = new System.Drawing.Size(79, 24);
            this.tsbtnClose.Text = "关  闭";
            this.tsbtnClose.Click += new System.EventHandler(this.tsbtnClose_Click);
            // 
            // ucQueryToolBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainToolBar);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ucQueryToolBar";
            this.Size = new System.Drawing.Size(98, 231);
            this.mainToolBar.ResumeLayout(false);
            this.mainToolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip mainToolBar;
        private System.Windows.Forms.ToolStripButton tsbtnexp;
        private System.Windows.Forms.ToolStripButton tsbtnRefresh;
        private System.Windows.Forms.ToolStripButton tsbtnFind;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbtnCombine;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbtnClose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    }
}
