﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;

namespace hotel.Win.Query
{
    public partial class CaoZuorzDialog : Form
    {
        public CaoZuorzDialog()
        {
            InitializeComponent();
            ControlsUtility.ResetColumnFromXML(maingrid);
        }
        public void ShowByRzjl(string rzid)
        {
            this.Text = "入住账号=" + rzid +"的，全部操作日志";
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.StartPosition = FormStartPosition.CenterParent;
            var _context = MyDataContext.GetDataContext;
            var qry = from ys in _context.CCRZ.AsNoTracking()
                      where ys.RZID == rzid
                      orderby ys.RQ descending
                      select ys;

            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            statusStrip1.Items[0].Text = "当前入住，共" + maingrid.Rows.Count.ToString() + "行";
            this.ShowDialog();
        }
        public void ShowByRoom(string roomId)
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Text = "房间号=" + roomId + ",最近7天的操作日志";
            this.StartPosition = FormStartPosition.CenterParent;
            var _context = MyDataContext.GetDataContext;
            var rq = DateTime.Now.Date.AddDays(-7);
            var qry = from ys in _context.CCRZ.AsNoTracking()
                      where ys.ROOM_ID == roomId && ys.RQ > rq
                      orderby ys.RQ descending
                      select ys;

            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            statusStrip1.Items[0].Text = "7天内，共" + maingrid.Rows.Count.ToString() + "行";
            this.ShowDialog();
        }

        private void maingrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (maingrid.Columns[e.ColumnIndex].Name == "Message")
                MessageBox.Show(maingrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
        }
    }
}
