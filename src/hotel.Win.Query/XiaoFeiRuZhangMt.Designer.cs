﻿namespace hotel.Win.Query
{
    partial class XiaoFeiRuZhangMt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridmain = new System.Windows.Forms.DataGridView();
            this.gboxfyjl = new System.Windows.Forms.GroupBox();
            this.txtdj = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gridxm = new System.Windows.Forms.DataGridView();
            this.XMDM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XMDJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.cobxfl = new System.Windows.Forms.ComboBox();
            this.txtdm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnadd = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtxfsl = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XFSL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnBZSM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XFID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnctxz = new System.Windows.Forms.Button();
            this.btnclose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridmain)).BeginInit();
            this.gboxfyjl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdj)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridxm)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtxfsl)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridmain
            // 
            this.gridmain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridmain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column1,
            this.XFSL,
            this.Column6,
            this.Column2,
            this.Column3,
            this.ColumnBZSM,
            this.XFID});
            this.gridmain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridmain.Location = new System.Drawing.Point(3, 65);
            this.gridmain.Name = "gridmain";
            this.gridmain.RowHeadersWidth = 20;
            this.gridmain.RowTemplate.Height = 23;
            this.gridmain.Size = new System.Drawing.Size(481, 444);
            this.gridmain.TabIndex = 6;
            // 
            // gboxfyjl
            // 
            this.gboxfyjl.Controls.Add(this.gridmain);
            this.gboxfyjl.Controls.Add(this.panel2);
            this.gboxfyjl.Location = new System.Drawing.Point(409, 3);
            this.gboxfyjl.Name = "gboxfyjl";
            this.gboxfyjl.Size = new System.Drawing.Size(487, 512);
            this.gboxfyjl.TabIndex = 31;
            this.gboxfyjl.TabStop = false;
            this.gboxfyjl.Text = "客人";
            // 
            // txtdj
            // 
            this.txtdj.DecimalPlaces = 2;
            this.txtdj.Location = new System.Drawing.Point(208, 28);
            this.txtdj.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtdj.Name = "txtdj";
            this.txtdj.Size = new System.Drawing.Size(91, 23);
            this.txtdj.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gridxm);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 454);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "消费项目选择";
            // 
            // gridxm
            // 
            this.gridxm.AllowUserToAddRows = false;
            this.gridxm.AllowUserToDeleteRows = false;
            this.gridxm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridxm.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.XMDM,
            this.Column10,
            this.Column14,
            this.XMDJ,
            this.XMID});
            this.gridxm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridxm.Location = new System.Drawing.Point(3, 63);
            this.gridxm.Name = "gridxm";
            this.gridxm.ReadOnly = true;
            this.gridxm.RowHeadersWidth = 15;
            this.gridxm.RowTemplate.Height = 23;
            this.gridxm.Size = new System.Drawing.Size(394, 388);
            this.gridxm.TabIndex = 5;
            this.gridxm.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridxm_CellClick);
            // 
            // XMDM
            // 
            this.XMDM.DataPropertyName = "DM";
            this.XMDM.HeaderText = "代码";
            this.XMDM.Name = "XMDM";
            this.XMDM.ReadOnly = true;
            this.XMDM.Width = 60;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "MC";
            this.Column10.HeaderText = "名称";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "YWMC";
            this.Column14.HeaderText = "快捷码";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 80;
            // 
            // XMDJ
            // 
            this.XMDJ.DataPropertyName = "DJ";
            this.XMDJ.HeaderText = "单价";
            this.XMDJ.Name = "XMDJ";
            this.XMDJ.ReadOnly = true;
            this.XMDJ.Width = 60;
            // 
            // XMID
            // 
            this.XMID.DataPropertyName = "ID";
            this.XMID.HeaderText = "ID";
            this.XMID.Name = "XMID";
            this.XMID.ReadOnly = true;
            this.XMID.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cobxfl);
            this.panel1.Controls.Add(this.txtdm);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(394, 44);
            this.panel1.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 24;
            this.label1.Text = "分类";
            // 
            // cobxfl
            // 
            this.cobxfl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobxfl.FormattingEnabled = true;
            this.cobxfl.Location = new System.Drawing.Point(49, 10);
            this.cobxfl.Name = "cobxfl";
            this.cobxfl.Size = new System.Drawing.Size(123, 21);
            this.cobxfl.TabIndex = 3;
            this.cobxfl.SelectedIndexChanged += new System.EventHandler(this.cobxfl_SelectedIndexChanged);
            // 
            // txtdm
            // 
            this.txtdm.Location = new System.Drawing.Point(271, 10);
            this.txtdm.Name = "txtdm";
            this.txtdm.Size = new System.Drawing.Size(119, 23);
            this.txtdm.TabIndex = 4;
            this.txtdm.TextChanged += new System.EventHandler(this.txtdm_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(180, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "代码/拼音";
            // 
            // btnadd
            // 
            this.btnadd.Location = new System.Drawing.Point(306, 25);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(90, 29);
            this.btnadd.TabIndex = 2;
            this.btnadd.Text = "增加消费";
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.btnadd);
            this.groupBox3.Controls.Add(this.txtdj);
            this.groupBox3.Controls.Add(this.txtxfsl);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(6, 3);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox3.Size = new System.Drawing.Size(400, 512);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "增加消费";
            // 
            // txtxfsl
            // 
            this.txtxfsl.Location = new System.Drawing.Point(87, 29);
            this.txtxfsl.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtxfsl.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtxfsl.Name = "txtxfsl";
            this.txtxfsl.Size = new System.Drawing.Size(69, 23);
            this.txtxfsl.TabIndex = 0;
            this.txtxfsl.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(163, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 20;
            this.label2.Text = "单价：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 14);
            this.label14.TabIndex = 2;
            this.label14.Text = "消费数量：";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "XFXM_MC";
            this.Column4.HeaderText = "项目名称";
            this.Column4.Name = "Column4";
            this.Column4.Width = 90;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "JE";
            this.Column1.HeaderText = "金额";
            this.Column1.Name = "Column1";
            this.Column1.Width = 70;
            // 
            // XFSL
            // 
            this.XFSL.DataPropertyName = "SL";
            this.XFSL.HeaderText = "数量";
            this.XFSL.Name = "XFSL";
            this.XFSL.Width = 60;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "DJ";
            this.Column6.HeaderText = "单价";
            this.Column6.Name = "Column6";
            this.Column6.Width = 60;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "CJRQ";
            this.Column2.HeaderText = "入帐日期";
            this.Column2.Name = "Column2";
            this.Column2.Width = 120;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "CJRY";
            this.Column3.HeaderText = "入帐人";
            this.Column3.Name = "Column3";
            // 
            // ColumnBZSM
            // 
            this.ColumnBZSM.DataPropertyName = "BZSM";
            this.ColumnBZSM.HeaderText = "备注说明";
            this.ColumnBZSM.Name = "ColumnBZSM";
            // 
            // XFID
            // 
            this.XFID.DataPropertyName = "ID";
            this.XFID.HeaderText = "ID";
            this.XFID.Name = "XFID";
            this.XFID.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnclose);
            this.panel2.Controls.Add(this.btnctxz);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(481, 46);
            this.panel2.TabIndex = 7;
            // 
            // btnctxz
            // 
            this.btnctxz.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnctxz.Location = new System.Drawing.Point(14, 6);
            this.btnctxz.Name = "btnctxz";
            this.btnctxz.Size = new System.Drawing.Size(128, 30);
            this.btnctxz.TabIndex = 26;
            this.btnctxz.Text = "冲调选中的消费";
            this.btnctxz.UseVisualStyleBackColor = true;
            this.btnctxz.Click += new System.EventHandler(this.btnctxz_Click);
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(388, 6);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(90, 29);
            this.btnclose.TabIndex = 27;
            this.btnclose.Text = "关  闭";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // XiaoFeiRuZhangMt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 521);
            this.Controls.Add(this.gboxfyjl);
            this.Controls.Add(this.groupBox3);
            this.Font = new System.Drawing.Font("SimSun", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "XiaoFeiRuZhangMt";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "消费入帐[补救]";
            ((System.ComponentModel.ISupportInitialize)(this.gridmain)).EndInit();
            this.gboxfyjl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtdj)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridxm)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtxfsl)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridmain;
        private System.Windows.Forms.GroupBox gboxfyjl;
        private System.Windows.Forms.NumericUpDown txtdj;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView gridxm;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMDM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMDJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMID;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cobxfl;
        private System.Windows.Forms.TextBox txtdm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown txtxfsl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn XFSL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnBZSM;
        private System.Windows.Forms.DataGridViewTextBoxColumn XFID;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Button btnctxz;
    }
}