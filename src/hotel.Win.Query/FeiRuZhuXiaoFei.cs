﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
using hotel.Win.Report;
using hotel.DAL;
using System.Linq.Expressions;
using hotel.Common;

namespace hotel.Win.Query
{
    /// <summary>
    /// 非入住消费
    /// </summary>
    public partial class FeiRuZhuXiaoFei : Form
    {
        hotelEntities _context;
        public FeiRuZhuXiaoFei()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            dateTimePicker1.Value = DateTime.Now.Date;
            dateTimePicker2.Value = DateTime.Now;
            BindData();
            ucQueryToolBar1.TargetDataGrid = maingrid;
            ucQueryToolBar1.RefreshFunction = () => { BindData(); };
            AddButton();
        }

        private void BindData()
        {           
            var qry = from q in _context.XFJL_ARC.AsNoTracking()
                      where q.RZJL_ID == SysConsts.Room9999RZID && q.CJRQ >= dateTimePicker1.Value && q.CJRQ <= dateTimePicker2.Value
                      orderby q.CJRQ descending
                      select new
                      {
                          q.CJRQ,
                          q.CJRY,
                          q.JE,
                          q.SL,
                          q.DJ,
                          q.XFXM_MC,
                          q.BZSM,
                          q.ID
                      };
            
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
        }
 
        private void btnquery_Click(object sender, EventArgs e)
        {
            BindData();
        }

        /// <summary>            
        /// 增加操作按钮
        /// </summary>
        private void AddButton()
        {
            ToolStripButton item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnPrint";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Size = new System.Drawing.Size(77, 40);
            item.Text = "重新打印";
            item.Image = global::hotel.Win.Query.Properties.Resources.Print;
            item.Click += new System.EventHandler(sbtnPrint_Click);
            ucQueryToolBar1.Insert(item);         
        }
        //补账
        private void sbtnPrint_Click(object sender, EventArgs e)
        {
            var ids = new List<int>();
            foreach (DataGridViewRow item  in maingrid.SelectedRows)
            {
                ids.Add((int)item.Cells["XFID"].Value);
            }
            if (ids.Count == 0)
            {
                MessageBox.Show("请选择你要打印的消费记录，可以多选");
                return;
            }
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.打印;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "非住房消费";
            logInfo.Rzid = SysConsts.Room9999RZID;
            logInfo.RoomId = SysConsts.Room9999;
            ReportEntry.ShowFeiXiaoFeiMingXiBill(ids,logInfo);
        }
       
    }
}
