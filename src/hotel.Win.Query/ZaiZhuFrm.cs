﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
namespace hotel.Win.Query
{
    /// <summary>
    /// 在住人员列表
    /// </summary>
    public partial class ZaiZhuFrm : Form
    {
        public ZaiZhuFrm()
        {
            InitializeComponent();
            ControlsUtility.ResetColumnFromXML(maingrid);
            BindData();
            ucQueryToolBar1.TargetDataGrid = maingrid;            
            ucQueryToolBar1.RefreshFunction = () => { BindData(); };
        }

        private void BindData()
        {
            using (var _context = MyDataContext.GetDataContext)
            {

                var qry = from rz in _context.UV_RZJL_RZRY_Single
                          join t in _context.XFJL on rz.ID equals t.RZJL_ID into g
                          where rz.RZBZ == "是"
                          orderby rz.ROOM_ID
                          select new
                          {                              
                              rz.ID,
                              rz.ROOM_ID,
                              rz.KRXM,
                              rz.KRXB,
                              rz.ZK,
                              rz.SJFJ,
                              rz.YJ,
                              XF = g.Sum(p=>p.JE),
                              YE = rz.YJ - g.Sum(p => p.JE),
                              rz.DDRQ,
                              rz.LDRQ,
                              rz.TS,
                              rz.BZSM,
                              rz.LFH
                          };

                maingrid.AutoGenerateColumns = false;
                var datas = qry.ToList();
                if (txtxm.TextLength > 0)
                    datas = datas.Where(p => p.KRXM != null && p.KRXM.Contains(txtxm.Text)).ToList();
                if (chbxQf.Checked)
                    datas = datas.Where(p => p.YJ - p.XF <= 0).ToList();
                maingrid.DataSource = datas;

                statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
             
            }
        }
        /// <summary>
        /// 自定单元格颜色
        /// </summary>
        private void BuildColor()
        {
            for (int i = 0; i < this.maingrid.Rows.Count; i++)
            {
                if (Convert.ToDecimal(this.maingrid["YE", i].Value)<0)                       
                        this.maingrid["YE", i].Style.BackColor = Color.Lime;                
            }

        }
        private void btnquery_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void txtxm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                BindData();
        }

        private void chbxQf_CheckedChanged(object sender, EventArgs e)
        {
            BindData();
        }

        private void maingrid_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            BuildColor();
        }
    }
}
