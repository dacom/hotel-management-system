﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using hotel.Common;
using hotel.Win.Common;
namespace hotel.Win.Query
{
    public partial class ucQueryToolBar : UserControl
    {
        public Action ExpExcelAfterFunction;
        public Action RefreshFunction;
        public Action CloseFunction;
        public DataGridView TargetDataGrid;
               
        public ucQueryToolBar()
        {
            InitializeComponent();
            VisibleFind = false;
            VisibleCombine = false;
        }

        #region 按钮可见性
        public bool VisibleFind
        {
            get { return tsbtnFind.Visible; }
            set { tsbtnFind.Visible = value; }
        }

        public bool VisibleRefresh
        {
            get { return tsbtnRefresh.Visible; }
            set { tsbtnRefresh.Visible = value; }
        }

        public bool VisibleExpExcel
        {
            get { return tsbtnexp.Visible; }
            set { tsbtnexp.Visible = value; }
        }

        public bool VisibleCombine
        {
            get { return tsbtnCombine.Visible; }
            set { tsbtnCombine.Visible = value; }
        }

        public bool VisibleClose
        {
            get { return tsbtnClose.Visible; }
            set { tsbtnClose.Visible = value; }
        }
        #endregion

        private void tsbtnFind_Click(object sender, EventArgs e)
        {

        }

        private void tsbtnRefresh_Click(object sender, EventArgs e)
        {
            if (RefreshFunction != null)
                RefreshFunction();
        }

        private void tsbtnexp_Click(object sender, EventArgs e)
        {
            if (TargetDataGrid == null)
            {
                MessageBox.Show("没有目标表格");
                return;
            }
            if (TargetDataGrid.DataSource == null)
                return;
            var fname = ExcelHelper.SaveAs(TargetDataGrid);
            if (fname == "")
                return;
            if (!string.IsNullOrEmpty(fname) && MessageBox.Show("是否打开" + fname, hotel.Common.SysConsts.SysInformationCaption, MessageBoxButtons.YesNo) == DialogResult.Yes)
                Process.Start(fname);
            if (ExpExcelAfterFunction != null)
                ExpExcelAfterFunction();
        }

        private void tsbtnCombine_Click(object sender, EventArgs e)
        {

        }

        private void tsbtnClose_Click(object sender, EventArgs e)
        {
            if (CloseFunction != null)
                CloseFunction();
            else
            {
                if (TargetDataGrid == null)
                    return;
                object parent = ControlsUtility.FindForm(TargetDataGrid);
                if (parent is Form)
                   (parent as Form).Close();
            }
        }

        /// <summary>
        /// 在最前面插入
        /// </summary>
        /// <param name="menuItem"></param>
        public void Insert(ToolStripButton menuItem)
        {
            mainToolBar.Items.Insert(0, menuItem);
            if (menuItem.Image == null)
                menuItem.Image = global::hotel.Win.Query.Properties.Resources.table;
            menuItem.ImageAlign = ContentAlignment.MiddleLeft;
        }
        public void Insert(ToolStripItem item)
        {
            mainToolBar.Items.Insert(0, item);
            if (item.Image == null)
                item.Image = global::hotel.Win.Query.Properties.Resources.table;
            item.ImageAlign = ContentAlignment.MiddleLeft;
        }
    }
}
