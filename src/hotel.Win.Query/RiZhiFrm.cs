﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
namespace hotel.Win.Query
{
    /// <summary>
    /// 日志查询
    /// </summary>
    public partial class RiZhiFrm : Form
    {
        public RiZhiFrm()
        {
            InitializeComponent();
            ControlsUtility.ResetColumnFromXML(maingrid);
            dateTimePicker1.Value = DateTime.Now.Date;
            dateTimePicker2.Value = DateTime.Now.Date.AddDays(0.99999);
            BindData();
            ucQueryToolBar1.TargetDataGrid = maingrid;
            ucQueryToolBar1.RefreshFunction = () => { BindData(); };
        }

        private void BindData()
        {
            var _context = MyDataContext.GetDataContext;
            var rq2 = dateTimePicker2.Value.AddHours(23.9);
            var qry = from ys in _context.XTRZ.AsNoTracking()
                      where ys.Date >= dateTimePicker1.Value && ys.Date <= rq2
                      orderby ys.Date descending
                      select ys;
            
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
        }

        private void btnquery_Click(object sender, EventArgs e)
        {
            BindData();
        }
    }
}
