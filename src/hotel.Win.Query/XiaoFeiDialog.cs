﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.DAL;
using System.Linq.Expressions;
using hotel.Common;
using hotel.Win.Common;
namespace hotel.Win.Query
{
    /// <summary>
    /// 查看结账后的消费记录
    /// </summary>
    public partial class XiaoFeiDialog : Form
    {
        hotelEntities _context = MyDataContext.GetDataContext;

        public XiaoFeiDialog(string frzid)
        {
            InitializeComponent();
            BindData(frzid);
        }

        private void BindData(string frzid)
        {
            List<XFJL_ARC> xfljs = new List<XFJL_ARC>();//消费累计
            List<XFJL_ARC> zfljs = new List<XFJL_ARC>();//支付累计
            //子单
            var qry = from r in _context.RZJL_ARC.AsNoTracking()
                      join x in _context.XFJL_ARC.AsNoTracking() on r.ID equals x.RZJL_ID
                       where r.F_ID == frzid
                  select x;
            if (qry.Count() > 0)
            {
                xfljs.AddRange(qry.Where(p => p.XFXM_MC != "押金" && p.XFBZ=="消"));
                zfljs.AddRange(qry.Where(p => p.XFXM_MC == "押金"));
            }
            //自身
            qry = _context.XFJL_ARC.AsNoTracking().Where(p => p.RZJL_ID == frzid);
            xfljs.AddRange(qry.Where(p => p.XFXM_MC != "押金" && p.XFBZ=="消"));
            zfljs.AddRange(qry.Where(p => p.XFXM_MC == "押金"));

            //消费累计           
            gridxf.AutoGenerateColumns = false;
            gridxf.DataSource = (from q in xfljs
                                 orderby q.ROOM_ID, q.CJRQ
                                 select new
                                 {
                                     ROOM_ID = q.ROOM_ID,
                                     q.CJRQ,
                                     q.CJRY,
                                     q.JE,
                                     q.SL,
                                     q.DJ,
                                     q.XFXM_MC,
                                     q.ZFFS,
                                     q.BZSM,
                                     q.ID
                                 }).ToList();
            statusStrip1.Items[0].Text = "消费合计:" + xfljs.Sum(p=>p.JE);
            //支付累计           
            gridzf.AutoGenerateColumns = false;
            gridzf.DataSource = (from q in zfljs
                                 orderby q.ROOM_ID, q.CJRQ
                                 select new
                                 {
                                     ROOM_ID = q.ROOM_ID,
                                     q.CJRQ,
                                     q.CJRY,
                                     q.SL,
                                     q.DJ,
                                     q.XFXM_MC,
                                     q.YJ,
                                     q.ZFFS,
                                     q.ID,
                                     q.BZSM
                                 }).ToList();
            statusStrip2.Items[0].Text = "支付合计:" + zfljs.Sum(p => p.YJ);
        }
    }
}
