﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
using hotel.DAL;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;

namespace hotel.Win.Query
{
    public partial class YulidianFrm : Form
    {
        public YulidianFrm()
        {
            InitializeComponent();
            ControlsUtility.ResetColumnFromXML(maingrid);           
            ucQueryToolBar1.TargetDataGrid = maingrid;
            ucQueryToolBar1.RefreshFunction = () => { BindData(); };
            dtprq.ValueChanged += new EventHandler(dtprq_ValueChanged);
            dtprq.Value = DateTime.Now.Date;        
        }

        private void BindData()
        {
            using (var _context = MyDataContext.GetDataContext)
            {
                var rq = dtprq.Value.Date;
                var query = from q in _context.UV_RZJL_RZRY_Single.AsNoTracking()
                            where DbFunctions.TruncateTime(q.LDRQ) == rq
                            select new
                            {
                                q.ROOM_ID,
                                q.KRXM,
                                q.KRXB,
                                q.KRLB,
                                q.DDRQ,
                                q.LDRQ,
                                q.SJFJ,
                                q.TS,
                                q.BZSM
                            };             
                maingrid.AutoGenerateColumns = false;
                maingrid.DataSource = query.ToList();
                statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
            }
        }

        void dtprq_ValueChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}
