﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
using hotel.Win.Report;
using hotel.DAL;
using System.Linq.Expressions;
using hotel.Common;
namespace hotel.Win.Query
{
    /// <summary>
    /// 在住人员列表
    /// </summary>
    public partial class JieZhangFrm : Form
    {
        hotelEntities _context;
        CaoZuorzDialog czFrm;
        public JieZhangFrm()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            ControlsUtility.ResetColumnFromXML(maingrid);
            dateTimePicker1.Value = DateTime.Now.Date;
            dateTimePicker2.Value = DateTime.Now.Date.AddDays(0.99999);
            BindData();
            ucQueryToolBar1.TargetDataGrid = maingrid;
            ucQueryToolBar1.RefreshFunction = () => { BindData(); };
            AddButton();
        }

        private void BindData()
        {           
            var qry = from rz in _context.RZJL_ARC.AsNoTracking()
                      join ry in _context.RZRY_ARC.AsNoTracking() on rz.ID equals ry.RZJL_ARC.ID into rzrys
                      where rz.JZBZ == "是" && rz.JZRQ >= dateTimePicker1.Value && rz.JZRQ <= dateTimePicker2.Value
                      orderby rz.JZRQ descending
                      select new
                      {
                          rz.JZRQ,
                          rz.JZRY,
                          rz.ID,
                          rz.F_ID,
                          rz.ROOM_ID,
                          KRXM = rzrys.FirstOrDefault().KRXM,
                          KRXB = rzrys.FirstOrDefault().KRXB,
                          rz.ZK,
                          rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.TS,
                          rz.BZSM
                      };
            
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
        }
      public void Bindgrid(Expression<Func<UV_RZJL_RZRY_Single_ARC, bool>> whereqQuery)
     {
            /* var qry = from rz in _context.RZJL_ARC
                      join ry in _context.RZRY_ARC on rz.ID equals ry.RZJL_ARC.ID into rzrys
                      where rz.JZBZ == "是" && rz.JZRQ >= dateTimePicker1.Value && rz.JZRQ <= dateTimePicker2.Value
                      orderby rz.CJRQ descending
                      select new
                      {
                          rz.JZRQ,
                          rz.ID,
                          rz.ROOM_ID,
                          KRXM = rzrys.FirstOrDefault().KRXM,
                          KRXB = rzrys.FirstOrDefault().KRXB,
                          rz.ZK,
                          rz.SKFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.TS,
                          rz.BMBZ
                      };
            
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
            */
         var qry =  _context.UV_RZJL_RZRY_Single_ARC.Where(whereqQuery.Compile());  
         maingrid.AutoGenerateColumns = false;
         maingrid.DataSource = qry.ToList();
         statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";    
     }
 
        private void btnquery_Click(object sender, EventArgs e)
        {
            Expression<Func<UV_RZJL_RZRY_Single_ARC, bool>> expression = PredicateExtensions.True<UV_RZJL_RZRY_Single_ARC>();
            expression = expression.And(o => o.JZBZ == "是");
            if (chkrq.Checked)
            {
                expression = expression.And(o => o.JZRQ >= dateTimePicker1.Value && o.JZRQ <= dateTimePicker2.Value);
            }
            if (!string.IsNullOrEmpty(txtzdh.Text))
            {
                expression = expression.And(o => o.ID.Contains(txtzdh.Text));
            }
            if (!string.IsNullOrEmpty(txtfh.Text))
            {
                expression = expression.And(o => o.ROOM_ID.Contains(txtfh.Text));
            }
            if (!string.IsNullOrEmpty(txtxm.Text))
            {
                expression = expression.And(o => o.KRXM.Contains(txtxm.Text));
            }
            Bindgrid(expression);
        }

        /// <summary>            
        /// 增加操作按钮
        /// </summary>
        private void AddButton()
        {
            ToolStripButton item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnxfmxd";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Size = new System.Drawing.Size(77, 40);
            item.Image = global::hotel.Win.Query.Properties.Resources.Print;
            item.Text = "消费明细单";
            item.Click += new System.EventHandler(sbtnxfmxd_Click);
            ucQueryToolBar1.Insert(item);

            item = new ToolStripButton();            
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnxfhzd";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);    
            item.Size = new System.Drawing.Size(77, 40);
            item.Text = "消费汇总单";
            item.Image = global::hotel.Win.Query.Properties.Resources.Print;
            item.Click += new System.EventHandler(sbtnxfhzd_Click);
            ucQueryToolBar1.Insert(item);

            item = new ToolStripButton();
            //item.Image = global::hotel.Win.Query.Properties.Resources.excel1;
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnxfjl";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Text = "消费记录";
            item.Image = global::hotel.Win.Query.Properties.Resources.DataFormExcel;
            item.Click += new System.EventHandler(sbtnxjl_Click);
            ucQueryToolBar1.Insert(item);
            //查看日志
            item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnckrz";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Text = "查看日志";
            item.Image = global::hotel.Win.Query.Properties.Resources.DataFormExcel;
            item.Click += new System.EventHandler(sbtnckrz_Click);
            ucQueryToolBar1.Insert(item);
            //
            item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnxfrz";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Text = "消费补账";
            item.Click += new System.EventHandler(sbtnxfrz_Click);
            ucQueryToolBar1.Insert(item);
            //撤销结账
            item = new ToolStripButton();
            //item.Image = global::hotel.Win.Query.Properties.Resources.excel1;
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnxfrz";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Text = "撤销结账";
            item.Click += new System.EventHandler(sbtncxjz_Click);
            ucQueryToolBar1.Insert(item); 
        }
        //查看日志
        private void sbtnckrz_Click(object sender, EventArgs e)
        {
            string rzzh = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            if (string.IsNullOrEmpty(rzzh))
                return;
            if (czFrm == null)
                czFrm = new CaoZuorzDialog();
            czFrm.ShowByRzjl(rzzh);
        }
        //撤销
        private void sbtncxjz_Click(object sender, EventArgs e)
        {
            if (!UserLoginInfo.HaveRole(EnumRoole.QianTaiJingLi))
            {
                MessageBox.Show("只有高于前台经理的角色才可以操作");
                return;
            }
            string rzzh = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            string frzzh = ControlsUtility.GetActiveGridValue(maingrid, "F_ID");
            if (string.IsNullOrEmpty(rzzh))
                return;
            var rzjl = _context.RZJL_ARC.Find(rzzh);
            if (rzjl == null)
                return;
            int cnt = 0;
            cnt = _context.RZJL.Count(p => p.ROOM_ID == rzjl.ROOM_ID && p.RZBZ == "是"); //有预定可以
            if (cnt > 0)
            {
                MessageBox.Show("房间" + rzjl.ROOM_ID  + ",已经有入住，操作失败");
                return;
            }
            var frm = new XiaoFeiDialog(string.IsNullOrEmpty(frzzh) ? rzzh : frzzh);
            frm.ShowDialog();
            if (MessageBox.Show("你确定撤销选中的账务吗？\n撤销后恢复到原房间进行入住。", SysConsts.SysInformationCaption, MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;
            //还住在原来的房间
          
            _context.RZJL_Restore(string.IsNullOrEmpty(frzzh) ? rzzh : frzzh, UserLoginInfo.FullName);
            var logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.恢复入住;
            logInfo.RoomId = rzjl.ROOM_ID;
            logInfo.Rzid = rzjl.ID;
            logInfo.Message = "撤销账务，恢复入住,结账时间=" + rzjl.JZRQ.ToString() + ",结账人员=" + rzjl.JZRY;
            logInfo.Save();
            BindData();
            MessageBox.Show("账务撤销成功，请手工刷新房态图");
        }
        //查看消费记录
        private void sbtnxjl_Click(object sender, EventArgs e)
        {
            string rzzh = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            string frzzh = ControlsUtility.GetActiveGridValue(maingrid, "F_ID");
            if (string.IsNullOrEmpty(rzzh))
                return;
            var frm = new XiaoFeiDialog(string.IsNullOrEmpty(frzzh) ? rzzh : frzzh);
            frm.ShowDialog();
        }
        //补账
        private void sbtnxfrz_Click(object sender, EventArgs e)
        {
            if (!UserLoginInfo.HaveRole(EnumRoole.QianTaiJingLi))
            {
                MessageBox.Show("只有高于前台经理的角色才可以操作");
                return;
            }
            string rzzh = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            if (string.IsNullOrEmpty(rzzh))
                return;
            var frm = new XiaoFeiRuZhangMt(rzzh);
            frm.ShowDialog();
        }
        private void sbtnxfhzd_Click(object sender, EventArgs e)
        {
            string rzzh = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            if (string.IsNullOrEmpty(rzzh))
                return;
            var rzjlModel = _context.RZJL_ARC.First(p => p.ID == rzzh);
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Abstract = LogAbstractEnum.打印;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "补打消费汇总表";
            var rzid = string.IsNullOrEmpty(rzjlModel.F_ID) ? rzjlModel.ID : rzjlModel.F_ID;
            logInfo.Rzid = rzid;
            hotel.Win.Report.ReportEntry.ShowXiaoFeiSumBill(XTCSModel.GetXTCS.JDMC, rzid, logInfo);
        }

        private void sbtnxfmxd_Click(object sender, EventArgs e)
        {
            string rzzh = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            string frzzh = ControlsUtility.GetActiveGridValue(maingrid, "F_ID");
            if (string.IsNullOrEmpty(rzzh))
                return;
            CcrzInfo logInfo = null;
            logInfo = new CcrzInfo();
            logInfo.Rzid = rzzh;            
            logInfo.Abstract = LogAbstractEnum.打印;
            logInfo.Type = LogTypeEnum.一般操作;
            logInfo.Message = "补打退房明细表";
            var rzid = string.IsNullOrEmpty(frzzh) ? rzzh : frzzh;
            logInfo.Rzid = rzid;
            var isHeDan = !string.IsNullOrEmpty(frzzh);
            if (!isHeDan)
                isHeDan = _context.RZJL_ARC.Count(p => p.F_ID == rzid) > 0;
            hotel.Win.Report.ReportEntry.ShowXiaoFeiMingXiBill(XTCSModel.GetXTCS.JDMC, rzid,isHeDan , logInfo);            
        }
    }
}
