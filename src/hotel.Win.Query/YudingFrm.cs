﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
using System.Diagnostics;
using hotel.Common;
using System.Data.Entity;
namespace hotel.Win.Query
{
    /// <summary>
    /// 预订客人列表
    /// </summary>
    public partial class YudingFrm : Form
    {
        public YudingFrm()
        {
            InitializeComponent();
            ControlsUtility.ResetColumnFromXML(maingrid);
            BindData(0);
            ucQueryToolBar1.TargetDataGrid = maingrid;
            //ucQueryToolBar1.CloseFunction = () => { this.Close(); };
            ucQueryToolBar1.RefreshFunction = () => { BindData(0); };
            AddButton();
        }

        private void BindData(int flag)
        {
            var _context = MyDataContext.GetDataContext;
           maingrid.AutoGenerateColumns = false;
           
            if (flag == 0)
            {
                var qry = from rz in _context.RZJL.AsNoTracking()
                      join ry in _context.RZRY.AsNoTracking() on rz.ID equals ry.RZJL.ID into rzrys
                      where rz.YDBZ == "是"
                      orderby rz.CJRQ descending
                      select new
                      {
                          rz.ID,
                          ROOM_ID = rz.ROOM.ID,
                          KRXM = rzrys.FirstOrDefault().KRXM,
                          KRXB = rzrys.FirstOrDefault().KRXB,
                          rz.ZK,
                          rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.TS,
                          rz.YJ,
                          rz.BZSM,
                          CZRY = rz.CJRY,
                          CZRQ = rz.CJRQ
                      };
                 maingrid.DataSource = qry.ToList();
            }
            else if (flag ==1)
            {
                DateTime dt = DateTime.Now.Date;
                var qry = from rz in _context.RZJL.AsNoTracking()
                      join ry in _context.RZRY.AsNoTracking() on rz.ID equals ry.RZJL.ID into rzrys
                      where rz.YDBZ == "是" && rz.DDRQ < dt
                      orderby rz.CJRQ descending
                      select new
                      {
                          rz.ID,
                          ROOM_ID = rz.ROOM_ID,
                          KRXM = rzrys.FirstOrDefault().KRXM,
                          KRXB = rzrys.FirstOrDefault().KRXB,
                          rz.YJ,
                          rz.ZK,
                          rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.TS,
                          rz.BZSM,
                          CZRY = rz.CJRY,
                          CZRQ = rz.CJRQ
                      };
                maingrid.DataSource = qry.ToList();
            }
            else
            {
                //已经取消的预定
                var qry = from rz in _context.UV_RZJL_RZRY_Single_ARC.AsNoTracking()                      
                      where rz.YDBZ == "是" && rz.JZBZ=="消"
                      orderby rz.JZRQ descending
                      select new
                      {
                          rz.ID,
                          ROOM_ID = rz.ROOM_ID,
                          rz.KRXM,
                          rz.KRXB,
                          rz.ZK,
                          rz.SJFJ,
                          rz.DDRQ,
                          rz.LDRQ,
                          rz.TS,
                          rz.YJ,
                          rz.BZSM,
                          CZRY = rz.JZRY,
                          CZRQ = rz.JZRQ
                      };
                maingrid.DataSource = qry.ToList();
            }
           
            statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
        }
        
        /// <summary>            
        /// 增加操作按钮
        /// </summary>
        private void AddButton()
        {
           
            ToolStripButton item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnqxyd";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Size = new System.Drawing.Size(77, 40);
            item.Text = "取消选中预订";
            item.Click += new System.EventHandler(sbtnqxyd_Click);
            ucQueryToolBar1.Insert(item);
            //删除子预定
            item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnsczyd";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Size = new System.Drawing.Size(77, 40);
            item.Text = "删除子预定";
            item.Click += new System.EventHandler(sbtnsczyd_Click);
            ucQueryToolBar1.Insert(item);
            //取消过期预定
            item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnqxgqyd";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Size = new System.Drawing.Size(77, 40);
            item.Text = "取消过期预定";
            item.Click += new System.EventHandler(sbtnqxgqyd_Click);
            ucQueryToolBar1.Insert(item);
            //预定入住
            /*item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnYdrz";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Size = new System.Drawing.Size(77, 40);
            item.Text = "预定入住";
            item.Click += new System.EventHandler(sbtnYdrz_Click);
            ucQueryToolBar1.Insert(item);
            */
            //分隔线
            ucQueryToolBar1.Insert(new ToolStripSeparator());
            item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnygq";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Size = new System.Drawing.Size(77, 40);
            item.Text = "已过期预订";
            item.Image = global::hotel.Win.Query.Properties.Resources.filter;
            item.Click += new System.EventHandler(sbtnygq_Click);
            ucQueryToolBar1.Insert(item);
            //已取消的预订
            item = new ToolStripButton();
            item.ImageTransparentColor = System.Drawing.Color.Magenta;
            item.Name = "sbtnyqx";
            item.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            item.Size = new System.Drawing.Size(77, 40);
            item.Text = "已取消的预订";
            item.Image = global::hotel.Win.Query.Properties.Resources.filter;
            item.Click += new System.EventHandler(sbtnyqx_Click);
            ucQueryToolBar1.Insert(item);
        }
        //预定入住
        /*private void sbtnYdrz_Click(object sender, EventArgs e)
        {
            string id = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            if (id == "")
                return;
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjlModel = _context.RZJL.First(p => p.ID == id);
                var frm = new jddjFrm(rzjls.First(), 0, _context);
            }
            BindData(0);
        }
        */

        //删除子预定
        private void sbtnsczyd_Click(object sender, EventArgs e)
        {
            string id = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            if (id == "")
                return;
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjlModel = _context.RZJL.First(p => p.ID == id);
                if (string.IsNullOrEmpty(rzjlModel.F_ID))
                {
                    MessageBox.Show("此预定不是子预定，不能删除");
                    return;
                }
                var sumYj = _context.XFJL.Where(p => p.RZJL_ID == rzjlModel.ID).Sum(p => p.YJ);
                if (MessageBox.Show("客人押金：" + sumYj.ToString() + "\n你确定删除当前选定的子预订记录吗？", hotel.Common.SysConsts.SysWarningCaption, MessageBoxButtons.YesNo) != DialogResult.Yes)
                    return;

                #region 日志纪录
                var logInfo = new CcrzInfo();
                logInfo.Abstract = LogAbstractEnum.预订删除;
                logInfo.RoomId = rzjlModel.ROOM_ID;
                logInfo.Rzid = rzjlModel.ID;
                logInfo.Message = "预定押金:" + sumYj;
                logInfo.Save();
                #endregion
                rzjlModel.F_ID = null;
                rzjlModel.LFH = null;
                rzjlModel.BZSM += ",单独取消";
                _context.SaveChanges();
                _context.RZJL_QXYD_PROC(rzjlModel.ID, UserLoginInfo.FullName);
            }

            BindData(0);
        }
        //取消过期预定
        private void sbtnqxgqyd_Click(object sender, EventArgs e)
        {
            using (var _context = MyDataContext.GetDataContext)
            {
                var dt = DateTime.Now.Date;
                int cnt = 0;

                foreach (var item in _context.RZJL.AsNoTracking().Where(p => p.YDBZ == "是" && p.DDRQ < dt && p.F_ID == null))
                {
                    //子房间有入住
                    if (_context.RZJL.Count(p => p.F_ID == item.ID && p.RZBZ == "是") > 0)
                    {
                        MessageBox.Show("此预定有子房间已经入住，不能取消!\n 主房间号=" + item.ROOM_ID);
                        return;
                    }
                    cnt++;
                    _context.RZJL_QXYD_PROC(item.ID, UserLoginInfo.FullName);
                }
                if (cnt > 0)
                    MessageBox.Show("执行成功,共取消" + cnt.ToString() + "个预定");
                else
                    MessageBox.Show("没有过期的预定");
            }            
            BindData(0);            
        }
        /// <summary>
        /// 已取消的预订
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sbtnyqx_Click(object sender, EventArgs e)
        {
            BindData(2);
        }
        /// <summary>
        /// 已过期预订
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sbtnygq_Click(object sender, EventArgs e)
        {
            BindData(1);
        }

        /// <summary>
        /// 取消选中预订
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sbtnqxyd_Click(object sender, EventArgs e)
        {
            string id = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            if (id == "")
                return;
            using (var _context = MyDataContext.GetDataContext)
            {
                var rzjlModel = _context.RZJL.First(p => p.ID == id);
                if (!string.IsNullOrEmpty(rzjlModel.F_ID))
                {
                    MessageBox.Show("子预定不能取消，请选择主预定房间");
                    return;
                }
                //子房间有入住
                if (_context.RZJL.Count(p => p.F_ID == rzjlModel.ID && p.RZBZ == "是") > 0)
                {
                    MessageBox.Show("此预定有子房间已经入住，不能取消!\n 主房间号=" + rzjlModel.ROOM_ID);
                    return;
                }
                var sumYj = _context.XFJL.Where(p => p.RZJL_ID == rzjlModel.ID).Sum(p => p.YJ);
                if (MessageBox.Show("客人押金：" + sumYj.ToString() + "\n你确定取消当前选定的预订记录吗？", hotel.Common.SysConsts.SysWarningCaption, MessageBoxButtons.YesNo) != DialogResult.Yes)
                    return;

                #region 日志纪录
                var logInfo = new CcrzInfo();
                logInfo.Abstract = LogAbstractEnum.预订取消;
                logInfo.RoomId = rzjlModel.ROOM_ID;
                logInfo.Rzid = rzjlModel.ID;
                logInfo.Message = "预定押金:" + sumYj;
                logInfo.Save();
                #endregion
                _context.RZJL_QXYD_PROC(rzjlModel.ID, UserLoginInfo.FullName);
            }

            BindData(0);
        }
    }
}
