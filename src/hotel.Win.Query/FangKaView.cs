﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.DAL;
using hotel.Win.Common;

namespace hotel.Win.Query
{
    public partial class FangKaView : Form
    {
        hotelEntities _context;
        string rzjlId;
        public FangKaView(string rzjlId)
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            this.rzjlId = rzjlId;
            BindData();

        }
        private void BindData()
        {
            var qry = from q in _context.FKSYXX.AsNoTracking()
                      where q.RZJL_ID == rzjlId
                      orderby q.KH
                      select q;

            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
        }
        private void btngh_Click(object sender, EventArgs e)
        {
            var str = Common.ControlsUtility.GetActiveGridValue(maingrid, "ColumnID");
            if (string.IsNullOrEmpty(str))
                return;
            int id = Convert.ToInt32(str);
            var data = _context.FKSYXX.Find(id);
            if (MessageBox.Show("卡号:" + data.KH + "\n你确定提前归还吗？", "房卡归还", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                return;
            data.SFGH = "是";
            _context.SaveChanges();
            MessageBox.Show("操作成功");
            BindData();
        }

        public void BindDatas(List<string> rzjlIds)
        {
            var qry = from q in _context.FKSYXX.AsNoTracking()
                      where rzjlIds.Contains(q.RZJL_ID)
                      orderby q.CJRQ
                      select q;

            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
