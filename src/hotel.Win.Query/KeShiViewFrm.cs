﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;
using hotel.Win.Report;
using hotel.DAL;
using System.Linq.Expressions;
using hotel.Common;

namespace hotel.Win.Query
{
    //客史
    public partial class KeShiViewFrm : Form
    {
        hotelEntities _context;
        public KeShiViewFrm()
        {
            InitializeComponent();
            _context = MyDataContext.GetDataContext;
            ControlsUtility.ResetColumnFromXML(maingrid);
            ControlsUtility.ResetColumnFromXML(gridrz);
            
            ucQueryToolBar1.TargetDataGrid = maingrid;
           
            BindData();
        }

        private void BindData()
        {
            ucQueryToolBar1.RefreshFunction = () => { BindData(); };
            var rq = DateTime.Now.Date.AddDays(-31);
            var xm = txtxm.Text.Trim();
            var qry = from ry in _context.KSDA.AsNoTracking()
                      where ry.CJRQ > rq
                      orderby ry.KRXM descending
                      select ry;

            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            statusStrip1.Items[0].Text = "30天内客人共" + maingrid.Rows.Count.ToString() + "行";
        }
        private void btnquery_Click(object sender, EventArgs e)
        {
            ucQueryToolBar1.RefreshFunction = () => { btnquery_Click(this,null); };
            var xm = txtxm.Text.Trim();
            var qry = from ry in _context.KSDA.AsNoTracking()
                      where ry.KRXM.Contains(xm)
                      orderby ry.KRXM descending
                      select ry;
            gridrz.AutoGenerateColumns = false;
            gridrz.DataSource = null;
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = qry.ToList();
            statusStrip1.Items[0].Text = "按姓名查找共" + maingrid.Rows.Count.ToString() + "行";

        }

        private void maingrid_SelectionChanged(object sender, EventArgs e)
        {
            var zjdm = ControlsUtility.GetActiveGridValue(maingrid, "ZJDM");
            var krzt = ControlsUtility.GetActiveGridValue(maingrid, "KRZT");
            if (krzt == "正常")
            {
                btnzt.Tag = "黑名";
                btnzt.Text = "加入黑名单";
            }
            else
            {
                btnzt.Tag = "正常";
                btnzt.Text = "取消黑名单";
            }
            var qry = from ry in _context.UV_RZJL_RZRY_ARC.AsNoTracking()
                      where ry.ZJDM == zjdm
                      orderby ry.JZRQ descending
                      select ry;

            gridrz.AutoGenerateColumns = false;
            gridrz.DataSource = qry.ToList();
            statusStrip1.Items[1].Text = "入住" + gridrz.Rows.Count.ToString() + "次";
        }

        private void txtxm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                btnquery_Click(this, null);
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            var zjdm = ControlsUtility.GetActiveGridValue(maingrid, "ZJDM");
            var str = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            if (string.IsNullOrEmpty(str))
                return;
            var id = Convert.ToInt32(str);
            var model = _context.KSDA.Find(id);
            if (model != null && MessageBox.Show("证件号码:" + zjdm + "\n只删除客人档案，不会删除入住信息，你确定删除吗？",SysConsts.SysWarningCaption,MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != System.Windows.Forms.DialogResult.Yes)
                return;
            _context.KSDA.Remove(model);
            _context.SaveChanges();
            MessageBox.Show("删除成功");
            ucQueryToolBar1.RefreshFunction();
        }

        private void btnzt_Click(object sender, EventArgs e)
        {
            var zjdm = ControlsUtility.GetActiveGridValue(maingrid, "ZJDM");
            var str = ControlsUtility.GetActiveGridValue(maingrid, "ID");
            if (string.IsNullOrEmpty(str))
                return;
            var id = Convert.ToInt32(str);
            var model = _context.KSDA.Find(id);
            if (model != null && MessageBox.Show("证件号码:" + zjdm + "\n你确认吗？", SysConsts.SysWarningCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != System.Windows.Forms.DialogResult.Yes)
                return;
            model.KRZT = btnzt.Tag.ToString();
            _context.SaveChanges();
            MessageBox.Show("修改成功");
            ucQueryToolBar1.RefreshFunction();
        }
    }
}
