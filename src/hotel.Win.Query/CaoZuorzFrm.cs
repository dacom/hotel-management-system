﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hotel.Win.Common;

namespace hotel.Win.Query
{
    public partial class CaoZuorzFrm : Form
    {
        public CaoZuorzFrm()
        {
            InitializeComponent();
            ControlsUtility.ResetColumnFromXML(maingrid);
            dateTimePicker1.Value = DateTime.Now.Date;
            dateTimePicker2.Value = DateTime.Now.Date.AddDays(0.99999);
            BindData();
            ucQueryToolBar1.TargetDataGrid = maingrid;
            ucQueryToolBar1.RefreshFunction = () => { BindData(); };
        }
       
        private void BindData()
        {
            var _context = MyDataContext.GetDataContext;
            var rq2 = dateTimePicker2.Value.AddHours(23.9);
           
            var qry = from ys in _context.CCRZ.AsNoTracking()
                      where ys.RQ >= dateTimePicker1.Value && ys.RQ <= rq2
                      orderby ys.RQ  descending
                      select ys;
            var datas = qry.ToList();
            if (txtfh.Text.Length > 0)
                datas = datas.Where(p => p.ROOM_ID== txtfh.Text).ToList();
            //入住账号
            if (txtzdh.Text.Length > 0)
                datas = datas.Where(p => p.RZID == txtzdh.Text).ToList();
            maingrid.AutoGenerateColumns = false;
            maingrid.DataSource = datas;
            statusStrip1.Items[0].Text = "共" + maingrid.Rows.Count.ToString() + "行";
        }

        private void btnquery_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void txtfh_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                BindData();
        }

        private void maingrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (maingrid.Columns[e.ColumnIndex].Name == "Message")
                MessageBox.Show(maingrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
        }       
    }
}
