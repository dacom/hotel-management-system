﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hotel.Common;
using System.IO;
using System.Drawing;

namespace hotel.IDCard.DKQ_A16D
{
    public class IDCardScanner : IIDCard
    {
        public static IDCardData ReadUSB()
        {
            int num = IDCardAPI.Syn_FindReader();
            if (num < 0 || num > 1016 || (num > 16 && num < 1000))
            {
                throw new ArgumentException(Convert.ToString(DateTime.Now) + "  未能找到读卡器");
            }
            if (IDCardAPI.Syn_OpenPort(num) != 0)
            {
                throw new ArgumentException(Convert.ToString(DateTime.Now) + "  无法连接读卡器");
            }
        
            byte[] array = new byte[4];
            byte[] array2 = new byte[8];
            if (IDCardAPI.Syn_SetNationType(2) != 0)
            {
                IDCardAPI.Syn_ClosePort(num);
                throw new ArgumentException(Convert.ToString(DateTime.Now) + "  读卡器民族格式设置失败");
            }
            IDCardAPI.Syn_StartFindIDCard(num, ref array[0], 0);
            IDCardAPI.Syn_SelectIDCard(num, ref array2[0], 0);
            IDCardData result = default(IDCardData);
            if (IDCardAPI.Syn_ReadMsg(num, 0, ref result) != 0)
            {
                IDCardAPI.Syn_ClosePort(num);
                throw new ArgumentException(Convert.ToString(DateTime.Now) + "  无法读取身份证信息");
            }
            //if (IDCardAPI.Syn_SetMaxRFByte(num, 80, 0) != 0)
            //{
            //    IDCardAPI.Syn_ClosePort(num);
            //    throw new ArgumentException(Convert.ToString(DateTime.Now) + "  设置读卡器最大通信字节失败");
            //}
            if (IDCardAPI.Syn_ClosePort(num) != 0)
            {
                throw new ArgumentException(Convert.ToString(DateTime.Now) + "  无法断开读卡器");
            }
            return result;
        }

        public IDCardInfo ReadCardInfo()
        {
            IDCardData iDCardData = ReadUSB();
           // if (iDCardData == null)
            //    throw new Exception("读取二代身份证出错，对象为空");
            var result = new IDCardInfo();

            result.Name = iDCardData.Name.Trim();

            if (iDCardData.Sex == "0")
            {
                result.Sex_Code = "2";
            }
            else
            {
                result.Sex_Code = "1";
            }
            result.NATION_Code = iDCardData.Nation;
            string born = iDCardData.Born;
            result.BIRTH = Convert.ToDateTime(string.Concat(new string[]
				{
					born.Substring(0, 4),
					"-",
					born.Substring(4, 2),
					"-",
					born.Substring(6, 2)
				}));

            result.ADDRESS = iDCardData.Address.Trim();
            result.IDC = iDCardData.IDCardNo;
            result.REGORG = iDCardData.GrantDept;
            if (Utility.IsDate(iDCardData.UserLifeBegin))
                result.STARTDATE = Convert.ToDateTime(iDCardData.UserLifeBegin);
            if (Utility.IsDate(iDCardData.UserLifeEnd))
                result.ENDDATE = Convert.ToDateTime(iDCardData.UserLifeEnd);
            else
                result.ENDDATE = DateTime.MaxValue;
            //照片
            var objFile = new FileInfo(iDCardData.PhotoFileName);
            if (objFile.Exists)
            {
                Image img = Image.FromFile(iDCardData.PhotoFileName);
                result.PIC_Image = (Image)img.Clone();
                System.IO.MemoryStream m = new MemoryStream();
                img.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                result.PIC_Byte = m.ToArray();
                img.Dispose();
                img = null;
            }
            return result;
        }
    }
}
