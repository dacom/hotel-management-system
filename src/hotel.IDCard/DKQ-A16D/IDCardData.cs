﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
namespace hotel.IDCard.DKQ_A16D
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct IDCardData
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string Name;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
        public string Sex;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string Nation;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 18)]
        public string Born;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 72)]
        public string Address;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 38)]
        public string IDCardNo;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string GrantDept;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 18)]
        public string UserLifeBegin;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 18)]
        public string UserLifeEnd;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 38)]
        public string reserved;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 255)]
        public string PhotoFileName;
    }
}
