﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace hotel.Win.Key
{
    public partial class Form2 : Form
    {
        string prikey = "<RSAKeyValue><Modulus>wRn06rdK96H5CebdwNDlvy1P7B7n3ODBr+RpnVlYca/sIy+SDgfF3/fB0u/7QlW9vol7ov3rWeL5DNEcIiMISCg+3KjiOpxtJ4TBaqkgda8nspSpG8fkytAUfbObrif7/n0Z/fb+ClcV0DmhTQZgwJDhZqmMuHBmjsyktrDLcN0=</Modulus><Exponent>AQAB</Exponent><P>/ZxvB16oaNFxBnKkzzLhQwePvMof+6Uz9bfONkVvn1Ko15k37fspHiTJ6+qBRkdmBSWAxUWR6/AdkTZnx39zGw==</P><Q>wuuboP8uNHj4OqPpUO1H3MrYz76cfBYvZagIaxUDQL1gnltnU/v08lJmN9ddY3nv/uh+821mqEbgdLr5wV1zZw==</Q><DP>N+b5mlhFKwWvskonBf0jJlFOX8D2F9QshedELzZTCn1/9jYx2+VXBOv7DmOcp08pr8y6e8GADs/Y1J93dMQlLQ==</DP><DQ>YIfeIvU9JK9j/2Z9fZ6FIEs05G05ghpajrWQtbdKTR18Y/BQU79Lp1Y3WSBHF6SjvlHVzSg41s2DC0wttB1bsQ==</DQ><InverseQ>v9Bm1gvXCQ/Et4k+c7qkNuotahARjyBXPEEK5RVk2wE5Efu7TlsCQvOgO7FD0sbjG8Te0ltEAduJ3+Erdc0DVQ==</InverseQ><D>qnqDCpGFwvghyIck8ay7mLulWr2yUAOxm9sZDwypE72PyUSfOnnMTT4LxXGPLvJJkSUJnU11cab73z/iCZVKIfZApkhEmY/PGZDeENN3CyBj2PcxtVQtKaQUNYfdRTvq6ilD6cgCc6ipNIX6+xEUwNnRbYlnhIYdDEEQZDbb1xU=</D></RSAKeyValue>";
        string pubkey = "<RSAKeyValue><Modulus>wRn06rdK96H5CebdwNDlvy1P7B7n3ODBr+RpnVlYca/sIy+SDgfF3/fB0u/7QlW9vol7ov3rWeL5DNEcIiMISCg+3KjiOpxtJ4TBaqkgda8nspSpG8fkytAUfbObrif7/n0Z/fb+ClcV0DmhTQZgwJDhZqmMuHBmjsyktrDLcN0=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        public Form2()
        {
            InitializeComponent();
            dateTimePicker1.Value = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtzcm.Text = Encryption.RSAEncrypt(pubkey, dateTimePicker1.Value.Date.ToString("yyyy-MM-dd"));


            //using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            //{
            //    // 公钥 
            //    string pubkey2 = rsa.ToXmlString(false);
            //    MessageBox.Show(pubkey2);
            //    // 私钥 
            //    string prikey2 = rsa.ToXmlString(true);
            //    MessageBox.Show(prikey2);
            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Encryption.RSADecrypt(prikey,txtzcm.Text));
        }
    }
}
