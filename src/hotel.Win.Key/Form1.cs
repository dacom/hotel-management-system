﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Management;

namespace hotel.Win.Key
{
    public partial class Form1 : Form
    {
        private string pass = "hkbg.china";
        public Form1()
        {
            InitializeComponent();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (txtmm.Text != pass)
            {
                MessageBox.Show("密码不正确");
                return;
            }
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                string prikey = "<RSAKeyValue><Modulus>wAauLh78Eg/QpRuVfHGdmN8LbFqErwGUOKp0n6tHiluSq2oaG6WIKDcvVIodJKf2TS2zZNnaRLQvcTTwnhOhJ40ymuFJFm3LMHxxdJ4lvgEveoMXtLLHxRZp6/ZMP9RTjWDiQUwQJwND4yF0bk23vn0nU5KNNRidh+i3Au5/2lc=</Modulus><Exponent>AQAB</Exponent><P>8hUgqFtW9WVdgo0C7FW+VS6Tlf12BOWUtnJnMqjHlP7ChDcV8ZLdjcZyOth6bcIf/zDbY0UHDZtpSXHuz1+iow==</P><Q>yxDXsG0Bp7WMG4tvDQEZQ6qKEWApAkQEJZt3ayfnrH0FRl2/Sb5ZTXYNP6lbja330ExW2HybcMyaLdxUW5yYvQ==</Q><DP>4kuLtrWDqWuzKQnGNMTGYnxirDA+Nb+i75YsD/gkcYxgqxWPZr5Ca3iZaZbZg2aXYIb8XmS1fivMFULy2Vlt/w==</DP><DQ>H9imWu7dgZXBQVies+VmNux59ruj8s0D8XnuQonhlXWN7DkrlL8bdkB7V26SAygzYyF1xUHVht0vmN2jBTML6Q==</DQ><InverseQ>6UoRlkC9kI179hFDjeSyJtJ7uVgYdKd8wz1TLBEJzWTTdcCsRkkgiflGvmlPras8dCtEhd1g//E1g+MNyUYx5w==</InverseQ><D>Ndwp0sniDfbdoNHh1evQNmapP5UMDkI1HsfAumBCSq5wrx+tfv+o9w6zedTbY8KwtOkSnkD0lbysaeZdbxk2Y/K8+IGtCpCUQOxwrusnFT1Cq/z9bsKaNPGRjDS7Q8vMNaF4yczQoW+40tyqwsqhSkD/rSY5Jz+VrzZYGSFvs5E=</D></RSAKeyValue>";
                rsa.FromXmlString(prikey);
                // 加密对象 
                RSAPKCS1SignatureFormatter f = new RSAPKCS1SignatureFormatter(rsa);
                f.SetHashAlgorithm("SHA1");
                byte[] source = System.Text.ASCIIEncoding.ASCII.GetBytes(txtyzm.Text);
                SHA1Managed sha = new SHA1Managed();
                byte[] result = sha.ComputeHash(source);

                byte[] b = f.CreateSignature(result);

                txtzcm.Text = Convert.ToBase64String(b);
                Clipboard.SetDataObject(txtzcm.Text);
                MessageBox.Show("生成成功，已经复制到剪切板中");
            }
        }

        private void txtmm_TextChanged(object sender, EventArgs e)
        {
            btnok.Enabled = txtmm.Text == pass;
            btncsm.Enabled = txtmm.Text == pass;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var cp = hotel.Common.Computer.Instance;
            txtyzm.Text = cp.GetZhuCeMa();
        }

        //MD5
        string MD5_Hash(string str_md5_in)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] bytes_md5_in = UTF8Encoding.Default.GetBytes(str_md5_in);
            byte[] bytes_md5_out = md5.ComputeHash(bytes_md5_in);
            string str_md5_out = BitConverter.ToString(bytes_md5_out);
            str_md5_out = str_md5_out.Replace("-", "");
            return str_md5_out;
        }

        //获得CPU的序列号       
        string getCpu()
        {
            string strCpu = null;
            ManagementClass myCpu = new ManagementClass("win32_Processor");
            ManagementObjectCollection myCpuConnection = myCpu.GetInstances();
            foreach (ManagementObject myObject in myCpuConnection)
            {
                strCpu = myObject.Properties["Processorid"].Value.ToString(); break;
            }
            return strCpu;
        }


        string GetDiskID()
        {
            try
            {
                //获取硬盘ID
                String HDid = "";
                ManagementClass mc = new ManagementClass("Win32_DiskDrive");
                ManagementObjectCollection moc = mc.GetInstances();
                foreach (ManagementObject mo in moc)
                {
                    HDid = (string)mo.Properties["Model"].Value;
                }
                moc = null;
                mc = null;
                return HDid;
            }
            catch
            {
                return "unknow";
            }
            finally
            {
            }

        }

        private void btncsm_Click(object sender, EventArgs e)
        {
            string pubkey = "<RSAKeyValue><Modulus>wRn06rdK96H5CebdwNDlvy1P7B7n3ODBr+RpnVlYca/sIy+SDgfF3/fB0u/7QlW9vol7ov3rWeL5DNEcIiMISCg+3KjiOpxtJ4TBaqkgda8nspSpG8fkytAUfbObrif7/n0Z/fb+ClcV0DmhTQZgwJDhZqmMuHBmjsyktrDLcN0=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
            txtcsm.Text = Encryption.RSAEncrypt(pubkey, dateTimePicker1.Value.Date.ToString("yyyy-MM-dd"));
        }
    }
}
